import tensorflow as tf
from Train import Build_Word2Vec
import pandas as pd
"""
Cosine Similarity will give the similarity score between a experience and a JD
"""
# TODO: 1. Write the whole program in spark so that it's much more accessible and readable

class Compute_Similarity():

    def __init__(self, data):
        self.data = data

    @property
    def _define_data(self):
        with tf.name_scope('final_data'):
            self.iterator = self.data.make_initializable_iterator()
            self.cand_key, self.Req_Key, self.exp, self.jd = self.iterator.get_next()

    @property
    def _cosine_similarity(self):
        with tf.name_scope('cosine_similarity'):
            num = tf.reduce_sum(tf.multiply(self.exp, self.jd))
            den = tf.multiply(tf.sqrt(tf.reduce_sum(tf.square(self.exp))), tf.sqrt(tf.reduce_sum(tf.square(self.jd))))
            self.similarity = tf.div(num, den)

    def Make_Graph(self):
        self._define_data
        self._cosine_similarity

    def compute_similarities(self):
        final_dataset = []
        with tf.Session() as sess:
            sess.run(self.iterator.initializer)
            sess.run(tf.global_variables_initializer())

            flag = True
            while flag:
                try:
                    ck, rk, sim = sess.run([self.cand_key, self.Req_Key, self.similarity])
                    final_dataset.append([ck[0], rk[0], sim])
                except tf.errors.OutOfRangeError:
                    flag = False  # No more iteration
        return final_dataset


def get_vector_repr(mode='Experience', Train = False):
    """
    Will retrieve all the vector representation for given mode
    :param mode: Expereince vs JD
    :param Train: flag representing to Train or not
    :return: Vector Representations for docs
    """
    return Build_Word2Vec.compute_vector_repr(mode, Train=Train)


def define_sim_generator():
    """
    Create generator to yield 1 item at a time
    :return: yield 1 item at a time
    """
    data = pd.read_csv("../Data/RecoData.csv", header=None,
                         names=['CandidateId', 'CandidateKey', 'ReqKey', 'Experience', 'Req_Desc', 'Req_Summary'])[['CandidateKey', 'ReqKey']]
    data.drop_duplicates(inplace=True)
    exp_dict = get_vector_repr()
    tf.reset_default_graph()
    jd_dict = get_vector_repr(mode='JD')
    tf.reset_default_graph()
    for idx in range(data.shape[0]):
        candidate = data.loc[idx, 'CandidateKey']
        jd = data.loc[idx, 'ReqKey']
        yield [int(candidate)], [int(jd)], exp_dict[candidate], jd_dict[jd]

def compute_similarity():
    data = tf.data.Dataset.from_generator(define_sim_generator, output_types=(tf.int32, tf.int32, tf.float32, tf.float32),
                                   output_shapes=(tf.TensorShape([1]), tf.TensorShape([1]), tf.TensorShape([None]),
                                                  tf.TensorShape([None])))
    cos_sim = Compute_Similarity(data)
    cos_sim.Make_Graph()
    pd.DataFrame(cos_sim.compute_similarities(), columns=['Cand_key', 'Req_Key', 'Sim']).to_csv('../Output/Similarity.csv')

compute_similarity()

