import tensorflow as tf
import numpy as np

class KnnSimilarity(object):

    def __init__(self, gen=None, req=None, data=None, k=50):
        self.req = req
        self.vec = data
        self.features = len(self.vec[0])
        self.iterable = gen.make_initializable_iterator()
        self.k = k

    def _define_data(self):
        with tf.name_scope('data'):
            self._req = tf.placeholder(shape=None, dtype=np.int32)
            self.X_bar = tf.placeholder(shape=[None, self.features], dtype=np.float32)
            self._jk, self.X = self.iterable.get_next()

    def _compute_distance(self):  # Manhattan Distance -- tf.expand_dims will be used if you are working with matrices not with vector and matrix
        with tf.name_scope('distance'):
            self.distance = tf.reduce_sum(tf.abs(tf.subtract(self.X, self.X_bar)), axis=1)  # Sum across last dimension

    def _define_kpoints(self):
        with tf.name_scope('kpoints'):
            _, self.top_k_indices = tf.nn.top_k(tf.negative(self.distance), k=self.k)
            self.top_reqs = tf.gather(self._req, self.top_k_indices)

    def Make_Graph(self):
        self._define_data()
        self._compute_distance()
        self._define_kpoints()

    def Train(self):

        indices = None

        with tf.Session() as sess:
            sess.run(self.iterable.initializer)
            sess.run(tf.global_variables_initializer())
            flag = True
            counter = 0
            while flag:
                try:
                    jk, reqs = sess.run([self._jk, self.top_reqs], feed_dict={self._req: self.req, self.X_bar: self.vec})
                    jk, reqs = jk.astype(str), reqs.astype(str)
                    with open('../Output/JD/k_points.txt', 'a+') as f:
                        z = jk.tolist()
                        z.extend(reqs[1:].tolist())
                        f.write('|'.join(z))
                        f.write('\n')
                        """
                        for item1, item2 in zip(jk, reqs):
                            z = [item1]
                            z.extend(item2[1:].tolist())
                        """
                    counter += 1
                    if counter % 1000 == 0:
                        print('Examples filled are: {}'.format(counter * 1))
                except tf.errors.OutOfRangeError:
                    flag = False


def get_vector_repr():
    """
    Will retrieve all the vector representation for given mode
    :param mode: Expereince vs JD
    :param Train: flag representing to Train or not
    :return: Vector Representations for docs
    """
    jk, vec = [], []
    count = 0
    with open('../Output/JD/vec_repr.txt',  'r') as f:
        for line in f.readlines():
            count += 1
            ln = line.split('|')
            jk.append(int(ln[0]))
            vec.append(ln[1:])
    return jk, vec


def generator(jk, vec):
    for item1, item2 in zip(jk, vec):
        yield item1, item2


def get_generator(jk, vec):
    sample = generator(jk, vec)
    batch_jk = np.zeros(1, dtype=np.int32)
    batch_vec = np.zeros([1, 200])
    while True:
        for idx in range(1):
            batch_jk[idx], batch_vec[idx] = next(sample)
        yield batch_jk, batch_vec


def compute_knn():
    jk, vec = get_vector_repr()
    datagen = tf.data.Dataset.from_generator(get_generator, output_types=(tf.int32, tf.float32),
                                            output_shapes=(tf.TensorShape([1]), tf.TensorShape([1, 200])), args=(jk, vec))
    knn = KnnSimilarity(gen=datagen, req=jk, data=vec)
    knn.Make_Graph()
    knn.Train()


compute_knn()