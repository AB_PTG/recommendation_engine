       �K"	  ����Abrain.Event:2i]i��@     @��	V�����A"��
I
args_0Const*
_output_shapes
: *
value
B :�N*
dtype0
I
args_1Const*
value
B :�*
dtype0*
_output_shapes
: 
H
args_2Const*
value	B :*
dtype0*
_output_shapes
: 
T
args_3Const*
valueB Bvisualization*
dtype0*
_output_shapes
: 
Q
args_4Const*
valueB B
Experience*
dtype0*
_output_shapes
: 
U
tensors/component_0Const*
value	B : *
dtype0*
_output_shapes
: 
L

batch_sizeConst*
value	B	 R*
dtype0	*
_output_shapes
: 
S
args_0_1Const*
valueB B
Experience*
dtype0*
_output_shapes
: 
J
args_1_1Const*
value	B :*
dtype0*
_output_shapes
: 
W
tensors_1/component_0Const*
value	B : *
dtype0*
_output_shapes
: 
G
ConstConst*
value	B : *
dtype0*
_output_shapes
: 
o
global_step
VariableV2*
dtype0*
_output_shapes
: *
	container *
shape: *
shared_name 
�
global_step/AssignAssignglobal_stepConst*
use_locking(*
T0*
_class
loc:@global_step*
validate_shape(*
_output_shapes
: 
j
global_step/readIdentityglobal_step*
T0*
_class
loc:@global_step*
_output_shapes
: 
�
data/IteratorIterator*%
output_shapes
:�:	�*
_output_shapes
: *
	container *
output_types
2*
shared_name 
�
data/TensorDatasetTensorDatasettensors/component_0*
_output_shapes
: *
output_shapes
: * 
_class
loc:@data/Iterator*
Toutput_types
2
�
data/FlatMapDatasetFlatMapDatasetdata/TensorDatasetargs_0args_1args_2args_3args_4*

Targuments	
2*
_output_shapes
: *%
output_shapes
:�:	�* 
_class
loc:@data/Iterator* 
fR
tf_map_func_PCOijuOn9mg*
output_types
2
g
data/MakeIteratorMakeIteratordata/FlatMapDatasetdata/Iterator* 
_class
loc:@data/Iterator
\
data/IteratorToStringHandleIteratorToStringHandledata/Iterator*
_output_shapes
: 
�
data/Iterator_1Iterator*
_output_shapes
: *
	container *
output_types
2*
shared_name *1
output_shapes 
:���������:���������
�
data/TensorDataset_1TensorDatasettensors_1/component_0*
output_shapes
: *"
_class
loc:@data/Iterator_1*
Toutput_types
2*
_output_shapes
: 
�
data/FlatMapDataset_1FlatMapDatasetdata/TensorDataset_1args_0_1args_1_1*

Targuments
2*
_output_shapes
: *1
output_shapes 
:���������:���������*"
_class
loc:@data/Iterator_1* 
fR
tf_map_func_8pkeaS83Jbc*
output_types
2
o
data/MakeIterator_1MakeIteratordata/FlatMapDataset_1data/Iterator_1*"
_class
loc:@data/Iterator_1
`
data/IteratorToStringHandle_1IteratorToStringHandledata/Iterator_1*
_output_shapes
: 
�
data/IteratorGetNextIteratorGetNextdata/Iterator*
output_types
2*%
output_shapes
:�:	�*&
_output_shapes
:�:	�
�
data/IteratorGetNext_1IteratorGetNextdata/Iterator_1*1
output_shapes 
:���������:���������*2
_output_shapes 
:���������:���������*
output_types
2
�
.nce_weights/Initializer/truncated_normal/shapeConst*
_output_shapes
:*
_class
loc:@nce_weights*
valueB"'  �   *
dtype0
�
-nce_weights/Initializer/truncated_normal/meanConst*
_class
loc:@nce_weights*
valueB
 *    *
dtype0*
_output_shapes
: 
�
/nce_weights/Initializer/truncated_normal/stddevConst*
dtype0*
_output_shapes
: *
_class
loc:@nce_weights*
valueB
 *�А=
�
8nce_weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormal.nce_weights/Initializer/truncated_normal/shape*
dtype0* 
_output_shapes
:
�N�*

seed *
T0*
_class
loc:@nce_weights*
seed2 
�
,nce_weights/Initializer/truncated_normal/mulMul8nce_weights/Initializer/truncated_normal/TruncatedNormal/nce_weights/Initializer/truncated_normal/stddev*
T0*
_class
loc:@nce_weights* 
_output_shapes
:
�N�
�
(nce_weights/Initializer/truncated_normalAdd,nce_weights/Initializer/truncated_normal/mul-nce_weights/Initializer/truncated_normal/mean*
T0*
_class
loc:@nce_weights* 
_output_shapes
:
�N�
�
nce_weights
VariableV2*
	container *
shape:
�N�*
dtype0* 
_output_shapes
:
�N�*
shared_name *
_class
loc:@nce_weights
�
nce_weights/AssignAssignnce_weights(nce_weights/Initializer/truncated_normal*
use_locking(*
T0*
_class
loc:@nce_weights*
validate_shape(* 
_output_shapes
:
�N�
t
nce_weights/readIdentitynce_weights*
T0*
_class
loc:@nce_weights* 
_output_shapes
:
�N�
h
weights/zeros/shape_as_tensorConst*
valueB:�N*
dtype0*
_output_shapes
:
X
weights/zeros/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
�
weights/zerosFillweights/zeros/shape_as_tensorweights/zeros/Const*
T0*

index_type0*
_output_shapes	
:�N
v
nce_bias
VariableV2*
dtype0*
_output_shapes	
:�N*
	container *
shape:�N*
shared_name 
�
nce_bias/AssignAssignnce_biasweights/zeros*
_class
loc:@nce_bias*
validate_shape(*
_output_shapes	
:�N*
use_locking(*
T0
f
nce_bias/readIdentitynce_bias*
T0*
_class
loc:@nce_bias*
_output_shapes	
:�N
�
-embeddings_1/Initializer/random_uniform/shapeConst*
_class
loc:@embeddings_1*
valueB"'  �   *
dtype0*
_output_shapes
:
�
+embeddings_1/Initializer/random_uniform/minConst*
_class
loc:@embeddings_1*
valueB
 *    *
dtype0*
_output_shapes
: 
�
+embeddings_1/Initializer/random_uniform/maxConst*
_class
loc:@embeddings_1*
valueB
 *  �?*
dtype0*
_output_shapes
: 
�
5embeddings_1/Initializer/random_uniform/RandomUniformRandomUniform-embeddings_1/Initializer/random_uniform/shape*

seed *
T0*
_class
loc:@embeddings_1*
seed2 *
dtype0* 
_output_shapes
:
�N�
�
+embeddings_1/Initializer/random_uniform/subSub+embeddings_1/Initializer/random_uniform/max+embeddings_1/Initializer/random_uniform/min*
T0*
_class
loc:@embeddings_1*
_output_shapes
: 
�
+embeddings_1/Initializer/random_uniform/mulMul5embeddings_1/Initializer/random_uniform/RandomUniform+embeddings_1/Initializer/random_uniform/sub* 
_output_shapes
:
�N�*
T0*
_class
loc:@embeddings_1
�
'embeddings_1/Initializer/random_uniformAdd+embeddings_1/Initializer/random_uniform/mul+embeddings_1/Initializer/random_uniform/min*
T0*
_class
loc:@embeddings_1* 
_output_shapes
:
�N�
�
embeddings_1
VariableV2*
_class
loc:@embeddings_1*
	container *
shape:
�N�*
dtype0* 
_output_shapes
:
�N�*
shared_name 
�
embeddings_1/AssignAssignembeddings_1'embeddings_1/Initializer/random_uniform* 
_output_shapes
:
�N�*
use_locking(*
T0*
_class
loc:@embeddings_1*
validate_shape(
w
embeddings_1/readIdentityembeddings_1*
_class
loc:@embeddings_1* 
_output_shapes
:
�N�*
T0
�
 embeddings/embedding_lookup/axisConst*
dtype0*
_output_shapes
: *
_class
loc:@embeddings_1*
value	B : 
�
embeddings/embedding_lookupGatherV2embeddings_1/readdata/IteratorGetNext embeddings/embedding_lookup/axis*
Tparams0*
_class
loc:@embeddings_1* 
_output_shapes
:
��*
Taxis0*
Tindices0
~
filling/filling_lookup/axisConst*
_class
loc:@embeddings_1*
value	B : *
dtype0*
_output_shapes
: 
�
filling/filling_lookupGatherV2embeddings_1/readdata/IteratorGetNext_1:1filling/filling_lookup/axis*(
_output_shapes
:����������*
Taxis0*
Tindices0*
Tparams0*
_class
loc:@embeddings_1
`
filling/Mean/reduction_indicesConst*
value	B : *
dtype0*
_output_shapes
: 
�
filling/MeanMeanfilling/filling_lookupfilling/Mean/reduction_indices*
	keep_dims( *

Tidx0*
T0*
_output_shapes	
:�
o
nce_loss/nce_loss/CastCastdata/IteratorGetNext:1*

SrcT0*
_output_shapes
:	�*

DstT0	
r
nce_loss/nce_loss/Reshape/shapeConst*
valueB:
���������*
dtype0*
_output_shapes
:
�
nce_loss/nce_loss/ReshapeReshapence_loss/nce_loss/Castnce_loss/nce_loss/Reshape/shape*
T0	*
Tshape0*
_output_shapes	
:�
�
,nce_loss/nce_loss/LogUniformCandidateSamplerLogUniformCandidateSamplernce_loss/nce_loss/Cast*
seed2 *
num_true*
	range_max�N*
num_sampled@*+
_output_shapes
:@:	�:@*

seed *
unique(
�
nce_loss/nce_loss/StopGradientStopGradient,nce_loss/nce_loss/LogUniformCandidateSampler*
T0	*
_output_shapes
:@
�
 nce_loss/nce_loss/StopGradient_1StopGradient.nce_loss/nce_loss/LogUniformCandidateSampler:1*
T0*
_output_shapes
:	�
�
 nce_loss/nce_loss/StopGradient_2StopGradient.nce_loss/nce_loss/LogUniformCandidateSampler:2*
_output_shapes
:@*
T0
_
nce_loss/nce_loss/concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/concatConcatV2nce_loss/nce_loss/Reshapence_loss/nce_loss/StopGradientnce_loss/nce_loss/concat/axis*
T0	*
N*
_output_shapes	
:�*

Tidx0
�
'nce_loss/nce_loss/embedding_lookup/axisConst*
_class
loc:@nce_weights*
value	B : *
dtype0*
_output_shapes
: 
�
"nce_loss/nce_loss/embedding_lookupGatherV2nce_weights/readnce_loss/nce_loss/concat'nce_loss/nce_loss/embedding_lookup/axis* 
_output_shapes
:
��*
Taxis0*
Tindices0	*
Tparams0*
_class
loc:@nce_weights
b
nce_loss/nce_loss/ShapeConst*
valueB:�*
dtype0*
_output_shapes
:
o
%nce_loss/nce_loss/strided_slice/stackConst*
_output_shapes
:*
valueB: *
dtype0
q
'nce_loss/nce_loss/strided_slice/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
q
'nce_loss/nce_loss/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
�
nce_loss/nce_loss/strided_sliceStridedSlicence_loss/nce_loss/Shape%nce_loss/nce_loss/strided_slice/stack'nce_loss/nce_loss/strided_slice/stack_1'nce_loss/nce_loss/strided_slice/stack_2*
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0*
shrink_axis_mask*

begin_mask *
ellipsis_mask 
d
nce_loss/nce_loss/stack/1Const*
_output_shapes
: *
valueB :
���������*
dtype0
�
nce_loss/nce_loss/stackPacknce_loss/nce_loss/strided_slicence_loss/nce_loss/stack/1*
T0*

axis *
N*
_output_shapes
:
n
nce_loss/nce_loss/Slice/beginConst*
valueB"        *
dtype0*
_output_shapes
:
�
nce_loss/nce_loss/SliceSlice"nce_loss/nce_loss/embedding_lookupnce_loss/nce_loss/Slice/beginnce_loss/nce_loss/stack*0
_output_shapes
:������������������*
Index0*
T0
d
nce_loss/nce_loss/Shape_1Const*
_output_shapes
:*
valueB:�*
dtype0
q
'nce_loss/nce_loss/strided_slice_1/stackConst*
valueB: *
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_1/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_1/stack_2Const*
dtype0*
_output_shapes
:*
valueB:
�
!nce_loss/nce_loss/strided_slice_1StridedSlicence_loss/nce_loss/Shape_1'nce_loss/nce_loss/strided_slice_1/stack)nce_loss/nce_loss/strided_slice_1/stack_1)nce_loss/nce_loss/strided_slice_1/stack_2*
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0*
shrink_axis_mask*

begin_mask *
ellipsis_mask 
]
nce_loss/nce_loss/stack_1/1Const*
value	B : *
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/stack_1Pack!nce_loss/nce_loss/strided_slice_1nce_loss/nce_loss/stack_1/1*
T0*

axis *
N*
_output_shapes
:
o
nce_loss/nce_loss/Slice_1/sizeConst*
_output_shapes
:*
valueB"��������*
dtype0
�
nce_loss/nce_loss/Slice_1Slice"nce_loss/nce_loss/embedding_lookupnce_loss/nce_loss/stack_1nce_loss/nce_loss/Slice_1/size*
Index0*
T0*(
_output_shapes
:����������
�
nce_loss/nce_loss/MatMulMatMulembeddings/embedding_lookupnce_loss/nce_loss/Slice_1*
T0*(
_output_shapes
:����������*
transpose_a( *
transpose_b(
�
)nce_loss/nce_loss/embedding_lookup_1/axisConst*
_class
loc:@nce_bias*
value	B : *
dtype0*
_output_shapes
: 
�
$nce_loss/nce_loss/embedding_lookup_1GatherV2nce_bias/readnce_loss/nce_loss/concat)nce_loss/nce_loss/embedding_lookup_1/axis*
_output_shapes	
:�*
Taxis0*
Tindices0	*
Tparams0*
_class
loc:@nce_bias
d
nce_loss/nce_loss/Shape_2Const*
valueB:�*
dtype0*
_output_shapes
:
i
nce_loss/nce_loss/Slice_2/beginConst*
dtype0*
_output_shapes
:*
valueB: 
�
nce_loss/nce_loss/Slice_2Slice$nce_loss/nce_loss/embedding_lookup_1nce_loss/nce_loss/Slice_2/beginnce_loss/nce_loss/Shape_2*
Index0*
T0*
_output_shapes	
:�
d
nce_loss/nce_loss/Shape_3Const*
valueB:�*
dtype0*
_output_shapes
:
q
nce_loss/nce_loss/Slice_3/sizeConst*
valueB:
���������*
dtype0*
_output_shapes
:
�
nce_loss/nce_loss/Slice_3Slice$nce_loss/nce_loss/embedding_lookup_1nce_loss/nce_loss/Shape_3nce_loss/nce_loss/Slice_3/size*
_output_shapes
:@*
Index0*
T0
p
nce_loss/nce_loss/Shape_4Shapence_loss/nce_loss/Slice*
T0*
out_type0*
_output_shapes
:
q
'nce_loss/nce_loss/strided_slice_2/stackConst*
valueB:*
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_2/stack_1Const*
dtype0*
_output_shapes
:*
valueB:
s
)nce_loss/nce_loss/strided_slice_2/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
�
!nce_loss/nce_loss/strided_slice_2StridedSlicence_loss/nce_loss/Shape_4'nce_loss/nce_loss/strided_slice_2/stack)nce_loss/nce_loss/strided_slice_2/stack_1)nce_loss/nce_loss/strided_slice_2/stack_2*
T0*
Index0*
shrink_axis_mask *
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
_output_shapes
:
t
#nce_loss/nce_loss/concat_1/values_0Const*
valueB"����   *
dtype0*
_output_shapes
:
a
nce_loss/nce_loss/concat_1/axisConst*
value	B : *
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/concat_1ConcatV2#nce_loss/nce_loss/concat_1/values_0!nce_loss/nce_loss/strided_slice_2nce_loss/nce_loss/concat_1/axis*
T0*
N*
_output_shapes
:*

Tidx0
b
 nce_loss/nce_loss/ExpandDims/dimConst*
value	B :*
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/ExpandDims
ExpandDimsembeddings/embedding_lookup nce_loss/nce_loss/ExpandDims/dim*
T0*$
_output_shapes
:��*

Tdim0
�
nce_loss/nce_loss/Reshape_1Reshapence_loss/nce_loss/Slicence_loss/nce_loss/concat_1*
T0*
Tshape0*4
_output_shapes"
 :������������������
�
nce_loss/nce_loss/MulMulnce_loss/nce_loss/ExpandDimsnce_loss/nce_loss/Reshape_1*
T0*$
_output_shapes
:��
v
#nce_loss/nce_loss/concat_2/values_0Const*
dtype0*
_output_shapes
:*
valueB:
���������
a
nce_loss/nce_loss/concat_2/axisConst*
value	B : *
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/concat_2ConcatV2#nce_loss/nce_loss/concat_2/values_0!nce_loss/nce_loss/strided_slice_2nce_loss/nce_loss/concat_2/axis*
N*
_output_shapes
:*

Tidx0*
T0
�
nce_loss/nce_loss/Reshape_2Reshapence_loss/nce_loss/Mulnce_loss/nce_loss/concat_2*
T0*
Tshape0*0
_output_shapes
:������������������
t
nce_loss/nce_loss/Shape_5Shapence_loss/nce_loss/Reshape_2*
_output_shapes
:*
T0*
out_type0
q
'nce_loss/nce_loss/strided_slice_3/stackConst*
_output_shapes
:*
valueB:*
dtype0
s
)nce_loss/nce_loss/strided_slice_3/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_3/stack_2Const*
_output_shapes
:*
valueB:*
dtype0
�
!nce_loss/nce_loss/strided_slice_3StridedSlicence_loss/nce_loss/Shape_5'nce_loss/nce_loss/strided_slice_3/stack)nce_loss/nce_loss/strided_slice_3/stack_1)nce_loss/nce_loss/strided_slice_3/stack_2*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0
]
nce_loss/nce_loss/stack_2/1Const*
value	B :*
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/stack_2Pack!nce_loss/nce_loss/strided_slice_3nce_loss/nce_loss/stack_2/1*
T0*

axis *
N*
_output_shapes
:
a
nce_loss/nce_loss/ones/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *  �?
�
nce_loss/nce_loss/onesFillnce_loss/nce_loss/stack_2nce_loss/nce_loss/ones/Const*
T0*

index_type0*'
_output_shapes
:���������
�
nce_loss/nce_loss/MatMul_1MatMulnce_loss/nce_loss/Reshape_2nce_loss/nce_loss/ones*
transpose_b( *
T0*'
_output_shapes
:���������*
transpose_a( 
t
!nce_loss/nce_loss/Reshape_3/shapeConst*
valueB:
���������*
dtype0*
_output_shapes
:
�
nce_loss/nce_loss/Reshape_3Reshapence_loss/nce_loss/MatMul_1!nce_loss/nce_loss/Reshape_3/shape*#
_output_shapes
:���������*
T0*
Tshape0
r
!nce_loss/nce_loss/Reshape_4/shapeConst*
_output_shapes
:*
valueB"����   *
dtype0
�
nce_loss/nce_loss/Reshape_4Reshapence_loss/nce_loss/Reshape_3!nce_loss/nce_loss/Reshape_4/shape*
T0*
Tshape0*'
_output_shapes
:���������
r
!nce_loss/nce_loss/Reshape_5/shapeConst*
valueB"����   *
dtype0*
_output_shapes
:
�
nce_loss/nce_loss/Reshape_5Reshapence_loss/nce_loss/Slice_2!nce_loss/nce_loss/Reshape_5/shape*
_output_shapes
:	�*
T0*
Tshape0
�
nce_loss/nce_loss/addAddnce_loss/nce_loss/Reshape_4nce_loss/nce_loss/Reshape_5*
T0*
_output_shapes
:	�
}
nce_loss/nce_loss/add_1Addnce_loss/nce_loss/MatMulnce_loss/nce_loss/Slice_3*
T0*
_output_shapes
:	�@
h
nce_loss/nce_loss/LogLog nce_loss/nce_loss/StopGradient_1*
_output_shapes
:	�*
T0
t
nce_loss/nce_loss/subSubnce_loss/nce_loss/addnce_loss/nce_loss/Log*
T0*
_output_shapes
:	�
e
nce_loss/nce_loss/Log_1Log nce_loss/nce_loss/StopGradient_2*
T0*
_output_shapes
:@
z
nce_loss/nce_loss/sub_1Subnce_loss/nce_loss/add_1nce_loss/nce_loss/Log_1*
T0*
_output_shapes
:	�@
a
nce_loss/nce_loss/concat_3/axisConst*
_output_shapes
: *
value	B :*
dtype0
�
nce_loss/nce_loss/concat_3ConcatV2nce_loss/nce_loss/subnce_loss/nce_loss/sub_1nce_loss/nce_loss/concat_3/axis*
T0*
N*
_output_shapes
:	�A*

Tidx0
r
!nce_loss/nce_loss/ones_like/ShapeConst*
dtype0*
_output_shapes
:*
valueB"�      
f
!nce_loss/nce_loss/ones_like/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *  �?
�
nce_loss/nce_loss/ones_likeFill!nce_loss/nce_loss/ones_like/Shape!nce_loss/nce_loss/ones_like/Const*
T0*

index_type0*
_output_shapes
:	�
`
nce_loss/nce_loss/truediv/yConst*
valueB
 *  �?*
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/truedivRealDivnce_loss/nce_loss/ones_likence_loss/nce_loss/truediv/y*
_output_shapes
:	�*
T0
}
,nce_loss/nce_loss/zeros_like/shape_as_tensorConst*
valueB"�   @   *
dtype0*
_output_shapes
:
g
"nce_loss/nce_loss/zeros_like/ConstConst*
_output_shapes
: *
valueB
 *    *
dtype0
�
nce_loss/nce_loss/zeros_likeFill,nce_loss/nce_loss/zeros_like/shape_as_tensor"nce_loss/nce_loss/zeros_like/Const*
T0*

index_type0*
_output_shapes
:	�@
a
nce_loss/nce_loss/concat_4/axisConst*
value	B :*
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/concat_4ConcatV2nce_loss/nce_loss/truedivnce_loss/nce_loss/zeros_likence_loss/nce_loss/concat_4/axis*
N*
_output_shapes
:	�A*

Tidx0*
T0
�
2nce_loss/sampled_losses/zeros_like/shape_as_tensorConst*
valueB"�   A   *
dtype0*
_output_shapes
:
m
(nce_loss/sampled_losses/zeros_like/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
�
"nce_loss/sampled_losses/zeros_likeFill2nce_loss/sampled_losses/zeros_like/shape_as_tensor(nce_loss/sampled_losses/zeros_like/Const*
T0*

index_type0*
_output_shapes
:	�A
�
$nce_loss/sampled_losses/GreaterEqualGreaterEqualnce_loss/nce_loss/concat_3"nce_loss/sampled_losses/zeros_like*
T0*
_output_shapes
:	�A
�
nce_loss/sampled_losses/SelectSelect$nce_loss/sampled_losses/GreaterEqualnce_loss/nce_loss/concat_3"nce_loss/sampled_losses/zeros_like*
T0*
_output_shapes
:	�A
h
nce_loss/sampled_losses/NegNegnce_loss/nce_loss/concat_3*
T0*
_output_shapes
:	�A
�
 nce_loss/sampled_losses/Select_1Select$nce_loss/sampled_losses/GreaterEqualnce_loss/sampled_losses/Negnce_loss/nce_loss/concat_3*
T0*
_output_shapes
:	�A
�
nce_loss/sampled_losses/mulMulnce_loss/nce_loss/concat_3nce_loss/nce_loss/concat_4*
_output_shapes
:	�A*
T0
�
nce_loss/sampled_losses/subSubnce_loss/sampled_losses/Selectnce_loss/sampled_losses/mul*
_output_shapes
:	�A*
T0
n
nce_loss/sampled_losses/ExpExp nce_loss/sampled_losses/Select_1*
T0*
_output_shapes
:	�A
m
nce_loss/sampled_losses/Log1pLog1pnce_loss/sampled_losses/Exp*
_output_shapes
:	�A*
T0
�
nce_loss/sampled_lossesAddnce_loss/sampled_losses/subnce_loss/sampled_losses/Log1p*
T0*
_output_shapes
:	�A
_
nce_loss/ShapeConst*
valueB"�   A   *
dtype0*
_output_shapes
:
f
nce_loss/strided_slice/stackConst*
valueB:*
dtype0*
_output_shapes
:
h
nce_loss/strided_slice/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
h
nce_loss/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
�
nce_loss/strided_sliceStridedSlicence_loss/Shapence_loss/strided_slice/stacknce_loss/strided_slice/stack_1nce_loss/strided_slice/stack_2*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0
R
nce_loss/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
z
nce_loss/stackPacknce_loss/strided_slicence_loss/stack/1*
T0*

axis *
N*
_output_shapes
:
X
nce_loss/ones/ConstConst*
valueB
 *  �?*
dtype0*
_output_shapes
: 
~
nce_loss/onesFillnce_loss/stacknce_loss/ones/Const*'
_output_shapes
:���������*
T0*

index_type0
�
nce_loss/MatMulMatMulnce_loss/sampled_lossesnce_loss/ones*
_output_shapes
:	�*
transpose_a( *
transpose_b( *
T0
i
nce_loss/Reshape/shapeConst*
valueB:
���������*
dtype0*
_output_shapes
:
x
nce_loss/ReshapeReshapence_loss/MatMulnce_loss/Reshape/shape*
Tshape0*
_output_shapes	
:�*
T0
X
nce_loss/ConstConst*
valueB: *
dtype0*
_output_shapes
:
{
nce_loss/nce_loss_1Meannce_loss/Reshapence_loss/Const*
T0*
_output_shapes
: *
	keep_dims( *

Tidx0
[
optimize/gradients/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
a
optimize/gradients/grad_ys_0Const*
valueB
 *  �?*
dtype0*
_output_shapes
: 
�
optimize/gradients/FillFilloptimize/gradients/Shapeoptimize/gradients/grad_ys_0*
T0*

index_type0*
_output_shapes
: 
�
9optimize/gradients/nce_loss/nce_loss_1_grad/Reshape/shapeConst*
valueB:*
dtype0*
_output_shapes
:
�
3optimize/gradients/nce_loss/nce_loss_1_grad/ReshapeReshapeoptimize/gradients/Fill9optimize/gradients/nce_loss/nce_loss_1_grad/Reshape/shape*
_output_shapes
:*
T0*
Tshape0
|
1optimize/gradients/nce_loss/nce_loss_1_grad/ConstConst*
_output_shapes
:*
valueB:�*
dtype0
�
0optimize/gradients/nce_loss/nce_loss_1_grad/TileTile3optimize/gradients/nce_loss/nce_loss_1_grad/Reshape1optimize/gradients/nce_loss/nce_loss_1_grad/Const*
_output_shapes	
:�*

Tmultiples0*
T0
x
3optimize/gradients/nce_loss/nce_loss_1_grad/Const_1Const*
valueB
 *   C*
dtype0*
_output_shapes
: 
�
3optimize/gradients/nce_loss/nce_loss_1_grad/truedivRealDiv0optimize/gradients/nce_loss/nce_loss_1_grad/Tile3optimize/gradients/nce_loss/nce_loss_1_grad/Const_1*
_output_shapes	
:�*
T0

.optimize/gradients/nce_loss/Reshape_grad/ShapeConst*
valueB"�      *
dtype0*
_output_shapes
:
�
0optimize/gradients/nce_loss/Reshape_grad/ReshapeReshape3optimize/gradients/nce_loss/nce_loss_1_grad/truediv.optimize/gradients/nce_loss/Reshape_grad/Shape*
Tshape0*
_output_shapes
:	�*
T0
�
.optimize/gradients/nce_loss/MatMul_grad/MatMulMatMul0optimize/gradients/nce_loss/Reshape_grad/Reshapence_loss/ones*
T0*(
_output_shapes
:����������*
transpose_a( *
transpose_b(
�
0optimize/gradients/nce_loss/MatMul_grad/MatMul_1MatMulnce_loss/sampled_losses0optimize/gradients/nce_loss/Reshape_grad/Reshape*
_output_shapes

:A*
transpose_a(*
transpose_b( *
T0
�
8optimize/gradients/nce_loss/MatMul_grad/tuple/group_depsNoOp/^optimize/gradients/nce_loss/MatMul_grad/MatMul1^optimize/gradients/nce_loss/MatMul_grad/MatMul_1
�
@optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependencyIdentity.optimize/gradients/nce_loss/MatMul_grad/MatMul9^optimize/gradients/nce_loss/MatMul_grad/tuple/group_deps*
_output_shapes
:	�A*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul
�
Boptimize/gradients/nce_loss/MatMul_grad/tuple/control_dependency_1Identity0optimize/gradients/nce_loss/MatMul_grad/MatMul_19^optimize/gradients/nce_loss/MatMul_grad/tuple/group_deps*
T0*C
_class9
75loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul_1*
_output_shapes

:A
�
@optimize/gradients/nce_loss/sampled_losses_grad/tuple/group_depsNoOpA^optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependency
�
Hoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependencyIdentity@optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependencyA^optimize/gradients/nce_loss/sampled_losses_grad/tuple/group_deps*
_output_shapes
:	�A*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul
�
Joptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency_1Identity@optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependencyA^optimize/gradients/nce_loss/sampled_losses_grad/tuple/group_deps*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul*
_output_shapes
:	�A
�
7optimize/gradients/nce_loss/sampled_losses/sub_grad/NegNegHoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency*
T0*
_output_shapes
:	�A
�
Doptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/sampled_losses/sub_grad/NegI^optimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency
�
Loptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependencyIdentityHoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependencyE^optimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/group_deps*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul*
_output_shapes
:	�A
�
Noptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency_1Identity7optimize/gradients/nce_loss/sampled_losses/sub_grad/NegE^optimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/group_deps*
_output_shapes
:	�A*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/sampled_losses/sub_grad/Neg
�
;optimize/gradients/nce_loss/sampled_losses/Log1p_grad/add/xConstK^optimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency_1*
dtype0*
_output_shapes
: *
valueB
 *  �?
�
9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/addAdd;optimize/gradients/nce_loss/sampled_losses/Log1p_grad/add/xnce_loss/sampled_losses/Exp*
_output_shapes
:	�A*
T0
�
@optimize/gradients/nce_loss/sampled_losses/Log1p_grad/Reciprocal
Reciprocal9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/add*
T0*
_output_shapes
:	�A
�
9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/mulMulJoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency_1@optimize/gradients/nce_loss/sampled_losses/Log1p_grad/Reciprocal*
T0*
_output_shapes
:	�A
�
Qoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/shape_as_tensorConst*
valueB"�   A   *
dtype0*
_output_shapes
:
�
Goptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
�
Aoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_likeFillQoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/shape_as_tensorGoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/Const*

index_type0*
_output_shapes
:	�A*
T0
�
=optimize/gradients/nce_loss/sampled_losses/Select_grad/SelectSelect$nce_loss/sampled_losses/GreaterEqualLoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependencyAoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like*
T0*
_output_shapes
:	�A
�
?optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1Select$nce_loss/sampled_losses/GreaterEqualAoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_likeLoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency*
T0*
_output_shapes
:	�A
�
Goptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/group_depsNoOp>^optimize/gradients/nce_loss/sampled_losses/Select_grad/Select@^optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1
�
Ooptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/control_dependencyIdentity=optimize/gradients/nce_loss/sampled_losses/Select_grad/SelectH^optimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/group_deps*
T0*P
_classF
DBloc:@optimize/gradients/nce_loss/sampled_losses/Select_grad/Select*
_output_shapes
:	�A
�
Qoptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/control_dependency_1Identity?optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1H^optimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/group_deps*
_output_shapes
:	�A*
T0*R
_classH
FDloc:@optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1
�
7optimize/gradients/nce_loss/sampled_losses/mul_grad/MulMulNoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency_1nce_loss/nce_loss/concat_4*
T0*
_output_shapes
:	�A
�
9optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1MulNoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency_1nce_loss/nce_loss/concat_3*
T0*
_output_shapes
:	�A
�
Doptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul:^optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1
�
Loptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/sampled_losses/mul_grad/MulE^optimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/group_deps*
_output_shapes
:	�A*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul
�
Noptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1E^optimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/group_deps*L
_classB
@>loc:@optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1*
_output_shapes
:	�A*
T0
�
7optimize/gradients/nce_loss/sampled_losses/Exp_grad/mulMul9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/mulnce_loss/sampled_losses/Exp*
T0*
_output_shapes
:	�A
�
Soptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/shape_as_tensorConst*
valueB"�   A   *
dtype0*
_output_shapes
:
�
Ioptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
�
Coptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_likeFillSoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/shape_as_tensorIoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/Const*
_output_shapes
:	�A*
T0*

index_type0
�
?optimize/gradients/nce_loss/sampled_losses/Select_1_grad/SelectSelect$nce_loss/sampled_losses/GreaterEqual7optimize/gradients/nce_loss/sampled_losses/Exp_grad/mulCoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like*
T0*
_output_shapes
:	�A
�
Aoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1Select$nce_loss/sampled_losses/GreaterEqualCoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like7optimize/gradients/nce_loss/sampled_losses/Exp_grad/mul*
T0*
_output_shapes
:	�A
�
Ioptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/group_depsNoOp@^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/SelectB^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1
�
Qoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependencyIdentity?optimize/gradients/nce_loss/sampled_losses/Select_1_grad/SelectJ^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/group_deps*R
_classH
FDloc:@optimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select*
_output_shapes
:	�A*
T0
�
Soptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependency_1IdentityAoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1J^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@optimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1*
_output_shapes
:	�A
�
7optimize/gradients/nce_loss/sampled_losses/Neg_grad/NegNegQoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependency*
T0*
_output_shapes
:	�A
�
optimize/gradients/AddNAddNOoptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/control_dependencyLoptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/control_dependencySoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependency_17optimize/gradients/nce_loss/sampled_losses/Neg_grad/Neg*
T0*P
_classF
DBloc:@optimize/gradients/nce_loss/sampled_losses/Select_grad/Select*
N*
_output_shapes
:	�A
y
7optimize/gradients/nce_loss/nce_loss/concat_3_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 
�
6optimize/gradients/nce_loss/nce_loss/concat_3_grad/modFloorModnce_loss/nce_loss/concat_3/axis7optimize/gradients/nce_loss/nce_loss/concat_3_grad/Rank*
T0*
_output_shapes
: 
�
8optimize/gradients/nce_loss/nce_loss/concat_3_grad/ShapeConst*
valueB"�      *
dtype0*
_output_shapes
:
�
:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape_1Const*
valueB"�   @   *
dtype0*
_output_shapes
:
�
?optimize/gradients/nce_loss/nce_loss/concat_3_grad/ConcatOffsetConcatOffset6optimize/gradients/nce_loss/nce_loss/concat_3_grad/mod8optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape_1*
N* 
_output_shapes
::
�
8optimize/gradients/nce_loss/nce_loss/concat_3_grad/SliceSliceoptimize/gradients/AddN?optimize/gradients/nce_loss/nce_loss/concat_3_grad/ConcatOffset8optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape*
Index0*
T0*
_output_shapes
:	�
�
:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1Sliceoptimize/gradients/AddNAoptimize/gradients/nce_loss/nce_loss/concat_3_grad/ConcatOffset:1:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape_1*
_output_shapes
:	�@*
Index0*
T0
�
Coptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/group_depsNoOp9^optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice;^optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1
�
Koptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependencyIdentity8optimize/gradients/nce_loss/nce_loss/concat_3_grad/SliceD^optimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/group_deps*K
_classA
?=loc:@optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice*
_output_shapes
:	�*
T0
�
Moptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency_1Identity:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1D^optimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/group_deps*
T0*M
_classC
A?loc:@optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1*
_output_shapes
:	�@
�
1optimize/gradients/nce_loss/nce_loss/sub_grad/NegNegKoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency*
T0*
_output_shapes
:	�
�
>optimize/gradients/nce_loss/nce_loss/sub_grad/tuple/group_depsNoOpL^optimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency2^optimize/gradients/nce_loss/nce_loss/sub_grad/Neg
�
Foptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependencyIdentityKoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency?^optimize/gradients/nce_loss/nce_loss/sub_grad/tuple/group_deps*
T0*K
_classA
?=loc:@optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice*
_output_shapes
:	�
�
Hoptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependency_1Identity1optimize/gradients/nce_loss/nce_loss/sub_grad/Neg?^optimize/gradients/nce_loss/nce_loss/sub_grad/tuple/group_deps*D
_class:
86loc:@optimize/gradients/nce_loss/nce_loss/sub_grad/Neg*
_output_shapes
:	�*
T0
�
5optimize/gradients/nce_loss/nce_loss/sub_1_grad/ShapeConst*
valueB"�   @   *
dtype0*
_output_shapes
:
�
7optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape_1Const*
valueB:@*
dtype0*
_output_shapes
:
�
Eoptimize/gradients/nce_loss/nce_loss/sub_1_grad/BroadcastGradientArgsBroadcastGradientArgs5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape7optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape_1*
T0*2
_output_shapes 
:���������:���������
�
3optimize/gradients/nce_loss/nce_loss/sub_1_grad/SumSumMoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency_1Eoptimize/gradients/nce_loss/nce_loss/sub_1_grad/BroadcastGradientArgs*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
�
7optimize/gradients/nce_loss/nce_loss/sub_1_grad/ReshapeReshape3optimize/gradients/nce_loss/nce_loss/sub_1_grad/Sum5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape*
_output_shapes
:	�@*
T0*
Tshape0
�
5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Sum_1SumMoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency_1Goptimize/gradients/nce_loss/nce_loss/sub_1_grad/BroadcastGradientArgs:1*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
�
3optimize/gradients/nce_loss/nce_loss/sub_1_grad/NegNeg5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Sum_1*
T0*
_output_shapes
:
�
9optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1Reshape3optimize/gradients/nce_loss/nce_loss/sub_1_grad/Neg7optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:@
�
@optimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape:^optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1
�
Hoptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/nce_loss/sub_1_grad/ReshapeA^optimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/group_deps*
_output_shapes
:	�@*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape
�
Joptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1A^optimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/group_deps*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1*
_output_shapes
:@*
T0
�
3optimize/gradients/nce_loss/nce_loss/add_grad/ShapeShapence_loss/nce_loss/Reshape_4*
T0*
out_type0*
_output_shapes
:
�
5optimize/gradients/nce_loss/nce_loss/add_grad/Shape_1Const*
_output_shapes
:*
valueB"�      *
dtype0
�
Coptimize/gradients/nce_loss/nce_loss/add_grad/BroadcastGradientArgsBroadcastGradientArgs3optimize/gradients/nce_loss/nce_loss/add_grad/Shape5optimize/gradients/nce_loss/nce_loss/add_grad/Shape_1*
T0*2
_output_shapes 
:���������:���������
�
1optimize/gradients/nce_loss/nce_loss/add_grad/SumSumFoptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependencyCoptimize/gradients/nce_loss/nce_loss/add_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*
	keep_dims( *

Tidx0
�
5optimize/gradients/nce_loss/nce_loss/add_grad/ReshapeReshape1optimize/gradients/nce_loss/nce_loss/add_grad/Sum3optimize/gradients/nce_loss/nce_loss/add_grad/Shape*
T0*
Tshape0*'
_output_shapes
:���������
�
3optimize/gradients/nce_loss/nce_loss/add_grad/Sum_1SumFoptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependencyEoptimize/gradients/nce_loss/nce_loss/add_grad/BroadcastGradientArgs:1*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
�
7optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1Reshape3optimize/gradients/nce_loss/nce_loss/add_grad/Sum_15optimize/gradients/nce_loss/nce_loss/add_grad/Shape_1*
Tshape0*
_output_shapes
:	�*
T0
�
>optimize/gradients/nce_loss/nce_loss/add_grad/tuple/group_depsNoOp6^optimize/gradients/nce_loss/nce_loss/add_grad/Reshape8^optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1
�
Foptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependencyIdentity5optimize/gradients/nce_loss/nce_loss/add_grad/Reshape?^optimize/gradients/nce_loss/nce_loss/add_grad/tuple/group_deps*'
_output_shapes
:���������*
T0*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/add_grad/Reshape
�
Hoptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependency_1Identity7optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1?^optimize/gradients/nce_loss/nce_loss/add_grad/tuple/group_deps*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1*
_output_shapes
:	�*
T0
�
5optimize/gradients/nce_loss/nce_loss/add_1_grad/ShapeShapence_loss/nce_loss/MatMul*
T0*
out_type0*
_output_shapes
:
�
7optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape_1Const*
valueB:@*
dtype0*
_output_shapes
:
�
Eoptimize/gradients/nce_loss/nce_loss/add_1_grad/BroadcastGradientArgsBroadcastGradientArgs5optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape7optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape_1*
T0*2
_output_shapes 
:���������:���������
�
3optimize/gradients/nce_loss/nce_loss/add_1_grad/SumSumHoptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependencyEoptimize/gradients/nce_loss/nce_loss/add_1_grad/BroadcastGradientArgs*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
�
7optimize/gradients/nce_loss/nce_loss/add_1_grad/ReshapeReshape3optimize/gradients/nce_loss/nce_loss/add_1_grad/Sum5optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape*
T0*
Tshape0*(
_output_shapes
:����������
�
5optimize/gradients/nce_loss/nce_loss/add_1_grad/Sum_1SumHoptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependencyGoptimize/gradients/nce_loss/nce_loss/add_1_grad/BroadcastGradientArgs:1*
	keep_dims( *

Tidx0*
T0*
_output_shapes
:
�
9optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1Reshape5optimize/gradients/nce_loss/nce_loss/add_1_grad/Sum_17optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:@
�
@optimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape:^optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1
�
Hoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/nce_loss/add_1_grad/ReshapeA^optimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape*(
_output_shapes
:����������
�
Joptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1A^optimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/group_deps*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1*
_output_shapes
:@
�
9optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/ShapeShapence_loss/nce_loss/Reshape_3*
T0*
out_type0*
_output_shapes
:
�
;optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/ReshapeReshapeFoptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependency9optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/Shape*#
_output_shapes
:���������*
T0*
Tshape0
�
9optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/ShapeConst*
valueB:�*
dtype0*
_output_shapes
:
�
;optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/ReshapeReshapeHoptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependency_19optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/Shape*
Tshape0*
_output_shapes	
:�*
T0
�
7optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMulMatMulHoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependencynce_loss/nce_loss/Slice_1* 
_output_shapes
:
��*
transpose_a( *
transpose_b( *
T0
�
9optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1MatMulHoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependencyembeddings/embedding_lookup*
T0*(
_output_shapes
:����������*
transpose_a(*
transpose_b( 
�
Aoptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul:^optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1
�
Ioptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMulB^optimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul* 
_output_shapes
:
��
�
Koptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1B^optimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1*(
_output_shapes
:����������
x
6optimize/gradients/nce_loss/nce_loss/Slice_3_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 
�
7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/ShapeConst*
_output_shapes
:*
valueB:@*
dtype0
{
9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
�
7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stackPack6optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Rank9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack/1*
T0*

axis *
N*
_output_shapes
:
�
9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/ReshapeReshapence_loss/nce_loss/Shape_37optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack*
T0*
Tshape0*
_output_shapes

:
�
9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Shape_1Const*
valueB:�*
dtype0*
_output_shapes
:
�
5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/subSub9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Shape_17optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Shape*
_output_shapes
:*
T0
�
7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/sub_1Sub5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/subnce_loss/nce_loss/Shape_3*
_output_shapes
:*
T0
�
;optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Reshape_1Reshape7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/sub_17optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack*
T0*
Tshape0*
_output_shapes

:

=optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concat/axisConst*
value	B :*
dtype0*
_output_shapes
: 
�
8optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concatConcatV29optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Reshape;optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Reshape_1=optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concat/axis*

Tidx0*
T0*
N*
_output_shapes

:
�
5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/PadPadJoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependency_18optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concat*
T0*
	Tpaddings0*
_output_shapes	
:�
�
9optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/ShapeShapence_loss/nce_loss/MatMul_1*
T0*
out_type0*
_output_shapes
:
�
;optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/ReshapeReshape;optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/Reshape9optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/Shape*'
_output_shapes
:���������*
T0*
Tshape0
x
6optimize/gradients/nce_loss/nce_loss/Slice_2_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 
�
7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/ShapeConst*
valueB:�*
dtype0*
_output_shapes
:
{
9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
�
7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stackPack6optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Rank9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack/1*
T0*

axis *
N*
_output_shapes
:
�
9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/ReshapeReshapence_loss/nce_loss/Slice_2/begin7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack*
T0*
Tshape0*
_output_shapes

:
�
9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Shape_1Const*
valueB:�*
dtype0*
_output_shapes
:
�
5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/subSub9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Shape_17optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Shape*
_output_shapes
:*
T0
�
7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/sub_1Sub5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/subnce_loss/nce_loss/Slice_2/begin*
_output_shapes
:*
T0
�
;optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Reshape_1Reshape7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/sub_17optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack*
_output_shapes

:*
T0*
Tshape0

=optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concat/axisConst*
value	B :*
dtype0*
_output_shapes
: 
�
8optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concatConcatV29optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Reshape;optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Reshape_1=optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concat/axis*
_output_shapes

:*

Tidx0*
T0*
N
�
5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/PadPad;optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/Reshape8optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concat*
_output_shapes	
:�*
T0*
	Tpaddings0
x
6optimize/gradients/nce_loss/nce_loss/Slice_1_grad/RankConst*
_output_shapes
: *
value	B :*
dtype0
�
7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/ShapeShapence_loss/nce_loss/Slice_1*
T0*
out_type0*
_output_shapes
:
{
9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
�
7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stackPack6optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Rank9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack/1*
T0*

axis *
N*
_output_shapes
:
�
9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/ReshapeReshapence_loss/nce_loss/stack_17optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack*
Tshape0*
_output_shapes

:*
T0
�
9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Shape_1Const*
dtype0*
_output_shapes
:*
valueB"�   �   
�
5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/subSub9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Shape_17optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Shape*
T0*
_output_shapes
:
�
7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/sub_1Sub5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/subnce_loss/nce_loss/stack_1*
T0*
_output_shapes
:
�
;optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Reshape_1Reshape7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/sub_17optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack*
T0*
Tshape0*
_output_shapes

:

=optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concat/axisConst*
value	B :*
dtype0*
_output_shapes
: 
�
8optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concatConcatV29optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Reshape;optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Reshape_1=optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concat/axis*
N*
_output_shapes

:*

Tidx0*
T0
�
5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/PadPadKoptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependency_18optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concat*
T0*
	Tpaddings0* 
_output_shapes
:
��
�
9optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMulMatMul;optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/Reshapence_loss/nce_loss/ones*
T0*0
_output_shapes
:������������������*
transpose_a( *
transpose_b(
�
;optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1MatMulnce_loss/nce_loss/Reshape_2;optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/Reshape*
T0*'
_output_shapes
:���������*
transpose_a(*
transpose_b( 
�
Coptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/group_depsNoOp:^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul<^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1
�
Koptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/control_dependencyIdentity9optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMulD^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/group_deps*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul*0
_output_shapes
:������������������*
T0
�
Moptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/control_dependency_1Identity;optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1D^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/group_deps*
T0*N
_classD
B@loc:@optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1*'
_output_shapes
:���������
�
optimize/gradients/AddN_1AddN5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Pad5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Pad*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Pad*
N*
_output_shapes	
:�*
T0
�
Boptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ShapeConst*
_class
loc:@nce_bias*
valueB	R�N*
dtype0	*
_output_shapes
:
�
Doptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ToInt32CastBoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Shape*
_output_shapes
:*

DstT0*

SrcT0	*
_class
loc:@nce_bias
�
Aoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/SizeConst*
value
B :�*
dtype0*
_output_shapes
: 
�
Koptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims/dimConst*
value	B : *
dtype0*
_output_shapes
: 
�
Goptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims
ExpandDimsAoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/SizeKoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims/dim*
_output_shapes
:*

Tdim0*
T0
�
Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stackConst*
valueB:*
dtype0*
_output_shapes
:
�
Roptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_1Const*
valueB: *
dtype0*
_output_shapes
:
�
Roptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
�
Joptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_sliceStridedSliceDoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ToInt32Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stackRoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_1Roptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_2*
T0*
Index0*
shrink_axis_mask *
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask*
_output_shapes
: 
�
Hoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
�
Coptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concatConcatV2Goptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDimsJoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_sliceHoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concat/axis*
T0*
N*
_output_shapes
:*

Tidx0
�
Doptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ReshapeReshapeoptimize/gradients/AddN_1Coptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concat*
_output_shapes	
:�*
T0*
Tshape0
�
Foptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Reshape_1Reshapence_loss/nce_loss/concatGoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims*
T0	*
Tshape0*
_output_shapes	
:�
�
9optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/ShapeConst*!
valueB"�      �   *
dtype0*
_output_shapes
:
�
;optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/ReshapeReshapeKoptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/control_dependency9optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/Shape*
T0*
Tshape0*$
_output_shapes
:��
�
3optimize/gradients/nce_loss/nce_loss/Mul_grad/ShapeConst*
dtype0*
_output_shapes
:*!
valueB"�      �   
�
5optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape_1Shapence_loss/nce_loss/Reshape_1*
T0*
out_type0*
_output_shapes
:
�
Coptimize/gradients/nce_loss/nce_loss/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs3optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape5optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape_1*
T0*2
_output_shapes 
:���������:���������
�
1optimize/gradients/nce_loss/nce_loss/Mul_grad/MulMul;optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/Reshapence_loss/nce_loss/Reshape_1*$
_output_shapes
:��*
T0
�
1optimize/gradients/nce_loss/nce_loss/Mul_grad/SumSum1optimize/gradients/nce_loss/nce_loss/Mul_grad/MulCoptimize/gradients/nce_loss/nce_loss/Mul_grad/BroadcastGradientArgs*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
�
5optimize/gradients/nce_loss/nce_loss/Mul_grad/ReshapeReshape1optimize/gradients/nce_loss/nce_loss/Mul_grad/Sum3optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape*
T0*
Tshape0*$
_output_shapes
:��
�
3optimize/gradients/nce_loss/nce_loss/Mul_grad/Mul_1Mulnce_loss/nce_loss/ExpandDims;optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/Reshape*
T0*$
_output_shapes
:��
�
3optimize/gradients/nce_loss/nce_loss/Mul_grad/Sum_1Sum3optimize/gradients/nce_loss/nce_loss/Mul_grad/Mul_1Eoptimize/gradients/nce_loss/nce_loss/Mul_grad/BroadcastGradientArgs:1*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
�
7optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1Reshape3optimize/gradients/nce_loss/nce_loss/Mul_grad/Sum_15optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape_1*
T0*
Tshape0*4
_output_shapes"
 :������������������
�
>optimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/group_depsNoOp6^optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape8^optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1
�
Foptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependencyIdentity5optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape?^optimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/group_deps*
T0*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape*$
_output_shapes
:��
�
Hoptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependency_1Identity7optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1?^optimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1*4
_output_shapes"
 :������������������
�
:optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/ShapeConst*
valueB"�   �   *
dtype0*
_output_shapes
:
�
<optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/ReshapeReshapeFoptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependency:optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/Shape*
T0*
Tshape0* 
_output_shapes
:
��
�
9optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/ShapeShapence_loss/nce_loss/Slice*
T0*
out_type0*
_output_shapes
:
�
;optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/ReshapeReshapeHoptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependency_19optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/Shape*
T0*
Tshape0*0
_output_shapes
:������������������
�
optimize/gradients/AddN_2AddNIoptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependency<optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/Reshape*
N* 
_output_shapes
:
��*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul
�
9optimize/gradients/embeddings/embedding_lookup_grad/ShapeConst*
_class
loc:@embeddings_1*%
valueB	"'      �       *
dtype0	*
_output_shapes
:
�
;optimize/gradients/embeddings/embedding_lookup_grad/ToInt32Cast9optimize/gradients/embeddings/embedding_lookup_grad/Shape*
_output_shapes
:*

DstT0*

SrcT0	*
_class
loc:@embeddings_1
{
8optimize/gradients/embeddings/embedding_lookup_grad/SizeConst*
value
B :�*
dtype0*
_output_shapes
: 
�
Boptimize/gradients/embeddings/embedding_lookup_grad/ExpandDims/dimConst*
value	B : *
dtype0*
_output_shapes
: 
�
>optimize/gradients/embeddings/embedding_lookup_grad/ExpandDims
ExpandDims8optimize/gradients/embeddings/embedding_lookup_grad/SizeBoptimize/gradients/embeddings/embedding_lookup_grad/ExpandDims/dim*

Tdim0*
T0*
_output_shapes
:
�
Goptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stackConst*
valueB:*
dtype0*
_output_shapes
:
�
Ioptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_1Const*
valueB: *
dtype0*
_output_shapes
:
�
Ioptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
�
Aoptimize/gradients/embeddings/embedding_lookup_grad/strided_sliceStridedSlice;optimize/gradients/embeddings/embedding_lookup_grad/ToInt32Goptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stackIoptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_1Ioptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_2*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask*
_output_shapes
:*
T0*
Index0*
shrink_axis_mask 
�
?optimize/gradients/embeddings/embedding_lookup_grad/concat/axisConst*
_output_shapes
: *
value	B : *
dtype0
�
:optimize/gradients/embeddings/embedding_lookup_grad/concatConcatV2>optimize/gradients/embeddings/embedding_lookup_grad/ExpandDimsAoptimize/gradients/embeddings/embedding_lookup_grad/strided_slice?optimize/gradients/embeddings/embedding_lookup_grad/concat/axis*

Tidx0*
T0*
N*
_output_shapes
:
�
;optimize/gradients/embeddings/embedding_lookup_grad/ReshapeReshapeoptimize/gradients/AddN_2:optimize/gradients/embeddings/embedding_lookup_grad/concat*
T0*
Tshape0* 
_output_shapes
:
��
�
=optimize/gradients/embeddings/embedding_lookup_grad/Reshape_1Reshapedata/IteratorGetNext>optimize/gradients/embeddings/embedding_lookup_grad/ExpandDims*
T0*
Tshape0*
_output_shapes	
:�
v
4optimize/gradients/nce_loss/nce_loss/Slice_grad/RankConst*
dtype0*
_output_shapes
: *
value	B :
�
5optimize/gradients/nce_loss/nce_loss/Slice_grad/ShapeShapence_loss/nce_loss/Slice*
T0*
out_type0*
_output_shapes
:
y
7optimize/gradients/nce_loss/nce_loss/Slice_grad/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
�
5optimize/gradients/nce_loss/nce_loss/Slice_grad/stackPack4optimize/gradients/nce_loss/nce_loss/Slice_grad/Rank7optimize/gradients/nce_loss/nce_loss/Slice_grad/stack/1*
T0*

axis *
N*
_output_shapes
:
�
7optimize/gradients/nce_loss/nce_loss/Slice_grad/ReshapeReshapence_loss/nce_loss/Slice/begin5optimize/gradients/nce_loss/nce_loss/Slice_grad/stack*
T0*
Tshape0*
_output_shapes

:
�
7optimize/gradients/nce_loss/nce_loss/Slice_grad/Shape_1Const*
_output_shapes
:*
valueB"�   �   *
dtype0
�
3optimize/gradients/nce_loss/nce_loss/Slice_grad/subSub7optimize/gradients/nce_loss/nce_loss/Slice_grad/Shape_15optimize/gradients/nce_loss/nce_loss/Slice_grad/Shape*
T0*
_output_shapes
:
�
5optimize/gradients/nce_loss/nce_loss/Slice_grad/sub_1Sub3optimize/gradients/nce_loss/nce_loss/Slice_grad/subnce_loss/nce_loss/Slice/begin*
T0*
_output_shapes
:
�
9optimize/gradients/nce_loss/nce_loss/Slice_grad/Reshape_1Reshape5optimize/gradients/nce_loss/nce_loss/Slice_grad/sub_15optimize/gradients/nce_loss/nce_loss/Slice_grad/stack*
T0*
Tshape0*
_output_shapes

:
}
;optimize/gradients/nce_loss/nce_loss/Slice_grad/concat/axisConst*
_output_shapes
: *
value	B :*
dtype0
�
6optimize/gradients/nce_loss/nce_loss/Slice_grad/concatConcatV27optimize/gradients/nce_loss/nce_loss/Slice_grad/Reshape9optimize/gradients/nce_loss/nce_loss/Slice_grad/Reshape_1;optimize/gradients/nce_loss/nce_loss/Slice_grad/concat/axis*
N*
_output_shapes

:*

Tidx0*
T0
�
3optimize/gradients/nce_loss/nce_loss/Slice_grad/PadPad;optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/Reshape6optimize/gradients/nce_loss/nce_loss/Slice_grad/concat*
T0*
	Tpaddings0* 
_output_shapes
:
��
�
optimize/gradients/AddN_3AddN5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Pad3optimize/gradients/nce_loss/nce_loss/Slice_grad/Pad*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Pad*
N* 
_output_shapes
:
��*
T0
�
@optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ShapeConst*
dtype0	*
_output_shapes
:*
_class
loc:@nce_weights*%
valueB	"'      �       
�
Boptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ToInt32Cast@optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Shape*

SrcT0	*
_class
loc:@nce_weights*
_output_shapes
:*

DstT0
�
?optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/SizeConst*
_output_shapes
: *
value
B :�*
dtype0
�
Ioptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims/dimConst*
value	B : *
dtype0*
_output_shapes
: 
�
Eoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims
ExpandDims?optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/SizeIoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims/dim*
_output_shapes
:*

Tdim0*
T0
�
Noptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stackConst*
_output_shapes
:*
valueB:*
dtype0
�
Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_1Const*
valueB: *
dtype0*
_output_shapes
:
�
Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
�
Hoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_sliceStridedSliceBoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ToInt32Noptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stackPoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_1Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_2*
_output_shapes
:*
Index0*
T0*
shrink_axis_mask *
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask
�
Foptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
�
Aoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concatConcatV2Eoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDimsHoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_sliceFoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concat/axis*
T0*
N*
_output_shapes
:*

Tidx0
�
Boptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ReshapeReshapeoptimize/gradients/AddN_3Aoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concat*
T0*
Tshape0* 
_output_shapes
:
��
�
Doptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Reshape_1Reshapence_loss/nce_loss/concatEoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims*
T0	*
Tshape0*
_output_shapes	
:�
k
&optimize/GradientDescent/learning_rateConst*
valueB
 *���=*
dtype0*
_output_shapes
: 
�
/optimize/GradientDescent/update_nce_weights/mulMulBoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Reshape&optimize/GradientDescent/learning_rate* 
_output_shapes
:
��*
T0*
_class
loc:@nce_weights
�
6optimize/GradientDescent/update_nce_weights/ScatterSub
ScatterSubnce_weightsDoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Reshape_1/optimize/GradientDescent/update_nce_weights/mul*
use_locking( *
Tindices0	*
T0*
_class
loc:@nce_weights* 
_output_shapes
:
�N�
�
,optimize/GradientDescent/update_nce_bias/mulMulDoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Reshape&optimize/GradientDescent/learning_rate*
_output_shapes	
:�*
T0*
_class
loc:@nce_bias
�
3optimize/GradientDescent/update_nce_bias/ScatterSub
ScatterSubnce_biasFoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Reshape_1,optimize/GradientDescent/update_nce_bias/mul*
_output_shapes	
:�N*
use_locking( *
Tindices0	*
T0*
_class
loc:@nce_bias
�
0optimize/GradientDescent/update_embeddings_1/mulMul;optimize/gradients/embeddings/embedding_lookup_grad/Reshape&optimize/GradientDescent/learning_rate*
T0*
_class
loc:@embeddings_1* 
_output_shapes
:
��
�
7optimize/GradientDescent/update_embeddings_1/ScatterSub
ScatterSubembeddings_1=optimize/gradients/embeddings/embedding_lookup_grad/Reshape_10optimize/GradientDescent/update_embeddings_1/mul*
_class
loc:@embeddings_1* 
_output_shapes
:
�N�*
use_locking( *
Tindices0*
T0
�
optimize/GradientDescent/updateNoOp8^optimize/GradientDescent/update_embeddings_1/ScatterSub4^optimize/GradientDescent/update_nce_bias/ScatterSub7^optimize/GradientDescent/update_nce_weights/ScatterSub
�
optimize/GradientDescent/valueConst ^optimize/GradientDescent/update*
_class
loc:@global_step*
value	B :*
dtype0*
_output_shapes
: 
�
optimize/GradientDescent	AssignAddglobal_stepoptimize/GradientDescent/value*
use_locking( *
T0*
_class
loc:@global_step*
_output_shapes
: 
b
summaries/loss/tagsConst*
dtype0*
_output_shapes
: *
valueB Bsummaries/loss
j
summaries/lossScalarSummarysummaries/loss/tagsnce_loss/nce_loss_1*
_output_shapes
: *
T0
u
summaries/histogram_loss/tagConst*)
value B Bsummaries/histogram_loss*
dtype0*
_output_shapes
: 
�
summaries/histogram_lossHistogramSummarysummaries/histogram_loss/tagnce_loss/nce_loss_1*
_output_shapes
: *
T0
w
summaries/Merge/MergeSummaryMergeSummarysummaries/losssummaries/histogram_loss*
_output_shapes
: *
N
P

save/ConstConst*
valueB Bmodel*
dtype0*
_output_shapes
: 
�
save/SaveV2/tensor_namesConst*
dtype0*
_output_shapes
:*E
value<B:Bembeddings_1Bglobal_stepBnce_biasBnce_weights
k
save/SaveV2/shape_and_slicesConst*
valueBB B B B *
dtype0*
_output_shapes
:
�
save/SaveV2SaveV2
save/Constsave/SaveV2/tensor_namessave/SaveV2/shape_and_slicesembeddings_1global_stepnce_biasnce_weights*
dtypes
2
}
save/control_dependencyIdentity
save/Const^save/SaveV2*
T0*
_class
loc:@save/Const*
_output_shapes
: 
�
save/RestoreV2/tensor_namesConst"/device:CPU:0*E
value<B:Bembeddings_1Bglobal_stepBnce_biasBnce_weights*
dtype0*
_output_shapes
:
}
save/RestoreV2/shape_and_slicesConst"/device:CPU:0*
dtype0*
_output_shapes
:*
valueBB B B B 
�
save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices"/device:CPU:0*
dtypes
2*$
_output_shapes
::::
�
save/AssignAssignembeddings_1save/RestoreV2*
use_locking(*
T0*
_class
loc:@embeddings_1*
validate_shape(* 
_output_shapes
:
�N�
�
save/Assign_1Assignglobal_stepsave/RestoreV2:1*
_output_shapes
: *
use_locking(*
T0*
_class
loc:@global_step*
validate_shape(
�
save/Assign_2Assignnce_biassave/RestoreV2:2*
validate_shape(*
_output_shapes	
:�N*
use_locking(*
T0*
_class
loc:@nce_bias
�
save/Assign_3Assignnce_weightssave/RestoreV2:3*
use_locking(*
T0*
_class
loc:@nce_weights*
validate_shape(* 
_output_shapes
:
�N�
V
save/restore_allNoOp^save/Assign^save/Assign_1^save/Assign_2^save/Assign_3
^
initNoOp^embeddings_1/Assign^global_step/Assign^nce_bias/Assign^nce_weights/Assign�
�
�
tf_init_func_O9ko4JKUVjc
arg0
pyfunc_placeholder
pyfunc_placeholder_1
pyfunc_placeholder_2
pyfunc_placeholder_3
pyfunc_placeholder_4

pyfunc	25A wrapper for Defun that facilitates shape inference.��
PyFuncPyFuncpyfunc_placeholderpyfunc_placeholder_1pyfunc_placeholder_2pyfunc_placeholder_3pyfunc_placeholder_4*
Tin	
2*
Tout
2	*
token
pyfunc_0"
pyfuncPyFunc:output:0
�
�
tf_map_func_8pkeaS83Jbc
arg0 
generatordataset_placeholder"
generatordataset_placeholder_1
generatordataset25A wrapper for Defun that facilitates shape inference.��
GeneratorDatasetGeneratorDatasetarg0generatordataset_placeholdergeneratordataset_placeholder_1*)
	next_funcR
tf_next_func_I9alSJeqyO0*
Tnext_func_args
 *
Tfinalize_func_args
 *1
output_shapes 
:���������:���������*1
finalize_func R
tf_finalize_func_371o9XzBGzc*)
	init_funcR
tf_init_func_ab9wLUDUcC8*
output_types
2*
Tinit_func_args
2"-
generatordatasetGeneratorDataset:handle:0
�
x
tf_next_func_I9alSJeqyO0
arg0	

pyfunc
pyfunc_025A wrapper for Defun that facilitates shape inference.�I
PyFuncPyFuncarg0*
Tin
2	*
Tout
2*
token
pyfunc_4"
pyfuncPyFunc:output:0"
pyfunc_0PyFunc:output:1
�
�
tf_map_func_PCOijuOn9mg
arg0 
generatordataset_placeholder"
generatordataset_placeholder_1"
generatordataset_placeholder_2"
generatordataset_placeholder_3"
generatordataset_placeholder_4
generatordataset25A wrapper for Defun that facilitates shape inference.��
GeneratorDatasetGeneratorDatasetarg0generatordataset_placeholdergeneratordataset_placeholder_1generatordataset_placeholder_2generatordataset_placeholder_3generatordataset_placeholder_4*)
	next_funcR
tf_next_func_udg8Vh0kgto*
Tnext_func_args
 *
Tfinalize_func_args
 *%
output_shapes
:�:	�*1
finalize_func R
tf_finalize_func_Lpot2pAvNXE*)
	init_funcR
tf_init_func_O9ko4JKUVjc*
output_types
2*
Tinit_func_args

2"-
generatordatasetGeneratorDataset:handle:0
�
n
tf_finalize_func_Lpot2pAvNXE
arg0	

pyfunc	25A wrapper for Defun that facilitates shape inference.�H
PyFuncPyFuncarg0*
Tin
2	*
Tout
2	*
token
pyfunc_2"
pyfuncPyFunc:output:0
�
n
tf_finalize_func_371o9XzBGzc
arg0	

pyfunc	25A wrapper for Defun that facilitates shape inference.�H
PyFuncPyFuncarg0*
Tin
2	*
Tout
2	*
token
pyfunc_5"
pyfuncPyFunc:output:0
�
x
tf_next_func_udg8Vh0kgto
arg0	

pyfunc
pyfunc_025A wrapper for Defun that facilitates shape inference.�I
PyFuncPyFuncarg0*
Tin
2	*
Tout
2*
token
pyfunc_1"
pyfunc_0PyFunc:output:1"
pyfuncPyFunc:output:0
�
�
tf_init_func_ab9wLUDUcC8
arg0
pyfunc_placeholder
pyfunc_placeholder_1

pyfunc	25A wrapper for Defun that facilitates shape inference.�m
PyFuncPyFuncpyfunc_placeholderpyfunc_placeholder_1*
Tin
2*
Tout
2	*
token
pyfunc_3"
pyfuncPyFunc:output:0"3��W     ��F|	�����AJ��
�'�&
:
Add
x"T
y"T
z"T"
Ttype:
2	
W
AddN
inputs"T*N
sum"T"
Nint(0"!
Ttype:
2	��
x
Assign
ref"T�

value"T

output_ref"T�"	
Ttype"
validate_shapebool("
use_lockingbool(�
s
	AssignAdd
ref"T�

value"T

output_ref"T�" 
Ttype:
2	"
use_lockingbool( 
R
BroadcastGradientArgs
s0"T
s1"T
r0"T
r1"T"
Ttype0:
2	
8
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype
I
ConcatOffset

concat_dim
shape*N
offset*N"
Nint(0
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
,
Exp
x"T
y"T"
Ttype:

2
W

ExpandDims

input"T
dim"Tdim
output"T"	
Ttype"
Tdimtype0:
2	
^
Fill
dims"
index_type

value"T
output"T"	
Ttype"

index_typetype0:
2	
�
FlatMapDataset
input_dataset
other_arguments2
Targuments

handle"	
ffunc"

Targuments
list(type)("
output_types
list(type)(0" 
output_shapeslist(shape)(0
9
FloorMod
x"T
y"T
z"T"
Ttype:

2	
�
GatherV2
params"Tparams
indices"Tindices
axis"Taxis
output"Tparams"
Tparamstype"
Tindicestype:
2	"
Taxistype:
2	
B
GreaterEqual
x"T
y"T
z
"
Ttype:
2	
V
HistogramSummary
tag
values"T
summary"
Ttype0:
2	
.
Identity

input"T
output"T"	
Ttype
�
Iterator

handle"
shared_namestring"
	containerstring"
output_types
list(type)(0" 
output_shapeslist(shape)(0�
�
IteratorGetNext
iterator

components2output_types"
output_types
list(type)(0" 
output_shapeslist(shape)(0�
C
IteratorToStringHandle
resource_handle
string_handle�
,
Log
x"T
y"T"
Ttype:

2
.
Log1p
x"T
y"T"
Ttype:

2
�
LogUniformCandidateSampler
true_classes	
sampled_candidates	
true_expected_count
sampled_expected_count"
num_trueint(0"
num_sampledint(0"
uniquebool"
	range_maxint(0"
seedint "
seed2int �
,
MakeIterator
dataset
iterator�
p
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:
	2
�
Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
8
MergeSummary
inputs*N
summary"
Nint(0
=
Mul
x"T
y"T
z"T"
Ttype:
2	�
.
Neg
x"T
y"T"
Ttype:

2	

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
_
Pad

input"T
paddings"	Tpaddings
output"T"	
Ttype"
	Tpaddingstype0:
2	
~
RandomUniform

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	�
>
RealDiv
x"T
y"T
z"T"
Ttype:
2	
5

Reciprocal
x"T
y"T"
Ttype:

2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
P
ScalarSummary
tags
values"T
summary"
Ttype:
2	
�

ScatterSub
ref"T�
indices"Tindices
updates"T

output_ref"T�" 
Ttype:
2	"
Tindicestype:
2	"
use_lockingbool( 
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
a
Slice

input"T
begin"Index
size"Index
output"T"	
Ttype"
Indextype:
2	
2
StopGradient

input"T
output"T"	
Ttype
�
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
:
Sub
x"T
y"T
z"T"
Ttype:
2	
�
Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
~
TensorDataset

components2Toutput_types

handle"
Toutput_types
list(type)(0" 
output_shapeslist(shape)(0�
c
Tile

input"T
	multiples"
Tmultiples
output"T"	
Ttype"

Tmultiplestype0:
2	
�
TruncatedNormal

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	�
s

VariableV2
ref"dtype�"
shapeshape"
dtypetype"
	containerstring "
shared_namestring �*1.9.02
b'unknown'��
I
args_0Const*
_output_shapes
: *
value
B :�N*
dtype0
I
args_1Const*
value
B :�*
dtype0*
_output_shapes
: 
H
args_2Const*
dtype0*
_output_shapes
: *
value	B :
T
args_3Const*
valueB Bvisualization*
dtype0*
_output_shapes
: 
Q
args_4Const*
valueB B
Experience*
dtype0*
_output_shapes
: 
U
tensors/component_0Const*
_output_shapes
: *
value	B : *
dtype0
L

batch_sizeConst*
value	B	 R*
dtype0	*
_output_shapes
: 
S
args_0_1Const*
valueB B
Experience*
dtype0*
_output_shapes
: 
J
args_1_1Const*
value	B :*
dtype0*
_output_shapes
: 
W
tensors_1/component_0Const*
value	B : *
dtype0*
_output_shapes
: 
G
ConstConst*
value	B : *
dtype0*
_output_shapes
: 
o
global_step
VariableV2*
dtype0*
_output_shapes
: *
	container *
shape: *
shared_name 
�
global_step/AssignAssignglobal_stepConst*
T0*
_class
loc:@global_step*
validate_shape(*
_output_shapes
: *
use_locking(
j
global_step/readIdentityglobal_step*
_output_shapes
: *
T0*
_class
loc:@global_step
�
data/IteratorIterator*
_output_shapes
: *
	container *
output_types
2*
shared_name *%
output_shapes
:�:	�
�
data/TensorDatasetTensorDatasettensors/component_0*
output_shapes
: * 
_class
loc:@data/Iterator*
Toutput_types
2*
_output_shapes
: 
�
data/FlatMapDatasetFlatMapDatasetdata/TensorDatasetargs_0args_1args_2args_3args_4*%
output_shapes
:�:	�* 
_class
loc:@data/Iterator* 
fR
tf_map_func_PCOijuOn9mg*
output_types
2*

Targuments	
2*
_output_shapes
: 
g
data/MakeIteratorMakeIteratordata/FlatMapDatasetdata/Iterator* 
_class
loc:@data/Iterator
\
data/IteratorToStringHandleIteratorToStringHandledata/Iterator*
_output_shapes
: 
�
data/Iterator_1Iterator*
shared_name *1
output_shapes 
:���������:���������*
_output_shapes
: *
	container *
output_types
2
�
data/TensorDataset_1TensorDatasettensors_1/component_0*
_output_shapes
: *
output_shapes
: *"
_class
loc:@data/Iterator_1*
Toutput_types
2
�
data/FlatMapDataset_1FlatMapDatasetdata/TensorDataset_1args_0_1args_1_1*1
output_shapes 
:���������:���������*"
_class
loc:@data/Iterator_1* 
fR
tf_map_func_8pkeaS83Jbc*
output_types
2*

Targuments
2*
_output_shapes
: 
o
data/MakeIterator_1MakeIteratordata/FlatMapDataset_1data/Iterator_1*"
_class
loc:@data/Iterator_1
`
data/IteratorToStringHandle_1IteratorToStringHandledata/Iterator_1*
_output_shapes
: 
�
data/IteratorGetNextIteratorGetNextdata/Iterator*%
output_shapes
:�:	�*&
_output_shapes
:�:	�*
output_types
2
�
data/IteratorGetNext_1IteratorGetNextdata/Iterator_1*2
_output_shapes 
:���������:���������*
output_types
2*1
output_shapes 
:���������:���������
�
.nce_weights/Initializer/truncated_normal/shapeConst*
_class
loc:@nce_weights*
valueB"'  �   *
dtype0*
_output_shapes
:
�
-nce_weights/Initializer/truncated_normal/meanConst*
_output_shapes
: *
_class
loc:@nce_weights*
valueB
 *    *
dtype0
�
/nce_weights/Initializer/truncated_normal/stddevConst*
_class
loc:@nce_weights*
valueB
 *�А=*
dtype0*
_output_shapes
: 
�
8nce_weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormal.nce_weights/Initializer/truncated_normal/shape*
T0*
_class
loc:@nce_weights*
seed2 *
dtype0* 
_output_shapes
:
�N�*

seed 
�
,nce_weights/Initializer/truncated_normal/mulMul8nce_weights/Initializer/truncated_normal/TruncatedNormal/nce_weights/Initializer/truncated_normal/stddev*
T0*
_class
loc:@nce_weights* 
_output_shapes
:
�N�
�
(nce_weights/Initializer/truncated_normalAdd,nce_weights/Initializer/truncated_normal/mul-nce_weights/Initializer/truncated_normal/mean*
T0*
_class
loc:@nce_weights* 
_output_shapes
:
�N�
�
nce_weights
VariableV2*
shared_name *
_class
loc:@nce_weights*
	container *
shape:
�N�*
dtype0* 
_output_shapes
:
�N�
�
nce_weights/AssignAssignnce_weights(nce_weights/Initializer/truncated_normal*
use_locking(*
T0*
_class
loc:@nce_weights*
validate_shape(* 
_output_shapes
:
�N�
t
nce_weights/readIdentitynce_weights*
T0*
_class
loc:@nce_weights* 
_output_shapes
:
�N�
h
weights/zeros/shape_as_tensorConst*
valueB:�N*
dtype0*
_output_shapes
:
X
weights/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    
�
weights/zerosFillweights/zeros/shape_as_tensorweights/zeros/Const*
_output_shapes	
:�N*
T0*

index_type0
v
nce_bias
VariableV2*
shared_name *
dtype0*
_output_shapes	
:�N*
	container *
shape:�N
�
nce_bias/AssignAssignnce_biasweights/zeros*
use_locking(*
T0*
_class
loc:@nce_bias*
validate_shape(*
_output_shapes	
:�N
f
nce_bias/readIdentitynce_bias*
_output_shapes	
:�N*
T0*
_class
loc:@nce_bias
�
-embeddings_1/Initializer/random_uniform/shapeConst*
_class
loc:@embeddings_1*
valueB"'  �   *
dtype0*
_output_shapes
:
�
+embeddings_1/Initializer/random_uniform/minConst*
_class
loc:@embeddings_1*
valueB
 *    *
dtype0*
_output_shapes
: 
�
+embeddings_1/Initializer/random_uniform/maxConst*
_class
loc:@embeddings_1*
valueB
 *  �?*
dtype0*
_output_shapes
: 
�
5embeddings_1/Initializer/random_uniform/RandomUniformRandomUniform-embeddings_1/Initializer/random_uniform/shape*
dtype0* 
_output_shapes
:
�N�*

seed *
T0*
_class
loc:@embeddings_1*
seed2 
�
+embeddings_1/Initializer/random_uniform/subSub+embeddings_1/Initializer/random_uniform/max+embeddings_1/Initializer/random_uniform/min*
T0*
_class
loc:@embeddings_1*
_output_shapes
: 
�
+embeddings_1/Initializer/random_uniform/mulMul5embeddings_1/Initializer/random_uniform/RandomUniform+embeddings_1/Initializer/random_uniform/sub*
T0*
_class
loc:@embeddings_1* 
_output_shapes
:
�N�
�
'embeddings_1/Initializer/random_uniformAdd+embeddings_1/Initializer/random_uniform/mul+embeddings_1/Initializer/random_uniform/min*
_class
loc:@embeddings_1* 
_output_shapes
:
�N�*
T0
�
embeddings_1
VariableV2* 
_output_shapes
:
�N�*
shared_name *
_class
loc:@embeddings_1*
	container *
shape:
�N�*
dtype0
�
embeddings_1/AssignAssignembeddings_1'embeddings_1/Initializer/random_uniform*
use_locking(*
T0*
_class
loc:@embeddings_1*
validate_shape(* 
_output_shapes
:
�N�
w
embeddings_1/readIdentityembeddings_1*
T0*
_class
loc:@embeddings_1* 
_output_shapes
:
�N�
�
 embeddings/embedding_lookup/axisConst*
_class
loc:@embeddings_1*
value	B : *
dtype0*
_output_shapes
: 
�
embeddings/embedding_lookupGatherV2embeddings_1/readdata/IteratorGetNext embeddings/embedding_lookup/axis*
_class
loc:@embeddings_1* 
_output_shapes
:
��*
Taxis0*
Tindices0*
Tparams0
~
filling/filling_lookup/axisConst*
_output_shapes
: *
_class
loc:@embeddings_1*
value	B : *
dtype0
�
filling/filling_lookupGatherV2embeddings_1/readdata/IteratorGetNext_1:1filling/filling_lookup/axis*
Taxis0*
Tindices0*
Tparams0*
_class
loc:@embeddings_1*(
_output_shapes
:����������
`
filling/Mean/reduction_indicesConst*
value	B : *
dtype0*
_output_shapes
: 
�
filling/MeanMeanfilling/filling_lookupfilling/Mean/reduction_indices*
T0*
_output_shapes	
:�*
	keep_dims( *

Tidx0
o
nce_loss/nce_loss/CastCastdata/IteratorGetNext:1*
_output_shapes
:	�*

DstT0	*

SrcT0
r
nce_loss/nce_loss/Reshape/shapeConst*
_output_shapes
:*
valueB:
���������*
dtype0
�
nce_loss/nce_loss/ReshapeReshapence_loss/nce_loss/Castnce_loss/nce_loss/Reshape/shape*
_output_shapes	
:�*
T0	*
Tshape0
�
,nce_loss/nce_loss/LogUniformCandidateSamplerLogUniformCandidateSamplernce_loss/nce_loss/Cast*
	range_max�N*
num_sampled@*+
_output_shapes
:@:	�:@*

seed *
unique(*
seed2 *
num_true
�
nce_loss/nce_loss/StopGradientStopGradient,nce_loss/nce_loss/LogUniformCandidateSampler*
T0	*
_output_shapes
:@
�
 nce_loss/nce_loss/StopGradient_1StopGradient.nce_loss/nce_loss/LogUniformCandidateSampler:1*
T0*
_output_shapes
:	�
�
 nce_loss/nce_loss/StopGradient_2StopGradient.nce_loss/nce_loss/LogUniformCandidateSampler:2*
T0*
_output_shapes
:@
_
nce_loss/nce_loss/concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/concatConcatV2nce_loss/nce_loss/Reshapence_loss/nce_loss/StopGradientnce_loss/nce_loss/concat/axis*
N*
_output_shapes	
:�*

Tidx0*
T0	
�
'nce_loss/nce_loss/embedding_lookup/axisConst*
_class
loc:@nce_weights*
value	B : *
dtype0*
_output_shapes
: 
�
"nce_loss/nce_loss/embedding_lookupGatherV2nce_weights/readnce_loss/nce_loss/concat'nce_loss/nce_loss/embedding_lookup/axis* 
_output_shapes
:
��*
Taxis0*
Tindices0	*
Tparams0*
_class
loc:@nce_weights
b
nce_loss/nce_loss/ShapeConst*
valueB:�*
dtype0*
_output_shapes
:
o
%nce_loss/nce_loss/strided_slice/stackConst*
valueB: *
dtype0*
_output_shapes
:
q
'nce_loss/nce_loss/strided_slice/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
q
'nce_loss/nce_loss/strided_slice/stack_2Const*
_output_shapes
:*
valueB:*
dtype0
�
nce_loss/nce_loss/strided_sliceStridedSlicence_loss/nce_loss/Shape%nce_loss/nce_loss/strided_slice/stack'nce_loss/nce_loss/strided_slice/stack_1'nce_loss/nce_loss/strided_slice/stack_2*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
Index0*
T0*
shrink_axis_mask
d
nce_loss/nce_loss/stack/1Const*
valueB :
���������*
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/stackPacknce_loss/nce_loss/strided_slicence_loss/nce_loss/stack/1*
T0*

axis *
N*
_output_shapes
:
n
nce_loss/nce_loss/Slice/beginConst*
valueB"        *
dtype0*
_output_shapes
:
�
nce_loss/nce_loss/SliceSlice"nce_loss/nce_loss/embedding_lookupnce_loss/nce_loss/Slice/beginnce_loss/nce_loss/stack*0
_output_shapes
:������������������*
Index0*
T0
d
nce_loss/nce_loss/Shape_1Const*
valueB:�*
dtype0*
_output_shapes
:
q
'nce_loss/nce_loss/strided_slice_1/stackConst*
valueB: *
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_1/stack_1Const*
_output_shapes
:*
valueB:*
dtype0
s
)nce_loss/nce_loss/strided_slice_1/stack_2Const*
dtype0*
_output_shapes
:*
valueB:
�
!nce_loss/nce_loss/strided_slice_1StridedSlicence_loss/nce_loss/Shape_1'nce_loss/nce_loss/strided_slice_1/stack)nce_loss/nce_loss/strided_slice_1/stack_1)nce_loss/nce_loss/strided_slice_1/stack_2*
shrink_axis_mask*
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
Index0*
T0
]
nce_loss/nce_loss/stack_1/1Const*
value	B : *
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/stack_1Pack!nce_loss/nce_loss/strided_slice_1nce_loss/nce_loss/stack_1/1*
_output_shapes
:*
T0*

axis *
N
o
nce_loss/nce_loss/Slice_1/sizeConst*
valueB"��������*
dtype0*
_output_shapes
:
�
nce_loss/nce_loss/Slice_1Slice"nce_loss/nce_loss/embedding_lookupnce_loss/nce_loss/stack_1nce_loss/nce_loss/Slice_1/size*(
_output_shapes
:����������*
Index0*
T0
�
nce_loss/nce_loss/MatMulMatMulembeddings/embedding_lookupnce_loss/nce_loss/Slice_1*
T0*(
_output_shapes
:����������*
transpose_a( *
transpose_b(
�
)nce_loss/nce_loss/embedding_lookup_1/axisConst*
_output_shapes
: *
_class
loc:@nce_bias*
value	B : *
dtype0
�
$nce_loss/nce_loss/embedding_lookup_1GatherV2nce_bias/readnce_loss/nce_loss/concat)nce_loss/nce_loss/embedding_lookup_1/axis*
_output_shapes	
:�*
Taxis0*
Tindices0	*
Tparams0*
_class
loc:@nce_bias
d
nce_loss/nce_loss/Shape_2Const*
_output_shapes
:*
valueB:�*
dtype0
i
nce_loss/nce_loss/Slice_2/beginConst*
dtype0*
_output_shapes
:*
valueB: 
�
nce_loss/nce_loss/Slice_2Slice$nce_loss/nce_loss/embedding_lookup_1nce_loss/nce_loss/Slice_2/beginnce_loss/nce_loss/Shape_2*
Index0*
T0*
_output_shapes	
:�
d
nce_loss/nce_loss/Shape_3Const*
valueB:�*
dtype0*
_output_shapes
:
q
nce_loss/nce_loss/Slice_3/sizeConst*
valueB:
���������*
dtype0*
_output_shapes
:
�
nce_loss/nce_loss/Slice_3Slice$nce_loss/nce_loss/embedding_lookup_1nce_loss/nce_loss/Shape_3nce_loss/nce_loss/Slice_3/size*
Index0*
T0*
_output_shapes
:@
p
nce_loss/nce_loss/Shape_4Shapence_loss/nce_loss/Slice*
out_type0*
_output_shapes
:*
T0
q
'nce_loss/nce_loss/strided_slice_2/stackConst*
_output_shapes
:*
valueB:*
dtype0
s
)nce_loss/nce_loss/strided_slice_2/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_2/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
�
!nce_loss/nce_loss/strided_slice_2StridedSlicence_loss/nce_loss/Shape_4'nce_loss/nce_loss/strided_slice_2/stack)nce_loss/nce_loss/strided_slice_2/stack_1)nce_loss/nce_loss/strided_slice_2/stack_2*
T0*
Index0*
shrink_axis_mask *
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
_output_shapes
:
t
#nce_loss/nce_loss/concat_1/values_0Const*
valueB"����   *
dtype0*
_output_shapes
:
a
nce_loss/nce_loss/concat_1/axisConst*
_output_shapes
: *
value	B : *
dtype0
�
nce_loss/nce_loss/concat_1ConcatV2#nce_loss/nce_loss/concat_1/values_0!nce_loss/nce_loss/strided_slice_2nce_loss/nce_loss/concat_1/axis*

Tidx0*
T0*
N*
_output_shapes
:
b
 nce_loss/nce_loss/ExpandDims/dimConst*
value	B :*
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/ExpandDims
ExpandDimsembeddings/embedding_lookup nce_loss/nce_loss/ExpandDims/dim*

Tdim0*
T0*$
_output_shapes
:��
�
nce_loss/nce_loss/Reshape_1Reshapence_loss/nce_loss/Slicence_loss/nce_loss/concat_1*
T0*
Tshape0*4
_output_shapes"
 :������������������
�
nce_loss/nce_loss/MulMulnce_loss/nce_loss/ExpandDimsnce_loss/nce_loss/Reshape_1*
T0*$
_output_shapes
:��
v
#nce_loss/nce_loss/concat_2/values_0Const*
dtype0*
_output_shapes
:*
valueB:
���������
a
nce_loss/nce_loss/concat_2/axisConst*
value	B : *
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/concat_2ConcatV2#nce_loss/nce_loss/concat_2/values_0!nce_loss/nce_loss/strided_slice_2nce_loss/nce_loss/concat_2/axis*
N*
_output_shapes
:*

Tidx0*
T0
�
nce_loss/nce_loss/Reshape_2Reshapence_loss/nce_loss/Mulnce_loss/nce_loss/concat_2*
T0*
Tshape0*0
_output_shapes
:������������������
t
nce_loss/nce_loss/Shape_5Shapence_loss/nce_loss/Reshape_2*
_output_shapes
:*
T0*
out_type0
q
'nce_loss/nce_loss/strided_slice_3/stackConst*
valueB:*
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_3/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_3/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
�
!nce_loss/nce_loss/strided_slice_3StridedSlicence_loss/nce_loss/Shape_5'nce_loss/nce_loss/strided_slice_3/stack)nce_loss/nce_loss/strided_slice_3/stack_1)nce_loss/nce_loss/strided_slice_3/stack_2*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
Index0*
T0
]
nce_loss/nce_loss/stack_2/1Const*
value	B :*
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/stack_2Pack!nce_loss/nce_loss/strided_slice_3nce_loss/nce_loss/stack_2/1*
T0*

axis *
N*
_output_shapes
:
a
nce_loss/nce_loss/ones/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *  �?
�
nce_loss/nce_loss/onesFillnce_loss/nce_loss/stack_2nce_loss/nce_loss/ones/Const*
T0*

index_type0*'
_output_shapes
:���������
�
nce_loss/nce_loss/MatMul_1MatMulnce_loss/nce_loss/Reshape_2nce_loss/nce_loss/ones*
T0*'
_output_shapes
:���������*
transpose_a( *
transpose_b( 
t
!nce_loss/nce_loss/Reshape_3/shapeConst*
valueB:
���������*
dtype0*
_output_shapes
:
�
nce_loss/nce_loss/Reshape_3Reshapence_loss/nce_loss/MatMul_1!nce_loss/nce_loss/Reshape_3/shape*
T0*
Tshape0*#
_output_shapes
:���������
r
!nce_loss/nce_loss/Reshape_4/shapeConst*
valueB"����   *
dtype0*
_output_shapes
:
�
nce_loss/nce_loss/Reshape_4Reshapence_loss/nce_loss/Reshape_3!nce_loss/nce_loss/Reshape_4/shape*
T0*
Tshape0*'
_output_shapes
:���������
r
!nce_loss/nce_loss/Reshape_5/shapeConst*
valueB"����   *
dtype0*
_output_shapes
:
�
nce_loss/nce_loss/Reshape_5Reshapence_loss/nce_loss/Slice_2!nce_loss/nce_loss/Reshape_5/shape*
Tshape0*
_output_shapes
:	�*
T0
�
nce_loss/nce_loss/addAddnce_loss/nce_loss/Reshape_4nce_loss/nce_loss/Reshape_5*
_output_shapes
:	�*
T0
}
nce_loss/nce_loss/add_1Addnce_loss/nce_loss/MatMulnce_loss/nce_loss/Slice_3*
T0*
_output_shapes
:	�@
h
nce_loss/nce_loss/LogLog nce_loss/nce_loss/StopGradient_1*
_output_shapes
:	�*
T0
t
nce_loss/nce_loss/subSubnce_loss/nce_loss/addnce_loss/nce_loss/Log*
_output_shapes
:	�*
T0
e
nce_loss/nce_loss/Log_1Log nce_loss/nce_loss/StopGradient_2*
_output_shapes
:@*
T0
z
nce_loss/nce_loss/sub_1Subnce_loss/nce_loss/add_1nce_loss/nce_loss/Log_1*
T0*
_output_shapes
:	�@
a
nce_loss/nce_loss/concat_3/axisConst*
value	B :*
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/concat_3ConcatV2nce_loss/nce_loss/subnce_loss/nce_loss/sub_1nce_loss/nce_loss/concat_3/axis*

Tidx0*
T0*
N*
_output_shapes
:	�A
r
!nce_loss/nce_loss/ones_like/ShapeConst*
valueB"�      *
dtype0*
_output_shapes
:
f
!nce_loss/nce_loss/ones_like/ConstConst*
valueB
 *  �?*
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/ones_likeFill!nce_loss/nce_loss/ones_like/Shape!nce_loss/nce_loss/ones_like/Const*
T0*

index_type0*
_output_shapes
:	�
`
nce_loss/nce_loss/truediv/yConst*
valueB
 *  �?*
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/truedivRealDivnce_loss/nce_loss/ones_likence_loss/nce_loss/truediv/y*
_output_shapes
:	�*
T0
}
,nce_loss/nce_loss/zeros_like/shape_as_tensorConst*
_output_shapes
:*
valueB"�   @   *
dtype0
g
"nce_loss/nce_loss/zeros_like/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/zeros_likeFill,nce_loss/nce_loss/zeros_like/shape_as_tensor"nce_loss/nce_loss/zeros_like/Const*
T0*

index_type0*
_output_shapes
:	�@
a
nce_loss/nce_loss/concat_4/axisConst*
value	B :*
dtype0*
_output_shapes
: 
�
nce_loss/nce_loss/concat_4ConcatV2nce_loss/nce_loss/truedivnce_loss/nce_loss/zeros_likence_loss/nce_loss/concat_4/axis*
T0*
N*
_output_shapes
:	�A*

Tidx0
�
2nce_loss/sampled_losses/zeros_like/shape_as_tensorConst*
valueB"�   A   *
dtype0*
_output_shapes
:
m
(nce_loss/sampled_losses/zeros_like/ConstConst*
_output_shapes
: *
valueB
 *    *
dtype0
�
"nce_loss/sampled_losses/zeros_likeFill2nce_loss/sampled_losses/zeros_like/shape_as_tensor(nce_loss/sampled_losses/zeros_like/Const*

index_type0*
_output_shapes
:	�A*
T0
�
$nce_loss/sampled_losses/GreaterEqualGreaterEqualnce_loss/nce_loss/concat_3"nce_loss/sampled_losses/zeros_like*
T0*
_output_shapes
:	�A
�
nce_loss/sampled_losses/SelectSelect$nce_loss/sampled_losses/GreaterEqualnce_loss/nce_loss/concat_3"nce_loss/sampled_losses/zeros_like*
_output_shapes
:	�A*
T0
h
nce_loss/sampled_losses/NegNegnce_loss/nce_loss/concat_3*
T0*
_output_shapes
:	�A
�
 nce_loss/sampled_losses/Select_1Select$nce_loss/sampled_losses/GreaterEqualnce_loss/sampled_losses/Negnce_loss/nce_loss/concat_3*
T0*
_output_shapes
:	�A
�
nce_loss/sampled_losses/mulMulnce_loss/nce_loss/concat_3nce_loss/nce_loss/concat_4*
_output_shapes
:	�A*
T0
�
nce_loss/sampled_losses/subSubnce_loss/sampled_losses/Selectnce_loss/sampled_losses/mul*
_output_shapes
:	�A*
T0
n
nce_loss/sampled_losses/ExpExp nce_loss/sampled_losses/Select_1*
T0*
_output_shapes
:	�A
m
nce_loss/sampled_losses/Log1pLog1pnce_loss/sampled_losses/Exp*
_output_shapes
:	�A*
T0
�
nce_loss/sampled_lossesAddnce_loss/sampled_losses/subnce_loss/sampled_losses/Log1p*
T0*
_output_shapes
:	�A
_
nce_loss/ShapeConst*
dtype0*
_output_shapes
:*
valueB"�   A   
f
nce_loss/strided_slice/stackConst*
_output_shapes
:*
valueB:*
dtype0
h
nce_loss/strided_slice/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
h
nce_loss/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
�
nce_loss/strided_sliceStridedSlicence_loss/Shapence_loss/strided_slice/stacknce_loss/strided_slice/stack_1nce_loss/strided_slice/stack_2*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0
R
nce_loss/stack/1Const*
dtype0*
_output_shapes
: *
value	B :
z
nce_loss/stackPacknce_loss/strided_slicence_loss/stack/1*
_output_shapes
:*
T0*

axis *
N
X
nce_loss/ones/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *  �?
~
nce_loss/onesFillnce_loss/stacknce_loss/ones/Const*
T0*

index_type0*'
_output_shapes
:���������
�
nce_loss/MatMulMatMulnce_loss/sampled_lossesnce_loss/ones*
_output_shapes
:	�*
transpose_a( *
transpose_b( *
T0
i
nce_loss/Reshape/shapeConst*
valueB:
���������*
dtype0*
_output_shapes
:
x
nce_loss/ReshapeReshapence_loss/MatMulnce_loss/Reshape/shape*
_output_shapes	
:�*
T0*
Tshape0
X
nce_loss/ConstConst*
valueB: *
dtype0*
_output_shapes
:
{
nce_loss/nce_loss_1Meannce_loss/Reshapence_loss/Const*
	keep_dims( *

Tidx0*
T0*
_output_shapes
: 
[
optimize/gradients/ShapeConst*
dtype0*
_output_shapes
: *
valueB 
a
optimize/gradients/grad_ys_0Const*
_output_shapes
: *
valueB
 *  �?*
dtype0
�
optimize/gradients/FillFilloptimize/gradients/Shapeoptimize/gradients/grad_ys_0*
T0*

index_type0*
_output_shapes
: 
�
9optimize/gradients/nce_loss/nce_loss_1_grad/Reshape/shapeConst*
valueB:*
dtype0*
_output_shapes
:
�
3optimize/gradients/nce_loss/nce_loss_1_grad/ReshapeReshapeoptimize/gradients/Fill9optimize/gradients/nce_loss/nce_loss_1_grad/Reshape/shape*
T0*
Tshape0*
_output_shapes
:
|
1optimize/gradients/nce_loss/nce_loss_1_grad/ConstConst*
valueB:�*
dtype0*
_output_shapes
:
�
0optimize/gradients/nce_loss/nce_loss_1_grad/TileTile3optimize/gradients/nce_loss/nce_loss_1_grad/Reshape1optimize/gradients/nce_loss/nce_loss_1_grad/Const*

Tmultiples0*
T0*
_output_shapes	
:�
x
3optimize/gradients/nce_loss/nce_loss_1_grad/Const_1Const*
valueB
 *   C*
dtype0*
_output_shapes
: 
�
3optimize/gradients/nce_loss/nce_loss_1_grad/truedivRealDiv0optimize/gradients/nce_loss/nce_loss_1_grad/Tile3optimize/gradients/nce_loss/nce_loss_1_grad/Const_1*
T0*
_output_shapes	
:�

.optimize/gradients/nce_loss/Reshape_grad/ShapeConst*
valueB"�      *
dtype0*
_output_shapes
:
�
0optimize/gradients/nce_loss/Reshape_grad/ReshapeReshape3optimize/gradients/nce_loss/nce_loss_1_grad/truediv.optimize/gradients/nce_loss/Reshape_grad/Shape*
_output_shapes
:	�*
T0*
Tshape0
�
.optimize/gradients/nce_loss/MatMul_grad/MatMulMatMul0optimize/gradients/nce_loss/Reshape_grad/Reshapence_loss/ones*
T0*(
_output_shapes
:����������*
transpose_a( *
transpose_b(
�
0optimize/gradients/nce_loss/MatMul_grad/MatMul_1MatMulnce_loss/sampled_losses0optimize/gradients/nce_loss/Reshape_grad/Reshape*
_output_shapes

:A*
transpose_a(*
transpose_b( *
T0
�
8optimize/gradients/nce_loss/MatMul_grad/tuple/group_depsNoOp/^optimize/gradients/nce_loss/MatMul_grad/MatMul1^optimize/gradients/nce_loss/MatMul_grad/MatMul_1
�
@optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependencyIdentity.optimize/gradients/nce_loss/MatMul_grad/MatMul9^optimize/gradients/nce_loss/MatMul_grad/tuple/group_deps*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul*
_output_shapes
:	�A
�
Boptimize/gradients/nce_loss/MatMul_grad/tuple/control_dependency_1Identity0optimize/gradients/nce_loss/MatMul_grad/MatMul_19^optimize/gradients/nce_loss/MatMul_grad/tuple/group_deps*
T0*C
_class9
75loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul_1*
_output_shapes

:A
�
@optimize/gradients/nce_loss/sampled_losses_grad/tuple/group_depsNoOpA^optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependency
�
Hoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependencyIdentity@optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependencyA^optimize/gradients/nce_loss/sampled_losses_grad/tuple/group_deps*
_output_shapes
:	�A*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul
�
Joptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency_1Identity@optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependencyA^optimize/gradients/nce_loss/sampled_losses_grad/tuple/group_deps*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul*
_output_shapes
:	�A
�
7optimize/gradients/nce_loss/sampled_losses/sub_grad/NegNegHoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency*
_output_shapes
:	�A*
T0
�
Doptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/sampled_losses/sub_grad/NegI^optimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency
�
Loptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependencyIdentityHoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependencyE^optimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/group_deps*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul*
_output_shapes
:	�A
�
Noptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency_1Identity7optimize/gradients/nce_loss/sampled_losses/sub_grad/NegE^optimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/sampled_losses/sub_grad/Neg*
_output_shapes
:	�A
�
;optimize/gradients/nce_loss/sampled_losses/Log1p_grad/add/xConstK^optimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency_1*
valueB
 *  �?*
dtype0*
_output_shapes
: 
�
9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/addAdd;optimize/gradients/nce_loss/sampled_losses/Log1p_grad/add/xnce_loss/sampled_losses/Exp*
T0*
_output_shapes
:	�A
�
@optimize/gradients/nce_loss/sampled_losses/Log1p_grad/Reciprocal
Reciprocal9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/add*
T0*
_output_shapes
:	�A
�
9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/mulMulJoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency_1@optimize/gradients/nce_loss/sampled_losses/Log1p_grad/Reciprocal*
T0*
_output_shapes
:	�A
�
Qoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/shape_as_tensorConst*
valueB"�   A   *
dtype0*
_output_shapes
:
�
Goptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
�
Aoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_likeFillQoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/shape_as_tensorGoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/Const*
_output_shapes
:	�A*
T0*

index_type0
�
=optimize/gradients/nce_loss/sampled_losses/Select_grad/SelectSelect$nce_loss/sampled_losses/GreaterEqualLoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependencyAoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like*
T0*
_output_shapes
:	�A
�
?optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1Select$nce_loss/sampled_losses/GreaterEqualAoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_likeLoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency*
T0*
_output_shapes
:	�A
�
Goptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/group_depsNoOp>^optimize/gradients/nce_loss/sampled_losses/Select_grad/Select@^optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1
�
Ooptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/control_dependencyIdentity=optimize/gradients/nce_loss/sampled_losses/Select_grad/SelectH^optimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/group_deps*
T0*P
_classF
DBloc:@optimize/gradients/nce_loss/sampled_losses/Select_grad/Select*
_output_shapes
:	�A
�
Qoptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/control_dependency_1Identity?optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1H^optimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/group_deps*
T0*R
_classH
FDloc:@optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1*
_output_shapes
:	�A
�
7optimize/gradients/nce_loss/sampled_losses/mul_grad/MulMulNoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency_1nce_loss/nce_loss/concat_4*
T0*
_output_shapes
:	�A
�
9optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1MulNoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency_1nce_loss/nce_loss/concat_3*
_output_shapes
:	�A*
T0
�
Doptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul:^optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1
�
Loptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/sampled_losses/mul_grad/MulE^optimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/group_deps*J
_class@
><loc:@optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul*
_output_shapes
:	�A*
T0
�
Noptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1E^optimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1*
_output_shapes
:	�A
�
7optimize/gradients/nce_loss/sampled_losses/Exp_grad/mulMul9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/mulnce_loss/sampled_losses/Exp*
T0*
_output_shapes
:	�A
�
Soptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/shape_as_tensorConst*
valueB"�   A   *
dtype0*
_output_shapes
:
�
Ioptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
�
Coptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_likeFillSoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/shape_as_tensorIoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/Const*
T0*

index_type0*
_output_shapes
:	�A
�
?optimize/gradients/nce_loss/sampled_losses/Select_1_grad/SelectSelect$nce_loss/sampled_losses/GreaterEqual7optimize/gradients/nce_loss/sampled_losses/Exp_grad/mulCoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like*
_output_shapes
:	�A*
T0
�
Aoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1Select$nce_loss/sampled_losses/GreaterEqualCoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like7optimize/gradients/nce_loss/sampled_losses/Exp_grad/mul*
_output_shapes
:	�A*
T0
�
Ioptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/group_depsNoOp@^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/SelectB^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1
�
Qoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependencyIdentity?optimize/gradients/nce_loss/sampled_losses/Select_1_grad/SelectJ^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/group_deps*
T0*R
_classH
FDloc:@optimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select*
_output_shapes
:	�A
�
Soptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependency_1IdentityAoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1J^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@optimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1*
_output_shapes
:	�A
�
7optimize/gradients/nce_loss/sampled_losses/Neg_grad/NegNegQoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependency*
T0*
_output_shapes
:	�A
�
optimize/gradients/AddNAddNOoptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/control_dependencyLoptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/control_dependencySoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependency_17optimize/gradients/nce_loss/sampled_losses/Neg_grad/Neg*
T0*P
_classF
DBloc:@optimize/gradients/nce_loss/sampled_losses/Select_grad/Select*
N*
_output_shapes
:	�A
y
7optimize/gradients/nce_loss/nce_loss/concat_3_grad/RankConst*
dtype0*
_output_shapes
: *
value	B :
�
6optimize/gradients/nce_loss/nce_loss/concat_3_grad/modFloorModnce_loss/nce_loss/concat_3/axis7optimize/gradients/nce_loss/nce_loss/concat_3_grad/Rank*
T0*
_output_shapes
: 
�
8optimize/gradients/nce_loss/nce_loss/concat_3_grad/ShapeConst*
dtype0*
_output_shapes
:*
valueB"�      
�
:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape_1Const*
dtype0*
_output_shapes
:*
valueB"�   @   
�
?optimize/gradients/nce_loss/nce_loss/concat_3_grad/ConcatOffsetConcatOffset6optimize/gradients/nce_loss/nce_loss/concat_3_grad/mod8optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape_1*
N* 
_output_shapes
::
�
8optimize/gradients/nce_loss/nce_loss/concat_3_grad/SliceSliceoptimize/gradients/AddN?optimize/gradients/nce_loss/nce_loss/concat_3_grad/ConcatOffset8optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape*
_output_shapes
:	�*
Index0*
T0
�
:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1Sliceoptimize/gradients/AddNAoptimize/gradients/nce_loss/nce_loss/concat_3_grad/ConcatOffset:1:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape_1*
Index0*
T0*
_output_shapes
:	�@
�
Coptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/group_depsNoOp9^optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice;^optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1
�
Koptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependencyIdentity8optimize/gradients/nce_loss/nce_loss/concat_3_grad/SliceD^optimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/group_deps*
_output_shapes
:	�*
T0*K
_classA
?=loc:@optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice
�
Moptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency_1Identity:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1D^optimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/group_deps*
T0*M
_classC
A?loc:@optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1*
_output_shapes
:	�@
�
1optimize/gradients/nce_loss/nce_loss/sub_grad/NegNegKoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency*
T0*
_output_shapes
:	�
�
>optimize/gradients/nce_loss/nce_loss/sub_grad/tuple/group_depsNoOpL^optimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency2^optimize/gradients/nce_loss/nce_loss/sub_grad/Neg
�
Foptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependencyIdentityKoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency?^optimize/gradients/nce_loss/nce_loss/sub_grad/tuple/group_deps*
_output_shapes
:	�*
T0*K
_classA
?=loc:@optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice
�
Hoptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependency_1Identity1optimize/gradients/nce_loss/nce_loss/sub_grad/Neg?^optimize/gradients/nce_loss/nce_loss/sub_grad/tuple/group_deps*
T0*D
_class:
86loc:@optimize/gradients/nce_loss/nce_loss/sub_grad/Neg*
_output_shapes
:	�
�
5optimize/gradients/nce_loss/nce_loss/sub_1_grad/ShapeConst*
_output_shapes
:*
valueB"�   @   *
dtype0
�
7optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape_1Const*
dtype0*
_output_shapes
:*
valueB:@
�
Eoptimize/gradients/nce_loss/nce_loss/sub_1_grad/BroadcastGradientArgsBroadcastGradientArgs5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape7optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape_1*
T0*2
_output_shapes 
:���������:���������
�
3optimize/gradients/nce_loss/nce_loss/sub_1_grad/SumSumMoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency_1Eoptimize/gradients/nce_loss/nce_loss/sub_1_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*
	keep_dims( *

Tidx0
�
7optimize/gradients/nce_loss/nce_loss/sub_1_grad/ReshapeReshape3optimize/gradients/nce_loss/nce_loss/sub_1_grad/Sum5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape*
_output_shapes
:	�@*
T0*
Tshape0
�
5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Sum_1SumMoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency_1Goptimize/gradients/nce_loss/nce_loss/sub_1_grad/BroadcastGradientArgs:1*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
�
3optimize/gradients/nce_loss/nce_loss/sub_1_grad/NegNeg5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Sum_1*
T0*
_output_shapes
:
�
9optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1Reshape3optimize/gradients/nce_loss/nce_loss/sub_1_grad/Neg7optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape_1*
_output_shapes
:@*
T0*
Tshape0
�
@optimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape:^optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1
�
Hoptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/nce_loss/sub_1_grad/ReshapeA^optimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape*
_output_shapes
:	�@
�
Joptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1A^optimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/group_deps*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1*
_output_shapes
:@
�
3optimize/gradients/nce_loss/nce_loss/add_grad/ShapeShapence_loss/nce_loss/Reshape_4*
T0*
out_type0*
_output_shapes
:
�
5optimize/gradients/nce_loss/nce_loss/add_grad/Shape_1Const*
valueB"�      *
dtype0*
_output_shapes
:
�
Coptimize/gradients/nce_loss/nce_loss/add_grad/BroadcastGradientArgsBroadcastGradientArgs3optimize/gradients/nce_loss/nce_loss/add_grad/Shape5optimize/gradients/nce_loss/nce_loss/add_grad/Shape_1*
T0*2
_output_shapes 
:���������:���������
�
1optimize/gradients/nce_loss/nce_loss/add_grad/SumSumFoptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependencyCoptimize/gradients/nce_loss/nce_loss/add_grad/BroadcastGradientArgs*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
�
5optimize/gradients/nce_loss/nce_loss/add_grad/ReshapeReshape1optimize/gradients/nce_loss/nce_loss/add_grad/Sum3optimize/gradients/nce_loss/nce_loss/add_grad/Shape*
T0*
Tshape0*'
_output_shapes
:���������
�
3optimize/gradients/nce_loss/nce_loss/add_grad/Sum_1SumFoptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependencyEoptimize/gradients/nce_loss/nce_loss/add_grad/BroadcastGradientArgs:1*
	keep_dims( *

Tidx0*
T0*
_output_shapes
:
�
7optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1Reshape3optimize/gradients/nce_loss/nce_loss/add_grad/Sum_15optimize/gradients/nce_loss/nce_loss/add_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:	�
�
>optimize/gradients/nce_loss/nce_loss/add_grad/tuple/group_depsNoOp6^optimize/gradients/nce_loss/nce_loss/add_grad/Reshape8^optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1
�
Foptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependencyIdentity5optimize/gradients/nce_loss/nce_loss/add_grad/Reshape?^optimize/gradients/nce_loss/nce_loss/add_grad/tuple/group_deps*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/add_grad/Reshape*'
_output_shapes
:���������*
T0
�
Hoptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependency_1Identity7optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1?^optimize/gradients/nce_loss/nce_loss/add_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1*
_output_shapes
:	�
�
5optimize/gradients/nce_loss/nce_loss/add_1_grad/ShapeShapence_loss/nce_loss/MatMul*
_output_shapes
:*
T0*
out_type0
�
7optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape_1Const*
valueB:@*
dtype0*
_output_shapes
:
�
Eoptimize/gradients/nce_loss/nce_loss/add_1_grad/BroadcastGradientArgsBroadcastGradientArgs5optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape7optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape_1*2
_output_shapes 
:���������:���������*
T0
�
3optimize/gradients/nce_loss/nce_loss/add_1_grad/SumSumHoptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependencyEoptimize/gradients/nce_loss/nce_loss/add_1_grad/BroadcastGradientArgs*
	keep_dims( *

Tidx0*
T0*
_output_shapes
:
�
7optimize/gradients/nce_loss/nce_loss/add_1_grad/ReshapeReshape3optimize/gradients/nce_loss/nce_loss/add_1_grad/Sum5optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape*
T0*
Tshape0*(
_output_shapes
:����������
�
5optimize/gradients/nce_loss/nce_loss/add_1_grad/Sum_1SumHoptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependencyGoptimize/gradients/nce_loss/nce_loss/add_1_grad/BroadcastGradientArgs:1*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
�
9optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1Reshape5optimize/gradients/nce_loss/nce_loss/add_1_grad/Sum_17optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:@
�
@optimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape:^optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1
�
Hoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/nce_loss/add_1_grad/ReshapeA^optimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape*(
_output_shapes
:����������
�
Joptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1A^optimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/group_deps*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1*
_output_shapes
:@
�
9optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/ShapeShapence_loss/nce_loss/Reshape_3*
T0*
out_type0*
_output_shapes
:
�
;optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/ReshapeReshapeFoptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependency9optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/Shape*
T0*
Tshape0*#
_output_shapes
:���������
�
9optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/ShapeConst*
valueB:�*
dtype0*
_output_shapes
:
�
;optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/ReshapeReshapeHoptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependency_19optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/Shape*
T0*
Tshape0*
_output_shapes	
:�
�
7optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMulMatMulHoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependencynce_loss/nce_loss/Slice_1*
T0* 
_output_shapes
:
��*
transpose_a( *
transpose_b( 
�
9optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1MatMulHoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependencyembeddings/embedding_lookup*(
_output_shapes
:����������*
transpose_a(*
transpose_b( *
T0
�
Aoptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul:^optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1
�
Ioptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMulB^optimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/group_deps*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul* 
_output_shapes
:
��*
T0
�
Koptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1B^optimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/group_deps*(
_output_shapes
:����������*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1
x
6optimize/gradients/nce_loss/nce_loss/Slice_3_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 
�
7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/ShapeConst*
valueB:@*
dtype0*
_output_shapes
:
{
9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
�
7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stackPack6optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Rank9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack/1*

axis *
N*
_output_shapes
:*
T0
�
9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/ReshapeReshapence_loss/nce_loss/Shape_37optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack*
_output_shapes

:*
T0*
Tshape0
�
9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Shape_1Const*
dtype0*
_output_shapes
:*
valueB:�
�
5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/subSub9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Shape_17optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Shape*
_output_shapes
:*
T0
�
7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/sub_1Sub5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/subnce_loss/nce_loss/Shape_3*
T0*
_output_shapes
:
�
;optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Reshape_1Reshape7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/sub_17optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack*
T0*
Tshape0*
_output_shapes

:

=optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concat/axisConst*
dtype0*
_output_shapes
: *
value	B :
�
8optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concatConcatV29optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Reshape;optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Reshape_1=optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concat/axis*
T0*
N*
_output_shapes

:*

Tidx0
�
5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/PadPadJoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependency_18optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concat*
T0*
	Tpaddings0*
_output_shapes	
:�
�
9optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/ShapeShapence_loss/nce_loss/MatMul_1*
T0*
out_type0*
_output_shapes
:
�
;optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/ReshapeReshape;optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/Reshape9optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/Shape*'
_output_shapes
:���������*
T0*
Tshape0
x
6optimize/gradients/nce_loss/nce_loss/Slice_2_grad/RankConst*
dtype0*
_output_shapes
: *
value	B :
�
7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/ShapeConst*
valueB:�*
dtype0*
_output_shapes
:
{
9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
�
7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stackPack6optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Rank9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack/1*
T0*

axis *
N*
_output_shapes
:
�
9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/ReshapeReshapence_loss/nce_loss/Slice_2/begin7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack*
T0*
Tshape0*
_output_shapes

:
�
9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Shape_1Const*
valueB:�*
dtype0*
_output_shapes
:
�
5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/subSub9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Shape_17optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Shape*
T0*
_output_shapes
:
�
7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/sub_1Sub5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/subnce_loss/nce_loss/Slice_2/begin*
_output_shapes
:*
T0
�
;optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Reshape_1Reshape7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/sub_17optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack*
_output_shapes

:*
T0*
Tshape0

=optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concat/axisConst*
value	B :*
dtype0*
_output_shapes
: 
�
8optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concatConcatV29optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Reshape;optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Reshape_1=optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concat/axis*
T0*
N*
_output_shapes

:*

Tidx0
�
5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/PadPad;optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/Reshape8optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concat*
	Tpaddings0*
_output_shapes	
:�*
T0
x
6optimize/gradients/nce_loss/nce_loss/Slice_1_grad/RankConst*
_output_shapes
: *
value	B :*
dtype0
�
7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/ShapeShapence_loss/nce_loss/Slice_1*
T0*
out_type0*
_output_shapes
:
{
9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
�
7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stackPack6optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Rank9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack/1*
T0*

axis *
N*
_output_shapes
:
�
9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/ReshapeReshapence_loss/nce_loss/stack_17optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack*
T0*
Tshape0*
_output_shapes

:
�
9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Shape_1Const*
_output_shapes
:*
valueB"�   �   *
dtype0
�
5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/subSub9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Shape_17optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Shape*
T0*
_output_shapes
:
�
7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/sub_1Sub5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/subnce_loss/nce_loss/stack_1*
T0*
_output_shapes
:
�
;optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Reshape_1Reshape7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/sub_17optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack*
T0*
Tshape0*
_output_shapes

:

=optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concat/axisConst*
value	B :*
dtype0*
_output_shapes
: 
�
8optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concatConcatV29optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Reshape;optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Reshape_1=optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concat/axis*
T0*
N*
_output_shapes

:*

Tidx0
�
5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/PadPadKoptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependency_18optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concat*
T0*
	Tpaddings0* 
_output_shapes
:
��
�
9optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMulMatMul;optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/Reshapence_loss/nce_loss/ones*
T0*0
_output_shapes
:������������������*
transpose_a( *
transpose_b(
�
;optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1MatMulnce_loss/nce_loss/Reshape_2;optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/Reshape*
T0*'
_output_shapes
:���������*
transpose_a(*
transpose_b( 
�
Coptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/group_depsNoOp:^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul<^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1
�
Koptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/control_dependencyIdentity9optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMulD^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/group_deps*0
_output_shapes
:������������������*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul
�
Moptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/control_dependency_1Identity;optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1D^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/group_deps*'
_output_shapes
:���������*
T0*N
_classD
B@loc:@optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1
�
optimize/gradients/AddN_1AddN5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Pad5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Pad*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Pad*
N*
_output_shapes	
:�*
T0
�
Boptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ShapeConst*
_output_shapes
:*
_class
loc:@nce_bias*
valueB	R�N*
dtype0	
�
Doptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ToInt32CastBoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Shape*
_output_shapes
:*

DstT0*

SrcT0	*
_class
loc:@nce_bias
�
Aoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/SizeConst*
value
B :�*
dtype0*
_output_shapes
: 
�
Koptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims/dimConst*
value	B : *
dtype0*
_output_shapes
: 
�
Goptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims
ExpandDimsAoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/SizeKoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims/dim*

Tdim0*
T0*
_output_shapes
:
�
Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stackConst*
_output_shapes
:*
valueB:*
dtype0
�
Roptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_1Const*
valueB: *
dtype0*
_output_shapes
:
�
Roptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
�
Joptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_sliceStridedSliceDoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ToInt32Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stackRoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_1Roptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_2*
shrink_axis_mask *
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask*
_output_shapes
: *
Index0*
T0
�
Hoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
�
Coptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concatConcatV2Goptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDimsJoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_sliceHoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concat/axis*
T0*
N*
_output_shapes
:*

Tidx0
�
Doptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ReshapeReshapeoptimize/gradients/AddN_1Coptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concat*
_output_shapes	
:�*
T0*
Tshape0
�
Foptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Reshape_1Reshapence_loss/nce_loss/concatGoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims*
Tshape0*
_output_shapes	
:�*
T0	
�
9optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/ShapeConst*!
valueB"�      �   *
dtype0*
_output_shapes
:
�
;optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/ReshapeReshapeKoptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/control_dependency9optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/Shape*$
_output_shapes
:��*
T0*
Tshape0
�
3optimize/gradients/nce_loss/nce_loss/Mul_grad/ShapeConst*!
valueB"�      �   *
dtype0*
_output_shapes
:
�
5optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape_1Shapence_loss/nce_loss/Reshape_1*
T0*
out_type0*
_output_shapes
:
�
Coptimize/gradients/nce_loss/nce_loss/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs3optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape5optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape_1*
T0*2
_output_shapes 
:���������:���������
�
1optimize/gradients/nce_loss/nce_loss/Mul_grad/MulMul;optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/Reshapence_loss/nce_loss/Reshape_1*
T0*$
_output_shapes
:��
�
1optimize/gradients/nce_loss/nce_loss/Mul_grad/SumSum1optimize/gradients/nce_loss/nce_loss/Mul_grad/MulCoptimize/gradients/nce_loss/nce_loss/Mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*
	keep_dims( *

Tidx0
�
5optimize/gradients/nce_loss/nce_loss/Mul_grad/ReshapeReshape1optimize/gradients/nce_loss/nce_loss/Mul_grad/Sum3optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape*$
_output_shapes
:��*
T0*
Tshape0
�
3optimize/gradients/nce_loss/nce_loss/Mul_grad/Mul_1Mulnce_loss/nce_loss/ExpandDims;optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/Reshape*
T0*$
_output_shapes
:��
�
3optimize/gradients/nce_loss/nce_loss/Mul_grad/Sum_1Sum3optimize/gradients/nce_loss/nce_loss/Mul_grad/Mul_1Eoptimize/gradients/nce_loss/nce_loss/Mul_grad/BroadcastGradientArgs:1*
	keep_dims( *

Tidx0*
T0*
_output_shapes
:
�
7optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1Reshape3optimize/gradients/nce_loss/nce_loss/Mul_grad/Sum_15optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape_1*
T0*
Tshape0*4
_output_shapes"
 :������������������
�
>optimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/group_depsNoOp6^optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape8^optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1
�
Foptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependencyIdentity5optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape?^optimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/group_deps*$
_output_shapes
:��*
T0*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape
�
Hoptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependency_1Identity7optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1?^optimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1*4
_output_shapes"
 :������������������
�
:optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/ShapeConst*
valueB"�   �   *
dtype0*
_output_shapes
:
�
<optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/ReshapeReshapeFoptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependency:optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/Shape*
T0*
Tshape0* 
_output_shapes
:
��
�
9optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/ShapeShapence_loss/nce_loss/Slice*
T0*
out_type0*
_output_shapes
:
�
;optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/ReshapeReshapeHoptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependency_19optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/Shape*
T0*
Tshape0*0
_output_shapes
:������������������
�
optimize/gradients/AddN_2AddNIoptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependency<optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/Reshape*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul*
N* 
_output_shapes
:
��
�
9optimize/gradients/embeddings/embedding_lookup_grad/ShapeConst*
_output_shapes
:*
_class
loc:@embeddings_1*%
valueB	"'      �       *
dtype0	
�
;optimize/gradients/embeddings/embedding_lookup_grad/ToInt32Cast9optimize/gradients/embeddings/embedding_lookup_grad/Shape*
_class
loc:@embeddings_1*
_output_shapes
:*

DstT0*

SrcT0	
{
8optimize/gradients/embeddings/embedding_lookup_grad/SizeConst*
value
B :�*
dtype0*
_output_shapes
: 
�
Boptimize/gradients/embeddings/embedding_lookup_grad/ExpandDims/dimConst*
value	B : *
dtype0*
_output_shapes
: 
�
>optimize/gradients/embeddings/embedding_lookup_grad/ExpandDims
ExpandDims8optimize/gradients/embeddings/embedding_lookup_grad/SizeBoptimize/gradients/embeddings/embedding_lookup_grad/ExpandDims/dim*
T0*
_output_shapes
:*

Tdim0
�
Goptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stackConst*
valueB:*
dtype0*
_output_shapes
:
�
Ioptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_1Const*
valueB: *
dtype0*
_output_shapes
:
�
Ioptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
�
Aoptimize/gradients/embeddings/embedding_lookup_grad/strided_sliceStridedSlice;optimize/gradients/embeddings/embedding_lookup_grad/ToInt32Goptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stackIoptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_1Ioptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_2*
Index0*
T0*
shrink_axis_mask *

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask*
_output_shapes
:
�
?optimize/gradients/embeddings/embedding_lookup_grad/concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
�
:optimize/gradients/embeddings/embedding_lookup_grad/concatConcatV2>optimize/gradients/embeddings/embedding_lookup_grad/ExpandDimsAoptimize/gradients/embeddings/embedding_lookup_grad/strided_slice?optimize/gradients/embeddings/embedding_lookup_grad/concat/axis*
T0*
N*
_output_shapes
:*

Tidx0
�
;optimize/gradients/embeddings/embedding_lookup_grad/ReshapeReshapeoptimize/gradients/AddN_2:optimize/gradients/embeddings/embedding_lookup_grad/concat*
T0*
Tshape0* 
_output_shapes
:
��
�
=optimize/gradients/embeddings/embedding_lookup_grad/Reshape_1Reshapedata/IteratorGetNext>optimize/gradients/embeddings/embedding_lookup_grad/ExpandDims*
T0*
Tshape0*
_output_shapes	
:�
v
4optimize/gradients/nce_loss/nce_loss/Slice_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 
�
5optimize/gradients/nce_loss/nce_loss/Slice_grad/ShapeShapence_loss/nce_loss/Slice*
_output_shapes
:*
T0*
out_type0
y
7optimize/gradients/nce_loss/nce_loss/Slice_grad/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
�
5optimize/gradients/nce_loss/nce_loss/Slice_grad/stackPack4optimize/gradients/nce_loss/nce_loss/Slice_grad/Rank7optimize/gradients/nce_loss/nce_loss/Slice_grad/stack/1*
T0*

axis *
N*
_output_shapes
:
�
7optimize/gradients/nce_loss/nce_loss/Slice_grad/ReshapeReshapence_loss/nce_loss/Slice/begin5optimize/gradients/nce_loss/nce_loss/Slice_grad/stack*
T0*
Tshape0*
_output_shapes

:
�
7optimize/gradients/nce_loss/nce_loss/Slice_grad/Shape_1Const*
valueB"�   �   *
dtype0*
_output_shapes
:
�
3optimize/gradients/nce_loss/nce_loss/Slice_grad/subSub7optimize/gradients/nce_loss/nce_loss/Slice_grad/Shape_15optimize/gradients/nce_loss/nce_loss/Slice_grad/Shape*
T0*
_output_shapes
:
�
5optimize/gradients/nce_loss/nce_loss/Slice_grad/sub_1Sub3optimize/gradients/nce_loss/nce_loss/Slice_grad/subnce_loss/nce_loss/Slice/begin*
_output_shapes
:*
T0
�
9optimize/gradients/nce_loss/nce_loss/Slice_grad/Reshape_1Reshape5optimize/gradients/nce_loss/nce_loss/Slice_grad/sub_15optimize/gradients/nce_loss/nce_loss/Slice_grad/stack*
_output_shapes

:*
T0*
Tshape0
}
;optimize/gradients/nce_loss/nce_loss/Slice_grad/concat/axisConst*
value	B :*
dtype0*
_output_shapes
: 
�
6optimize/gradients/nce_loss/nce_loss/Slice_grad/concatConcatV27optimize/gradients/nce_loss/nce_loss/Slice_grad/Reshape9optimize/gradients/nce_loss/nce_loss/Slice_grad/Reshape_1;optimize/gradients/nce_loss/nce_loss/Slice_grad/concat/axis*
N*
_output_shapes

:*

Tidx0*
T0
�
3optimize/gradients/nce_loss/nce_loss/Slice_grad/PadPad;optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/Reshape6optimize/gradients/nce_loss/nce_loss/Slice_grad/concat* 
_output_shapes
:
��*
T0*
	Tpaddings0
�
optimize/gradients/AddN_3AddN5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Pad3optimize/gradients/nce_loss/nce_loss/Slice_grad/Pad*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Pad*
N* 
_output_shapes
:
��*
T0
�
@optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ShapeConst*
_class
loc:@nce_weights*%
valueB	"'      �       *
dtype0	*
_output_shapes
:
�
Boptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ToInt32Cast@optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Shape*

SrcT0	*
_class
loc:@nce_weights*
_output_shapes
:*

DstT0
�
?optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/SizeConst*
_output_shapes
: *
value
B :�*
dtype0
�
Ioptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims/dimConst*
value	B : *
dtype0*
_output_shapes
: 
�
Eoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims
ExpandDims?optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/SizeIoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims/dim*
T0*
_output_shapes
:*

Tdim0
�
Noptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stackConst*
valueB:*
dtype0*
_output_shapes
:
�
Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_1Const*
valueB: *
dtype0*
_output_shapes
:
�
Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_2Const*
dtype0*
_output_shapes
:*
valueB:
�
Hoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_sliceStridedSliceBoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ToInt32Noptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stackPoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_1Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_2*
shrink_axis_mask *
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask*
_output_shapes
:*
Index0*
T0
�
Foptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
�
Aoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concatConcatV2Eoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDimsHoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_sliceFoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concat/axis*
T0*
N*
_output_shapes
:*

Tidx0
�
Boptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ReshapeReshapeoptimize/gradients/AddN_3Aoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concat*
T0*
Tshape0* 
_output_shapes
:
��
�
Doptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Reshape_1Reshapence_loss/nce_loss/concatEoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims*
Tshape0*
_output_shapes	
:�*
T0	
k
&optimize/GradientDescent/learning_rateConst*
valueB
 *���=*
dtype0*
_output_shapes
: 
�
/optimize/GradientDescent/update_nce_weights/mulMulBoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Reshape&optimize/GradientDescent/learning_rate* 
_output_shapes
:
��*
T0*
_class
loc:@nce_weights
�
6optimize/GradientDescent/update_nce_weights/ScatterSub
ScatterSubnce_weightsDoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Reshape_1/optimize/GradientDescent/update_nce_weights/mul*
T0*
_class
loc:@nce_weights* 
_output_shapes
:
�N�*
use_locking( *
Tindices0	
�
,optimize/GradientDescent/update_nce_bias/mulMulDoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Reshape&optimize/GradientDescent/learning_rate*
_output_shapes	
:�*
T0*
_class
loc:@nce_bias
�
3optimize/GradientDescent/update_nce_bias/ScatterSub
ScatterSubnce_biasFoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Reshape_1,optimize/GradientDescent/update_nce_bias/mul*
use_locking( *
Tindices0	*
T0*
_class
loc:@nce_bias*
_output_shapes	
:�N
�
0optimize/GradientDescent/update_embeddings_1/mulMul;optimize/gradients/embeddings/embedding_lookup_grad/Reshape&optimize/GradientDescent/learning_rate*
T0*
_class
loc:@embeddings_1* 
_output_shapes
:
��
�
7optimize/GradientDescent/update_embeddings_1/ScatterSub
ScatterSubembeddings_1=optimize/gradients/embeddings/embedding_lookup_grad/Reshape_10optimize/GradientDescent/update_embeddings_1/mul*
use_locking( *
Tindices0*
T0*
_class
loc:@embeddings_1* 
_output_shapes
:
�N�
�
optimize/GradientDescent/updateNoOp8^optimize/GradientDescent/update_embeddings_1/ScatterSub4^optimize/GradientDescent/update_nce_bias/ScatterSub7^optimize/GradientDescent/update_nce_weights/ScatterSub
�
optimize/GradientDescent/valueConst ^optimize/GradientDescent/update*
_class
loc:@global_step*
value	B :*
dtype0*
_output_shapes
: 
�
optimize/GradientDescent	AssignAddglobal_stepoptimize/GradientDescent/value*
_output_shapes
: *
use_locking( *
T0*
_class
loc:@global_step
b
summaries/loss/tagsConst*
valueB Bsummaries/loss*
dtype0*
_output_shapes
: 
j
summaries/lossScalarSummarysummaries/loss/tagsnce_loss/nce_loss_1*
T0*
_output_shapes
: 
u
summaries/histogram_loss/tagConst*)
value B Bsummaries/histogram_loss*
dtype0*
_output_shapes
: 
�
summaries/histogram_lossHistogramSummarysummaries/histogram_loss/tagnce_loss/nce_loss_1*
T0*
_output_shapes
: 
w
summaries/Merge/MergeSummaryMergeSummarysummaries/losssummaries/histogram_loss*
N*
_output_shapes
: 
P

save/ConstConst*
valueB Bmodel*
dtype0*
_output_shapes
: 
�
save/SaveV2/tensor_namesConst*E
value<B:Bembeddings_1Bglobal_stepBnce_biasBnce_weights*
dtype0*
_output_shapes
:
k
save/SaveV2/shape_and_slicesConst*
valueBB B B B *
dtype0*
_output_shapes
:
�
save/SaveV2SaveV2
save/Constsave/SaveV2/tensor_namessave/SaveV2/shape_and_slicesembeddings_1global_stepnce_biasnce_weights*
dtypes
2
}
save/control_dependencyIdentity
save/Const^save/SaveV2*
_output_shapes
: *
T0*
_class
loc:@save/Const
�
save/RestoreV2/tensor_namesConst"/device:CPU:0*E
value<B:Bembeddings_1Bglobal_stepBnce_biasBnce_weights*
dtype0*
_output_shapes
:
}
save/RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
valueBB B B B *
dtype0
�
save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices"/device:CPU:0*$
_output_shapes
::::*
dtypes
2
�
save/AssignAssignembeddings_1save/RestoreV2*
_class
loc:@embeddings_1*
validate_shape(* 
_output_shapes
:
�N�*
use_locking(*
T0
�
save/Assign_1Assignglobal_stepsave/RestoreV2:1*
validate_shape(*
_output_shapes
: *
use_locking(*
T0*
_class
loc:@global_step
�
save/Assign_2Assignnce_biassave/RestoreV2:2*
T0*
_class
loc:@nce_bias*
validate_shape(*
_output_shapes	
:�N*
use_locking(
�
save/Assign_3Assignnce_weightssave/RestoreV2:3*
use_locking(*
T0*
_class
loc:@nce_weights*
validate_shape(* 
_output_shapes
:
�N�
V
save/restore_allNoOp^save/Assign^save/Assign_1^save/Assign_2^save/Assign_3
^
initNoOp^embeddings_1/Assign^global_step/Assign^nce_bias/Assign^nce_weights/Assign�
�
�
tf_init_func_O9ko4JKUVjc
arg0
pyfunc_placeholder
pyfunc_placeholder_1
pyfunc_placeholder_2
pyfunc_placeholder_3
pyfunc_placeholder_4

pyfunc	25A wrapper for Defun that facilitates shape inference.��
PyFuncPyFuncpyfunc_placeholderpyfunc_placeholder_1pyfunc_placeholder_2pyfunc_placeholder_3pyfunc_placeholder_4*
Tin	
2*
Tout
2	*
token
pyfunc_0"
pyfuncPyFunc:output:0
�
�
tf_map_func_8pkeaS83Jbc
arg0 
generatordataset_placeholder"
generatordataset_placeholder_1
generatordataset25A wrapper for Defun that facilitates shape inference.��
GeneratorDatasetGeneratorDatasetarg0generatordataset_placeholdergeneratordataset_placeholder_1*)
	next_funcR
tf_next_func_I9alSJeqyO0*
Tnext_func_args
 *
Tfinalize_func_args
 *1
output_shapes 
:���������:���������*1
finalize_func R
tf_finalize_func_371o9XzBGzc*)
	init_funcR
tf_init_func_ab9wLUDUcC8*
output_types
2*
Tinit_func_args
2"-
generatordatasetGeneratorDataset:handle:0
�
x
tf_next_func_I9alSJeqyO0
arg0	

pyfunc
pyfunc_025A wrapper for Defun that facilitates shape inference.�I
PyFuncPyFuncarg0*
token
pyfunc_4*
Tin
2	*
Tout
2"
pyfuncPyFunc:output:0"
pyfunc_0PyFunc:output:1
�
�
tf_map_func_PCOijuOn9mg
arg0 
generatordataset_placeholder"
generatordataset_placeholder_1"
generatordataset_placeholder_2"
generatordataset_placeholder_3"
generatordataset_placeholder_4
generatordataset25A wrapper for Defun that facilitates shape inference.��
GeneratorDatasetGeneratorDatasetarg0generatordataset_placeholdergeneratordataset_placeholder_1generatordataset_placeholder_2generatordataset_placeholder_3generatordataset_placeholder_4*
Tinit_func_args

2*)
	next_funcR
tf_next_func_udg8Vh0kgto*
Tnext_func_args
 *
Tfinalize_func_args
 *%
output_shapes
:�:	�*1
finalize_func R
tf_finalize_func_Lpot2pAvNXE*)
	init_funcR
tf_init_func_O9ko4JKUVjc*
output_types
2"-
generatordatasetGeneratorDataset:handle:0
�
n
tf_finalize_func_Lpot2pAvNXE
arg0	

pyfunc	25A wrapper for Defun that facilitates shape inference.�H
PyFuncPyFuncarg0*
Tin
2	*
Tout
2	*
token
pyfunc_2"
pyfuncPyFunc:output:0
�
n
tf_finalize_func_371o9XzBGzc
arg0	

pyfunc	25A wrapper for Defun that facilitates shape inference.�H
PyFuncPyFuncarg0*
Tin
2	*
Tout
2	*
token
pyfunc_5"
pyfuncPyFunc:output:0
�
x
tf_next_func_udg8Vh0kgto
arg0	

pyfunc
pyfunc_025A wrapper for Defun that facilitates shape inference.�I
PyFuncPyFuncarg0*
Tin
2	*
Tout
2*
token
pyfunc_1"
pyfunc_0PyFunc:output:1"
pyfuncPyFunc:output:0
�
�
tf_init_func_ab9wLUDUcC8
arg0
pyfunc_placeholder
pyfunc_placeholder_1

pyfunc	25A wrapper for Defun that facilitates shape inference.�m
PyFuncPyFuncpyfunc_placeholderpyfunc_placeholder_1*
Tin
2*
Tout
2	*
token
pyfunc_3"
pyfuncPyFunc:output:0""=
	summaries0
.
summaries/loss:0
summaries/histogram_loss:0"�
trainable_variables��
e
nce_weights:0nce_weights/Assignnce_weights/read:02*nce_weights/Initializer/truncated_normal:08
A

nce_bias:0nce_bias/Assignnce_bias/read:02weights/zeros:08
g
embeddings_1:0embeddings_1/Assignembeddings_1/read:02)embeddings_1/Initializer/random_uniform:08"(
train_op

optimize/GradientDescent"3
	iterators&
$
data/Iterator:0
data/Iterator_1:0"�
	variables��
@
global_step:0global_step/Assignglobal_step/read:02Const:0
e
nce_weights:0nce_weights/Assignnce_weights/read:02*nce_weights/Initializer/truncated_normal:08
A

nce_bias:0nce_bias/Assignnce_bias/read:02weights/zeros:08
g
embeddings_1:0embeddings_1/Assignembeddings_1/read:02)embeddings_1/Initializer/random_uniform:08;�Q�       p�*	������A��*�

summaries/loss2�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) qfm�=@2زv�5f@��h:np@�������:              �?        �O���       p�*	!�����A��*�

summaries/loss)��@
}
summaries/histogram_loss*a	    q@    q@      �?!    q@)@�e(�:@2زv�5f@��h:np@�������:              �?        x?��       p�*	I����A��*�

summaries/lossn�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ѿ�Y�=@2زv�5f@��h:np@�������:              �?        p�Pj�       p�*	������A��*�

summaries/lossuا@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@^���;@2زv�5f@��h:np@�������:              �?        A��L�       p�*	�L����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   `6�@   `6�@      �?!   `6�@)@�l98�:@2زv�5f@��h:np@�������:              �?        C���       p�*	x�����A��*�

summaries/loss� �@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) !L{�:@2زv�5f@��h:np@�������:              �?        
��Ŧ       p�*	;�����A��*�

summaries/loss�[�@
}
summaries/histogram_loss*a	   �z�@   �z�@      �?!   �z�@)@�#�:@2زv�5f@��h:np@�������:              �?        �:�^�       p�*	�R����A��*�

summaries/loss�8�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ����=@2زv�5f@��h:np@�������:              �?        ���Ħ       p�*	�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��Q@   ��Q@      �?!   ��Q@)@��h<@2زv�5f@��h:np@�������:              �?        %Q#��       p�*	o�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �_@   �_@      �?!   �_@)@j_%Ë<@2زv�5f@��h:np@�������:              �?        ͒�r�       p�*	�>����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) i�"��=@2زv�5f@��h:np@�������:              �?        �Yឦ       p�*	a�����A��*�

summaries/losst �@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ����A@2��h:np@S���߮@�������:              �?        �Wk�       p�*	������A��*�

summaries/lossj��@
}
summaries/histogram_loss*a	   @m@   @m@      �?!   @m@) �	�Fx>@2زv�5f@��h:np@�������:              �?        �����       p�*	�J����A��*�

summaries/loss�J�@
}
summaries/histogram_loss*a	    T)@    T)@      �?!    T)@)  9@��;@2زv�5f@��h:np@�������:              �?        N�t��       p�*	�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@��-�=@2زv�5f@��h:np@�������:              �?        ��V��       p�*	Y�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    �q@    �q@      �?!    �q@) @�T��<@2زv�5f@��h:np@�������:              �?        (�I�       p�*	�M����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�]�s�;@2زv�5f@��h:np@�������:              �?        Q�3^�       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �^@   �^@      �?!   �^@) �m٘;@2زv�5f@��h:np@�������:              �?        tN"��       p�*	#�����A��*�

summaries/loss�B�@
}
summaries/histogram_loss*a	   @P�@   @P�@      �?!   @P�@) ��>�P=@2زv�5f@��h:np@�������:              �?        a�@�       p�*	I����A��*�

summaries/loss|k�@
}
summaries/histogram_loss*a	   �o-@   �o-@      �?!   �o-@) 9��<@2زv�5f@��h:np@�������:              �?        ^Iм�       p�*	J�����A��*�

summaries/loss�0�@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@Rzz��=@2زv�5f@��h:np@�������:              �?        >9���       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    {�@    {�@      �?!    {�@) �1�Æ;@2زv�5f@��h:np@�������:              �?        �e¦       p�*	�1����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) d��%�=@2زv�5f@��h:np@�������:              �?        �����       p�*	j�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��s@   ��s@      �?!   ��s@)@���$:@2زv�5f@��h:np@�������:              �?        ��/5�       p�*	�����A��*�

summaries/lossZ\�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �3�j�=@2زv�5f@��h:np@�������:              �?        o	�       p�*	�����A��*�

summaries/loss�@
}
summaries/histogram_loss*a	    A~@    A~@      �?!    A~@) ��N�<@2زv�5f@��h:np@�������:              �?        ��Ml�       p�*	wi����A��*�

summaries/loss�3�@
}
summaries/histogram_loss*a	   @q�@   @q�@      �?!   @q�@) �C��=@2زv�5f@��h:np@�������:              �?        �ګ!�       p�*	������A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) ���q;@2زv�5f@��h:np@�������:              �?        ̡:��       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��C@   ��C@      �?!   ��C@)@2�?�B<@2زv�5f@��h:np@�������:              �?        92�	�       p�*	�R����A��*�

summaries/loss쑦@
}
summaries/histogram_loss*a	   �=�@   �=�@      �?!   �=�@) dLOd;@2زv�5f@��h:np@�������:              �?        >t���       p�*	{�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��5@   ��5@      �?!   ��5@) �ԙ�9@2!��v�@زv�5f@�������:              �?        ��       p�*	9�����A��*�

summaries/lossh��@
}
summaries/histogram_loss*a	    �6@    �6@      �?!    �6@) �2��9@2!��v�@زv�5f@�������:              �?        \�ڦ       p�*	'6����A��*�

summaries/lossj�@
}
summaries/histogram_loss*a	   @�#@   @�#@      �?!   @�#@) ��ơ�;@2زv�5f@��h:np@�������:              �?        pt]ަ       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �hn2=@2زv�5f@��h:np@�������:              �?        Q��	�       p�*	������A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ����y:@2زv�5f@��h:np@�������:              �?        (��       p�*	�%����A��*�

summaries/loss;�@
}
summaries/histogram_loss*a	   �b'@   �b'@      �?!   �b'@) d�2�b9@2!��v�@زv�5f@�������:              �?        �N�5�       p�*	�m����A��*�

summaries/loss�k�@
}
summaries/histogram_loss*a	    p�@    p�@      �?!    p�@)  I(=@2زv�5f@��h:np@�������:              �?        �n�\�       p�*	������A*�

summaries/lossd�@
}
summaries/histogram_loss*a	   �Lb@   �Lb@      �?!   �Lb@) č�d�<@2زv�5f@��h:np@�������:              �?        ����       p�*	5����AÍ*�

summaries/loss�v�@
}
summaries/histogram_loss*a	    �n@    �n@      �?!    �n@) @A��<@2زv�5f@��h:np@�������:              �?        h���       p�*	�Q����Ač*�

summaries/loss4�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@��*�;@2زv�5f@��h:np@�������:              �?        ��B��       p�*	ܝ����Aō*�

summaries/loss�)�@
}
summaries/histogram_loss*a	    <%@    <%@      �?!    <%@)  a���>@2زv�5f@��h:np@�������:              �?        �Z���       p�*	-�����Aƍ*�

summaries/lossFO�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ����<@2زv�5f@��h:np@�������:              �?        W��m�       p�*	�=����AǍ*�

summaries/lossD��@
}
summaries/histogram_loss*a	   �(w@   �(w@      �?!   �(w@) �ֺA�<@2زv�5f@��h:np@�������:              �?        !�53�       p�*	�����Aȍ*�

summaries/loss�M�@
}
summaries/histogram_loss*a	   ��I@   ��I@      �?!   ��I@) A���R<@2زv�5f@��h:np@�������:              �?        s�Z��       p�*	�����Aɍ*�

summaries/loss}��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�{Ӑ=@2زv�5f@��h:np@�������:              �?        @kp�       p�*	s3����Aʍ*�

summaries/loss8C�@
}
summaries/histogram_loss*a	    g�@    g�@      �?!    g�@) ���P=@2زv�5f@��h:np@�������:              �?        �����       p�*	�����Aˍ*�

summaries/loss.ʰ@
}
summaries/histogram_loss*a	   �E@   �E@      �?!   �E@) (���>@2زv�5f@��h:np@�������:              �?        %�!P�       p�*	6�����A̍*�

summaries/loss4�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) Qԍ�;@2زv�5f@��h:np@�������:              �?        ]�B�       p�*	�����A͍*�

summaries/lossʗ�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ��@�m=@2زv�5f@��h:np@�������:              �?        3ג��       p�*	�b����A΍*�

summaries/loss��@
}
summaries/histogram_loss*a	   �Q}@   �Q}@      �?!   �Q}@)@��|��<@2زv�5f@��h:np@�������:              �?        A�[7�       p�*	�����Aύ*�

summaries/lossoܤ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�ˊ:@2زv�5f@��h:np@�������:              �?        g��       p�*	 ����AЍ*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) D��X@@2��h:np@S���߮@�������:              �?        �M6¦       p�*	�V����Aэ*�

summaries/loss� �@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) !0��=@2زv�5f@��h:np@�������:              �?        D�=�       p�*	O�����Aҍ*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��5@   ��5@      �?!   ��5@) �U��<@2زv�5f@��h:np@�������:              �?        n��R�       p�*	�����AӍ*�

summaries/loss3��@
}
summaries/histogram_loss*a	   `2@   `2@      �?!   `2@)@��#�<@2زv�5f@��h:np@�������:              �?        ᖜ�       p�*	2[����Aԍ*�

summaries/loss[�@
}
summaries/histogram_loss*a	    a�@    a�@      �?!    a�@) �+��:@2زv�5f@��h:np@�������:              �?        ��bŦ       p�*	�����AՍ*�

summaries/lossP�@
}
summaries/histogram_loss*a	   �J@   �J@      �?!   �J@)@��~�S<@2زv�5f@��h:np@�������:              �?        c��
�       p�*	������A֍*�

summaries/loss.��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ��Q�;@2زv�5f@��h:np@�������:              �?        �1�b�       p�*	VE����A׍*�

summaries/loss�è@
}
summaries/histogram_loss*a	   �p@   �p@      �?!   �p@) ��L�;@2زv�5f@��h:np@�������:              �?        �V��       p�*	`�����A؍*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �	I�e;@2زv�5f@��h:np@�������:              �?        K�6�       p�*	&�����Aٍ*�

summaries/loss�(�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) y}�E�:@2زv�5f@��h:np@�������:              �?        r6�       p�*	,����Aڍ*�

summaries/loss�<�@
}
summaries/histogram_loss*a	    �G@    �G@      �?!    �G@)  ٶ$M<@2زv�5f@��h:np@�������:              �?        wV2�       p�*	�w����Aۍ*�

summaries/lossM/�@
}
summaries/histogram_loss*a	   ��%@   ��%@      �?!   ��%@)@J���;@2زv�5f@��h:np@�������:              �?        W���       p�*	������A܍*�

summaries/loss0-�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @z^{R:@2زv�5f@��h:np@�������:              �?        G�٦       p�*	M����Aݍ*�

summaries/loss岪@
}
summaries/histogram_loss*a	   �\V@   �\V@      �?!   �\V@)@6�I�t<@2زv�5f@��h:np@�������:              �?        ���D�       p�*	�h����Aލ*�

summaries/loss1%�@
}
summaries/histogram_loss*a	    �$@    �$@      �?!    �$@)@�L��;@2زv�5f@��h:np@�������:              �?        �L�*�       p�*	ص����Aߍ*�

summaries/lossޚ�@
}
summaries/histogram_loss*a	   �[S@   �[S@      �?!   �[S@) !�!�l<@2زv�5f@��h:np@�������:              �?        pw,��       p�*	����A��*�

summaries/loss|u�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ���a=@2زv�5f@��h:np@�������:              �?        �h�̦       p�*	]P����A�*�

summaries/loss e�@
}
summaries/histogram_loss*a	    �,@    �,@      �?!    �,@)  @v�<@2زv�5f@��h:np@�������:              �?        �߬Ҧ       p�*	������A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   �u!@   �u!@      �?!   �u!@)@����;@2زv�5f@��h:np@�������:              �?        ����       p�*	AK����A�*�

summaries/lossꑦ@
}
summaries/histogram_loss*a	   @=�@   @=�@      �?!   @=�@) y��c;@2زv�5f@��h:np@�������:              �?        ��V�       p�*	������A�*�

summaries/loss�
�@
}
summaries/histogram_loss*a	   �U�@   �U�@      �?!   �U�@)@:�=��=@2زv�5f@��h:np@�������:              �?        ׆V��       p�*	 (����A�*�

summaries/losse�@
}
summaries/histogram_loss*a	   ��l@   ��l@      �?!   ��l@) $�z	�<@2زv�5f@��h:np@�������:              �?        ��2ݦ       p�*	ar����A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   `ѽ@   `ѽ@      �?!   `ѽ@)@�/��=@2زv�5f@��h:np@�������:              �?        ���l�       p�*	������A�*�

summaries/loss
o�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) k̓`;@2زv�5f@��h:np@�������:              �?        ڈ�Ц       p�*	�����A�*�

summaries/loss�.�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@6F׍K;@2زv�5f@��h:np@�������:              �?        W�a��       p�*	Vh����A�*�

summaries/losspn�@
}
summaries/histogram_loss*a	    �M@    �M@      �?!    �M@) @�]<@2زv�5f@��h:np@�������:              �?        �玊�       p�*	�����A�*�

summaries/loss�@
}
summaries/histogram_loss*a	   �@�@   �@�@      �?!   �@�@) 4%�=@2زv�5f@��h:np@�������:              �?        A�       p�*	�����A�*�

summaries/loss#ѫ@
}
summaries/histogram_loss*a	   `$z@   `$z@      �?!   `$z@)@�
�C�<@2زv�5f@��h:np@�������:              �?        �/�`�       p�*	�R����A�*�

summaries/loss��@
}
summaries/histogram_loss*a	    =�@    =�@      �?!    =�@) �h�0=@2زv�5f@��h:np@�������:              �?        #�Do�       p�*	�����A�*�

summaries/lossx?�@
}
summaries/histogram_loss*a	    �'@    �'@      �?!    �'@) 7�;@2زv�5f@��h:np@�������:              �?        @�ܶ�       p�*	������A�*�

summaries/loss�ڭ@
}
summaries/histogram_loss*a	    ^�@    ^�@      �?!    ^�@)@�U>i�=@2زv�5f@��h:np@�������:              �?        �����       p�*	�;����A�*�

summaries/loss�F�@
}
summaries/histogram_loss*a	    �(@    �(@      �?!    �(@)@pt,��;@2زv�5f@��h:np@�������:              �?        (��6�       p�*	������A��*�

summaries/loss�	�@
}
summaries/histogram_loss*a	   `<�@   `<�@      �?!   `<�@)@�o�4�:@2زv�5f@��h:np@�������:              �?        9���       p�*	������A�*�

summaries/lossb��@
}
summaries/histogram_loss*a	   @�q@   @�q@      �?!   @�q@) a7��<@2زv�5f@��h:np@�������:              �?        c���       p�*	�����A�*�

summaries/loss�
�@
}
summaries/histogram_loss*a	   �X�@   �X�@      �?!   �X�@) ��
�?;@2زv�5f@��h:np@�������:              �?        ���       p�*	k����A�*�

summaries/lossL�@
}
summaries/histogram_loss*a	   ��\@   ��\@      �?!   ��\@) ���n�9@2!��v�@زv�5f@�������:              �?        5}��       p�*	{�����A�*�

summaries/loss�m�@
}
summaries/histogram_loss*a	   @�m@   @�m@      �?!   @�m@) ��x::@2زv�5f@��h:np@�������:              �?        xO¦       p�*	
����A��*�

summaries/lossd��@
}
summaries/histogram_loss*a	   �lr@   �lr@      �?!   �lr@) ����<@2زv�5f@��h:np@�������:              �?        m�֦       p�*	^����A��*�

summaries/loss]��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�(F�#;@2زv�5f@��h:np@�������:              �?        ��eզ       p�*	������A��*�

summaries/loss�U�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @P��:@2زv�5f@��h:np@�������:              �?        *A�       p�*	������A��*�

summaries/lossr�@
}
summaries/histogram_loss*a	   @�C@   @�C@      �?!   @�C@) �3�qC<@2زv�5f@��h:np@�������:              �?        ^�zA�       p�*	�=����A��*�

summaries/loss�E�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ����;@2زv�5f@��h:np@�������:              �?        ���J�       p�*	������A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   `5@   `5@      �?!   `5@)@��(�;@2زv�5f@��h:np@�������:              �?        ۢ       p�*	�����A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@!'ԇ;@2زv�5f@��h:np@�������:              �?        �4m�       p�*	�����A��*�

summaries/lossn�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) є�`;@2زv�5f@��h:np@�������:              �?        ��*�       p�*	�i����A��*�

summaries/lossQ�@
}
summaries/histogram_loss*a	    #�@    #�@      �?!    #�@)@5�Q;@2زv�5f@��h:np@�������:              �?        MC�n�       p�*	h�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �=<@   �=<@      �?!   �=<@) Q�/<@2زv�5f@��h:np@�������:              �?        ��|��       p�*	�����A��*�

summaries/loss�%�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �,š�<@2زv�5f@��h:np@�������:              �?        �\9�       p�*	UL����A��*�

summaries/loss�¯@
}
summaries/histogram_loss*a	   �Q�@   �Q�@      �?!   �Q�@)@j (�*>@2زv�5f@��h:np@�������:              �?        �PL٦       p�*	՗����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �@   �@      �?!   �@) ��&o>@2زv�5f@��h:np@�������:              �?        =$��       p�*	������A��*�

summaries/loss� �@
}
summaries/histogram_loss*a	    @    @      �?!    @)@��AȚ;@2زv�5f@��h:np@�������:              �?        �SD��       p�*	�5����A��*�

summaries/loss;T�@
}
summaries/histogram_loss*a	   `�J@   `�J@      �?!   `�J@)@f���T<@2زv�5f@��h:np@�������:              �?        �K(��       p�*	������A��*�

summaries/loss*,�@
}
summaries/histogram_loss*a	   @�e@   @�e@      �?!   @�e@) ����<@2زv�5f@��h:np@�������:              �?        *���       p�*	������A��*�

summaries/loss�I�@
}
summaries/histogram_loss*a	   @>i@   @>i@      �?!   @>i@) 1����<@2زv�5f@��h:np@�������:              �?        ~ة|�       p�*	�&����A��*�

summaries/loss> �@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �9T<;@2زv�5f@��h:np@�������:              �?        :og�       p�*	�r����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �P@   �P@      �?!   �P@) d ��c<@2زv�5f@��h:np@�������:              �?        s�       p�*	������A��*�

summaries/loss�ߨ@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �P��;@2زv�5f@��h:np@�������:              �?        ��-f�       p�*		����A��*�

summaries/loss^m�@
}
summaries/histogram_loss*a	   ��m@   ��m@      �?!   ��m@) �K�Ҳ<@2زv�5f@��h:np@�������:              �?        n��-�       p�*		o����A��*�

summaries/lossf�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) )נ3=@2زv�5f@��h:np@�������:              �?        ����       p�*	C�����A��*�

summaries/loss�g�@
}
summaries/histogram_loss*a	   ��L@   ��L@      �?!   ��L@)@����[<@2زv�5f@��h:np@�������:              �?        1��       p�*	-����A��*�

summaries/loss +�@
}
summaries/histogram_loss*a	    `�@    `�@      �?!    `�@)  @NHJ;@2زv�5f@��h:np@�������:              �?        7�#6�       p�*	������A��*�

summaries/loss�~�@
}
summaries/histogram_loss*a	   �ُ@   �ُ@      �?!   �ُ@)@��l:@2زv�5f@��h:np@�������:              �?        Q㵦       p�*	9�����A��*�

summaries/loss2�@
}
summaries/histogram_loss*a	   @�`@   @�`@      �?!   @�`@) q�mx7@2!��v�@زv�5f@�������:              �?        lV�¦       p�*	�A����A��*�

summaries/loss�ͭ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) d�=@2زv�5f@��h:np@�������:              �?        pi�u�       p�*	Տ����A��*�

summaries/loss�\�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) �K"�_>@2زv�5f@��h:np@�������:              �?        �.Q��       p�*	B�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �Q"@   �Q"@      �?!   �Q"@)@j���>@2زv�5f@��h:np@�������:              �?        ߷�7�       p�*	S<����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ��N�u;@2زv�5f@��h:np@�������:              �?        �N�z�       p�*	"�����A��*�

summaries/loss~ܮ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �3L�=@2زv�5f@��h:np@�������:              �?        *f%h�       p�*	������A��*�

summaries/loss1u�@
}
summaries/histogram_loss*a	    �.@    �.@      �?!    �.@)@���
<@2زv�5f@��h:np@�������:              �?        ���1�       p�*	�$����A��*�

summaries/losso��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�D��:@2زv�5f@��h:np@�������:              �?        �>�`�       p�*	'�����A��*�

summaries/lossWs�@
}
summaries/histogram_loss*a	   �j�@   �j�@      �?!   �j�@)@��2r;@2زv�5f@��h:np@�������:              �?        Σ$s�       p�*	������A��*�

summaries/loss�>�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) 9����;@2زv�5f@��h:np@�������:              �?        ���w�       p�*	�3����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �5�@   �5�@      �?!   �5�@) ���5>@2زv�5f@��h:np@�������:              �?        +}6�       p�*	U�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @؝@   @؝@      �?!   @؝@) �Rϱ�:@2زv�5f@��h:np@�������:              �?        +k�       p�*	U�����A��*�

summaries/lossM�@
}
summaries/histogram_loss*a	   ��)@   ��)@      �?!   ��)@) y����;@2زv�5f@��h:np@�������:              �?        c��1�       p�*	�$����A��*�

summaries/loss~1�@
}
summaries/histogram_loss*a	   �/F@   �/F@      �?!   �/F@) �^:qI<@2زv�5f@��h:np@�������:              �?        �q��       p�*	�r����A��*�

summaries/lossו�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�~�5m;@2زv�5f@��h:np@�������:              �?        a��Ŧ       p�*	�����A��*�

summaries/loss�֩@
}
summaries/histogram_loss*a	    �:@    �:@      �?!    �:@)  �.:+<@2زv�5f@��h:np@�������:              �?        ��l��       p�*	�����A��*�

summaries/loss+��@
}
summaries/histogram_loss*a	   `�t@   `�t@      �?!   `�t@)@N����<@2زv�5f@��h:np@�������:              �?        π���       p�*	h^����A��*�

summaries/loss>��@
}
summaries/histogram_loss*a	   ��p@   ��p@      �?!   ��p@) ���j:@2زv�5f@��h:np@�������:              �?        �Ā�       p�*	�����A��*�

summaries/loss�9�@
}
summaries/histogram_loss*a	    2�@    2�@      �?!    2�@) @\d�M=@2زv�5f@��h:np@�������:              �?        ��X�       p�*	v�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��`@   ��`@      �?!   ��`@) I�ؐ<@2زv�5f@��h:np@�������:              �?        6@Ҭ�       p�*	�G����A��*�

summaries/losso��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@Y�}:@2زv�5f@��h:np@�������:              �?        �	Yn�       p�*	������A��*�

summaries/loss$�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) D���J:@2زv�5f@��h:np@�������:              �?        �-��       p�*	 �����A��*�

summaries/loss�Ψ@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ����;@2زv�5f@��h:np@�������:              �?        <�%��       p�*	w-����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @ӗ@   @ӗ@      �?!   @ӗ@) )=�0�:@2زv�5f@��h:np@�������:              �?        �(���       p�*	*w����A��*�

summaries/loss�ŧ@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) y~�|;@2زv�5f@��h:np@�������:              �?        �۹g�       p�*	&�����A��*�

summaries/loss�&�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) *��;@2زv�5f@��h:np@�������:              �?        �ID��       p�*	7����A��*�

summaries/loss�y�@
}
summaries/histogram_loss*a	   �9�@   �9�@      �?!   �9�@) ���uc=@2زv�5f@��h:np@�������:              �?        6��       p�*	�d����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    أ@    أ@      �?!    أ@)@`�*�:@2زv�5f@��h:np@�������:              �?        �k��       p�*	������A��*�

summaries/lossP��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @�il;@2زv�5f@��h:np@�������:              �?        ^�t �       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) 	�lYl=@2زv�5f@��h:np@�������:              �?        ��XS�       p�*	V����A��*�

summaries/loss d�@
}
summaries/histogram_loss*a	    �,@    �,@      �?!    �,@)  AZV<@2زv�5f@��h:np@�������:              �?        mS��       p�*	@�����A��*�

summaries/lossj��@
}
summaries/histogram_loss*a	   @�>@   @�>@      �?!   @�>@) ��H�5<@2زv�5f@��h:np@�������:              �?        Lգ%�       p�*	'�����A��*�

summaries/loss"4�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) !u$�K=@2زv�5f@��h:np@�������:              �?        i�*�       p�*	�W ���A��*�

summaries/loss�:�@
}
summaries/histogram_loss*a	   @[�@   @[�@      �?!   @[�@) i�|uO;@2زv�5f@��h:np@�������:              �?        �T�=�       p�*	� ���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �W�@   �W�@      �?!   �W�@) ����;@2زv�5f@��h:np@�������:              �?        ]v�r�       p�*	����A��*�

summaries/lossc��@
}
summaries/histogram_loss*a	   `,�@   `,�@      �?!   `,�@)@�#�f;@2زv�5f@��h:np@�������:              �?        &�Φ       p�*	�a���A��*�

summaries/loss�׬@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��,=@2زv�5f@��h:np@�������:              �?        ���       p�*	�����A��*�

summaries/loss�K�@
}
summaries/histogram_loss*a	    ti@    ti@      �?!    ti@)@�nۇ�<@2زv�5f@��h:np@�������:              �?        �u�K�       p�*	����A��*�

summaries/loss*��@
}
summaries/histogram_loss*a	   @e@   @e@      �?!   @e@) ��ysB:@2زv�5f@��h:np@�������:              �?        x��̦       p�*	�G���A��*�

summaries/lossF��@
}
summaries/histogram_loss*a	   �h�@   �h�@      �?!   �h�@) ɽ��v;@2زv�5f@��h:np@�������:              �?        ;.�N�       p�*	2����A��*�

summaries/loss\$�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) D�LH;@2زv�5f@��h:np@�������:              �?        N/��       p�*	�����A��*�

summaries/loss�ī@
}
summaries/histogram_loss*a	    �x@    �x@      �?!    �x@)@p��&�<@2زv�5f@��h:np@�������:              �?        r����       p�*	�-���A��*�

summaries/lossÉ�@
}
summaries/histogram_loss*a	   `8�@   `8�@      �?!   `8�@)@��
Z=@2زv�5f@��h:np@�������:              �?        Kz6�       p�*	����A��*�

summaries/loss T�@
}
summaries/histogram_loss*a	    �
@    �
@      �?!    �
@)   䖫;@2زv�5f@��h:np@�������:              �?        �i��       p�*	����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �~�@   �~�@      �?!   �~�@)@fe�<@2زv�5f@��h:np@�������:              �?        ��Ȧ       p�*	� ���A��*�

summaries/lossH��@
}
summaries/histogram_loss*a	    I@    I@      �?!    I@) ���;@2زv�5f@��h:np@�������:              �?        ���       p�*	fk���A��*�

summaries/lossN��@
}
summaries/histogram_loss*a	   �I�@   �I�@      �?!   �I�@) ���f;@2زv�5f@��h:np@�������:              �?        H�!�       p�*	 ����A��*�

summaries/loss�®@
}
summaries/histogram_loss*a	   @S�@   @S�@      �?!   @S�@) )qQG�=@2زv�5f@��h:np@�������:              �?        �4���       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   `�|@   `�|@      �?!   `�|@)@�~�?<:@2زv�5f@��h:np@�������:              �?        �F�;�       p�*	bI���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �q @   �q @      �?!   �q @)@�&G*�;@2زv�5f@��h:np@�������:              �?        ����       p�*	����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ` U@   ` U@      �?!   ` U@)@�=ؕ�9@2!��v�@زv�5f@�������:              �?        ��y�       p�*	����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��=@   ��=@      �?!   ��=@) yo��2<@2زv�5f@��h:np@�������:              �?        �X�ަ       p�*	c)���A*�

summaries/loss�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @���:@2زv�5f@��h:np@�������:              �?        �FR�       p�*	t���AÎ*�

summaries/loss4:�@
}
summaries/histogram_loss*a	   �F'@   �F'@      �?!   �F'@) �樐b9@2!��v�@زv�5f@�������:              �?        y��       p�*	�����AĎ*�

summaries/loss�-�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �Eg�<@2زv�5f@��h:np@�������:              �?        �=���       p�*	����AŎ*�

summaries/loss\��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) D8�8@2!��v�@زv�5f@�������:              �?        M����       p�*	�Y���AƎ*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) $���,9@2!��v�@زv�5f@�������:              �?        V�b�       p�*	�����Aǎ*�

summaries/lossHڝ@
}
summaries/histogram_loss*a	    I�@    I�@      �?!    I�@) ��]U8@2!��v�@زv�5f@�������:              �?        �5ϖ�       p�*	����AȎ*�

summaries/loss(�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) QhŖo5@2{2�.��@!��v�@�������:              �?        �a07�       p�*	ʬ���AɎ*�

summaries/lossa�@
}
summaries/histogram_loss*a	    , @    , @      �?!    , @)@�yXy@>@2زv�5f@��h:np@�������:              �?        ���6�       p�*	�S	���Aʎ*�

summaries/loss�$�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) $�UH�<@2زv�5f@��h:np@�������:              �?        �|E�       p�*	�	���Aˎ*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��P@   ��P@      �?!   ��P@) �O�e<@2زv�5f@��h:np@�������:              �?        �0Ħ       p�*	w
���A̎*�

summaries/loss;:�@
}
summaries/histogram_loss*a	   `G�@   `G�@      �?!   `G�@)@f2�AO;@2زv�5f@��h:np@�������:              �?        n��       p�*	�j
���A͎*�

summaries/loss��@
}
summaries/histogram_loss*a	    < @    < @      �?!    < @)@�aA�P9@2!��v�@زv�5f@�������:              �?        6��_�       p�*	��
���AΎ*�

summaries/loss^�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �����<@2زv�5f@��h:np@�������:              �?        F��       p�*	����Aώ*�

summaries/loss|/�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �W%�=@2زv�5f@��h:np@�������:              �?        �럦       p�*	����AЎ*�

summaries/loss*<�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ���9�<@2زv�5f@��h:np@�������:              �?        b��.�       p�*	G����Aю*�

summaries/loss��@
}
summaries/histogram_loss*a	   @�=@   @�=@      �?!   @�=@) �I��2<@2زv�5f@��h:np@�������:              �?        L���       p�*	�H���AҎ*�

summaries/loss�~�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ���e;@2زv�5f@��h:np@�������:              �?        ب��       p�*	�����Aӎ*�

summaries/loss��@
}
summaries/histogram_loss*a	   �@   �@      �?!   �@)@�ǂ�;@2زv�5f@��h:np@�������:              �?        ��+�       p�*	�����AԎ*�

summaries/loss�˫@
}
summaries/histogram_loss*a	    ty@    ty@      �?!    ty@)  �mj�<@2زv�5f@��h:np@�������:              �?        �%6f�       p�*	C:���AՎ*�

summaries/lossR�@
}
summaries/histogram_loss*a	   @
�@   @
�@      �?!   @
�@) ���{�<@2زv�5f@��h:np@�������:              �?        |���       p�*	���A֎*�

summaries/loss�-�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@�0��=@2زv�5f@��h:np@�������:              �?        ~�m��       p�*	1����A׎*�

summaries/loss���@
}
summaries/histogram_loss*a	   �W7@   �W7@      �?!   �W7@) A)�"<@2زv�5f@��h:np@�������:              �?        ?��U�       p�*	 ���A؎*�

summaries/loss�@
}
summaries/histogram_loss*a	   ��<@   ��<@      �?!   ��<@) �x�/<@2زv�5f@��h:np@�������:              �?        ��q��       p�*	�k���Aَ*�

summaries/lossf��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) )�ɛ�=@2زv�5f@��h:np@�������:              �?        vF�-�       p�*	����Aڎ*�

summaries/lossd+�@
}
summaries/histogram_loss*a	   �l@   �l@      �?!   �l@) į�>�;@2زv�5f@��h:np@�������:              �?        �����       p�*	)���Aێ*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@���j;@2زv�5f@��h:np@�������:              �?        ���0�       p�*	�R���A܎*�

summaries/loss;3�@
}
summaries/histogram_loss*a	   `gF@   `gF@      �?!   `gF@)@�#'J<@2زv�5f@��h:np@�������:              �?        k����       p�*	�����Aݎ*�

summaries/loss|8�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �YM=@2زv�5f@��h:np@�������:              �?        ��*�       p�*	o����Aގ*�

summaries/loss���@
}
summaries/histogram_loss*a	   �v@   �v@      �?!   �v@) �\�+:@2زv�5f@��h:np@�������:              �?        bv       p�*	�A���Aߎ*�

summaries/lossJ��@
}
summaries/histogram_loss*a	   @�r@   @�r@      �?!   @�r@) YA#�7@2!��v�@زv�5f@�������:              �?        �=E�       p�*	����A��*�

summaries/loss�Ǡ@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ��ƅ>9@2!��v�@زv�5f@�������:              �?        me��       p�*	�����A�*�

summaries/lossT�@
}
summaries/histogram_loss*a	   ��J@   ��J@      �?!   ��J@) 1LT��9@2!��v�@زv�5f@�������:              �?        �fq �       p�*	B)���A�*�

summaries/loss1�@
}
summaries/histogram_loss*a	   �/�@   �/�@      �?!   �/�@)@@w��<@2زv�5f@��h:np@�������:              �?        �� �       p�*	�����A�*�

summaries/lossM��@
}
summaries/histogram_loss*a	   �I�@   �I�@      �?!   �I�@)@����:@2زv�5f@��h:np@�������:              �?        ����       p�*	N����A�*�

summaries/lossEf�@
}
summaries/histogram_loss*a	   �Ȭ@   �Ȭ@      �?!   �Ȭ@)@�Cs?�:@2زv�5f@��h:np@�������:              �?        �M�a�       p�*	K���A�*�

summaries/loss�`�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �1s�~8@2!��v�@زv�5f@�������:              �?        �"�&�       p�*	�p���A�*�

summaries/lossj9�@
}
summaries/histogram_loss*a	   @-�@   @-�@      �?!   @-�@) ��è:@2زv�5f@��h:np@�������:              �?        o .Ҧ       p�*	�����A�*�

summaries/lossU*�@
}
summaries/histogram_loss*a	   �J�@   �J�@      �?!   �J�@)@ N:�<@2زv�5f@��h:np@�������:              �?        C�&ͦ       p�*	f���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   �z@   �z@      �?!   �z@) ��D>@2زv�5f@��h:np@�������:              �?        ���t�       p�*	Bb���A�*�

summaries/loss�]�@
}
summaries/histogram_loss*a	   @�k@   @�k@      �?!   @�k@) 	枫�<@2زv�5f@��h:np@�������:              �?        _�YU�       p�*	|����A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@ȝW=�=@2زv�5f@��h:np@�������:              �?        5����       p�*	����A�*�

summaries/lossh�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@���>^;@2زv�5f@��h:np@�������:              �?        S�+�       p�*	?U���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   �1u@   �1u@      �?!   �1u@)@xV��<@2زv�5f@��h:np@�������:              �?        ��:Ц       p�*	�����A�*�

summaries/loss|p�@
}
summaries/histogram_loss*a	   �n@   �n@      �?!   �n@) /�ݳ<@2زv�5f@��h:np@�������:              �?        Ĩ��       p�*	�����A�*�

summaries/lossٻ�@
}
summaries/histogram_loss*a	    {�@    {�@      �?!    {�@)@|/��y;@2زv�5f@��h:np@�������:              �?        �f�ۦ       p�*	�5���A�*�

summaries/lossnY�@
}
summaries/histogram_loss*a	   �-�@   �-�@      �?!   �-�@) �j0sY;@2زv�5f@��h:np@�������:              �?        C��       p�*	�}���A��*�

summaries/loss�Z�@
}
summaries/histogram_loss*a	   �TK@   �TK@      �?!   �TK@) �H$!W<@2زv�5f@��h:np@�������:              �?        ��>�       p�*	d����A�*�

summaries/loss5`�@
}
summaries/histogram_loss*a	   �@   �@      �?!   �@)@��m��;@2زv�5f@��h:np@�������:              �?        !�1 �       p�*	����A�*�

summaries/lossa�@
}
summaries/histogram_loss*a	    L @    L @      �?!    L @)@0j�ǐ;@2زv�5f@��h:np@�������:              �?        zGͦ       p�*	ݐ���A�*�

summaries/losss�@
}
summaries/histogram_loss*a	   `�=@   `�=@      �?!   `�=@)@ꎎ�2<@2زv�5f@��h:np@�������:              �?        Q�y�       p�*	�����A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) A���:@2زv�5f@��h:np@�������:              �?        � bl�       p�*	�<���A��*�

summaries/loss�-�@
}
summaries/histogram_loss*a	   @�e@   @�e@      �?!   @�e@) 9M�� :@2!��v�@زv�5f@�������:              �?        {�DA�       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �Șo�:@2زv�5f@��h:np@�������:              �?        �f<h�       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �ڭ)�:@2زv�5f@��h:np@�������:              �?        !�֦       p�*	����A��*�

summaries/loss7��@
}
summaries/histogram_loss*a	   �v@   �v@      �?!   �v@)@�j�8�<@2زv�5f@��h:np@�������:              �?        W�<ܦ       p�*	�f���A��*�

summaries/loss�~�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ��>@2زv�5f@��h:np@�������:              �?        �Lx��       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �c�@   �c�@      �?!   �c�@) �J��=@2زv�5f@��h:np@�������:              �?        ë<��       p�*	�����A��*�

summaries/loss(��@
}
summaries/histogram_loss*a	    �r@    �r@      �?!    �r@) ��+�!:@2زv�5f@��h:np@�������:              �?        �=�3�       p�*	�E���A��*�

summaries/loss�[�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) H��:@2زv�5f@��h:np@�������:              �?        !!w]�       p�*	ґ���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   � �@   � �@      �?!   � �@)@h���=@2زv�5f@��h:np@�������:              �?        }�O!�       p�*	/����A��*�

summaries/loss;��@
}
summaries/histogram_loss*a	   `�0@   `�0@      �?!   `�0@)@�Q��<@2زv�5f@��h:np@�������:              �?        uj͌�       p�*	(���A��*�

summaries/lossx��@
}
summaries/histogram_loss*a	    o�@    o�@      �?!    o�@) "�8=@2زv�5f@��h:np@�������:              �?        ����       p�*	Xu���A��*�

summaries/loss�b�@
}
summaries/histogram_loss*a	    Q�@    Q�@      �?!    Q�@)@TK�#=@2زv�5f@��h:np@�������:              �?        S�bѦ       p�*	�����A��*�

summaries/loss�̦@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) Q-�+;@2زv�5f@��h:np@�������:              �?        ��ʦ       p�*	�
���A��*�

summaries/loss�ɩ@
}
summaries/histogram_loss*a	   �79@   �79@      �?!   �79@) A���&<@2زv�5f@��h:np@�������:              �?        D�       p�*	�X���A��*�

summaries/loss�C�@
}
summaries/histogram_loss*a	    {�@    {�@      �?!    {�@) ��reR;@2زv�5f@��h:np@�������:              �?        �F�Ŧ       p�*	q����A��*�

summaries/loss"<�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) !�7�T>@2زv�5f@��h:np@�������:              �?        X����       p�*	�~���A��*�

summaries/loss�.�@
}
summaries/histogram_loss*a	   �څ@   �څ@      �?!   �څ@) �7\��<@2زv�5f@��h:np@�������:              �?        wF��       p�*	L����A��*�

summaries/lossy��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@�Lc�=@2زv�5f@��h:np@�������:              �?        �����       p�*	�N���A��*�

summaries/lossA�@
}
summaries/histogram_loss*a	    �<@    �<@      �?!    �<@)@�.�0<@2زv�5f@��h:np@�������:              �?        �.A�       p�*	f����A��*�

summaries/loss�˨@
}
summaries/histogram_loss*a	    @    @      �?!    @)@6U�;@2زv�5f@��h:np@�������:              �?        w|�       p�*	����A��*�

summaries/loss8��@
}
summaries/histogram_loss*a	    �u@    �u@      �?!    �u@) /�7�<@2زv�5f@��h:np@�������:              �?        p���       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @_B@   @_B@      �?!   @_B@) 	\M?<@2زv�5f@��h:np@�������:              �?        "N�       p�*	>����A��*�

summaries/lossk�@
}
summaries/histogram_loss*a	   `- @   `- @      �?!   `- @)@��w�;@2زv�5f@��h:np@�������:              �?        N�=��       p�*	rT���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �o0�A=@2زv�5f@��h:np@�������:              �?        ��$�       p�*	����A��*�

summaries/loss.��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) k��z=@2زv�5f@��h:np@�������:              �?        �eq�       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �p@   �p@      �?!   �p@) �b��;@2زv�5f@��h:np@�������:              �?        AM�m�       p�*	�; ���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@P�=>�<@2زv�5f@��h:np@�������:              �?        37M�       p�*	�� ���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��a@   ��a@      �?!   ��a@) A6,��9@2!��v�@زv�5f@�������:              �?        �/��       p�*	� ���A��*�

summaries/lossbE�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) a~��=@2زv�5f@��h:np@�������:              �?        3�<Ʀ       p�*	l!���A��*�

summaries/loss�`�@
}
summaries/histogram_loss*a	   �@   �@      �?!   �@) B�Yf�@@2��h:np@S���߮@�������:              �?        ��b�       p�*	�l!���A��*�

summaries/lossPƩ@
}
summaries/histogram_loss*a	    �8@    �8@      �?!    �8@) @���%<@2زv�5f@��h:np@�������:              �?        >�sq�       p�*	�!���A��*�

summaries/lossW�@
}
summaries/histogram_loss*a	   ��J@   ��J@      �?!   ��J@) d����9@2!��v�@زv�5f@�������:              �?        s�;��       p�*	>"���A��*�

summaries/loss(I�@
}
summaries/histogram_loss*a	    %�@    %�@      �?!    %�@) ��1� ;@2زv�5f@��h:np@�������:              �?        ��-�       p�*	�X"���A��*�

summaries/loss�V�@
}
summaries/histogram_loss*a	    �J@    �J@      �?!    �J@)@@�#�U<@2زv�5f@��h:np@�������:              �?        �c��       p�*	&�"���A��*�

summaries/lossVE�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) 9�@�Q=@2زv�5f@��h:np@�������:              �?        �Î��       p�*	?�"���A��*�

summaries/loss
�@
}
summaries/histogram_loss*a	   @�/@   @�/@      �?!   @�/@) ��><@2زv�5f@��h:np@�������:              �?        i�Ȧ       p�*	EE#���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) 	���=@2زv�5f@��h:np@�������:              �?        K�K[�       p�*	��#���A��*�

summaries/lossiL�@
}
summaries/histogram_loss*a	    �I@    �I@      �?!    �I@)@Ġ�dR<@2زv�5f@��h:np@�������:              �?        ���N�       p�*	Y�#���A��*�

summaries/loss�	�@
}
summaries/histogram_loss*a	    7@    7@      �?!    7@) �w0�;@2زv�5f@��h:np@�������:              �?        �"o�       p�*	�($���A��*�

summaries/lossEB�@
}
summaries/histogram_loss*a	   �H�@   �H�@      �?!   �H�@)@�鼉�=@2زv�5f@��h:np@�������:              �?        ��N0�       p�*	pw$���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �x_@   �x_@      �?!   �x_@)@,��֌<@2زv�5f@��h:np@�������:              �?        ���T�       p�*	��$���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) 	�H�;@2زv�5f@��h:np@�������:              �?         � C�       p�*	s%���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  ��H�:@2زv�5f@��h:np@�������:              �?        cϯ��       p�*	6^%���A��*�

summaries/lossz׬@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ��S�,=@2زv�5f@��h:np@�������:              �?        �/�Z�       p�*	��%���A��*�

summaries/loss�O�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) ��A�;@2زv�5f@��h:np@�������:              �?        ��f�       p�*	��%���A��*�

summaries/loss!?�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@�P���;@2زv�5f@��h:np@�������:              �?        ~@dH�       p�*	�G&���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  Y�D�;@2زv�5f@��h:np@�������:              �?        Ĕ�V�       p�*	��&���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) /ǘ�;@2زv�5f@��h:np@�������:              �?        ����       p�*	~�&���A��*�

summaries/losst��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) $eߦ=@2زv�5f@��h:np@�������:              �?        �1�P�       p�*	�3'���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �7U@   �7U@      �?!   �7U@)@ o�xq<@2زv�5f@��h:np@�������:              �?        'AA��       p�*	[�'���A��*�

summaries/lossB�@
}
summaries/histogram_loss*a	   @�B@   @�B@      �?!   @�B@) A�Hi�9@2!��v�@زv�5f@�������:              �?        �ӛ�       p�*	!�'���A��*�

summaries/lossF��@
}
summaries/histogram_loss*a	   ��P@   ��P@      �?!   ��P@) �s�Qe<@2زv�5f@��h:np@�������:              �?        �Ԛ��       p�*	1(���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �22@   �22@      �?!   �22@) d?�~9@2!��v�@زv�5f@�������:              �?        gb��       p�*	��(���A��*�

summaries/loss�
�@
}
summaries/histogram_loss*a	   �S@   �S@      �?!   �S@)@�3��B4@2{2�.��@!��v�@�������:              �?        � ��       p�*	�Q)���A��*�

summaries/loss�{�@
}
summaries/histogram_loss*a	    v/@    v/@      �?!    v/@) @�HV�4@2{2�.��@!��v�@�������:              �?        M�
Ħ       p�*	��)���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  ���j;@2زv�5f@��h:np@�������:              �?        ��~�       p�*	Y�)���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �b@   �b@      �?!   �b@) �|�ѓ<@2زv�5f@��h:np@�������:              �?        ŘԦ       p�*	�Q*���A��*�

summaries/loss�2�@
}
summaries/histogram_loss*a	   @S�@   @S�@      �?!   @S�@) )!��L;@2زv�5f@��h:np@�������:              �?        ڎx��       p�*	��*���A��*�

summaries/lossk�@
}
summaries/histogram_loss*a	   `-�@   `-�@      �?!   `-�@)@��F��=@2زv�5f@��h:np@�������:              �?        nqh�       p�*	3�*���A��*�

summaries/loss�	�@
}
summaries/histogram_loss*a	   �7a@   �7a@      �?!   �7a@) ��a��<@2زv�5f@��h:np@�������:              �?        ��5��       p�*	UO+���A��*�

summaries/loss^��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �YWm$=@2زv�5f@��h:np@�������:              �?        �b�       p�*	��+���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	    ^P@    ^P@      �?!    ^P@)@�i�~�9@2!��v�@زv�5f@�������:              �?        �[�       p�*	��+���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �>|@   �>|@      �?!   �>|@)@��a::@2زv�5f@��h:np@�������:              �?        ��ި�       p�*	t@,���A��*�

summaries/loss3�@
}
summaries/histogram_loss*a	   `�|@   `�|@      �?!   `�|@)@
�	k;:@2زv�5f@��h:np@�������:              �?        !p� �       p�*	��,���A��*�

summaries/lossA��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@�����>@2زv�5f@��h:np@�������:              �?        4d�צ       p�*	U�,���A��*�

summaries/loss
ݬ@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �q.=@2زv�5f@��h:np@�������:              �?        �'s$�       p�*	R'-���A��*�

summaries/loss2;�@
}
summaries/histogram_loss*a	   @f�@   @f�@      �?!   @f�@) q;�O;@2زv�5f@��h:np@�������:              �?        qD|�       p�*	�p-���A��*�

summaries/lossw��@
}
summaries/histogram_loss*a	   �Q@   �Q@      �?!   �Q@)@ԩ�af<@2زv�5f@��h:np@�������:              �?        �H��       p�*	i�-���A��*�

summaries/loss&�@
}
summaries/histogram_loss*a	   �$@   �$@      �?!   �$@) i$BY�;@2زv�5f@��h:np@�������:              �?        ���W�       p�*	.���A��*�

summaries/loss%�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) y��?@2��h:np@S���߮@�������:              �?        K���       p�*	"T.���A��*�

summaries/loss�&�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@?t�=@2زv�5f@��h:np@�������:              �?        ^����       p�*	�.���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    �u@    �u@      �?!    �u@)  	�<@2زv�5f@��h:np@�������:              �?        ��É�       p�*	I�.���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@Z
��9@2!��v�@زv�5f@�������:              �?        �T�Ħ       p�*	
M/���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    3�@    3�@      �?!    3�@) ��ty�=@2زv�5f@��h:np@�������:              �?        �k�L�       p�*	 �/���A��*�

summaries/loss�Ц@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@����,;@2زv�5f@��h:np@�������:              �?        �ɛ��       p�*	��/���A��*�

summaries/lossrԠ@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) �@��B9@2!��v�@زv�5f@�������:              �?        �o���       p�*	�;0���A��*�

summaries/loss�*�@
}
summaries/histogram_loss*a	    ^e@    ^e@      �?!    ^e@) @�4��9@2!��v�@زv�5f@�������:              �?        ��       p�*	�0���A*�

summaries/loss=�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) $����<@2زv�5f@��h:np@�������:              �?        ��1|�       p�*	��0���AÏ*�

summaries/lossH��@
}
summaries/histogram_loss*a	    �t@    �t@      �?!    �t@) y9��<@2زv�5f@��h:np@�������:              �?        ����       p�*	 :1���Aď*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@���;@2زv�5f@��h:np@�������:              �?        h�9�       p�*	�1���Aŏ*�

summaries/loss���@
}
summaries/histogram_loss*a	   @�q@   @�q@      �?!   @�q@) �S'��<@2زv�5f@��h:np@�������:              �?        �.Φ       p�*	>�1���AƏ*�

summaries/loss*A�@
}
summaries/histogram_loss*a	   @%�@   @%�@      �?!   @%�@) ��k)�=@2زv�5f@��h:np@�������:              �?        ��-��       p�*	�92���AǏ*�

summaries/loss�ݧ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@~ ���;@2زv�5f@��h:np@�������:              �?        \r��       p�*	ɏ2���Aȏ*�

summaries/lossζ�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) qc:�;@2زv�5f@��h:np@�������:              �?        }�4^�       p�*	�2���Aɏ*�

summaries/lossΗ�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) q*}�t:@2زv�5f@��h:np@�������:              �?        �%���       p�*	83���Aʏ*�

summaries/loss�Y�@
}
summaries/histogram_loss*a	   @9@   @9@      �?!   @9@) ل'~�;@2زv�5f@��h:np@�������:              �?        ���.�       p�*	\�3���Aˏ*�

summaries/loss�e�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) �zj�;@2زv�5f@��h:np@�������:              �?        �j,��       p�*	'�3���A̏*�

summaries/loss�B�@
}
summaries/histogram_loss*a	    RH@    RH@      �?!    RH@) @�#O<@2زv�5f@��h:np@�������:              �?        )"�!�       p�*	�y4���A͏*�

summaries/lossXI�@
}
summaries/histogram_loss*a	    +i@    +i@      �?!    +i@) ��$Ħ<@2زv�5f@��h:np@�������:              �?        ����       p�*	z�4���AΏ*�

summaries/loss�X�@
}
summaries/histogram_loss*a	   �k@   �k@      �?!   �k@) Q6��<@2زv�5f@��h:np@�������:              �?        ��/��       p�*	W$5���AϏ*�

summaries/loss���@
}
summaries/histogram_loss*a	    q�@    q�@      �?!    q�@) �q�:@2زv�5f@��h:np@�������:              �?        !�8�       p�*	$z5���AЏ*�

summaries/loss�/�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@� �CS:@2زv�5f@��h:np@�������:              �?        ����       p�*	��5���Aя*�

summaries/lossXo�@
}
summaries/histogram_loss*a	    �M@    �M@      �?!    �M@) �[S^<@2زv�5f@��h:np@�������:              �?        ����       p�*	�,6���Aҏ*�

summaries/loss��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ����:@2زv�5f@��h:np@�������:              �?        *Kn�       p�*	r�6���Aӏ*�

summaries/lossg��@
}
summaries/histogram_loss*a	   �l�@   �l�@      �?!   �l�@)@�x/L=@2زv�5f@��h:np@�������:              �?        ����       p�*	�7���Aԏ*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��^@   ��^@      �?!   ��^@) dd�
�9@2!��v�@زv�5f@�������:              �?        Q@돦       p�*	�i7���AՏ*�

summaries/lossN��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �Й�;@2زv�5f@��h:np@�������:              �?        b �y�       p�*	]�7���A֏*�

summaries/loss=��@
}
summaries/histogram_loss*a	   ��t@   ��t@      �?!   ��t@)@�J\��<@2زv�5f@��h:np@�������:              �?        �G�       p�*	�%8���A׏*�

summaries/lossv�@
}
summaries/histogram_loss*a	   �N�@   �N�@      �?!   �N�@) �ӂ��=@2زv�5f@��h:np@�������:              �?        _�W̦       p�*	Pr8���A؏*�

summaries/loss��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @�i��;@2زv�5f@��h:np@�������:              �?        �����       p�*	�8���Aُ*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @|��o;@2زv�5f@��h:np@�������:              �?        ��VU�       p�*	#9���Aڏ*�

summaries/loss���@
}
summaries/histogram_loss*a	   �u@   �u@      �?!   �u@) a�(:@2زv�5f@��h:np@�������:              �?        v7��       p�*	�r9���Aۏ*�

summaries/lossH��@
}
summaries/histogram_loss*a	    )�@    )�@      �?!    )�@) i�%=@2زv�5f@��h:np@�������:              �?        %N_J�       p�*	��9���A܏*�

summaries/loss�G�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@x�`� >@2زv�5f@��h:np@�������:              �?        i���       p�*	�:���Aݏ*�

summaries/loss6�@
}
summaries/histogram_loss*a	   ��]@   ��]@      �?!   ��]@) ��j��<@2زv�5f@��h:np@�������:              �?        �n+��       p�*	n:���Aޏ*�

summaries/loss �@
}
summaries/histogram_loss*a	    Ă@    Ă@      �?!    Ă@)  a�K:@2زv�5f@��h:np@�������:              �?        ���N�       p�*	�:���Aߏ*�

summaries/loss�C�@
}
summaries/histogram_loss*a	   �u@   �u@      �?!   �u@) ��8*9@2!��v�@زv�5f@�������:              �?        �lX�       p�*	k;���A��*�

summaries/loss
ר@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) sp��;@2زv�5f@��h:np@�������:              �?        �*��       p�*	}^;���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@hR(R4;@2زv�5f@��h:np@�������:              �?        $�IX�       p�*	 �;���A�*�

summaries/loss{��@
}
summaries/histogram_loss*a	   `os@   `os@      �?!   `os@)@F0E�<@2زv�5f@��h:np@�������:              �?        _WB��       p�*	��;���A�*�

summaries/loss�ɥ@
}
summaries/histogram_loss*a	   �<�@   �<�@      �?!   �<�@) ���w�:@2زv�5f@��h:np@�������:              �?        �I�m�       p�*	�D<���A�*�

summaries/loss�,�@
}
summaries/histogram_loss*a	   ��E@   ��E@      �?!   ��E@) Ds� ?@2زv�5f@��h:np@�������:              �?        �i��       p�*	՘<���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) Z'�:@2زv�5f@��h:np@�������:              �?        �.�       p�*	��<���A�*�

summaries/loss{��@
}
summaries/histogram_loss*a	   `o�@   `o�@      �?!   `o�@)@Fc�c�=@2زv�5f@��h:np@�������:              �?        �[��       p�*	�3=���A�*�

summaries/loss6Ӧ@
}
summaries/histogram_loss*a	   �f�@   �f�@      �?!   �f�@) ك�-;@2زv�5f@��h:np@�������:              �?        ��eT�       p�*	=�=���A�*�

summaries/loss8x�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) #)�:@2زv�5f@��h:np@�������:              �?        |γ��       p�*	��=���A�*�

summaries/lossP|�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @f���8@2!��v�@زv�5f@�������:              �?        ���ۦ       p�*	�$>���A�*�

summaries/lossR9�@
}
summaries/histogram_loss*a	   @*g@   @*g@      �?!   @*g@) �g���7@2!��v�@زv�5f@�������:              �?        ۦ       p�*	io>���A�*�

summaries/loss�b�@
}
summaries/histogram_loss*a	   @Z�@   @Z�@      �?!   @Z�@) ]W�8@2!��v�@زv�5f@�������:              �?        Nф�       p�*	��>���A�*�

summaries/loss�U�@
}
summaries/histogram_loss*a	   ��j@   ��j@      �?!   ��j@) IE�>�7@2!��v�@زv�5f@�������:              �?        :��       p�*	#?���A�*�

summaries/loss>��@
}
summaries/histogram_loss*a	   ��_@   ��_@      �?!   ��_@) ���u7@2!��v�@زv�5f@�������:              �?        �a��       p�*	$b?���A�*�

summaries/loss�J�@
}
summaries/histogram_loss*a	   `R�@   `R�@      �?!   `R�@)@��M�:@2زv�5f@��h:np@�������:              �?        �u���       p�*	�?���A�*�

summaries/loss�~�@
}
summaries/histogram_loss*a	   ��/@   ��/@      �?!   ��/@)@���<@2زv�5f@��h:np@�������:              �?        V�䫦       p�*	� @���A��*�

summaries/loss6%�@
}
summaries/histogram_loss*a	   ��$@   ��$@      �?!   ��$@) �)���;@2زv�5f@��h:np@�������:              �?        ��[�       p�*	�M@���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �ԩ:�;@2زv�5f@��h:np@�������:              �?        ysڦ       p�*	$�@���A�*�

summaries/loss�z�@
}
summaries/histogram_loss*a	   @S/@   @S/@      �?!   @S/@) )��v9@2!��v�@زv�5f@�������:              �?        �u�       p�*	��@���A�*�

summaries/loss=K�@
}
summaries/histogram_loss*a	   �g)@   �g)@      �?!   �g)@)@"�)�;@2زv�5f@��h:np@�������:              �?        �Y�       p�*	�6A���A�*�

summaries/loss_2�@
}
summaries/histogram_loss*a	   �KF@   �KF@      �?!   �KF@)@�O�I<@2زv�5f@��h:np@�������:              �?        9���       p�*	4�A���A��*�

summaries/loss�e�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@��!�:@2زv�5f@��h:np@�������:              �?        ��1�       p�*	�A���A��*�

summaries/loss�[�@
}
summaries/histogram_loss*a	   �|�@   �|�@      �?!   �|�@) �TvUY=@2زv�5f@��h:np@�������:              �?        �p��       p�*	�B���A��*�

summaries/loss�>�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) �q��;@2زv�5f@��h:np@�������:              �?        ?�{�       p�*	/nB���A��*�

summaries/losst�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@g��:@2زv�5f@��h:np@�������:              �?        u��Ħ       p�*	��B���A��*�

summaries/loss�L�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  ��CU;@2زv�5f@��h:np@�������:              �?        ᳇-�       p�*	0C���A��*�

summaries/loss�2�@
}
summaries/histogram_loss*a	   @]f@   @]f@      �?!   @]f@) yx:@2زv�5f@��h:np@�������:              �?        �"��       p�*	�[C���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) 1��=@2زv�5f@��h:np@�������:              �?        �z�ަ       p�*	m�C���A��*�

summaries/loss� �@
}
summaries/histogram_loss*a	   �@@   �@@      �?!   �@@) d6\N9<@2زv�5f@��h:np@�������:              �?        ieA�       p�*	b�C���A��*�

summaries/loss�͢@
}
summaries/histogram_loss*a	   ��Y@   ��Y@      �?!   ��Y@) Q·Q�9@2!��v�@زv�5f@�������:              �?        �,�#�       p�*	�LD���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �?@   �?@      �?!   �?@) ��j�6<@2زv�5f@��h:np@�������:              �?        T$»�       p�*	ݘD���A��*�

summaries/loss
��@
}
summaries/histogram_loss*a	   @�v@   @�v@      �?!   @�v@) ����<@2زv�5f@��h:np@�������:              �?        ,�)�       p�*	��D���A��*�

summaries/loss&X�@
}
summaries/histogram_loss*a	   �K@   �K@      �?!   �K@) i�4LV<@2زv�5f@��h:np@�������:              �?        j�*�       p�*	*7E���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��<@   ��<@      �?!   ��<@)@�C��>@2زv�5f@��h:np@�������:              �?        
6��       p�*		�E���A��*�

summaries/loss�ߪ@
}
summaries/histogram_loss*a	   ��[@   ��[@      �?!   ��[@) �&�u�<@2زv�5f@��h:np@�������:              �?        �JA��       p�*	g�E���A��*�

summaries/lossy�@
}
summaries/histogram_loss*a	   @ �@   @ �@      �?!   @ �@) ���j:@2زv�5f@��h:np@�������:              �?        QKT�       p�*	�#F���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) 	: f�8@2!��v�@زv�5f@�������:              �?        ?&���       p�*	�pF���A��*�

summaries/lossdz�@
}
summaries/histogram_loss*a	   �Lo@   �Lo@      �?!   �Lo@) ��/�<@2زv�5f@��h:np@�������:              �?        �0�$�       p�*	D�F���A��*�

summaries/lossp��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @�cez=@2زv�5f@��h:np@�������:              �?        ֛��       p�*	G���A��*�

summaries/loss5�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�pJ�3=@2زv�5f@��h:np@�������:              �?        #i�i�       p�*	�[G���A��*�

summaries/loss�.�@
}
summaries/histogram_loss*a	   `�%@   `�%@      �?!   `�%@)@z�r�>@2زv�5f@��h:np@�������:              �?        x���       p�*	��G���A��*�

summaries/loss�0�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) $3���:@2زv�5f@��h:np@�������:              �?        &5gU�       p�*	��G���A��*�

summaries/loss�V�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��.;@2زv�5f@��h:np@�������:              �?        
�^��       p�*	�JH���A��*�

summaries/losslh�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) d����?@2��h:np@S���߮@�������:              �?        D[�2�       p�*	�H���A��*�

summaries/loss!�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@���0=@2زv�5f@��h:np@�������:              �?        �v�/�       p�*	�wI���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   @`}@   @`}@      �?!   @`}@) +"��?@2��h:np@S���߮@�������:              �?        Z ���       p�*	�J���A��*�

summaries/loss/ѩ@
}
summaries/histogram_loss*a	   �%:@   �%:@      �?!   �%:@)@��v)<@2زv�5f@��h:np@�������:              �?        ��-�       p�*	�\J���A��*�

summaries/lossA��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@�B�;@2زv�5f@��h:np@�������:              �?        �S;E�       p�*	m�J���A��*�

summaries/loss�"�@
}
summaries/histogram_loss*a	   �^$@   �^$@      �?!   �^$@) �d��;@2زv�5f@��h:np@�������:              �?        �v,��       p�*	U�J���A��*�

summaries/lossW*�@
}
summaries/histogram_loss*a	   �J�@   �J�@      �?!   �J�@)@d*�:�<@2زv�5f@��h:np@�������:              �?        SL�R�       p�*	�FK���A��*�

summaries/loss@��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  � m;@2زv�5f@��h:np@�������:              �?        ��e?�       p�*	5�K���A��*�

summaries/loss�O�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) )���<@2زv�5f@��h:np@�������:              �?        1W^�       p�*	�K���A��*�

summaries/loss�ة@
}
summaries/histogram_loss*a	   �;@   �;@      �?!   �;@)@�q��+<@2زv�5f@��h:np@�������:              �?        ~���       p�*	�5L���A��*�

summaries/loss�d�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ٛ��=@2زv�5f@��h:np@�������:              �?        j4#Ӧ       p�*	�L���A��*�

summaries/lossD�@
}
summaries/histogram_loss*a	   ��h@   ��h@      �?!   ��h@) ����<@2زv�5f@��h:np@�������:              �?        ��|"�       p�*	p�L���A��*�

summaries/loss$T�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) D駸W;@2زv�5f@��h:np@�������:              �?        q!���       p�*	~M���A��*�

summaries/lossl��@
}
summaries/histogram_loss*a	   �m?@   �m?@      �?!   �m?@) d=�z7<@2زv�5f@��h:np@�������:              �?        ��%�       p�*	iM���A��*�

summaries/loss�m�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ) C�;@2زv�5f@��h:np@�������:              �?        �:y�       p�*	ϽM���A��*�

summaries/loss,��@
}
summaries/histogram_loss*a	   �e�@   �e�@      �?!   �e�@) ��Io�:@2زv�5f@��h:np@�������:              �?        ����       p�*	sN���A��*�

summaries/loss7�@
}
summaries/histogram_loss*a	   ��=@   ��=@      �?!   ��=@)@�ܤn2<@2زv�5f@��h:np@�������:              �?        #Ӛe�       p�*	]N���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@ &Qa�:@2زv�5f@��h:np@�������:              �?        ���       p�*	1�N���A��*�

summaries/loss�D�@
}
summaries/histogram_loss*a	   ��(@   ��(@      �?!   ��(@)@F;���>@2زv�5f@��h:np@�������:              �?        \1�v�       p�*	��N���A��*�

summaries/loss�L�@
}
summaries/histogram_loss*a	   ��I@   ��I@      �?!   ��I@) ��шR<@2زv�5f@��h:np@�������:              �?        !�eަ       p�*	+OO���A��*�

summaries/loss[�@
}
summaries/histogram_loss*a	   `�`@   `�`@      �?!   `�`@)@��<@2زv�5f@��h:np@�������:              �?        YW�       p�*	�O���A��*�

summaries/lossZF�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) �U�3X>@2زv�5f@��h:np@�������:              �?        ���Ϧ       p�*	��O���A��*�

summaries/lossaϰ@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@��Ws�>@2زv�5f@��h:np@�������:              �?        f�2�       p�*	L7P���A��*�

summaries/lossuͩ@
}
summaries/histogram_loss*a	   ��9@   ��9@      �?!   ��9@)@ޥW:(<@2زv�5f@��h:np@�������:              �?        uB��       p�*	M�P���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    5"@    5"@      �?!    5"@) ��A�;@2زv�5f@��h:np@�������:              �?        �̚ئ       p�*	��P���A��*�

summaries/lossx�@
}
summaries/histogram_loss*a	    Ϣ@    Ϣ@      �?!    Ϣ@) 6
�A=@2زv�5f@��h:np@�������:              �?        �3U�       p�*	�%Q���A��*�

summaries/lossgp�@
}
summaries/histogram_loss*a	   �N@   �N@      �?!   �N@)@\��^^<@2زv�5f@��h:np@�������:              �?        Ŕ���       p�*	�rQ���A��*�

summaries/lossī�@
}
summaries/histogram_loss*a	   �xu@   �xu@      �?!   �xu@) �۵):@2زv�5f@��h:np@�������:              �?        �f6�       p�*	�Q���A��*�

summaries/lossOx�@
}
summaries/histogram_loss*a	   �	/@   �	/@      �?!   �	/@)@
6<@2زv�5f@��h:np@�������:              �?        &�O��       p�*	R���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ��T�=@2زv�5f@��h:np@�������:              �?        k��       p�*	,_R���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ="@    ="@      �?!    ="@)@���)�;@2زv�5f@��h:np@�������:              �?        ��=�       p�*	m�R���A��*�

summaries/loss].�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@rc@
�:@2زv�5f@��h:np@�������:              �?        M�@̦       p�*	��R���A��*�

summaries/loss3�@
}
summaries/histogram_loss*a	   @b&@   @b&@      �?!   @b&@) Q�Q`9@2!��v�@زv�5f@�������:              �?        C���       p�*	�JS���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) iz���:@2زv�5f@��h:np@�������:              �?        �&�       p�*	ڕS���A��*�

summaries/loss�#�@
}
summaries/histogram_loss*a	   �w@   �w@      �?!   �w@) A`���;@2زv�5f@��h:np@�������:              �?        {��o�       p�*	��S���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �;�ؾ=@2زv�5f@��h:np@�������:              �?        zOiO�       p�*	s,T���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @�B@   @�B@      �?!   @�B@) 9j�@<@2زv�5f@��h:np@�������:              �?        �2�˦       p�*	BzT���A��*�

summaries/loss4��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ���C:@2زv�5f@��h:np@�������:              �?        4�5��       p�*	��T���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    �3@    �3@      �?!    �3@) ���>@2زv�5f@��h:np@�������:              �?        9�\u�       p�*	�U���A��*�

summaries/lossfʣ@
}
summaries/histogram_loss*a	   �Ly@   �Ly@      �?!   �Ly@) )Hz�2:@2زv�5f@��h:np@�������:              �?        P�MM�       p�*	�gU���A��*�

summaries/loss�K�@
}
summaries/histogram_loss*a	   �i@   �i@      �?!   �i@) � w
:@2زv�5f@��h:np@�������:              �?        �]�       p�*	 �U���A��*�

summaries/loss|Ĩ@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �c��;@2زv�5f@��h:np@�������:              �?        �����       p�*	�U���A��*�

summaries/lossR�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ����<@2زv�5f@��h:np@�������:              �?        @�w��       p�*	�MV���A��*�

summaries/loss�a�@
}
summaries/histogram_loss*a	    1�@    1�@      �?!    1�@) &\;@2زv�5f@��h:np@�������:              �?        hjsv�       p�*	z�V���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �1�@   �1�@      �?!   �1�@) ����<;@2زv�5f@��h:np@�������:              �?        '��Z�       p�*	�W���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �a�@   �a�@      �?!   �a�@) $B��=@2زv�5f@��h:np@�������:              �?        T6d��       p�*	�kW���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   `�Q@   `�Q@      �?!   `�Q@)@���g<@2زv�5f@��h:np@�������:              �?        �)<��       p�*	I�W���A��*�

summaries/loss�n�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@N�6%f>@2زv�5f@��h:np@�������:              �?        �-{�       p�*	X���A��*�

summaries/loss �@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  @E�:@2زv�5f@��h:np@�������:              �?        O٘�       p�*	RX���A��*�

summaries/lossN�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �'�D>@2زv�5f@��h:np@�������:              �?        [��       p�*	�X���A��*�

summaries/losslά@
}
summaries/histogram_loss*a	   �͙@   �͙@      �?!   �͙@) d�)=@2زv�5f@��h:np@�������:              �?        �&���       p�*	r�X���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �1�@   �1�@      �?!   �1�@) $)�E�=@2زv�5f@��h:np@�������:              �?        ��\�       p�*	IY���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �X@   �X@      �?!   �X@) ėy<@2زv�5f@��h:np@�������:              �?        ��˦       p�*	�Y���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    u@    u@      �?!    u@)@@�Ϣ�<@2زv�5f@��h:np@�������:              �?        �/�z�       p�*	v�Y���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @ؒ��;@2زv�5f@��h:np@�������:              �?        �B��       p�*	�(Z���A*�

summaries/loss�Ǩ@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�t)��;@2زv�5f@��h:np@�������:              �?        �6��       p�*	!xZ���AÐ*�

summaries/lossq��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@���t=@2زv�5f@��h:np@�������:              �?        tA��       p�*	��Z���AĐ*�

summaries/loss	A�@
}
summaries/histogram_loss*a	    !�@    !�@      �?!    !�@)@�d�X:@2زv�5f@��h:np@�������:              �?        6}�ʦ       p�*	[���AŐ*�

summaries/lossX@�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) ��P=@2زv�5f@��h:np@�������:              �?        �]b�       p�*	a[���AƐ*�

summaries/loss.��@
}
summaries/histogram_loss*a	   �e@   �e@      �?!   �e@) ��G�;@2زv�5f@��h:np@�������:              �?        U��K�       p�*	�[���Aǐ*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��\@   ��\@      �?!   ��\@) �N�=�9@2!��v�@زv�5f@�������:              �?        �m?r�       p�*	�\���AȐ*�

summaries/loss��@
}
summaries/histogram_loss*a	   �!�@   �!�@      �?!   �!�@) $v�9;@2زv�5f@��h:np@�������:              �?        �MϦ       p�*	CZ\���Aɐ*�

summaries/lossV|�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) 9�RP�=@2زv�5f@��h:np@�������:              �?        \����       p�*	ߧ\���Aʐ*�

summaries/lossk�@
}
summaries/histogram_loss*a	   `�"@   `�"@      �?!   `�"@)@.$#MW9@2!��v�@زv�5f@�������:              �?        ��zצ       p�*	��\���Aː*�

summaries/lossp�@
}
summaries/histogram_loss*a	    N @    N @      �?!    N @) @|�̐;@2زv�5f@��h:np@�������:              �?        ���ڦ       p�*	@]���A̐*�

summaries/loss+~�@
}
summaries/histogram_loss*a	   `�o@   `�o@      �?!   `�o@)@��r�<@2زv�5f@��h:np@�������:              �?        W�Y�       p�*	��]���A͐*�

summaries/loss�ק@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �
ق;@2زv�5f@��h:np@�������:              �?        �t�٦       p�*	��]���Aΐ*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  ��g�=@2زv�5f@��h:np@�������:              �?        ��m�       p�*	�4^���Aϐ*�

summaries/loss2Ϧ@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) qy"W,;@2زv�5f@��h:np@�������:              �?        ��8ަ       p�*	�^���AА*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �1���:@2زv�5f@��h:np@�������:              �?        �C�d�       p�*	[�^���Aѐ*�

summaries/loss�}�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) q�>�;@2زv�5f@��h:np@�������:              �?        �&O�       p�*	�_���AҐ*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@D��B=@2زv�5f@��h:np@�������:              �?        �U�_�       p�*	�k_���AӐ*�

summaries/loss*ե@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ��h�:@2زv�5f@��h:np@�������:              �?        ��`�       p�*	ϸ_���AԐ*�

summaries/lossHܨ@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) �w�;@2زv�5f@��h:np@�������:              �?        �[e�       p�*	�`���AՐ*�

summaries/loss�)�@
}
summaries/histogram_loss*a	    2�@    2�@      �?!    2�@) @��QQ:@2زv�5f@��h:np@�������:              �?        �$i;�       p�*	�W`���A֐*�

summaries/loss|�@
}
summaries/histogram_loss*a	   �/|@   �/|@      �?!   �/|@) ͐��<@2زv�5f@��h:np@�������:              �?        L�[=�       p�*	T�`���Aא*�

summaries/loss@p�@
}
summaries/histogram_loss*a	    .@    .@      �?!    .@)  nY	<@2زv�5f@��h:np@�������:              �?        b`�;�       p�*		�`���Aؐ*�

summaries/loss&��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) i `�m>@2زv�5f@��h:np@�������:              �?        ��8��       p�*	�La���Aِ*�

summaries/loss�e�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �8h!�:@2زv�5f@��h:np@�������:              �?        Y�A�       p�*	Ùa���Aڐ*�

summaries/loss=�@
}
summaries/histogram_loss*a	   �g@   �g@      �?!   �g@)@"Oq��;@2زv�5f@��h:np@�������:              �?        {w��       p�*	��a���Aې*�

summaries/loss1��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@�p���=@2زv�5f@��h:np@�������:              �?        ��;�       p�*	.8b���Aܐ*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �p�9w=@2زv�5f@��h:np@�������:              �?        ī/��       p�*	4�b���Aݐ*�

summaries/loss���@
}
summaries/histogram_loss*a	   @�?@   @�?@      �?!   @�?@) �]�8<@2زv�5f@��h:np@�������:              �?        ��i�       p�*	��b���Aސ*�

summaries/loss][�@
}
summaries/histogram_loss*a	   �kk@   �kk@      �?!   �kk@)@�O�ˬ<@2زv�5f@��h:np@�������:              �?        �h:æ       p�*	�!c���Aߐ*�

summaries/loss�a�@
}
summaries/histogram_loss*a	   �:�@   �:�@      �?!   �:�@) ����;@2زv�5f@��h:np@�������:              �?        eGGU�       p�*	alc���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �t@   �t@      �?!   �t@) �ȉ�<@2زv�5f@��h:np@�������:              �?        !�ؼ�       p�*	E�c���A�*�

summaries/loss�E�@
}
summaries/histogram_loss*a	   ��H@   ��H@      �?!   ��H@) �]�7	?@2زv�5f@��h:np@�������:              �?        �t�a�       p�*	(	d���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	    �"@    �"@      �?!    �"@)@�"�)�;@2زv�5f@��h:np@�������:              �?        �X�զ       p�*	\Ud���A�*�

summaries/loss弩@
}
summaries/histogram_loss*a	   ��7@   ��7@      �?!   ��7@)@6�n�"<@2زv�5f@��h:np@�������:              �?        
�N�       p�*	��d���A�*�

summaries/loss&ɨ@
}
summaries/histogram_loss*a	   �$@   �$@      �?!   �$@) i,�'�;@2زv�5f@��h:np@�������:              �?        ��ۦ       p�*	��d���A�*�

summaries/loss B�@
}
summaries/histogram_loss*a	    D�@    D�@      �?!    D�@)  !�2Y:@2زv�5f@��h:np@�������:              �?        ١.)�       p�*	+6e���A�*�

summaries/lossD�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @ �wR;@2زv�5f@��h:np@�������:              �?        �3Qn�       p�*	ge���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) 鑦z�;@2زv�5f@��h:np@�������:              �?        �� ��       p�*	1�e���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   �26@   �26@      �?!   �26@)@.X&6�9@2!��v�@زv�5f@�������:              �?        �F= �       p�*	�f���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) Q�S1�:@2زv�5f@��h:np@�������:              �?        \_$�       p�*	�hf���A�*�

summaries/loss�M�@
}
summaries/histogram_loss*a	   ൉@   ൉@      �?!   ൉@)@h���\:@2زv�5f@��h:np@�������:              �?        ����       p�*	o�f���A�*�

summaries/lossqh�@
}
summaries/histogram_loss*a	    M@    M@      �?!    M@)@x h�9@2!��v�@زv�5f@�������:              �?        ��+q�       p�*	z�f���A�*�

summaries/loss�b�@
}
summaries/histogram_loss*a	   `Sl@   `Sl@      �?!   `Sl@)@vB׭:@2زv�5f@��h:np@�������:              �?        �5c�       p�*	�Kg���A�*�

summaries/loss�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�?�+�:@2زv�5f@��h:np@�������:              �?        /7��       p�*	��g���A�*�

summaries/loss�ئ@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) ��q/;@2زv�5f@��h:np@�������:              �?        �Mt�       p�*	��g���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) q����;@2زv�5f@��h:np@�������:              �?        �Ƒ��       p�*	�<h���A�*�

summaries/lossS��@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@��v�;@2زv�5f@��h:np@�������:              �?        Z	��       p�*	��h���A�*�

summaries/loss�ם@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) ��T8@2!��v�@زv�5f@�������:              �?        �飦       p�*	�Ei���A�*�

summaries/lossR�@
}
summaries/histogram_loss*a	   @*@   @*@      �?!   @*@) �G@�9@2!��v�@زv�5f@�������:              �?        -O��       p�*	Ѵi���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  DI>)>@2زv�5f@��h:np@�������:              �?        �4Eܦ       p�*	�j���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   `ށ@   `ށ@      �?!   `ށ@)@����<@2زv�5f@��h:np@�������:              �?        
J��       p�*	eoj���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @#Q@   @#Q@      �?!   @#Q@) �5��f<@2زv�5f@��h:np@�������:              �?        ��.��       p�*	��j���A��*�

summaries/lossyi�@
}
summaries/histogram_loss*a	    /-@    /-@      �?!    /-@)@̞1��>@2زv�5f@��h:np@�������:              �?        � '0�       p�*	nk���A��*�

summaries/lossj�@
}
summaries/histogram_loss*a	   @M=@   @M=@      �?!   @M=@) �|��1<@2زv�5f@��h:np@�������:              �?        ����       p�*	�hk���A��*�

summaries/loss ��@
}
summaries/histogram_loss*a	    D@    D@      �?!    D@)  �gN9@2!��v�@زv�5f@�������:              �?        �,h��       p�*	��k���A��*�

summaries/loss�z�@
}
summaries/histogram_loss*a	   �]�@   �]�@      �?!   �]�@) dr�c=@2زv�5f@��h:np@�������:              �?        �S3�       p�*	l���A��*�

summaries/loss�ڣ@
}
summaries/histogram_loss*a	   �S{@   �S{@      �?!   �S{@)@�KK8:@2زv�5f@��h:np@�������:              �?        B%��       p�*	TVl���A��*�

summaries/lossb�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) a��}B=@2زv�5f@��h:np@�������:              �?        <8�զ       p�*	ңl���A��*�

summaries/loss�N�@
}
summaries/histogram_loss*a	   �ݩ@   �ݩ@      �?!   �ݩ@) d*#��:@2زv�5f@��h:np@�������:              �?        ��       p�*	k�l���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@
t���;@2زv�5f@��h:np@�������:              �?        �a���       p�*	�Am���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �S@   �S@      �?!   �S@) �~r�9@2!��v�@زv�5f@�������:              �?        �\c��       p�*	*�m���A��*�

summaries/lossG�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@l����:@2زv�5f@��h:np@�������:              �?        p��{�       p�*	4�m���A��*�

summaries/loss˷�@
}
summaries/histogram_loss*a	   `�6@   `�6@      �?!   `�6@)@�v~!<@2زv�5f@��h:np@�������:              �?        ���       p�*	g&n���A��*�

summaries/lossJ��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) Y���:@2زv�5f@��h:np@�������:              �?        h���       p�*	hwn���A��*�

summaries/loss�
�@
}
summaries/histogram_loss*a	    ]�@    ]�@      �?!    ]�@) ��m~G:@2زv�5f@��h:np@�������:              �?        �����       p�*	��n���A��*�

summaries/loss/,�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@(����8@2!��v�@زv�5f@�������:              �?        ���>�       p�*	�o���A��*�

summaries/loss݋�@
}
summaries/histogram_loss*a	   �{�@   �{�@      �?!   �{�@)@2��i;@2زv�5f@��h:np@�������:              �?        �h܇�       p�*	tbo���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �p@   �p@      �?!   �p@) �Bg��>@2زv�5f@��h:np@�������:              �?        ����       p�*	`�o���A��*�

summaries/lossU��@
}
summaries/histogram_loss*a	   �*�@   �*�@      �?!   �*�@)@��OW�:@2زv�5f@��h:np@�������:              �?        �����       p�*	Gp���A��*�

summaries/loss.��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �H�;@2زv�5f@��h:np@�������:              �?        �~�g�       p�*	�Wp���A��*�

summaries/lossv��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��M!;@2زv�5f@��h:np@�������:              �?        ��W��       p�*	j�p���A��*�

summaries/loss;;�@
}
summaries/histogram_loss*a	   `g�@   `g�@      �?!   `g�@)@��7�:@2زv�5f@��h:np@�������:              �?        kY<�       p�*	0�p���A��*�

summaries/losspd�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @��l9@2!��v�@زv�5f@�������:              �?        �|���       p�*	�?q���A��*�

summaries/loss)��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@��"iC:@2زv�5f@��h:np@�������:              �?        �|��       p�*	��q���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @;@   @;@      �?!   @;@) i����;@2زv�5f@��h:np@�������:              �?        �R+�       p�*	<�q���A��*�

summaries/loss�[�@
}
summaries/histogram_loss*a	   �v�@   �v�@      �?!   �v�@)@4��;@2زv�5f@��h:np@�������:              �?        ��f�       p�*	,r���A��*�

summaries/loss>Ч@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �3�V�;@2زv�5f@��h:np@�������:              �?        �.F�       p�*	�yr���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @�_@   @�_@      �?!   @�_@) Q���<@2زv�5f@��h:np@�������:              �?        r8���       p�*	��r���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @pv@   @pv@      �?!   @pv@) ��bS�<@2زv�5f@��h:np@�������:              �?        -��U�       p�*	�s���A��*�

summaries/loss�$�@
}
summaries/histogram_loss*a	   ��$@   ��$@      �?!   ��$@) ���f�;@2زv�5f@��h:np@�������:              �?        �
��       p�*	1ds���A��*�

summaries/loss�Ȫ@
}
summaries/histogram_loss*a	   @Y@   @Y@      �?!   @Y@) �q�{<@2زv�5f@��h:np@�������:              �?        H���       p�*	��s���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �=��/=@2زv�5f@��h:np@�������:              �?        ҿo�       p�*	��s���A��*�

summaries/loss�&�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@��:�=@2زv�5f@��h:np@�������:              �?        ��{�       p�*	QKt���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) dZo�:@2زv�5f@��h:np@�������:              �?        dP�k�       p�*	��t���A��*�

summaries/loss7l�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@����!9@2!��v�@زv�5f@�������:              �?        �h�       p�*	��t���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @1]@   @1]@      �?!   @1]@) ���9@2!��v�@زv�5f@�������:              �?        ��4�       p�*	5u���A��*�

summaries/lossr�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ��9@2!��v�@زv�5f@�������:              �?        �7�       p�*	��u���A��*�

summaries/lossGx�@
}
summaries/histogram_loss*a	   �o@   �o@      �?!   �o@)@�(��:@2زv�5f@��h:np@�������:              �?        ܺ��       p�*	��u���A��*�

summaries/loss�`�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) IF�c:@2زv�5f@��h:np@�������:              �?        ��;J�       p�*	� v���A��*�

summaries/loss�x�@
}
summaries/histogram_loss*a	   �O@   �O@      �?!   �O@)@ح�*a<@2زv�5f@��h:np@�������:              �?        �"�ݦ       p�*	�ov���A��*�

summaries/lossS<�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@��#VW:@2زv�5f@��h:np@�������:              �?        �Z�       p�*	��v���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    Tu@    Tu@      �?!    Tu@)  9^�(:@2زv�5f@��h:np@�������:              �?        �6��       p�*	�Iw���A��*�

summaries/lossvզ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �d�a.;@2زv�5f@��h:np@�������:              �?        �j�Q�       p�*	��w���A��*�

summaries/lossI&�@
}
summaries/histogram_loss*a	    �d@    �d@      �?!    �d@)@4��
�<@2زv�5f@��h:np@�������:              �?        �W�^�       p�*	�w���A��*�

summaries/loss`�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@*�?�[;@2زv�5f@��h:np@�������:              �?        qg��       p�*	�4x���A��*�

summaries/loss�[�@
}
summaries/histogram_loss*a	   �q+@   �q+@      �?!   �q+@) $5�<@2زv�5f@��h:np@�������:              �?        ��F�       p�*	�x���A��*�

summaries/loss�$�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��M�<@2زv�5f@��h:np@�������:              �?        ��Q�       p�*	��x���A��*�

summaries/lossSӨ@
}
summaries/histogram_loss*a	   `j@   `j@      �?!   `j@)@:{؂�;@2زv�5f@��h:np@�������:              �?        �3��       p�*	� y���A��*�

summaries/loss�Ѯ@
}
summaries/histogram_loss*a	   �8�@   �8�@      �?!   �8�@) I9Cu�=@2زv�5f@��h:np@�������:              �?        >o��       p�*	/my���A��*�

summaries/lossў�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@�)���8@2!��v�@زv�5f@�������:              �?        ��T�       p�*	�y���A��*�

summaries/loss,��@
}
summaries/histogram_loss*a	   �x@   �x@      �?!   �x@) ��/:@2زv�5f@��h:np@�������:              �?        �F���       p�*	�z���A��*�

summaries/losss��@
}
summaries/histogram_loss*a	   `ε@   `ε@      �?!   `ε@)@�Ǖ�:@2زv�5f@��h:np@�������:              �?        ��n]�       p�*	Rz���A��*�

summaries/loss:��@
}
summaries/histogram_loss*a	   @g@   @g@      �?!   @g@) Ir+~�>@2زv�5f@��h:np@�������:              �?        �n���       p�*	P�z���A��*�

summaries/lossԲ@
}
summaries/histogram_loss*a	    �Z@    �Z@      �?!    �Z@) ��E�:?@2زv�5f@��h:np@�������:              �?        �I��       p�*	��z���A��*�

summaries/lossf��@
}
summaries/histogram_loss*a	   �lv@   �lv@      �?!   �lv@) )��?@2��h:np@S���߮@�������:              �?        J��       p�*	�:{���A��*�

summaries/loss�)�@
}
summaries/histogram_loss*a	   @0e@   @0e@      �?!   @0e@) ����<@2زv�5f@��h:np@�������:              �?        ;�Y�       p�*	E�{���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �B_@   �B_@      �?!   �B_@) y�aF�<@2زv�5f@��h:np@�������:              �?        �ï�       p�*	��{���A��*�

summaries/lossl�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) d���:@2زv�5f@��h:np@�������:              �?        �1�Y�       p�*	�(|���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	     @     @      �?!     @) @0qL<=@2زv�5f@��h:np@�������:              �?        z���       p�*	z|���A��*�

summaries/loss:��@
}
summaries/histogram_loss*a	   @8@   @8@      �?!   @8@) I�:�#<@2زv�5f@��h:np@�������:              �?        ثgަ       p�*	�|���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @�O@   @�O@      �?!   @�O@) )�nc<@2زv�5f@��h:np@�������:              �?        �z��       p�*	�}���A��*�

summaries/loss"�@
}
summaries/histogram_loss*a	   �C�@   �C�@      �?!   �C�@)@��?bG;@2زv�5f@��h:np@�������:              �?        �kŢ�       p�*	�_}���A��*�

summaries/loss�o�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ����;@2زv�5f@��h:np@�������:              �?        -N���       p�*	X�}���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@°�.{;@2زv�5f@��h:np@�������:              �?        ��Ԧ       p�*	&�}���A��*�

summaries/loss�l�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �TE�:@2زv�5f@��h:np@�������:              �?        ��hF�       p�*	�J~���A��*�

summaries/loss8��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �lh;@2زv�5f@��h:np@�������:              �?        cu�       p�*	��~���A��*�

summaries/loss� �@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) ��eDE=@2زv�5f@��h:np@�������:              �?        �@菦       p�*	e�~���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��Q@   ��Q@      �?!   ��Q@) ��	��9@2!��v�@زv�5f@�������:              �?        ��q �       p�*	4���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �>�@   �>�@      �?!   �>�@)@/aB;@2زv�5f@��h:np@�������:              �?        ��j�       p�*	����A��*�

summaries/lossn�@
}
summaries/histogram_loss*a	   �-�@   �-�@      �?!   �-�@) ѪL�B=@2زv�5f@��h:np@�������:              �?        �       p�*	O����A��*�

summaries/lossbܢ@
}
summaries/histogram_loss*a	   @�[@   @�[@      �?!   @�[@) a%p��9@2!��v�@زv�5f@�������:              �?        _^7��       p�*	/����A��*�

summaries/loss�@
}
summaries/histogram_loss*a	    #<@    #<@      �?!    #<@) ���.<@2زv�5f@��h:np@�������:              �?        ��"�       p�*	�g����A��*�

summaries/lossxӱ@
}
summaries/histogram_loss*a	    o:@    o:@      �?!    o:@) §��>@2زv�5f@��h:np@�������:              �?        Ri��       p�*	޲����A��*�

summaries/loss�զ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �f�i.;@2زv�5f@��h:np@�������:              �?        ����       p�*	� ����A��*�

summaries/lossyq�@
}
summaries/histogram_loss*a	    /@    /@      �?!    /@)@�GH�;@2زv�5f@��h:np@�������:              �?        ��(�       p�*	X����A��*�

summaries/loss6ά@
}
summaries/histogram_loss*a	   �ƙ@   �ƙ@      �?!   �ƙ@) ټ�o)=@2زv�5f@��h:np@�������:              �?        �����       p�*	������A��*�

summaries/loss�ާ@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @n��;@2زv�5f@��h:np@�������:              �?        ig�{�       p�*	����A��*�

summaries/losse��@
}
summaries/histogram_loss*a	   �L�@   �L�@      �?!   �L�@)@�Z�U�=@2زv�5f@��h:np@�������:              �?        \d5x�       p�*	�J����A*�

summaries/loss�-�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�$��=@2زv�5f@��h:np@�������:              �?        9�{��       p�*	ؗ����AÑ*�

summaries/loss��@
}
summaries/histogram_loss*a	    C�@    C�@      �?!    C�@)@�1��:@2زv�5f@��h:np@�������:              �?        ���/�       p�*	�����Ađ*�

summaries/losso��@
}
summaries/histogram_loss*a	   �Q@   �Q@      �?!   �Q@)@��c	h<@2زv�5f@��h:np@�������:              �?        7g�(�       p�*	L9����Aő*�

summaries/loss��@
}
summaries/histogram_loss*a	   �!�@   �!�@      �?!   �!�@)@����9;@2زv�5f@��h:np@�������:              �?        ��2�       p�*	'�����AƑ*�

summaries/loss���@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@J�z;@2زv�5f@��h:np@�������:              �?        �cjf�       p�*	0փ���AǑ*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��C@   ��C@      �?!   ��C@)@�td&C<@2زv�5f@��h:np@�������:              �?        ,�!M�       p�*	g(����Aȑ*�

summaries/loss1�@
}
summaries/histogram_loss*a	    &=@    &=@      �?!    &=@)@��m1<@2زv�5f@��h:np@�������:              �?        ���=�       p�*	uu����Aɑ*�

summaries/lossp��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @$iaq:@2زv�5f@��h:np@�������:              �?        Emp¦       p�*	���Aʑ*�

summaries/loss���@
}
summaries/histogram_loss*a	   �5x@   �5x@      �?!   �5x@) ��0:@2زv�5f@��h:np@�������:              �?        �˳`�       p�*	H����Aˑ*�

summaries/loss!v�@
}
summaries/histogram_loss*a	    �N@    �N@      �?!    �N@)@�n�9@2!��v�@زv�5f@�������:              �?        O��ͦ       p�*	�v����A̑*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��Q@   ��Q@      �?!   ��Q@)@RZ��h<@2زv�5f@��h:np@�������:              �?        G�h��       p�*	υ���A͑*�

summaries/loss���@
}
summaries/histogram_loss*a	   @\�@   @\�@      �?!   @\�@) �LW�:@2زv�5f@��h:np@�������:              �?        �&���       p�*	T����AΑ*�

summaries/loss��@
}
summaries/histogram_loss*a	   �>�@   �>�@      �?!   �>�@) �B;@2زv�5f@��h:np@�������:              �?        ��צ       p�*	�i����Aϑ*�

summaries/loss^��@
}
summaries/histogram_loss*a	   �k5@   �k5@      �?!   �k5@) ���?�9@2!��v�@زv�5f@�������:              �?        ���p�       p�*	x�����AБ*�

summaries/lossέ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) 1�1	�=@2زv�5f@��h:np@�������:              �?        ���D�       p�*	:����Aё*�

summaries/loss�o�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�&�af>@2زv�5f@��h:np@�������:              �?        r��       p�*	"T����Aґ*�

summaries/loss�׮@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �EDm�=@2زv�5f@��h:np@�������:              �?        ���       p�*	�����Aӑ*�

summaries/loss�c�@
}
summaries/histogram_loss*a	   @v�@   @v�@      �?!   @v�@) �ɀ�=@2زv�5f@��h:np@�������:              �?        Qul�       p�*	9����Aԑ*�

summaries/loss���@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@��;;@2زv�5f@��h:np@�������:              �?        ̰\v�       p�*	�@����AՑ*�

summaries/lossO�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@���@;@2زv�5f@��h:np@�������:              �?        c����       p�*	͏����A֑*�

summaries/loss��@
}
summaries/histogram_loss*a	   @r@   @r@      �?!   @r@) �{��;@2زv�5f@��h:np@�������:              �?        9��,�       p�*	'����Aב*�

summaries/loss��@
}
summaries/histogram_loss*a	    S�@    S�@      �?!    S�@) �.�Z�;@2زv�5f@��h:np@�������:              �?        ����       p�*	T�����Aؑ*�

summaries/loss�2�@
}
summaries/histogram_loss*a	   @^�@   @^�@      �?!   @^�@) 1ۅ��:@2زv�5f@��h:np@�������:              �?        ]��A�       p�*	�U����Aّ*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�$Z��=@2زv�5f@��h:np@�������:              �?        ��
"�       p�*	x�����Aڑ*�

summaries/lossN�@
}
summaries/histogram_loss*a	   �i�@   �i�@      �?!   �i�@) ��J�:@2زv�5f@��h:np@�������:              �?        Y��G�       p�*	�����Aۑ*�

summaries/lossL�@
}
summaries/histogram_loss*a	   ��	@   ��	@      �?!   ��	@) �t���;@2زv�5f@��h:np@�������:              �?        BBـ�       p�*	M����Aܑ*�

summaries/loss�'�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) $�^�P:@2زv�5f@��h:np@�������:              �?        ]ճ�       p�*	U�����Aݑ*�

summaries/loss���@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) �F')99@2!��v�@زv�5f@�������:              �?        y"��       p�*	�����Aޑ*�

summaries/loss�+�@
}
summaries/histogram_loss*a	   @p%@   @p%@      �?!   @p%@) �;B��;@2زv�5f@��h:np@�������:              �?        on�n�       p�*	>A����Aߑ*�

summaries/loss6�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �����:@2زv�5f@��h:np@�������:              �?        �"a'�       p�*	����A��*�

summaries/loss�Y�@
}
summaries/histogram_loss*a	   �:�@   �:�@      �?!   �:�@) �{<@@2��h:np@S���߮@�������:              �?        &��N�       p�*	�ٌ���A�*�

summaries/loss8H�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) ���=@2زv�5f@��h:np@�������:              �?        �]@�       p�*	�'����A�*�

summaries/lossZ	�@
}
summaries/histogram_loss*a	   @+@   @+@      �?!   @+@) ��5�9@2!��v�@زv�5f@�������:              �?        i5_��       p�*	�r����A�*�

summaries/loss�խ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �M��=@2زv�5f@��h:np@�������:              �?        .|�       p�*	0ҍ���A�*�

summaries/lossޞ�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) !�*p;@2زv�5f@��h:np@�������:              �?        �,���       p�*	�"����A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   � �@   � �@      �?!   � �@)@���j=@2زv�5f@��h:np@�������:              �?        ϖi��       p�*	�u����A�*�

summaries/lossN�@
}
summaries/histogram_loss*a	   �) @   �) @      �?!   �) @) �l?��;@2زv�5f@��h:np@�������:              �?        �n;צ       p�*	�Ȏ���A�*�

summaries/loss�ˢ@
}
summaries/histogram_loss*a	   @Y@   @Y@      �?!   @Y@) 	����9@2!��v�@زv�5f@�������:              �?        _9��       p�*	�����A�*�

summaries/lossm;�@
}
summaries/histogram_loss*a	   �m'@   �m'@      �?!   �m'@)@[���;@2زv�5f@��h:np@�������:              �?        ���f�       p�*	�`����A�*�

summaries/loss�U�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  ��;@2زv�5f@��h:np@�������:              �?        Y���       p�*	_�����A�*�

summaries/lossB��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) Aj�.!>@2زv�5f@��h:np@�������:              �?        i�@P�       p�*	h����A�*�

summaries/loss~@�@
}
summaries/histogram_loss*a	   �@   �@      �?!   �@) ��g-�;@2زv�5f@��h:np@�������:              �?        ��,�       p�*	UO����A�*�

summaries/loss�M�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@#R�\:@2زv�5f@��h:np@�������:              �?        M��ܦ       p�*	������A�*�

summaries/lossAݧ@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@�ҁ��;@2زv�5f@��h:np@�������:              �?        q����       p�*	�����A�*�

summaries/lossK/�@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@�'ˆ�;@2زv�5f@��h:np@�������:              �?        �oo�       p�*	�=����A�*�

summaries/loss_�@
}
summaries/histogram_loss*a	   ��b@   ��b@      �?!   ��b@)@Н�_�9@2!��v�@زv�5f@�������:              �?        ����       p�*	덑���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	    ]�@    ]�@      �?!    ]�@) �����:@2زv�5f@��h:np@�������:              �?        M��'�       p�*	�ܑ���A�*�

summaries/loss�Ϥ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) Q�tĆ:@2زv�5f@��h:np@�������:              �?        �`tH�       p�*	.����A�*�

summaries/lossⷥ@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �8Z��:@2زv�5f@��h:np@�������:              �?        ���       p�*	~����A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   �3�@   �3�@      �?!   �3�@) a��k;@2زv�5f@��h:np@�������:              �?        ����       p�*	K˒���A��*�

summaries/lossp�@
}
summaries/histogram_loss*a	    n�@    n�@      �?!    n�@) @�#�0=@2زv�5f@��h:np@�������:              �?        �{�Ӧ       p�*	(����A��*�

summaries/lossB��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) A�lZ�;@2زv�5f@��h:np@�������:              �?        ����       p�*	�s����A��*�

summaries/loss�l�@
}
summaries/histogram_loss*a	   ��-@   ��-@      �?!   ��-@)@�hiur9@2!��v�@زv�5f@�������:              �?        �N���       p�*	������A��*�

summaries/loss#v�@
}
summaries/histogram_loss*a	   `Ď@   `Ď@      �?!   `Ď@)@2��i:@2زv�5f@��h:np@�������:              �?        �)r�       p�*	����A��*�

summaries/loss�,�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) iy��:@2زv�5f@��h:np@�������:              �?        P���       p�*	OX����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   `�a@   `�a@      �?!   `�a@)@B�|n�<@2زv�5f@��h:np@�������:              �?        x�tl�       p�*	������A��*�

summaries/loss-ˮ@
}
summaries/histogram_loss*a	   �e�@   �e�@      �?!   �e�@)@z�4�=@2زv�5f@��h:np@�������:              �?        1�Y\�       p�*	g����A��*�

summaries/losswѣ@
}
summaries/histogram_loss*a	   �.z@   �.z@      �?!   �.z@)@Ta;5:@2زv�5f@��h:np@�������:              �?        æ       p�*	�<����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �q\@   �q\@      �?!   �q\@) $e�1�9@2!��v�@زv�5f@�������:              �?        ���ʦ       p�*	�����A��*�

summaries/loss]p�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@r`{z;@2زv�5f@��h:np@�������:              �?        ��f��       p�*	�ڕ���A��*�

summaries/loss�V�@
}
summaries/histogram_loss*a	   ��J@   ��J@      �?!   ��J@) Y���U<@2زv�5f@��h:np@�������:              �?        �Q�:�       p�*	�&����A��*�

summaries/loss L�@
}
summaries/histogram_loss*a	    �I@    �I@      �?!    �I@)  �HLR<@2زv�5f@��h:np@�������:              �?        ��       p�*	Bu����A��*�

summaries/lossjѫ@
}
summaries/histogram_loss*a	   @-z@   @-z@      �?!   @-z@) ���[�<@2زv�5f@��h:np@�������:              �?        �~���       p�*	�Җ���A��*�

summaries/losss5�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@j4�!U:@2زv�5f@��h:np@�������:              �?        �4�       p�*	kE����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @0�@   @0�@      �?!   @0�@) ���>�;@2زv�5f@��h:np@�������:              �?        �B�       p�*	O�����A��*�

summaries/lossVŢ@
}
summaries/histogram_loss*a	   ��X@   ��X@      �?!   ��X@) 9^=��9@2!��v�@زv�5f@�������:              �?        1Qצ       p�*	]ޗ���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) �`l��:@2زv�5f@��h:np@�������:              �?        -]���       p�*	�(����A��*�

summaries/loss|��@
}
summaries/histogram_loss*a	   �o�@   �o�@      �?!   �o�@) ��wx:@2زv�5f@��h:np@�������:              �?        $H�G�       p�*	�t����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) y\���;@2زv�5f@��h:np@�������:              �?        ma�Ҧ       p�*	����A��*�

summaries/loss>�@
}
summaries/histogram_loss*a	   �=@   �=@      �?!   �=@) ���>@2زv�5f@��h:np@�������:              �?        кܙ�       p�*	�����A��*�

summaries/loss�K�@
}
summaries/histogram_loss*a	   @~�@   @~�@      �?!   @~�@) 1���S=@2زv�5f@��h:np@�������:              �?        ��@:�       p�*	]����A��*�

summaries/loss/��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@(B1��:@2زv�5f@��h:np@�������:              �?        �w�3�       p�*	������A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   `1�@   `1�@      �?!   `1�@)@^�r��8@2!��v�@زv�5f@�������:              �?        3@#�       p�*	������A��*�

summaries/loss<�@
}
summaries/histogram_loss*a	   ��'@   ��'@      �?!   ��'@) 1��%c9@2!��v�@زv�5f@�������:              �?        H���       p�*	�C����A��*�

summaries/lossԌ�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �#�,9@2!��v�@زv�5f@�������:              �?        ��       p�*	֎����A��*�

summaries/losso�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@z�^�:@2زv�5f@��h:np@�������:              �?        "(��       p�*	7�����A��*�

summaries/loss�"�@
}
summaries/histogram_loss*a	    [D@    [D@      �?!    [D@) ����9@2!��v�@زv�5f@�������:              �?        �;�/�       p�*	n3����A��*�

summaries/loss&�@
}
summaries/histogram_loss*a	   �D|@   �D|@      �?!   �D|@) iǢ��<@2زv�5f@��h:np@�������:              �?        }
EB�       p�*	�~����A��*�

summaries/loss�m�@
}
summaries/histogram_loss*a	   @�-@   @�-@      �?!   @�-@) �C�<@2زv�5f@��h:np@�������:              �?        ��r��       p�*	W͛���A��*�

summaries/loss�-�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @�~K;@2زv�5f@��h:np@�������:              �?        n
��       p�*	�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �`^�;@2زv�5f@��h:np@�������:              �?        ����       p�*	�i����A��*�

summaries/lossPۤ@
}
summaries/histogram_loss*a	    j�@    j�@      �?!    j�@) @~�n�:@2زv�5f@��h:np@�������:              �?        m쵁�       p�*	1�����A��*�

summaries/lossTǩ@
}
summaries/histogram_loss*a	   ��8@   ��8@      �?!   ��8@) ��2&<@2زv�5f@��h:np@�������:              �?        P+�`�       p�*	#����A��*�

summaries/loss`)�@
}
summaries/histogram_loss*a	    ,@    ,@      �?!    ,@)  �+��;@2زv�5f@��h:np@�������:              �?        ���       p�*	`����A��*�

summaries/lossj8�@
}
summaries/histogram_loss*a	   @'@   @'@      �?!   @'@) ����;@2زv�5f@��h:np@�������:              �?        y�G��       p�*	⮝���A��*�

summaries/loss�ԥ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �����:@2زv�5f@��h:np@�������:              �?        �/U�       p�*	������A��*�

summaries/lossbp�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ay�h:@2زv�5f@��h:np@�������:              �?        D����       p�*	0O����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   `� @   `� @      �?!   `� @)@J8�2�;@2زv�5f@��h:np@�������:              �?        �[��       p�*	8�����A��*�

summaries/lossż�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@���y;@2زv�5f@��h:np@�������:              �?        DT�M�       p�*	�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �P@   �P@      �?!   �P@)@�~@�c<@2زv�5f@��h:np@�������:              �?        �ǥ��       p�*	D����A��*�

summaries/loss�ר@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) ����;@2زv�5f@��h:np@�������:              �?        2|�_�       p�*	J�����A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �!k#�=@2زv�5f@��h:np@�������:              �?        ���       p�*	�����A��*�

summaries/losst�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) $?�\�:@2زv�5f@��h:np@�������:              �?        ����       p�*	�2����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) D���:@2زv�5f@��h:np@�������:              �?        ]7�g�       p�*	ߊ����A��*�

summaries/lossIg�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@��c>@2زv�5f@��h:np@�������:              �?        ���       p�*	A֠���A��*�

summaries/loss�]�@
}
summaries/histogram_loss*a	    �K@    �K@      �?!    �K@)@����9@2!��v�@زv�5f@�������:              �?        lW��       p�*	c"����A��*�

summaries/loss�?�@
}
summaries/histogram_loss*a	    �'@    �'@      �?!    �'@) @�>�;@2زv�5f@��h:np@�������:              �?        hMڦ       p�*	@m����A��*�

summaries/lossI;�@
}
summaries/histogram_loss*a	    iG@    iG@      �?!    iG@)@����L<@2زv�5f@��h:np@�������:              �?        ���       p�*	켡���A��*�

summaries/lossT��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �N�"�:@2زv�5f@��h:np@�������:              �?        `�(�       p�*	�	����A��*�

summaries/loss$Y�@
}
summaries/histogram_loss*a	   �$�@   �$�@      �?!   �$�@) D���=@2زv�5f@��h:np@�������:              �?        �Kd�       p�*	�T����A��*�

summaries/loss:��@
}
summaries/histogram_loss*a	   @G�@   @G�@      �?!   @G�@) I�@H�:@2زv�5f@��h:np@�������:              �?        }�na�       p�*		�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   `<c@   `<c@      �?!   `<c@)@�l~�9@2!��v�@زv�5f@�������:              �?        zy��       p�*	�����A��*�

summaries/losslc�@
}
summaries/histogram_loss*a	   �m�@   �m�@      �?!   �m�@) d-a�\;@2زv�5f@��h:np@�������:              �?        ��b��       p�*	>����A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   `#�@   `#�@      �?!   `#�@)@6
D��:@2زv�5f@��h:np@�������:              �?        A�S��       p�*	o�����A��*�

summaries/loss�7�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) Q&]�;@2زv�5f@��h:np@�������:              �?        ����       p�*	�գ���A��*�

summaries/losskޫ@
}
summaries/histogram_loss*a	   `�{@   `�{@      �?!   `�{@)@.���<@2زv�5f@��h:np@�������:              �?        ��DO�       p�*	%$����A��*�

summaries/lossa�@
}
summaries/histogram_loss*a	    � @    � @      �?!    � @)@�;_�A>@2زv�5f@��h:np@�������:              �?        �k�       p�*	v����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    p�@    p�@      �?!    p�@)  g,8;@2زv�5f@��h:np@�������:              �?        4]���       p�*	�ˤ���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ��	��;@2زv�5f@��h:np@�������:              �?        :��Ŧ       p�*	Q����A��*�

summaries/loss?�@
}
summaries/histogram_loss*a	   ��G@   ��G@      �?!   ��G@) $���M<@2زv�5f@��h:np@�������:              �?        [Է�       p�*	bd����A��*�

summaries/loss:�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) I�I5�:@2زv�5f@��h:np@�������:              �?        ��f#�       p�*	-�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��>@   ��>@      �?!   ��>@) ę�66<@2زv�5f@��h:np@�������:              �?        �i�       p�*	{�����A��*�

summaries/loss_�@
}
summaries/histogram_loss*a	   �k�@   �k�@      �?!   �k�@)@POڑB;@2زv�5f@��h:np@�������:              �?        F�b�       p�*	�K����A��*�

summaries/lossr�@
}
summaries/histogram_loss*a	    @�@    @�@      �?!    @�@)@ �|a;@2زv�5f@��h:np@�������:              �?        T� ��       p�*	y�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �0�@   �0�@      �?!   �0�@) �D��k;@2زv�5f@��h:np@�������:              �?         ���       p�*	�����A��*�

summaries/lossnF�@
}
summaries/histogram_loss*a	   �ͨ@   �ͨ@      �?!   �ͨ@) �R=@2زv�5f@��h:np@�������:              �?        n�;�       p�*	79����A��*�

summaries/loss�S�@
}
summaries/histogram_loss*a	   @u
@   @u
@      �?!   @u
@) 9�E�\>@2زv�5f@��h:np@�������:              �?        ԫG�       p�*	˃����A��*�

summaries/lossM��@
}
summaries/histogram_loss*a	   ��7@   ��7@      �?!   ��7@)@J��#<@2زv�5f@��h:np@�������:              �?        9�Ƃ�       p�*	Ч���A��*�

summaries/loss
��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) �;��:@2زv�5f@��h:np@�������:              �?        �m�}�       p�*	"����A��*�

summaries/lossv�@
}
summaries/histogram_loss*a	   �n=@   �n=@      �?!   �n=@) �v�.2<@2زv�5f@��h:np@�������:              �?        I����       p�*	jj����A��*�

summaries/loss E�@
}
summaries/histogram_loss*a	    �H@    �H@      �?!    �H@)  @�ٶ9@2!��v�@زv�5f@�������:              �?        &���       p�*	R�����A��*�

summaries/loss0��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @z7g�;@2زv�5f@��h:np@�������:              �?        Ma*�       p�*	c-����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@P�4;@2زv�5f@��h:np@�������:              �?        �q ��       p�*	˩���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) \l��;@2زv�5f@��h:np@�������:              �?        �7��       p�*	�3����A��*�

summaries/loss|1�@
}
summaries/histogram_loss*a	   �/F@   �/F@      �?!   �/F@) -�pI<@2زv�5f@��h:np@�������:              �?        {�l�       p�*	瑪���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   ��b@   ��b@      �?!   ��b@)@@�t�<@2زv�5f@��h:np@�������:              �?        H[�ަ       p�*	~����A��*�

summaries/loss ��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)   dU.9@2!��v�@زv�5f@�������:              �?        B�)��       p�*	�+����A��*�

summaries/loss֖�@
}
summaries/histogram_loss*a	   �ڒ@   �ڒ@      �?!   �ڒ@) ��k�7@2!��v�@زv�5f@�������:              �?        �޺�       p�*	A{����A*�

summaries/loss�]�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) $̺~;@2زv�5f@��h:np@�������:              �?        �v*�       p�*	�ǫ���AÒ*�

summaries/lossEU�@
}
summaries/histogram_loss*a	   ��J@   ��J@      �?!   ��J@)@&�WU<@2زv�5f@��h:np@�������:              �?        ׬�Ħ       p�*	/����AĒ*�

summaries/loss�H�@
}
summaries/histogram_loss*a	    i@    i@      �?!    i@) @����<@2زv�5f@��h:np@�������:              �?        \�@�       p�*	�f����AŒ*�

summaries/loss��@
}
summaries/histogram_loss*a	   � �@   � �@      �?!   � �@)@����=@2زv�5f@��h:np@�������:              �?        ��H��       p�*	������Aƒ*�

summaries/loss�m�@
}
summaries/histogram_loss*a	    �M@    �M@      �?!    �M@) �i��]<@2زv�5f@��h:np@�������:              �?        ��8�       p�*	������Aǒ*�

summaries/loss�e�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �	)
;@2زv�5f@��h:np@�������:              �?        �M���       p�*	�H����AȒ*�

summaries/loss�J�@
}
summaries/histogram_loss*a	   �U)@   �U)@      �?!   �U)@) ��7��;@2زv�5f@��h:np@�������:              �?        �/�       p�*	p�����Aɒ*�

summaries/lossr�@
}
summaries/histogram_loss*a	   @nb@   @nb@      �?!   @nb@) ���<@2زv�5f@��h:np@�������:              �?        ���       p�*	*����Aʒ*�

summaries/loss�N�@
}
summaries/histogram_loss*a	   �҉@   �҉@      �?!   �҉@)@D�l�<@2زv�5f@��h:np@�������:              �?        ����       p�*	�+����A˒*�

summaries/loss/7�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�N�ã=@2زv�5f@��h:np@�������:              �?        ��ͦ       p�*	�v����A̒*�

summaries/loss&R�@
}
summaries/histogram_loss*a	   �D
@   �D
@      �?!   �D
@) i��;@2زv�5f@��h:np@�������:              �?         nЅ�       p�*	
�����A͒*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) 1���:@2زv�5f@��h:np@�������:              �?        *=��       p�*	]����AΒ*�

summaries/loss(ç@
}
summaries/histogram_loss*a	    e�@    e�@      �?!    e�@) �}�|;@2زv�5f@��h:np@�������:              �?        ��=j�       p�*	�a����Aϒ*�

summaries/loss�+�@
}
summaries/histogram_loss*a	   @}�@   @}�@      �?!   @}�@) y~��<@2زv�5f@��h:np@�������:              �?        ؚ�Ҧ       p�*	������AВ*�

summaries/loss^"�@
}
summaries/histogram_loss*a	   �K�@   �K�@      �?!   �K�@) �F���<@2زv�5f@��h:np@�������:              �?        ο7�       p�*	������Aђ*�

summaries/lossUs�@
}
summaries/histogram_loss*a	   �j�@   �j�@      �?!   �j�@)@���w�:@2زv�5f@��h:np@�������:              �?        3�       p�*	�E����AҒ*�

summaries/loss�C�@
}
summaries/histogram_loss*a	    s�@    s�@      �?!    s�@)@\�D�:@2زv�5f@��h:np@�������:              �?        &��u�       p�*	������AӒ*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) db�hv:@2زv�5f@��h:np@�������:              �?        �ٗئ       p�*	L߰���AԒ*�

summaries/loss6^�@
}
summaries/histogram_loss*a	   �ƫ@   �ƫ@      �?!   �ƫ@) ���Z=@2زv�5f@��h:np@�������:              �?        ��ʦ       p�*	�8����AՒ*�

summaries/loss�@
}
summaries/histogram_loss*a	   �/�@   �/�@      �?!   �/�@)@@�y��:@2زv�5f@��h:np@�������:              �?        ��s�       p�*	������A֒*�

summaries/loss���@
}
summaries/histogram_loss*a	   �V@   �V@      �?!   �V@) ��o�79@2!��v�@زv�5f@�������:              �?        �����       p�*	�Ա���Aג*�

summaries/loss�!�@
}
summaries/histogram_loss*a	    >�@    >�@      �?!    >�@) @��RG;@2زv�5f@��h:np@�������:              �?        �����       p�*	`"����Aؒ*�

summaries/loss^��@
}
summaries/histogram_loss*a	   ��t@   ��t@      �?!   ��t@) ��! ':@2زv�5f@��h:np@�������:              �?        �r�       p�*	q����Aْ*�

summaries/loss�l�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@ȡ];@2زv�5f@��h:np@�������:              �?        ����       p�*	]�����Aڒ*�

summaries/loss��@
}
summaries/histogram_loss*a	    �c@    �c@      �?!    �c@)@t��j�9@2!��v�@زv�5f@�������:              �?        '�GR�       p�*	8����Aے*�

summaries/loss��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)  s?�;@2زv�5f@��h:np@�������:              �?        N��       p�*	u[����Aܒ*�

summaries/losst�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) $�j�:@2زv�5f@��h:np@�������:              �?        �,F��       p�*	㦳���Aݒ*�

summaries/lossv̯@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �(K.>@2زv�5f@��h:np@�������:              �?        M�=�       p�*	�����Aޒ*�

summaries/loss��@
}
summaries/histogram_loss*a	   �Y�@   �Y�@      �?!   �Y�@)@؈Y�2;@2زv�5f@��h:np@�������:              �?        ?'�_�       p�*	�@����Aߒ*�

summaries/loss7�@
}
summaries/histogram_loss*a	   �� @   �� @      �?!   �� @)@t^�;@2زv�5f@��h:np@�������:              �?        �w�ߦ       p�*	������A��*�

summaries/loss>��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ����;@2زv�5f@��h:np@�������:              �?        � ��       p�*	fٴ���A�*�

summaries/loss�o�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) ��ZĴ;@2زv�5f@��h:np@�������:              �?        ���       p�*	�&����A�*�

summaries/loss�v�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) Q��;@2زv�5f@��h:np@�������:              �?        '*�ʦ       p�*	�t����A�*�

summaries/loss^ܣ@
}
summaries/histogram_loss*a	   ��{@   ��{@      �?!   ��{@) �lY�8:@2زv�5f@��h:np@�������:              �?        {��<�       p�*	&ĵ���A�*�

summaries/loss�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) }��:@2زv�5f@��h:np@�������:              �?        ����       p�*	�����A�*�

summaries/loss���@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)  1x[=@2زv�5f@��h:np@�������:              �?        P�`8�       p�*	�_����A�*�

summaries/loss�I�@
}
summaries/histogram_loss*a	   �1i@   �1i@      �?!   �1i@) $I�զ<@2زv�5f@��h:np@�������:              �?        ���       p�*	}�����A�*�

summaries/loss�G�@
}
summaries/histogram_loss*a	   ��(@   ��(@      �?!   ��(@) �_��;@2زv�5f@��h:np@�������:              �?        c�       p�*	*����A�*�

summaries/loss畫@
}
summaries/histogram_loss*a	   �r@   �r@      �?!   �r@)@�-�f�<@2زv�5f@��h:np@�������:              �?        Ku�       p�*	*n����A�*�

summaries/lossā�@
}
summaries/histogram_loss*a	   �80@   �80@      �?!   �80@) �ǣ%<@2زv�5f@��h:np@�������:              �?        y�ȣ�       p�*	w�����A�*�

summaries/lossd��@
}
summaries/histogram_loss*a	   ��_@   ��_@      �?!   ��_@) ���<@2زv�5f@��h:np@�������:              �?        ��\�       p�*	����A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   �U@   �U@      �?!   �U@) �9@2!��v�@زv�5f@�������:              �?        ?*P��       p�*	�o����A�*�

summaries/loss$n�@
}
summaries/histogram_loss*a	   ��M@   ��M@      �?!   ��M@) D�̝]<@2زv�5f@��h:np@�������:              �?        �5��       p�*	�ĸ���A�*�

summaries/loss[h�@
}
summaries/histogram_loss*a	   `@   `@      �?!   `@)@���c>@2زv�5f@��h:np@�������:              �?        ���       p�*	+����A�*�

summaries/lossDʬ@
}
summaries/histogram_loss*a	   �H�@   �H�@      �?!   �H�@) ���(=@2زv�5f@��h:np@�������:              �?        o��]�       p�*	�_����A�*�

summaries/loss�n�@
}
summaries/histogram_loss*a	   �ѭ@   �ѭ@      �?!   �ѭ@) $'�:@2زv�5f@��h:np@�������:              �?        ���̦       p�*	�ع���A�*�

summaries/lossb<�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) a�$��:@2زv�5f@��h:np@�������:              �?        G�\a�       p�*	�&����A�*�

summaries/loss/�@
}
summaries/histogram_loss*a	   ��E@   ��E@      �?!   ��E@) yE �9@2!��v�@زv�5f@�������:              �?        m�`��       p�*	~����A�*�

summaries/loss��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)  $zK\8@2!��v�@زv�5f@�������:              �?        y�aŦ       p�*	�κ���A�*�

summaries/loss�@
}
summaries/histogram_loss*a	   �A�@   �A�@      �?!   �A�@) 1��=8@2!��v�@زv�5f@�������:              �?        �����       p�*	d����A��*�

summaries/loss '�@
}
summaries/histogram_loss*a	    �d@    �d@      �?!    �d@)  @���9@2!��v�@زv�5f@�������:              �?        ��6�       p�*	Ml����A��*�

summaries/loss=�@
}
summaries/histogram_loss*a	   @�g@   @�g@      �?!   @�g@) �Yl�:@2زv�5f@��h:np@�������:              �?        �����       p�*	������A��*�

summaries/losshަ@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �"L1;@2زv�5f@��h:np@�������:              �?        �\��       p�*	����A��*�

summaries/loss�ۦ@
}
summaries/histogram_loss*a	    x�@    x�@      �?!    x�@)  �hn0;@2زv�5f@��h:np@�������:              �?        �C��       p�*	�Y����A��*�

summaries/lossX �@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) ��5]N:@2زv�5f@��h:np@�������:              �?        ��x�       p�*	m�����A��*�

summaries/lossx�@
}
summaries/histogram_loss*a	     �@     �@      �?!     �@)@ <c�;@2زv�5f@��h:np@�������:              �?        �m��       p�*	������A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   `�>@   `�>@      �?!   `�>@)@��45<@2زv�5f@��h:np@�������:              �?        �       p�*	0D����A��*�

summaries/lossD��@
}
summaries/histogram_loss*a	   �Hu@   �Hu@      �?!   �Hu@) ��:�<@2زv�5f@��h:np@�������:              �?        �ꠠ�       p�*	%�����A��*�

summaries/loss�O�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �%܍]:@2زv�5f@��h:np@�������:              �?        �*�V�       p�*	m����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��?;@2زv�5f@��h:np@�������:              �?        Ϧ�Ȧ       p�*	�+����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    Ԕ@    Ԕ@      �?!    Ԕ@)  �\zy:@2زv�5f@��h:np@�������:              �?        <{�W�       p�*	�x����A��*�

summaries/lossf �@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) )
Ea<;@2زv�5f@��h:np@�������:              �?        
�T�       p�*	�ľ���A��*�

summaries/lossM|�@
}
summaries/histogram_loss*a	   ��o@   ��o@      �?!   ��o@)@�+��:@2زv�5f@��h:np@�������:              �?        �����       p�*	~����A��*�

summaries/loss�%�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@b��G=@2زv�5f@��h:np@�������:              �?        r)�f�       p�*	k����A��*�

summaries/lossU�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@�g��=@2زv�5f@��h:np@�������:              �?        ��㝦       p�*	������A��*�

summaries/loss8�@
}
summaries/histogram_loss*a	    �@@    �@@      �?!    �@@) Ϟ�:<@2زv�5f@��h:np@�������:              �?        ���       p�*	�	����A��*�

summaries/loss{�@
}
summaries/histogram_loss*a	    co@    co@      �?!    co@) ��~:@2زv�5f@��h:np@�������:              �?        2b�0�       p�*	}W����A��*�

summaries/lossJo�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) YP̱g:@2زv�5f@��h:np@�������:              �?        �AbY�       p�*	n�����A��*�

summaries/loss�|�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ��Y�;@2زv�5f@��h:np@�������:              �?        ��T�       p�*	������A��*�

summaries/loss�C�@
}
summaries/histogram_loss*a	   �{h@   �{h@      �?!   �{h@) D9	�:@2زv�5f@��h:np@�������:              �?        ���       p�*	RD����A��*�

summaries/loss;�@
}
summaries/histogram_loss*a	   �c@   �c@      �?!   �c@)@pӕi�;@2زv�5f@��h:np@�������:              �?        ��ַ�       p�*	�����A��*�

summaries/loss�Ƭ@
}
summaries/histogram_loss*a	   �ט@   �ט@      �?!   �ט@)@�9�&=@2زv�5f@��h:np@�������:              �?        ���X�       p�*	U�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �y@@   �y@@      �?!   �y@@) q�iC:<@2زv�5f@��h:np@�������:              �?        G�;L�       p�*	�+����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) !��8@2!��v�@زv�5f@�������:              �?        ��Cc�       p�*	�x����A��*�

summaries/loss8��@
}
summaries/histogram_loss*a	    '4@    '4@      �?!    '4@) �]�<@2زv�5f@��h:np@�������:              �?        1;0�       p�*	������A��*�

summaries/loss�m�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �j�=@2زv�5f@��h:np@�������:              �?        ��/'�       p�*	����A��*�

summaries/lossf��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) )�X@=@2زv�5f@��h:np@�������:              �?        !4|�       p�*	h_����A��*�

summaries/loss�N�@
}
summaries/histogram_loss*a	   @�i@   @�i@      �?!   @�i@) ��gD:@2زv�5f@��h:np@�������:              �?        ��1��       p�*	������A��*�

summaries/lossJ�@
}
summaries/histogram_loss*a	   @�B@   @�B@      �?!   @�B@) YN@<@2زv�5f@��h:np@�������:              �?        A��       p�*	U�����A��*�

summaries/loss/��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@��z;@2زv�5f@��h:np@�������:              �?        ��       p�*	D����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @�_@   @�_@      �?!   @�_@) 9]Hx�<@2زv�5f@��h:np@�������:              �?        1�l�       p�*	�����A��*�

summaries/loss�L�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) i�r�;@2زv�5f@��h:np@�������:              �?        �%k^�       p�*	������A��*�

summaries/loss�v�@
}
summaries/histogram_loss*a	   �ݎ@   �ݎ@      �?!   �ݎ@)@�L�J�7@2!��v�@زv�5f@�������:              �?        ��,��       p�*	/3����A��*�

summaries/lossb��@
}
summaries/histogram_loss*a	   @L�@   @L�@      �?!   @L�@) a��j:;@2زv�5f@��h:np@�������:              �?        9�$��       p�*	������A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �!�=@2زv�5f@��h:np@�������:              �?        �8���       p�*	������A��*�

summaries/lossց�@
}
summaries/histogram_loss*a	   �:0@   �:0@      �?!   �:0@) �W�+<@2زv�5f@��h:np@�������:              �?        @���       p�*	�����A��*�

summaries/loss ��@
}
summaries/histogram_loss*a	    @W@    @W@      �?!    @W@)   ��v<@2زv�5f@��h:np@�������:              �?        K^�       p�*	h����A��*�

summaries/loss�R�@
}
summaries/histogram_loss*a	   �R�@   �R�@      �?!   �R�@)@�r��:@2زv�5f@��h:np@�������:              �?        |�a�       p�*	������A��*�

summaries/loss�.�@
}
summaries/histogram_loss*a	   �؅@   �؅@      �?!   �؅@) I��<@2زv�5f@��h:np@�������:              �?        ���v�       p�*	;�����A��*�

summaries/loss�P�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@6{ěU=@2زv�5f@��h:np@�������:              �?        K���       p�*	QN����A��*�

summaries/lossd��@
}
summaries/histogram_loss*a	   ��U@   ��U@      �?!   ��U@) ���r<@2زv�5f@��h:np@�������:              �?        ��       p�*	������A��*�

summaries/loss�}�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ���Z=@2زv�5f@��h:np@�������:              �?        K0���       p�*	������A��*�

summaries/loss�#�@
}
summaries/histogram_loss*a	   �y�@   �y�@      �?!   �y�@)@��i_�8@2!��v�@زv�5f@�������:              �?        �}��       p�*	�<����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) $ڠ;@2زv�5f@��h:np@�������:              �?        ��צ       p�*	+�����A��*�

summaries/loss�~�@
}
summaries/histogram_loss*a	   @կ@   @կ@      �?!   @կ@) 9�s!�:@2زv�5f@��h:np@�������:              �?        x��       p�*	������A��*�

summaries/loss"��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) !�u��;@2زv�5f@��h:np@�������:              �?        �`��       p�*	c(����A��*�

summaries/loss�v�@
}
summaries/histogram_loss*a	   �܎@   �܎@      �?!   �܎@)@6���=@2زv�5f@��h:np@�������:              �?        ���W�       p�*	������A��*�

summaries/loss�	�@
}
summaries/histogram_loss*a	    ;A@    ;A@      �?!    ;A@) �90l�>@2زv�5f@��h:np@�������:              �?        d,�I�       p�*	>����A��*�

summaries/lossPP�@
}
summaries/histogram_loss*a	    
�@    
�@      �?!    
�@) @���>@2زv�5f@��h:np@�������:              �?        F�3�       p�*	j�����A��*�

summaries/loss7�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�;���:@2زv�5f@��h:np@�������:              �?        �Xq�       p�*	en����A��*�

summaries/loss42�@
}
summaries/histogram_loss*a	   �Ff@   �Ff@      �?!   �Ff@) �>:@2زv�5f@��h:np@�������:              �?        �4�|�       p�*	������A��*�

summaries/loss�O�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  ��;@2زv�5f@��h:np@�������:              �?        S
��       p�*	�����A��*�

summaries/loss,�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �I��5;@2زv�5f@��h:np@�������:              �?        '�       p�*	-[����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @\�@   @\�@      �?!   @\�@) �.R�;@2زv�5f@��h:np@�������:              �?        m��       p�*	������A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �@   �@      �?!   �@)@�iK��;@2زv�5f@��h:np@�������:              �?        ���       p�*	������A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    ё@    ё@      �?!    ё@)@T��=@2زv�5f@��h:np@�������:              �?        �WUG�       p�*	�G����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) ���:@2زv�5f@��h:np@�������:              �?        ����       p�*	-�����A��*�

summaries/lossXL�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �e,U;@2زv�5f@��h:np@�������:              �?        �Oa��       p�*	)�����A��*�

summaries/lossf:�@
}
summaries/histogram_loss*a	   �L@   �L@      �?!   �L@) )��,�;@2زv�5f@��h:np@�������:              �?        �K���       p�*	~7����A��*�

summaries/lossB��@
}
summaries/histogram_loss*a	   @ȕ@   @ȕ@      �?!   @ȕ@) Arӧ=@2زv�5f@��h:np@�������:              �?        �r��       p�*	������A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @�;=@2زv�5f@��h:np@�������:              �?        �|�       p�*	������A��*�

summaries/loss�T�@
}
summaries/histogram_loss*a	   @�*@   @�*@      �?!   @�*@) �E�K <@2زv�5f@��h:np@�������:              �?        �
a��       p�*	�$����A��*�

summaries/loss�И@
}
summaries/histogram_loss*a	   �@   �@      �?!   �@)@�"�6@2!��v�@زv�5f@�������:              �?        ��<-�       p�*	�t����A��*�

summaries/loss�Ң@
}
summaries/histogram_loss*a	    \Z@    \Z@      �?!    \Z@)@pz���9@2!��v�@زv�5f@�������:              �?        ��'��       p�*	߿����A��*�

summaries/loss>5�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��G�"8@2!��v�@زv�5f@�������:              �?        �K	�       p�*	I����A��*�

summaries/loss�P�@
}
summaries/histogram_loss*a	   @
@   @
@      �?!   @
@) 9�"~�;@2زv�5f@��h:np@�������:              �?        ���       p�*	�`����A��*�

summaries/loss8x�@
}
summaries/histogram_loss*a	    o@    o@      �?!    o@) #�:@2زv�5f@��h:np@�������:              �?        ����       p�*	6�����A��*�

summaries/loss�3�@
}
summaries/histogram_loss*a	   �yf@   �yf@      �?!   �yf@)@X��:@2زv�5f@��h:np@�������:              �?        ��@�       p�*	`�����A��*�

summaries/loss{ט@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@F��%�6@2!��v�@زv�5f@�������:              �?        KdX�       p�*	�Q����A��*�

summaries/lossru�@
}
summaries/histogram_loss*a	   @�N@   @�N@      �?!   @�N@) �YR`<@2زv�5f@��h:np@�������:              �?        �tE��       p�*	Н����A��*�

summaries/lossw�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@T�G�;@2زv�5f@��h:np@�������:              �?        ��^:�       p�*	_�����A��*�

summaries/lossF�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �i��;@2زv�5f@��h:np@�������:              �?        *����       p�*	�=����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �Z�9=@2زv�5f@��h:np@�������:              �?        �]�f�       p�*	e�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  d��;;@2زv�5f@��h:np@�������:              �?        �+i��       p�*	o�����A��*�

summaries/loss|�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) O��:@2زv�5f@��h:np@�������:              �?        ���       p�*	� ����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) $'<F�;@2زv�5f@��h:np@�������:              �?        �)�Z�       p�*	Hp����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@�ڦ�;@2زv�5f@��h:np@�������:              �?        �.��       p�*	������A��*�

summaries/loss>]�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ���T�:@2زv�5f@��h:np@�������:              �?        �)U��       p�*	1����A*�

summaries/lossB��@
}
summaries/histogram_loss*a	   @(�@   @(�@      �?!   @(�@) A�˒;@2زv�5f@��h:np@�������:              �?        �(�¦       p�*	WW����AÓ*�

summaries/loss7H�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@���n ;@2زv�5f@��h:np@�������:              �?        j���       p�*	L�����Aē*�

summaries/loss�?�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) Aĉ��=@2زv�5f@��h:np@�������:              �?        �/�A�       p�*	�����Aœ*�

summaries/loss&��@
}
summaries/histogram_loss*a	   ��?@   ��?@      �?!   ��?@) i.��8<@2زv�5f@��h:np@�������:              �?        (�R�       p�*	=����AƓ*�

summaries/lossf��@
}
summaries/histogram_loss*a	   ��s@   ��s@      �?!   ��s@) )�`��<@2زv�5f@��h:np@�������:              �?        CD���       p�*	������AǓ*�

summaries/loss��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ��F;@2زv�5f@��h:np@�������:              �?        8���       p�*	R�����Aȓ*�

summaries/loss�%�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��1�H;@2زv�5f@��h:np@�������:              �?        Z����       p�*	� ����Aɓ*�

summaries/lossSè@
}
summaries/histogram_loss*a	   `j@   `j@      �?!   `j@)@:�}<�;@2زv�5f@��h:np@�������:              �?        sL�	�       p�*		l����Aʓ*�

summaries/loss��@
}
summaries/histogram_loss*a	    PB@    PB@      �?!    PB@)  ��$?<@2زv�5f@��h:np@�������:              �?        �և�       p�*	������A˓*�

summaries/loss`m�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  ���=@2زv�5f@��h:np@�������:              �?        h��       p�*	c����A̓*�

summaries/loss��@
}
summaries/histogram_loss*a	    Z@    Z@      �?!    Z@) @��̘;@2زv�5f@��h:np@�������:              �?        ����       p�*	By����A͓*�

summaries/loss=D�@
}
summaries/histogram_loss*a	   ��h@   ��h@      �?!   ��h@)@����<@2زv�5f@��h:np@�������:              �?        �-�       p�*	L�����AΓ*�

summaries/lossJ
�@
}
summaries/histogram_loss*a	   @I�@   @I�@      �?!   @I�@) Yw=�?;@2زv�5f@��h:np@�������:              �?        \�	�       p�*	�$����Aϓ*�

summaries/loss�`�@
}
summaries/histogram_loss*a	   �L@   �L@      �?!   �L@)@N�/Y<@2زv�5f@��h:np@�������:              �?        ��@��       p�*	�q����AГ*�

summaries/loss.3�@
}
summaries/histogram_loss*a	   �ef@   �ef@      �?!   �ef@) ׳�:@2زv�5f@��h:np@�������:              �?        `�Q��       p�*	�����Aѓ*�

summaries/lossD�@
}
summaries/histogram_loss*a	   `�(@   `�(@      �?!   `�(@)@恷�;@2زv�5f@��h:np@�������:              �?        h;�       p�*	w����Aғ*�

summaries/loss�ƭ@
}
summaries/histogram_loss*a	   �ظ@   �ظ@      �?!   �ظ@) ��Ώ}=@2زv�5f@��h:np@�������:              �?        �����       p�*	�`����Aӓ*�

summaries/loss�@
}
summaries/histogram_loss*a	   @^t@   @^t@      �?!   @^t@) 1����<@2زv�5f@��h:np@�������:              �?        <�;�       p�*	|�����Aԓ*�

summaries/lossM�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@���1�=@2زv�5f@��h:np@�������:              �?        ��S�       p�*	. ����AՓ*�

summaries/lossP��@
}
summaries/histogram_loss*a	    jP@    jP@      �?!    jP@) @�f�d<@2زv�5f@��h:np@�������:              �?        �dt�       p�*	YO����A֓*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �2�;@2زv�5f@��h:np@�������:              �?        J���       p�*	������Aד*�

summaries/loss�q�@
}
summaries/histogram_loss*a	    8�@    8�@      �?!    8�@)  ���:@2زv�5f@��h:np@�������:              �?        �"�       p�*	t�����Aؓ*�

summaries/loss#��@
}
summaries/histogram_loss*a	   `D�@   `D�@      �?!   `D�@)@2d��f;@2زv�5f@��h:np@�������:              �?        �`l�       p�*	?����Aٓ*�

summaries/loss(ҩ@
}
summaries/histogram_loss*a	    E:@    E:@      �?!    E:@) �iU�)<@2زv�5f@��h:np@�������:              �?        �Jd�       p�*	3�����Aړ*�

summaries/lossn��@
}
summaries/histogram_loss*a	   �M3@   �M3@      �?!   �M3@) �!�P<@2زv�5f@��h:np@�������:              �?        f9��       p�*	@�����Aۓ*�

summaries/loss���@
}
summaries/histogram_loss*a	    Ps@    Ps@      �?!    Ps@)  ��#:@2زv�5f@��h:np@�������:              �?        Q��P�       p�*	R&����Aܓ*�

summaries/loss;W�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@��,�X;@2زv�5f@��h:np@�������:              �?        Ǖ�9�       p�*	�t����Aݓ*�

summaries/loss٘�@
}
summaries/histogram_loss*a	    @    @      �?!    @)@��>�;@2زv�5f@��h:np@�������:              �?        ˎh�       p�*	������Aޓ*�

summaries/loss'�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@|ߍ݆;@2زv�5f@��h:np@�������:              �?        ����       p�*	�����Aߓ*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�@?��:@2زv�5f@��h:np@�������:              �?        �K���       p�*	�e����A��*�

summaries/loss�ǣ@
}
summaries/histogram_loss*a	   `�x@   `�x@      �?!   `�x@)@*l�2:@2زv�5f@��h:np@�������:              �?        �#A�       p�*	�����A�*�

summaries/lossj�@
}
summaries/histogram_loss*a	   �AM@   �AM@      �?!   �AM@)@8�A\<@2زv�5f@��h:np@�������:              �?        U
M¦       p�*	�����A�*�

summaries/lossSB�@
}
summaries/histogram_loss*a	   `J�@   `J�@      �?!   `J�@)@�9���=@2زv�5f@��h:np@�������:              �?        ���       p�*	�S����A�*�

summaries/loss_]�@
}
summaries/histogram_loss*a	   ો@   ો@      �?!   ો@)@P���a:@2زv�5f@��h:np@�������:              �?        I��h�       p�*	8�����A�*�

summaries/loss�_�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) A�l�[;@2زv�5f@��h:np@�������:              �?        .�G�       p�*	������A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��~@   ��~@      �?!   ��~@) q��@:@2زv�5f@��h:np@�������:              �?        ��I�       p�*	.=����A�*�

summaries/lossR]�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) �+!29@2!��v�@زv�5f@�������:              �?        ^Gߦ       p�*	������A�*�

summaries/lossի�@
}
summaries/histogram_loss*a	   �z@   �z@      �?!   �z@)@Ώe�59@2!��v�@زv�5f@�������:              �?        ��Mg�       p�*	0�����A�*�

summaries/loss�s�@
}
summaries/histogram_loss*a	   @wn@   @wn@      �?!   @wn@) �(�$:@2زv�5f@��h:np@�������:              �?        �ec�       p�*	9+����A�*�

summaries/lossg��@
}
summaries/histogram_loss*a	   �Lp@   �Lp@      �?!   �Lp@)@\��ʝ7@2!��v�@زv�5f@�������:              �?        �Aޒ�       p�*	�x����A�*�

summaries/loss7ڧ@
}
summaries/histogram_loss*a	   �F�@   �F�@      �?!   �F�@)@��p��;@2زv�5f@��h:np@�������:              �?        �A�֦       p�*	������A�*�

summaries/loss #�@
}
summaries/histogram_loss*a	    `D@    `D@      �?!    `D@)  @2�D<@2زv�5f@��h:np@�������:              �?        �$�G�       p�*	b����A�*�

summaries/loss�,�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ����J;@2زv�5f@��h:np@�������:              �?        ��h��       p�*	�e����A�*�

summaries/loss�<�@
}
summaries/histogram_loss*a	   @�'@   @�'@      �?!   @�'@) ��V�;@2زv�5f@��h:np@�������:              �?        =�[m�       p�*	�����A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   �?�@   �?�@      �?!   �?�@)@Y�C=@2زv�5f@��h:np@�������:              �?        �aed�       p�*	P�����A�*�

summaries/loss`ު@
}
summaries/histogram_loss*a	    �[@    �[@      �?!    �[@)  �*�<@2زv�5f@��h:np@�������:              �?        <Ѧ       p�*	�L����A�*�

summaries/lossL��@
}
summaries/histogram_loss*a	   ��>@   ��>@      �?!   ��>@) �=���>@2زv�5f@��h:np@�������:              �?        ��2�       p�*	Z�����A�*�

summaries/loss��@
}
summaries/histogram_loss*a	    _b@    _b@      �?!    _b@)@�}�J�9@2!��v�@زv�5f@�������:              �?        (�I�       p�*	������A�*�

summaries/loss�ǡ@
}
summaries/histogram_loss*a	   `�8@   `�8@      �?!   `�8@)@r4�-�9@2!��v�@زv�5f@�������:              �?        �榦       p�*	U4����A�*�

summaries/lossg��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@\��1�;@2زv�5f@��h:np@�������:              �?        �惨�       p�*	������A��*�

summaries/lossby�@
}
summaries/histogram_loss*a	   @,�@   @,�@      �?!   @,�@) arO�=@2زv�5f@��h:np@�������:              �?        SNh��       p�*	�����A��*�

summaries/lossƻ�@
}
summaries/histogram_loss*a	   �x�@   �x�@      �?!   �x�@) I���y;@2زv�5f@��h:np@�������:              �?        t�8�       p�*	"����A��*�

summaries/loss\�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �d���:@2زv�5f@��h:np@�������:              �?        �#���       p�*	�o����A��*�

summaries/loss|��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �A>w;@2زv�5f@��h:np@�������:              �?        :;+(�       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) $*��r;@2زv�5f@��h:np@�������:              �?        ]�a�       p�*	�����A��*�

summaries/lossb��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) a����:@2زv�5f@��h:np@�������:              �?        [���       p�*	cb����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) Yp<�:@2زv�5f@��h:np@�������:              �?        |-���       p�*	������A��*�

summaries/lossu,�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@^R��;@2زv�5f@��h:np@�������:              �?        �J��       p�*	�����A��*�

summaries/lossvŬ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ���{&=@2زv�5f@��h:np@�������:              �?        &z�       p�*	0F����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @14@   @14@      �?!   @14@) ��`%�9@2!��v�@زv�5f@�������:              �?        ���       p�*	}�����A��*�

summaries/lossjP�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ���V;@2زv�5f@��h:np@�������:              �?        3��6�       p�*	�����A��*�

summaries/loss*��@
}
summaries/histogram_loss*a	   @E�@   @E�@      �?!   @E�@) ��>�f;@2زv�5f@��h:np@�������:              �?        c2�Φ       p�*	�6����A��*�

summaries/loss;N�@
}
summaries/histogram_loss*a	   `ǉ@   `ǉ@      �?!   `ǉ@)@f �]:@2زv�5f@��h:np@�������:              �?        N#&n�       p�*	������A��*�

summaries/lossZ#�@
}
summaries/histogram_loss*a	   @k$@   @k$@      �?!   @k$@) �n���6@2!��v�@زv�5f@�������:              �?        �>��       p�*	������A��*�

summaries/loss�9�@
}
summaries/histogram_loss*a	    2G@    2G@      �?!    2G@) @\L?@2زv�5f@��h:np@�������:              �?        ����       p�*	� ����A��*�

summaries/lossNƟ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �/���8@2!��v�@زv�5f@�������:              �?        0g�U�       p�*	�n����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��c@   ��c@      �?!   ��c@)@ڬDv�9@2!��v�@زv�5f@�������:              �?        _'$��       p�*	
�����A��*�

summaries/losso��@
}
summaries/histogram_loss*a	   �-�@   �-�@      �?!   �-�@)@�Ø|j6@2!��v�@زv�5f@�������:              �?        ���       p�*	}����A��*�

summaries/loss^��@
}
summaries/histogram_loss*a	   ��V@   ��V@      �?!   ��V@) ��u<@2زv�5f@��h:np@�������:              �?        
���       p�*	&R����A��*�

summaries/loss�9�@
}
summaries/histogram_loss*a	   �;g@   �;g@      �?!   �;g@)@�Gȇ7@2!��v�@زv�5f@�������:              �?        �Ѩ�       p�*	y�����A��*�

summaries/lossCƛ@
}
summaries/histogram_loss*a	   `�x@   `�x@      �?!   `�x@)@bm�k�7@2!��v�@زv�5f@�������:              �?        �Ƽ�       p�*	VK����A��*�

summaries/lossP��@
}
summaries/histogram_loss*a	    j@    j@      �?!    j@) @>f(89@2!��v�@زv�5f@�������:              �?        I�'��       p�*	������A��*�

summaries/loss�Z�@
}
summaries/histogram_loss*a	   �W�@   �W�@      �?!   �W�@)@�[���7@2!��v�@زv�5f@�������:              �?        Ǝ��       p�*	X����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@����8@2!��v�@زv�5f@�������:              �?        ��}=�       p�*	=f����A��*�

summaries/loss5��@
}
summaries/histogram_loss*a	   �FW@   �FW@      �?!   �FW@)@�Cu�v<@2زv�5f@��h:np@�������:              �?        )��>�       p�*	ڰ����A��*�

summaries/loss�;�@
}
summaries/histogram_loss*a	    p�@    p�@      �?!    p�@)  uW:@2زv�5f@��h:np@�������:              �?        gi��       p�*	������A��*�

summaries/loss l�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  �:�^=@2زv�5f@��h:np@�������:              �?        f��4�       p�*	0I����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    X@    X@      �?!    X@)@`5.�29@2!��v�@زv�5f@�������:              �?        %���       p�*	������A��*�

summaries/loss�1�@
}
summaries/histogram_loss*a	   �:@   �:@      �?!   �:@) �\͞6@2!��v�@زv�5f@�������:              �?        _��       p�*	h�����A��*�

summaries/loss61�@
}
summaries/histogram_loss*a	   �&f@   �&f@      �?!   �&f@) �m���0@2�DK��@{2�.��@�������:              �?        "8[�       p�*	35����A��*�

summaries/loss�͎@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@=h5�3@2{2�.��@!��v�@�������:              �?        b?Ŧ       p�*	������A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) d�ad�;@2زv�5f@��h:np@�������:              �?        ��w��       p�*	�����A��*�

summaries/loss�X�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) �,,�=@2زv�5f@��h:np@�������:              �?        �V��       p�*	q ����A��*�

summaries/lossJ�@
}
summaries/histogram_loss*a	   @�=@   @�=@      �?!   @�=@) Yk�3<@2زv�5f@��h:np@�������:              �?        �zl��       p�*	k����A��*�

summaries/lossx�@
}
summaries/histogram_loss*a	    /�@    /�@      �?!    /�@) �؂7;@2زv�5f@��h:np@�������:              �?        ���j�       p�*	o�����A��*�

summaries/loss�V�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) y�>5;@2زv�5f@��h:np@�������:              �?        �Ǥ�       p�*	�����A��*�

summaries/loss$�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) ���:@2زv�5f@��h:np@�������:              �?        �
6�       p�*	�Y����A��*�

summaries/loss�r�@
}
summaries/histogram_loss*a	   `YN@   `YN@      �?!   `YN@)@>[E*_<@2زv�5f@��h:np@�������:              �?        u��Ǧ       p�*	������A��*�

summaries/lossI��@
}
summaries/histogram_loss*a	    �U@    �U@      �?!    �U@)@4��B�9@2!��v�@زv�5f@�������:              �?        �8�Q�       p�*	������A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@�V9y�:@2زv�5f@��h:np@�������:              �?        O��Ȧ       p�*	I����A��*�

summaries/losslP�@
}
summaries/histogram_loss*a	   �*@   �*@      �?!   �*@) d����;@2زv�5f@��h:np@�������:              �?        �c���       p�*	������A��*�

summaries/lossF߯@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ���4>@2زv�5f@��h:np@�������:              �?        �D7�       p�*	K�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �Q_@   �Q_@      �?!   �Q_@)@���n�<@2زv�5f@��h:np@�������:              �?        (+���       p�*	�6����A��*�

summaries/loss�f�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ��\��;@2زv�5f@��h:np@�������:              �?        B�Ro�       p�*	������A��*�

summaries/loss�|�@
}
summaries/histogram_loss*a	   ��/@   ��/@      �?!   ��/@) I�~<@2زv�5f@��h:np@�������:              �?        ���-�       p�*	
�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    7�@    7�@      �?!    7�@) ]-��=@2زv�5f@��h:np@�������:              �?        �]��       p�*	�)����A��*�

summaries/loss�J�@
}
summaries/histogram_loss*a	   �_�@   �_�@      �?!   �_�@)@�τU;@2زv�5f@��h:np@�������:              �?        ��[��       p�*	�v����A��*�

summaries/loss*��@
}
summaries/histogram_loss*a	   @E�@   @E�@      �?!   @E�@) �C��"=@2زv�5f@��h:np@�������:              �?        �8�Ȧ       p�*	������A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ����=@2زv�5f@��h:np@�������:              �?        q��       p�*	����A��*�

summaries/lossjH�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ��7[:@2زv�5f@��h:np@�������:              �?        Ϣ�       p�*	�c����A��*�

summaries/lossi�@
}
summaries/histogram_loss*a	   � M@   � M@      �?!   � M@)@�Fn�[<@2زv�5f@��h:np@�������:              �?        (��ߦ       p�*	:�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��~@   ��~@      �?!   ��~@) A�9�<@2زv�5f@��h:np@�������:              �?        Ʃ��       p�*	������A��*�

summaries/loss�@�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@���n�8@2!��v�@زv�5f@�������:              �?        [+Ц       p�*	�K����A��*�

summaries/loss=�@
}
summaries/histogram_loss*a	   ࠧ@   ࠧ@      �?!   ࠧ@)@��d�:@2زv�5f@��h:np@�������:              �?        o4a��       p�*	9�����A��*�

summaries/loss	ϭ@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@�cf^�=@2زv�5f@��h:np@�������:              �?        ﱗ�       p�*	������A��*�

summaries/loss.ȫ@
}
summaries/histogram_loss*a	   �y@   �y@      �?!   �y@) �~B�<@2زv�5f@��h:np@�������:              �?        ?d�s�       p�*	e3����A��*�

summaries/lossX�@
}
summaries/histogram_loss*a	   � �@   � �@      �?!   � �@)@�ҥ�:@2زv�5f@��h:np@�������:              �?        q~h6�       p�*	�����A��*�

summaries/loss+#�@
}
summaries/histogram_loss*a	   `e�@   `e�@      �?!   `e�@)@N�<#F=@2زv�5f@��h:np@�������:              �?        �6+��       p�*	������A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �Z @   �Z @      �?!   �Z @)@$��$Q9@2!��v�@زv�5f@�������:              �?        ?uE_�       p�*	�����A��*�

summaries/loss8��@
}
summaries/histogram_loss*a	    _@    _@      �?!    _@) #���9@2!��v�@زv�5f@�������:              �?        � &�       p�*	�i����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �aR@   �aR@      �?!   �aR@) 1E���9@2!��v�@زv�5f@�������:              �?        ��	�       p�*	6�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    t�@    t�@      �?!    t�@)@�F�:;@2زv�5f@��h:np@�������:              �?        DMx��       p�*	� ����A��*�

summaries/loss�3�@
}
summaries/histogram_loss*a	   �}&@   �}&@      �?!   �}&@) Q,�f�;@2زv�5f@��h:np@�������:              �?        �#RY�       p�*	wg����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ֽ@    ֽ@      �?!    ֽ@) @�Zc�:@2زv�5f@��h:np@�������:              �?        �ڼ֦       p�*	������A��*�

summaries/loss#�@
}
summaries/histogram_loss*a	   �b@   �b@      �?!   �b@) d�Ã�;@2زv�5f@��h:np@�������:              �?        �3��       p�*	2����A��*�

summaries/lossCb�@
}
summaries/histogram_loss*a	   `HL@   `HL@      �?!   `HL@)@b׬�Y<@2زv�5f@��h:np@�������:              �?        �	���       p�*	j�����A��*�

summaries/lossr+�@
}
summaries/histogram_loss*a	   @nE@   @nE@      �?!   @nE@) �߱nG<@2زv�5f@��h:np@�������:              �?        ��?�       p�*	-�����A��*�

summaries/loss\�@
}
summaries/histogram_loss*a	   ��A@   ��A@      �?!   ��A@) D���=<@2زv�5f@��h:np@�������:              �?        �#�       p�*	 ����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �'.9;@2زv�5f@��h:np@�������:              �?        �k�       p�*	�q����A��*�

summaries/loss	��@
}
summaries/histogram_loss*a	    S@    S@      �?!    S@)@���k<@2زv�5f@��h:np@�������:              �?        <+�w�       p�*	������A��*�

summaries/loss�&�@
}
summaries/histogram_loss*a	   �٤@   �٤@      �?!   �٤@) qs�¢:@2زv�5f@��h:np@�������:              �?        9��s�       p�*	(a����A��*�

summaries/loss�ӫ@
}
summaries/histogram_loss*a	    z@    z@      �?!    z@) �47�<@2زv�5f@��h:np@�������:              �?        �&�z�       p�*	������A��*�

summaries/loss�
�@
}
summaries/histogram_loss*a	   �V�@   �V�@      �?!   �V�@) Y�Q��=@2زv�5f@��h:np@�������:              �?        �Sr٦       p�*	�����A��*�

summaries/lossW�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) ��>;@2زv�5f@��h:np@�������:              �?        �KŦ       p�*	d����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ��qx�:@2زv�5f@��h:np@�������:              �?        ����       p�*	{�����A��*�

summaries/loss�B�@
}
summaries/histogram_loss*a	   @[h@   @[h@      �?!   @[h@) iH,��<@2زv�5f@��h:np@�������:              �?        	�,�       p�*	�����A��*�

summaries/loss�k�@
}
summaries/histogram_loss*a	    {�@    {�@      �?!    {�@) ��c;@2زv�5f@��h:np@�������:              �?        Na�       p�*	ka����A��*�

summaries/loss=,�@
}
summaries/histogram_loss*a	   ��%@   ��%@      �?!   ��%@)@��+^9@2!��v�@زv�5f@�������:              �?        ���H�       p�*	#�����A*�

summaries/lossd��@
}
summaries/histogram_loss*a	   �l�@   �l�@      �?!   �l�@) �����:@2زv�5f@��h:np@�������:              �?        �k��       p�*	�����AÔ*�

summaries/loss<��@
}
summaries/histogram_loss*a	   �6@   �6@      �?!   �6@) ��"�<@2زv�5f@��h:np@�������:              �?        ��X��       p�*	�k����AĔ*�

summaries/loss�.�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�V5|K;@2زv�5f@��h:np@�������:              �?        �H%e�       p�*	������AŔ*�

summaries/loss�/�@
}
summaries/histogram_loss*a	   ��e@   ��e@      �?!   ��e@) ��9|:@2!��v�@زv�5f@�������:              �?        ��       p�*	� ����AƔ*�

summaries/loss⟯@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ᠵ�>@2زv�5f@��h:np@�������:              �?         ��       p�*	�q����Aǔ*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�h��:@2زv�5f@��h:np@�������:              �?        ��k�       p�*	������AȔ*�

summaries/loss�X�@
}
summaries/histogram_loss*a	    k@    k@      �?!    k@) @^v�<@2زv�5f@��h:np@�������:              �?        �1� �       p�*	�!����Aɔ*�

summaries/loss�@
}
summaries/histogram_loss*a	   @� @   @� @      �?!   @� @) ���;@2زv�5f@��h:np@�������:              �?        Jݛ�       p�*	�p����Aʔ*�

summaries/loss\G�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) D
{�S;@2زv�5f@��h:np@�������:              �?        ��[٦       p�*	h�����A˔*�

summaries/lossڔ�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) i2��l;@2زv�5f@��h:np@�������:              �?        f�&��       p�*	�) ���A̔*�

summaries/lossl/�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) d5�w�=@2زv�5f@��h:np@�������:              �?        63aѦ       p�*	�w ���A͔*�

summaries/loss��@
}
summaries/histogram_loss*a	   �6"@   �6"@      �?!   �6"@)@~��;@2زv�5f@��h:np@�������:              �?        c�;¦       p�*	�� ���AΔ*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�D���<@2زv�5f@��h:np@�������:              �?        S� �       p�*	9'���Aϔ*�

summaries/lossV\�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) 9{ߛa:@2زv�5f@��h:np@�������:              �?        )\>�       p�*	�y���AД*�

summaries/lossb!�@
}
summaries/histogram_loss*a	   @,�@   @,�@      �?!   @,�@) a���E=@2زv�5f@��h:np@�������:              �?        ���       p�*	b����Aє*�

summaries/loss�(�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@�r�^�:@2زv�5f@��h:np@�������:              �?        g��       p�*	�+���AҔ*�

summaries/loss�;�@
}
summaries/histogram_loss*a	   �~�@   �~�@      �?!   �~�@) �N8W:@2زv�5f@��h:np@�������:              �?        r6�V�       p�*	�x���AӔ*�

summaries/loss[]�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@�OV^�:@2زv�5f@��h:np@�������:              �?        �3_1�       p�*	����AԔ*�

summaries/lossP�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @5G�;@2زv�5f@��h:np@�������:              �?        m��=�       p�*	%���AՔ*�

summaries/loss���@
}
summaries/histogram_loss*a	    2�@    2�@      �?!    2�@) @\'Gu:@2زv�5f@��h:np@�������:              �?        O�֦       p�*	@n���A֔*�

summaries/loss���@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ���t;@2زv�5f@��h:np@�������:              �?        ,��y�       p�*	�����Aה*�

summaries/loss���@
}
summaries/histogram_loss*a	   �X@   �X@      �?!   �X@) �U��9@2!��v�@زv�5f@�������:              �?        �*�I�       p�*	����Aؔ*�

summaries/loss��@
}
summaries/histogram_loss*a	   @�}@   @�}@      �?!   @�}@) ��0�<@2زv�5f@��h:np@�������:              �?        T����       p�*	�g���Aٔ*�

summaries/lossM��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@JR�[�;@2زv�5f@��h:np@�������:              �?        �A���       p�*	�����Aڔ*�

summaries/lossM�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@J��;@2زv�5f@��h:np@�������:              �?        ����       p�*	�]���A۔*�

summaries/loss���@
}
summaries/histogram_loss*a	   @�_@   @�_@      �?!   @�_@) iTg��9@2!��v�@زv�5f@�������:              �?        ��s&�       p�*	�����Aܔ*�

summaries/lossmC�@
}
summaries/histogram_loss*a	   �m�@   �m�@      �?!   �m�@)@����:@2زv�5f@��h:np@�������:              �?        ��(�       p�*	�"���Aݔ*�

summaries/lossȣ�@
}
summaries/histogram_loss*a	    yT@    yT@      �?!    yT@) z��9@2!��v�@زv�5f@�������:              �?        �Y�       p�*	xz���Aޔ*�

summaries/loss�'�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@|��P:@2زv�5f@��h:np@�������:              �?         �¦       p�*	)����Aߔ*�

summaries/loss)�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@����K:@2زv�5f@��h:np@�������:              �?        ,�p�       p�*	���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��V@   ��V@      �?!   ��V@) 	"Y�u<@2زv�5f@��h:np@�������:              �?        7��>�       p�*	�u���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@z��Q�=@2زv�5f@��h:np@�������:              �?        �]_�       p�*	�����A�*�

summaries/lossߊ�@
}
summaries/histogram_loss*a	   �[�@   �[�@      �?!   �[�@)@�˲�:@2زv�5f@��h:np@�������:              �?        �\ަ       p�*	���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   �`5@   �`5@      �?!   �`5@) �Q#�9@2!��v�@زv�5f@�������:              �?        D��       p�*	^g���A�*�

summaries/loss$��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) D{��;@2زv�5f@��h:np@�������:              �?        �0�q�       p�*	9����A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��a@   ��a@      �?!   ��a@) �T)�<@2زv�5f@��h:np@�������:              �?        �MA\�       p�*	k		���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) 1䷬�<@2زv�5f@��h:np@�������:              �?        �u��       p�*	�V	���A�*�

summaries/lossDS�@
}
summaries/histogram_loss*a	   �hj@   �hj@      �?!   �hj@) �J��<@2زv�5f@��h:np@�������:              �?        �rr��       p�*	�	���A�*�

summaries/loss$�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �ۛ;@2زv�5f@��h:np@�������:              �?        pV#�       p�*	H5
���A�*�

summaries/loss)�@
}
summaries/histogram_loss*a	    �=@    �=@      �?!    �=@)@�g�i2<@2زv�5f@��h:np@�������:              �?        [/��       p�*	K�
���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�=�:@2زv�5f@��h:np@�������:              �?        ��)$�       p�*	�c���A�*�

summaries/loss�P�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @�hQ�:@2زv�5f@��h:np@�������:              �?        �Ĭ�       p�*	����A�*�

summaries/loss}��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@B]{:@2زv�5f@��h:np@�������:              �?        �,S��       p�*	-���A�*�

summaries/loss�z�@
}
summaries/histogram_loss*a	   @Uo@   @Uo@      �?!   @Uo@) 9�xF�<@2زv�5f@��h:np@�������:              �?        Ȓ���       p�*	�b���A�*�

summaries/lossN5�@
}
summaries/histogram_loss*a	   ��F@   ��F@      �?!   ��F@) �X��J<@2زv�5f@��h:np@�������:              �?        ��,��       p�*	�����A�*�

summaries/losshf�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) ��]��;@2زv�5f@��h:np@�������:              �?        ^V��       p�*	�&���A�*�

summaries/loss2�@
}
summaries/histogram_loss*a	   @#@   @#@      �?!   @#@) qZ=�;@2زv�5f@��h:np@�������:              �?        ����       p�*	�����A�*�

summaries/losscr�@
}
summaries/histogram_loss*a	   `L�@   `L�@      �?!   `L�@)@��:�`=@2زv�5f@��h:np@�������:              �?        d�z�       p�*	�*���A�*�

summaries/loss6��@
}
summaries/histogram_loss*a	   �fP@   �fP@      �?!   �fP@) ���9@2!��v�@زv�5f@�������:              �?        0L�r�       p�*	I����A�*�

summaries/lossr��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) �D��:@2زv�5f@��h:np@�������:              �?        ��6�       p�*	5���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �?@   �?@      �?!   �?@)@Ҍ<��9@2!��v�@زv�5f@�������:              �?        ��š�       p�*	|~���A��*�

summaries/loss`1�@
}
summaries/histogram_loss*a	    ,�@    ,�@      �?!    ,�@)  y��:@2زv�5f@��h:np@�������:              �?        �Լ�       p�*	6����A��*�

summaries/lossï�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@�d";@2زv�5f@��h:np@�������:              �?        �Λ��       p�*	+N���A��*�

summaries/loss�ڦ@
}
summaries/histogram_loss*a	   �^�@   �^�@      �?!   �^�@) $�+0;@2زv�5f@��h:np@�������:              �?        $_~*�       p�*	�����A��*�

summaries/loss�I�@
}
summaries/histogram_loss*a	   `?	@   `?	@      �?!   `?	@)@G�K�;@2زv�5f@��h:np@�������:              �?        ��]Φ       p�*	�P���A��*�

summaries/loss� �@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) $Ӳ�F;@2زv�5f@��h:np@�������:              �?        %��       p�*	����A��*�

summaries/loss�e�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �=��:@2زv�5f@��h:np@�������:              �?        ���I�       p�*	�/���A��*�

summaries/lossE��@
}
summaries/histogram_loss*a	   ��R@   ��R@      �?!   ��R@)@&�APk<@2زv�5f@��h:np@�������:              �?        ����       p�*	M����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    U�@    U�@      �?!    U�@) �#r.(>@2زv�5f@��h:np@�������:              �?        �3�       p�*	����A��*�

summaries/loss@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) Q�/`=@2زv�5f@��h:np@�������:              �?        h��       p�*	]r���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �� @   �� @      �?!   �� @) �yt��;@2زv�5f@��h:np@�������:              �?        ���L�       p�*	�����A��*�

summaries/lossH�@
}
summaries/histogram_loss*a	    	@    	@      �?!    	@)@��G��;@2زv�5f@��h:np@�������:              �?        ��y�       p�*	h���A��*�

summaries/loss�Ѩ@
}
summaries/histogram_loss*a	   @3@   @3@      �?!   @3@) )4o��;@2زv�5f@��h:np@�������:              �?        ����       p�*	����A��*�

summaries/lossÆ�@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@".�.*9@2!��v�@زv�5f@�������:              �?        {���       p�*	~;���A��*�

summaries/lossC��@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@bzU �;@2زv�5f@��h:np@�������:              �?        M�m�       p�*	A����A��*�

summaries/lossT��@
}
summaries/histogram_loss*a	   ��r@   ��r@      �?!   ��r@) �T5�<@2زv�5f@��h:np@�������:              �?        NU�m�       p�*	J����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@�f�"z=@2زv�5f@��h:np@�������:              �?        9�c�       p�*	xH���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @Y�@   @Y�@      �?!   @Y�@) ����7@2!��v�@زv�5f@�������:              �?        $?a�       p�*	����A��*�

summaries/loss!ر@
}
summaries/histogram_loss*a	    ;@    ;@      �?!    ;@)@m%�>@2زv�5f@��h:np@�������:              �?        
8$��       p�*	�����A��*�

summaries/loss�C�@
}
summaries/histogram_loss*a	   �p�@   �p�@      �?!   �p�@) �ڨJR;@2زv�5f@��h:np@�������:              �?        ���       p�*	ZJ���A��*�

summaries/loss"ޥ@
}
summaries/histogram_loss*a	   @Ļ@   @Ļ@      �?!   @Ļ@) !�$�:@2زv�5f@��h:np@�������:              �?        ��=��       p�*	G����A��*�

summaries/loss�b�@
}
summaries/histogram_loss*a	   @S�@   @S�@      �?!   @S�@) )��:@2زv�5f@��h:np@�������:              �?        ���       p�*	�<���A��*�

summaries/loss�J�@
}
summaries/histogram_loss*a	   �\)@   �\)@      �?!   �\)@) �qe��;@2زv�5f@��h:np@�������:              �?        �I�Ц       p�*	D����A��*�

summaries/losst�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) $sT��;@2زv�5f@��h:np@�������:              �?        ��#'�       p�*	X����A��*�

summaries/loss#�@
}
summaries/histogram_loss*a	   `d�@   `d�@      �?!   `d�@)@��9�Z8@2!��v�@زv�5f@�������:              �?        ��q�       p�*	T<���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��5@   ��5@      �?!   ��5@) �?�K�9@2!��v�@زv�5f@�������:              �?        5b��       p�*	D����A��*�

summaries/lossZB�@
}
summaries/histogram_loss*a	   @K�@   @K�@      �?!   @K�@) 顧�Q;@2زv�5f@��h:np@�������:              �?        Iǅۦ       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @�7@   @�7@      �?!   @�7@) �-q"<@2زv�5f@��h:np@�������:              �?        ܣ��       p�*	�;���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) ��d�:@2زv�5f@��h:np@�������:              �?        ��v��       p�*	����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �8@   �8@      �?!   �8@)@� G�#<@2زv�5f@��h:np@�������:              �?        ����       p�*	z����A��*�

summaries/loss�M�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@B��<@2زv�5f@��h:np@�������:              �?        EA��       p�*	�-���A��*�

summaries/loss~w�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ��м:@2زv�5f@��h:np@�������:              �?        am�       p�*	t~���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    Y�@    Y�@      �?!    Y�@) O'�:@2زv�5f@��h:np@�������:              �?        HԦ       p�*	����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  O�t=@2زv�5f@��h:np@�������:              �?        �>!f�       p�*	�'���A��*�

summaries/loss�n�@
}
summaries/histogram_loss*a	   @ح@   @ح@      �?!   @ح@) ����:@2زv�5f@��h:np@�������:              �?        ���e�       p�*	�~���A��*�

summaries/lossߦ�@
}
summaries/histogram_loss*a	   ��t@   ��t@      �?!   ��t@)@����<@2زv�5f@��h:np@�������:              �?        ��v�       p�*	����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �W�@   �W�@      �?!   �W�@) A9i�">@2زv�5f@��h:np@�������:              �?        �d��       p�*	�)���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �$:ZH:@2زv�5f@��h:np@�������:              �?        2�k�       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @�`@   @�`@      �?!   @�`@) 9p$��9@2!��v�@زv�5f@�������:              �?        B���       p�*	~����A��*�

summaries/loss�ǥ@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) ���:@2زv�5f@��h:np@�������:              �?        }-æ       p�*	2���A��*�

summaries/losso͠@
}
summaries/histogram_loss*a	   �@   �@      �?!   �@)@���[@9@2!��v�@زv�5f@�������:              �?        �=,��       p�*	^����A��*�

summaries/lossb��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) a�r�w;@2زv�5f@��h:np@�������:              �?        #�O�       p�*	�����A��*�

summaries/loss�R�@
}
summaries/histogram_loss*a	   �V�@   �V�@      �?!   �V�@) YF":�7@2!��v�@زv�5f@�������:              �?        �߁��       p�*	n2���A��*�

summaries/lossI��@
}
summaries/histogram_loss*a	    )�@    )�@      �?!    )�@)@��l�8@2!��v�@زv�5f@�������:              �?        C3���       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��=@   ��=@      �?!   ��=@)@ ��9@2!��v�@زv�5f@�������:              �?        �c�_�       p�*	6����A��*�

summaries/lossR�@
}
summaries/histogram_loss*a	   �C�@   �C�@      �?!   �C�@)@�w���:@2زv�5f@��h:np@�������:              �?        �T�       p�*	1 ���A��*�

summaries/lossȫ�@
}
summaries/histogram_loss*a	    yu@    yu@      �?!    yu@) 3��<@2زv�5f@��h:np@�������:              �?        7���       p�*	H� ���A��*�

summaries/loss�ץ@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@�t���:@2زv�5f@��h:np@�������:              �?        ��æ       p�*	� ���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@�_j�;@2زv�5f@��h:np@�������:              �?        ���+�       p�*	�,!���A��*�

summaries/loss:��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) Ia�;@2زv�5f@��h:np@�������:              �?        %XNq�       p�*	{�!���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	    �]@    �]@      �?!    �]@)@H繊�9@2!��v�@زv�5f@�������:              �?        ��
��       p�*	��!���A��*�

summaries/loss[��@
}
summaries/histogram_loss*a	   `@   `@      �?!   `@)@(�A�;@2زv�5f@��h:np@�������:              �?        �U¦       p�*	)"���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   `2@   `2@      �?!   `2@)@�Ut�}9@2!��v�@زv�5f@�������:              �?        ��'+�       p�*	=�"���A��*�

summaries/loss�N�@
}
summaries/histogram_loss*a	   `�	@   `�	@      �?!   `�	@)@�)'֩;@2زv�5f@��h:np@�������:              �?        �A�       p�*	{�"���A��*�

summaries/loss,�@
}
summaries/histogram_loss*a	   �E�@   �E�@      �?!   �E�@) ����:@2زv�5f@��h:np@�������:              �?        �IZ�       p�*	9*#���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    �|@    �|@      �?!    �|@) @�)�<@2زv�5f@��h:np@�������:              �?        &�"��       p�*	�#���A��*�

summaries/loss~��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �P'��;@2زv�5f@��h:np@�������:              �?        �r���       p�*	�#���A��*�

summaries/lossUI�@
}
summaries/histogram_loss*a	   �*�@   �*�@      �?!   �*�@)@��xS=@2زv�5f@��h:np@�������:              �?        [YՐ�       p�*	�'$���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �w@   �w@      �?!   �w@) ��<�B:@2زv�5f@��h:np@�������:              �?        &9,�       p�*	��$���A��*�

summaries/loss�X�@
}
summaries/histogram_loss*a	   `K@   `K@      �?!   `K@)@JㅵC7@2!��v�@زv�5f@�������:              �?        !:+�       p�*	��$���A��*�

summaries/loss ��@
}
summaries/histogram_loss*a	    D�@    D�@      �?!    D�@)  �kn=@2زv�5f@��h:np@�������:              �?        �Y��       p�*	p%%���A��*�

summaries/loss㡦@
}
summaries/histogram_loss*a	   `<�@   `<�@      �?!   `<�@)@��0�;@2زv�5f@��h:np@�������:              �?        �K��       p�*	.w%���A��*�

summaries/lossc��@
}
summaries/histogram_loss*a	   `�w@   `�w@      �?!   `�w@)@��Q/:@2زv�5f@��h:np@�������:              �?        �y��       p�*	��%���A��*�

summaries/loss
��@
}
summaries/histogram_loss*a	   @�~@   @�~@      �?!   @�~@) 	R}@:@2زv�5f@��h:np@�������:              �?        lI몦       p�*	� &���A��*�

summaries/loss$Ϫ@
}
summaries/histogram_loss*a	   ��Y@   ��Y@      �?!   ��Y@) D���}<@2زv�5f@��h:np@�������:              �?        p�zզ       p�*	�n&���A��*�

summaries/loss¸�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) �ܜ�x;@2زv�5f@��h:np@�������:              �?        b0E6�       p�*	��&���A��*�

summaries/loss�4�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@��M�M;@2زv�5f@��h:np@�������:              �?        ��e�       p�*	�'���A��*�

summaries/loss:��@
}
summaries/histogram_loss*a	   @'7@   @'7@      �?!   @'7@) I8>��9@2!��v�@زv�5f@�������:              �?        Z���       p�*	r'���A��*�

summaries/loss鐥@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@=��:@2زv�5f@��h:np@�������:              �?        �|0�       p�*	 �'���A��*�

summaries/lossnc�@
}
summaries/histogram_loss*a	   �m�@   �m�@      �?!   �m�@) ѐ�p=@2زv�5f@��h:np@�������:              �?        p��F�       p�*	:&(���A��*�

summaries/lossUv�@
}
summaries/histogram_loss*a	   �ʮ@   �ʮ@      �?!   �ʮ@)@p�:@2زv�5f@��h:np@�������:              �?        =߹J�       p�*	�t(���A��*�

summaries/lossޤ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@8��P�:@2زv�5f@��h:np@�������:              �?        ��j��       p�*	��(���A��*�

summaries/lossʪ@
}
summaries/histogram_loss*a	   `AY@   `AY@      �?!   `AY@)@W�=|<@2زv�5f@��h:np@�������:              �?        �gҝ�       p�*	�!)���A��*�

summaries/loss�2�@
}
summaries/histogram_loss*a	    W�@    W�@      �?!    W�@) +t�:@2زv�5f@��h:np@�������:              �?        ֽ�9�       p�*	Kr)���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    �5@    �5@      �?!    �5@)  D[ �9@2!��v�@زv�5f@�������:              �?        a�4�       p�*	��)���A��*�

summaries/lossN\�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �	u�:@2زv�5f@��h:np@�������:              �?        ���#�       p�*	h_*���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ��6V,9@2!��v�@زv�5f@�������:              �?        f��˦       p�*	�+���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@ش�:@2زv�5f@��h:np@�������:              �?        UTA�       p�*	5�+���A*�

summaries/lossQf�@
}
summaries/histogram_loss*a	    �,@    �,@      �?!    �,@)@h��vp9@2!��v�@زv�5f@�������:              �?        �����       p�*	��+���AÕ*�

summaries/loss�ڦ@
}
summaries/histogram_loss*a	   �\�@   �\�@      �?!   �\�@) �F�&0;@2زv�5f@��h:np@�������:              �?        �=��       p�*	|E,���Aĕ*�

summaries/loss@��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  �@�s:@2زv�5f@��h:np@�������:              �?        �M^�       p�*	��,���Aŕ*�

summaries/loss���@
}
summaries/histogram_loss*a	   �u�@   �u�@      �?!   �u�@) �A�x:@2زv�5f@��h:np@�������:              �?        q��       p�*	��,���Aƕ*�

summaries/lossۣ�@
}
summaries/histogram_loss*a	   `{4@   `{4@      �?!   `{4@)@V�p<@2زv�5f@��h:np@�������:              �?        !5k��       p�*	C=-���AǕ*�

summaries/losshA�@
}
summaries/histogram_loss*a	    -�@    -�@      �?!    -�@) �~}�X:@2زv�5f@��h:np@�������:              �?        �>��       p�*	��-���Aȕ*�

summaries/lossZ��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) �u�;@2زv�5f@��h:np@�������:              �?        4����       p�*	L�-���Aɕ*�

summaries/loss6x�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ٪�;@2زv�5f@��h:np@�������:              �?        ����       p�*	�+.���Aʕ*�

summaries/losslY�@
}
summaries/histogram_loss*a	   �-�@   �-�@      �?!   �-�@) d�rY;@2زv�5f@��h:np@�������:              �?        �Ȋ�       p�*	�v.���A˕*�

summaries/loss���@
}
summaries/histogram_loss*a	   �x@   �x@      �?!   �x@) ���/:@2زv�5f@��h:np@�������:              �?        �f�       p�*	v�.���A̕*�

summaries/lossѧ@
}
summaries/histogram_loss*a	   � �@   � �@      �?!   � �@)@��˗�;@2زv�5f@��h:np@�������:              �?        �]�N�       p�*	�/���A͕*�

summaries/loss��@
}
summaries/histogram_loss*a	   �"�@   �"�@      �?!   �"�@) y�C��;@2زv�5f@��h:np@�������:              �?        ����       p�*	=e/���AΕ*�

summaries/loss���@
}
summaries/histogram_loss*a	    T@    T@      �?!    T@)@P���;@2زv�5f@��h:np@�������:              �?        �� l�       p�*	��/���Aϕ*�

summaries/lossd�@
}
summaries/histogram_loss*a	   ��C@   ��C@      �?!   ��C@) ��C<@2زv�5f@��h:np@�������:              �?        `0���       p�*	�0���AЕ*�

summaries/lossj7�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) �����<@2زv�5f@��h:np@�������:              �?        C�       p�*	�V0���Aѕ*�

summaries/loss"?�@
}
summaries/histogram_loss*a	   @�G@   @�G@      �?!   @�G@) !p.�M<@2زv�5f@��h:np@�������:              �?        ��܆�       p�*	��0���Aҕ*�

summaries/lossR�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ����C;@2زv�5f@��h:np@�������:              �?        �7��       p�*	M�0���Aӕ*�

summaries/lossH�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) �[�;@2زv�5f@��h:np@�������:              �?        �r�       p�*	�I1���Aԕ*�

summaries/lossP>�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @�
v�;@2زv�5f@��h:np@�������:              �?        C0!�       p�*	��1���AՕ*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@���=@2زv�5f@��h:np@�������:              �?        _00��       p�*	��1���A֕*�

summaries/loss��@
}
summaries/histogram_loss*a	    V=@    V=@      �?!    V=@)@��6�1<@2زv�5f@��h:np@�������:              �?        ��b�       p�*		42���Aו*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@��S�E:@2زv�5f@��h:np@�������:              �?        }��ɦ       p�*	��2���Aؕ*�

summaries/loss��@
}
summaries/histogram_loss*a	   �p�@   �p�@      �?!   �p�@) ��<��:@2زv�5f@��h:np@�������:              �?        �W���       p�*	��2���Aٕ*�

summaries/loss���@
}
summaries/histogram_loss*a	   �Ԓ@   �Ԓ@      �?!   �Ԓ@) D&�Vt:@2زv�5f@��h:np@�������:              �?        �i\E�       p�*	%3���Aڕ*�

summaries/loss���@
}
summaries/histogram_loss*a	   `֟@   `֟@      �?!   `֟@)@J�ԕ:@2زv�5f@��h:np@�������:              �?        �*[�       p�*	`u3���Aە*�

summaries/loss�d�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �}�;@2زv�5f@��h:np@�������:              �?        �^#�       p�*	U�3���Aܕ*�

summaries/loss�6�@
}
summaries/histogram_loss*a	   @�F@   @�F@      �?!   @�F@) 1��e�9@2!��v�@زv�5f@�������:              �?        c�5�       p�*	�4���Aݕ*�

summaries/lossOע@
}
summaries/histogram_loss*a	   ��Z@   ��Z@      �?!   ��Z@)@�rDM�9@2!��v�@زv�5f@�������:              �?        d!|b�       p�*	zn4���Aޕ*�

summaries/loss��@
}
summaries/histogram_loss*a	   � �@   � �@      �?!   � �@)@��}>8@2!��v�@زv�5f@�������:              �?        %��L�       p�*	�4���Aߕ*�

summaries/loss(�@
}
summaries/histogram_loss*a	    e@    e@      �?!    e@) @@��9@2!��v�@زv�5f@�������:              �?        �`vަ       p�*	z5���A��*�

summaries/loss� �@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�"S:a8@2!��v�@زv�5f@�������:              �?        N�v>�       p�*	m5���A�*�

summaries/loss8v�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �툅8@2!��v�@زv�5f@�������:              �?        ']��       p�*	�5���A�*�

summaries/lossZ�@
}
summaries/histogram_loss*a	   @kA@   @kA@      �?!   @kA@) �6���9@2!��v�@زv�5f@�������:              �?        
�,��       p�*	G6���A�*�

summaries/lossm��@
}
summaries/histogram_loss*a	   �-�@   �-�@      �?!   �-�@)@^
bn;@2زv�5f@��h:np@�������:              �?        |Fĭ�       p�*	@Q6���A�*�

summaries/lossv3�@
}
summaries/histogram_loss*a	   �n�@   �n�@      �?!   �n�@) �ر�:@2زv�5f@��h:np@�������:              �?        I�;'�       p�*	f�6���A�*�

summaries/lossRq�@
}
summaries/histogram_loss*a	   @*@   @*@      �?!   @*@) �_*v#9@2!��v�@زv�5f@�������:              �?        GB���       p�*	��6���A�*�

summaries/loss,��@
}
summaries/histogram_loss*a	   �r@   �r@      �?!   �r@) �a�z�<@2زv�5f@��h:np@�������:              �?        ����       p�*	C97���A�*�

summaries/loss^�@
}
summaries/histogram_loss*a	   �k�@   �k�@      �?!   �k�@) ���ڋ;@2زv�5f@��h:np@�������:              �?        �HŦ       p�*	�7���A�*�

summaries/lossVܢ@
}
summaries/histogram_loss*a	   ��[@   ��[@      �?!   ��[@) 9����9@2!��v�@زv�5f@�������:              �?        �?~�       p�*	�<8���A�*�

summaries/lossޗ�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) !�S;@2زv�5f@��h:np@�������:              �?        ��J��       p�*	>�8���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   �� @   �� @      �?!   �� @) �_:�;@2زv�5f@��h:np@�������:              �?        ��+�       p�*	��8���A�*�

summaries/loss�ҥ@
}
summaries/histogram_loss*a	   @W�@   @W�@      �?!   @W�@) �kNT�:@2زv�5f@��h:np@�������:              �?        �c9Ϧ       p�*	�79���A�*�

summaries/loss� �@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) 99�<@2زv�5f@��h:np@�������:              �?        ��5�       p�*	��9���A�*�

summaries/losse��@
}
summaries/histogram_loss*a	   �l4@   �l4@      �?!   �l4@)@v��H<@2زv�5f@��h:np@�������:              �?        &u��       p�*	:���A�*�

summaries/loss�|�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) Q�&�8@2!��v�@زv�5f@�������:              �?        �#�N�       p�*	��:���A�*�

summaries/lossJ�@
}
summaries/histogram_loss*a	   @	�@   @	�@      �?!   @	�@) YU=O�8@2!��v�@زv�5f@�������:              �?        \)��       p�*	k�:���A�*�

summaries/loss"`�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) !a�#�8@2!��v�@زv�5f@�������:              �?        �x٦       p�*	@;���A�*�

summaries/loss�z�@
}
summaries/histogram_loss*a	    _�@    _�@      �?!    _�@) T�jd;@2زv�5f@��h:np@�������:              �?        Ht���       p�*	p�;���A�*�

summaries/loss(�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) �MN�;@2زv�5f@��h:np@�������:              �?        ��j��       p�*	��;���A�*�

summaries/loss|��@
}
summaries/histogram_loss*a	   �o�@   �o�@      �?!   �o�@) ���p:@2زv�5f@��h:np@�������:              �?        ����       p�*	�2<���A��*�

summaries/loss?
�@
}
summaries/histogram_loss*a	   �G@   �G@      �?!   �G@)@�>�\�;@2زv�5f@��h:np@�������:              �?        eX�       p�*	t�<���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) In�K:@2زv�5f@��h:np@�������:              �?        �'$�       p�*	��<���A��*�

summaries/loss�֦@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@��h�.;@2زv�5f@��h:np@�������:              �?        ��<Ϧ       p�*	�)=���A��*�

summaries/loss0I�@
}
summaries/histogram_loss*a	    &)@    &)@      �?!    &)@) @�m�;@2زv�5f@��h:np@�������:              �?        ���       p�*	�y=���A��*�

summaries/loss�ީ@
}
summaries/histogram_loss*a	   ��;@   ��;@      �?!   ��;@) Y
��-<@2زv�5f@��h:np@�������:              �?        J��=�       p�*	*�=���A��*�

summaries/lossD��@
}
summaries/histogram_loss*a	   ��>@   ��>@      �?!   ��>@) ���&7@2!��v�@زv�5f@�������:              �?        ���       p�*	�>���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) 1���:@2زv�5f@��h:np@�������:              �?        \�Z�       p�*	Di>���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �� @   �� @      �?!   �� @) !��@�;@2زv�5f@��h:np@�������:              �?        M���       p�*	��>���A��*�

summaries/lossY�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@�9�=;@2زv�5f@��h:np@�������:              �?         (mk�       p�*	 ?���A��*�

summaries/loss
q�@
}
summaries/histogram_loss*a	   @!N@   @!N@      �?!   @!N@) u���9@2!��v�@زv�5f@�������:              �?        �.��       p�*	2]?���A��*�

summaries/loss�?�@
}
summaries/histogram_loss*a	   ��g@   ��g@      �?!   ��g@)@2!ט:@2زv�5f@��h:np@�������:              �?        �3��       p�*	p�?���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    �R@    �R@      �?!    �R@)  �ρ�9@2!��v�@زv�5f@�������:              �?        0:"�       p�*	*�?���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   �A@   �A@      �?!   �A@)@*�*^�;@2زv�5f@��h:np@�������:              �?        �:�G�       p�*	+I@���A��*�

summaries/lossf�@
}
summaries/histogram_loss*a	   �l\@   �l\@      �?!   �l\@) )�]��<@2زv�5f@��h:np@�������:              �?        ����       p�*	��@���A��*�

summaries/lossFI�@
}
summaries/histogram_loss*a	   �(i@   �(i@      �?!   �(i@) �?�	:@2زv�5f@��h:np@�������:              �?        Lf�c�       p�*	��@���A��*�

summaries/loss�,�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @
6��;@2زv�5f@��h:np@�������:              �?        ��M��       p�*	7A���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �"v@   �"v@      �?!   �"v@) d*}�*:@2زv�5f@��h:np@�������:              �?        ��6g�       p�*	n�A���A��*�

summaries/lossU��@
}
summaries/histogram_loss*a	   �j�@   �j�@      �?!   �j�@)@��e4>@2زv�5f@��h:np@�������:              �?        -f�       p�*	��A���A��*�

summaries/lossp��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @�~:�:@2زv�5f@��h:np@�������:              �?        
PA6�       p�*	-$B���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��#@   ��#@      �?!   ��#@) �`i��;@2زv�5f@��h:np@�������:              �?        �Zt��       p�*	�rB���A��*�

summaries/loss6��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �*��8@2!��v�@زv�5f@�������:              �?        �d�       p�*	)�B���A��*�

summaries/lossFѤ@
}
summaries/histogram_loss*a	   �(�@   �(�@      �?!   �(�@) ��03�:@2زv�5f@��h:np@�������:              �?        ;x&��       p�*	]C���A��*�

summaries/lossRr�@
}
summaries/histogram_loss*a	   @JN@   @JN@      �?!   @JN@) �H�_<@2زv�5f@��h:np@�������:              �?        'v�T�       p�*	�hC���A��*�

summaries/lossE
�@
}
summaries/histogram_loss*a	   �H�@   �H�@      �?!   �H�@)@�]�}�=@2زv�5f@��h:np@�������:              �?        K't�       p�*	o�C���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �@   �@      �?!   �@) ����;@2زv�5f@��h:np@�������:              �?        F�M�       p�*	�D���A��*�

summaries/lossj0�@
}
summaries/histogram_loss*a	   @@   @@      �?!   @@) ���;@2زv�5f@��h:np@�������:              �?        [|Ҧ       p�*	"TD���A��*�

summaries/loss�>�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@"Bs�P;@2زv�5f@��h:np@�������:              �?        .F���       p�*	s�D���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��b@   ��b@      �?!   ��b@)@<!1&�<@2زv�5f@��h:np@�������:              �?        �-֖�       p�*	E�D���A��*�

summaries/loss(��@
}
summaries/histogram_loss*a	    �Q@    �Q@      �?!    �Q@) �E��g<@2زv�5f@��h:np@�������:              �?        m��R�       p�*	tCE���A��*�

summaries/loss�*�@
}
summaries/histogram_loss*a	   @S�@   @S�@      �?!   @S�@) )�K'n8@2!��v�@زv�5f@�������:              �?        �N�˦       p�*	��E���A��*�

summaries/loss>�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@ �:@2زv�5f@��h:np@�������:              �?        ��q��       p�*	f�E���A��*�

summaries/loss2��@
}
summaries/histogram_loss*a	   @F@   @F@      �?!   @F@) qhu>@2زv�5f@��h:np@�������:              �?        k�8�       p�*	KF���A��*�

summaries/loss�N�@
}
summaries/histogram_loss*a	   ��	@   ��	@      �?!   ��	@) �w�[>@2زv�5f@��h:np@�������:              �?        ��Bl�       p�*	�F���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @}@   @}@      �?!   @}@) 	<x�=:@2زv�5f@��h:np@�������:              �?        �����       p�*	)�F���A��*�

summaries/lossx�@
}
summaries/histogram_loss*a	    �b@    �b@      �?!    �b@) 6���<@2زv�5f@��h:np@�������:              �?        �}�٦       p�*	9G���A��*�

summaries/loss C�@
}
summaries/histogram_loss*a	    `�@    `�@      �?!    `�@)  @�R;@2زv�5f@��h:np@�������:              �?        �u �       p�*	ƊG���A��*�

summaries/loss�p�@
}
summaries/histogram_loss*a	    @    @      �?!    @) ����;@2زv�5f@��h:np@�������:              �?         � @�       p�*	�G���A��*�

summaries/loss�|�@
}
summaries/histogram_loss*a	   ��/@   ��/@      �?!   ��/@) �N_�w9@2!��v�@زv�5f@�������:              �?        .��o�       p�*	�$H���A��*�

summaries/loss&T�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) i=�K =@2زv�5f@��h:np@�������:              �?        ���       p�*	�zH���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @8@   @8@      �?!   @8@) i�F$<@2زv�5f@��h:np@�������:              �?        ��e٦       p�*	��H���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	    �4@    �4@      �?!    �4@) @�B�<@2زv�5f@��h:np@�������:              �?        Z��a�       p�*	�I���A��*�

summaries/loss�N�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) ��;@2زv�5f@��h:np@�������:              �?        k��T�       p�*	dI���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) $q�l�;@2زv�5f@��h:np@�������:              �?        I��w�       p�*	��I���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    �s@    �s@      �?!    �s@)  !��<@2زv�5f@��h:np@�������:              �?        �f��       p�*	`J���A��*�

summaries/loss�V�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) D�҉X;@2زv�5f@��h:np@�������:              �?        
�ꉦ       p�*	�}J���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   `cP@   `cP@      �?!   `cP@)@6),�?@2زv�5f@��h:np@�������:              �?        ���       p�*	!K���A��*�

summaries/losse'�@
}
summaries/histogram_loss*a	   ��D@   ��D@      �?!   ��D@)@v����>@2زv�5f@��h:np@�������:              �?        [X���       p�*	2�K���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    pv@    pv@      �?!    pv@)  �R�<@2زv�5f@��h:np@�������:              �?        �n7�       p�*	"L���A��*�

summaries/loss,l�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �IǓ_;@2زv�5f@��h:np@�������:              �?        �>O%�       p�*	�L���A��*�

summaries/loss�=�@
}
summaries/histogram_loss*a	   @�G@   @�G@      �?!   @�G@) Ɋz�M<@2زv�5f@��h:np@�������:              �?        G�B�       p�*	��L���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) d���;@2زv�5f@��h:np@�������:              �?        %WU��       p�*	f/M���A��*�

summaries/lossl�@
}
summaries/histogram_loss*a	   �m@   �m@      �?!   �m@) d��mI>@2زv�5f@��h:np@�������:              �?        ~���       p�*	�{M���A��*�

summaries/loss6�@
}
summaries/histogram_loss*a	   ��|@   ��|@      �?!   ��|@) ٟ���<@2زv�5f@��h:np@�������:              �?        �J��       p�*	2�M���A��*�

summaries/loss�b�@
}
summaries/histogram_loss*a	   �V�@   �V�@      �?!   �V�@) Y��
	;@2زv�5f@��h:np@�������:              �?        �M�ڦ       p�*	]N���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �[ @   �[ @      �?!   �[ @) !��;@2زv�5f@��h:np@�������:              �?        oQӦ       p�*	bN���A��*�

summaries/lossL:�@
}
summaries/histogram_loss*a	   �I�@   �I�@      �?!   �I�@) ����V:@2زv�5f@��h:np@�������:              �?        }���       p�*	��N���A��*�

summaries/loss5-�@
}
summaries/histogram_loss*a	   ��%@   ��%@      �?!   ��%@)@>k�-�;@2زv�5f@��h:np@�������:              �?        {צX�       p�*	a�N���A��*�

summaries/loss�c�@
}
summaries/histogram_loss*a	   @r�@   @r�@      �?!   @r�@) я��\;@2زv�5f@��h:np@�������:              �?        ��J��       p�*	MKO���A��*�

summaries/loss�l�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) ��f:@2زv�5f@��h:np@�������:              �?        ���Φ       p�*	��O���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   `y�@   `y�@      �?!   `y�@)@�h}3;@2زv�5f@��h:np@�������:              �?        vK8��       p�*	��O���A��*�

summaries/loss-x�@
}
summaries/histogram_loss*a	   �O@   �O@      �?!   �O@)@���`<@2زv�5f@��h:np@�������:              �?        4?���       p�*	1BP���A��*�

summaries/loss�q�@
}
summaries/histogram_loss*a	    9@    9@      �?!    9@)@�Øb�;@2زv�5f@��h:np@�������:              �?        bp,�       p�*	�P���A��*�

summaries/loss�ި@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ���S�;@2زv�5f@��h:np@�������:              �?        ?�?R�       p�*	��P���A��*�

summaries/loss� �@
}
summaries/histogram_loss*a	    D@    D@      �?!    D@) @{�C<@2زv�5f@��h:np@�������:              �?        �p�       p�*	CQ���A��*�

summaries/loss<��@
}
summaries/histogram_loss*a	   ��5@   ��5@      �?!   ��5@) ����<@2زv�5f@��h:np@�������:              �?        {���       p�*		�Q���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    �#@    �#@      �?!    �#@)  �(R�;@2زv�5f@��h:np@�������:              �?        .�ym�       p�*	��Q���A��*�

summaries/loss@
}
summaries/histogram_loss*a	   �ݳ@   �ݳ@      �?!   �ݳ@)@�x���:@2زv�5f@��h:np@�������:              �?        �]�7�       p�*	5?R���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) 1��=@2زv�5f@��h:np@�������:              �?        �����       p�*	*�R���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) Y���;@2زv�5f@��h:np@�������:              �?        �}2��       p�*	��R���A��*�

summaries/lossX٤@
}
summaries/histogram_loss*a	    +�@    +�@      �?!    +�@) ��Q̉:@2زv�5f@��h:np@�������:              �?        ��       p�*	�5S���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    @    @      �?!    @) @�R2�;@2زv�5f@��h:np@�������:              �?        �D��       p�*	8�S���A��*�

summaries/lossF��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ɦό;@2زv�5f@��h:np@�������:              �?        F7�       p�*	��S���A��*�

summaries/loss�t�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �?�#>@2زv�5f@��h:np@�������:              �?        \�Ȧ       p�*	�/T���A��*�

summaries/lossx��@
}
summaries/histogram_loss*a	    /@    /@      �?!    /@) j�-�;@2زv�5f@��h:np@�������:              �?        bxC��       p�*	h}T���A��*�

summaries/lossF�@
}
summaries/histogram_loss*a	   �( @   �( @      �?!   �( @) �g�j�;@2زv�5f@��h:np@�������:              �?        m%�y�       p�*	`�T���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �mh^g8@2!��v�@زv�5f@�������:              �?        ���[�       p�*	�U���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    ߵ@    ߵ@      �?!    ߵ@) ����:@2زv�5f@��h:np@�������:              �?        1*�K�       p�*	�mU���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) d����:@2زv�5f@��h:np@�������:              �?        ���?�       p�*	��U���A*�

summaries/loss�ߝ@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) )j�W8@2!��v�@زv�5f@�������:              �?        o�)��       p�*	�V���AÖ*�

summaries/loss訛@
}
summaries/histogram_loss*a	    u@    u@      �?!    u@) �T~�7@2!��v�@زv�5f@�������:              �?        !���       p�*	9fV���AĖ*�

summaries/lossg��@
}
summaries/histogram_loss*a	   �L�@   �L�@      �?!   �L�@)@\MJ�<8@2!��v�@زv�5f@�������:              �?        �ta�       p�*	��V���AŖ*�

summaries/loss�m�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @֧`;@2زv�5f@��h:np@�������:              �?        �Mh�       p�*	�W���AƖ*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@@��:@2زv�5f@��h:np@�������:              �?        +vӦ       p�*	XW���Aǖ*�

summaries/lossa�@
}
summaries/histogram_loss*a	    �a@    �a@      �?!    �a@)@0ϋb�<@2زv�5f@��h:np@�������:              �?        s6��       p�*	��W���AȖ*�

summaries/loss���@
}
summaries/histogram_loss*a	   �T�@   �T�@      �?!   �T�@) D�fv�=@2زv�5f@��h:np@�������:              �?        ��a�       p�*	/X���Aɖ*�

summaries/loss�K�@
}
summaries/histogram_loss*a	   `}I@   `}I@      �?!   `}I@)@n��
�9@2!��v�@زv�5f@�������:              �?        [�Ճ�       p�*	R�X���Aʖ*�

summaries/lossk��@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@.��h;@2زv�5f@��h:np@�������:              �?        >�W�       p�*	
�X���A˖*�

summaries/loss���@
}
summaries/histogram_loss*a	   �r�@   �r�@      �?!   �r�@) �f�xl;@2زv�5f@��h:np@�������:              �?        �舣�       p�*	�Y���A̖*�

summaries/loss�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) 1�;@2زv�5f@��h:np@�������:              �?        
H�3�       p�*	&sY���A͖*�

summaries/lossX8�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) ���LM=@2زv�5f@��h:np@�������:              �?        ���ɦ       p�*	�Y���AΖ*�

summaries/loss	�@
}
summaries/histogram_loss*a	   � A@   � A@      �?!   � A@)@�_g�;<@2زv�5f@��h:np@�������:              �?        �����       p�*	Z���Aϖ*�

summaries/lossᎪ@
}
summaries/histogram_loss*a	    �Q@    �Q@      �?!    �Q@)@p���h<@2زv�5f@��h:np@�������:              �?        /Gm�       p�*	�fZ���AЖ*�

summaries/loss���@
}
summaries/histogram_loss*a	   `S?@   `S?@      �?!   `S?@)@vFK�9@2!��v�@زv�5f@�������:              �?        ��� �       p�*	��Z���Aі*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�E�	9@2!��v�@زv�5f@�������:              �?        �#[��       p�*	[���AҖ*�

summaries/loss�Τ@
}
summaries/histogram_loss*a	    י@    י@      �?!    י@) )�`�:@2زv�5f@��h:np@�������:              �?        "��k�       p�*	=y[���AӖ*�

summaries/loss�u�@
}
summaries/histogram_loss*a	   ��N@   ��N@      �?!   ��N@) $Q�>�9@2!��v�@زv�5f@�������:              �?        	+c%�       p�*	v�[���AԖ*�

summaries/lossޡ@
}
summaries/histogram_loss*a	   `�;@   `�;@      �?!   `�;@)@E�B�9@2!��v�@زv�5f@�������:              �?        ?�)�       p�*	d>\���AՖ*�

summaries/loss�o�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @�V��8@2!��v�@زv�5f@�������:              �?        &p��       p�*	��\���A֖*�

summaries/loss V�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)   9��:@2زv�5f@��h:np@�������:              �?        ?ƞ��       p�*	1]���Aז*�

summaries/loss^��@
}
summaries/histogram_loss*a	   ��W@   ��W@      �?!   ��W@) ��:x<@2زv�5f@��h:np@�������:              �?        ��զ       p�*	̗]���Aؖ*�

summaries/loss���@
}
summaries/histogram_loss*a	   �Ww@   �Ww@      �?!   �Ww@) A)���<@2زv�5f@��h:np@�������:              �?        �T<��       p�*	#1^���Aٖ*�

summaries/loss`.�@
}
summaries/histogram_loss*a	    �E@    �E@      �?!    �E@)  �y��9@2!��v�@زv�5f@�������:              �?        ;�6��       p�*	�^���Aږ*�

summaries/loss�1�@
}
summaries/histogram_loss*a	    ?@    ?@      �?!    ?@) 8�g�;@2زv�5f@��h:np@�������:              �?        c5}R�       p�*	�$_���Aۖ*�

summaries/loss a�@
}
summaries/histogram_loss*a	    $,@    $,@      �?!    $,@)  QFX<@2زv�5f@��h:np@�������:              �?        ƕ��       p�*	�_���Aܖ*�

summaries/loss�F�@
}
summaries/histogram_loss*a	   �ֈ@   �ֈ@      �?!   �ֈ@) ����Z:@2زv�5f@��h:np@�������:              �?        U���       p�*	�`���Aݖ*�

summaries/loss��@
}
summaries/histogram_loss*a	   �\@   �\@      �?!   �\@) �WŃ<@2زv�5f@��h:np@�������:              �?        P�Q
�       p�*	�`���Aޖ*�

summaries/loss�o�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@����`;@2زv�5f@��h:np@�������:              �?        ��J��       p�*	�<a���Aߖ*�

summaries/loss�Ţ@
}
summaries/histogram_loss*a	   �X@   �X@      �?!   �X@)@�����9@2!��v�@زv�5f@�������:              �?        @����       p�*	ԝa���A��*�

summaries/lossUn�@
}
summaries/histogram_loss*a	   ��m@   ��m@      �?!   ��m@)@�F%�<@2زv�5f@��h:np@�������:              �?        
��       p�*	�-b���A�*�

summaries/loss&q�@
}
summaries/histogram_loss*a	   �$n@   �$n@      �?!   �$n@) i�R:@2زv�5f@��h:np@�������:              �?        [�Ҧ       p�*	I�b���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) t��i=@2زv�5f@��h:np@�������:              �?        /�kq�       p�*	j/c���A�*�

summaries/loss ��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)  �u=@2زv�5f@��h:np@�������:              �?        ��f�       p�*	�c���A�*�

summaries/loss;��@
}
summaries/histogram_loss*a	   `�u@   `�u@      �?!   `�u@)@fU���<@2زv�5f@��h:np@�������:              �?        ��t�       p�*	 
d���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �xG�9;@2زv�5f@��h:np@�������:              �?        �%�Ѧ       p�*	��d���A�*�

summaries/loss+�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@Na��:@2زv�5f@��h:np@�������:              �?        ap��       p�*	��d���A�*�

summaries/loss¢�@
}
summaries/histogram_loss*a	   @XT@   @XT@      �?!   @XT@) ��H$o<@2زv�5f@��h:np@�������:              �?        pC�m�       p�*	ȳe���A�*�

summaries/loss�u�@
}
summaries/histogram_loss*a	   ��n@   ��n@      �?!   ��n@) asO�:@2زv�5f@��h:np@�������:              �?        Vj�F�       p�*	�Cf���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   �S@   �S@      �?!   �S@) �,��k<@2زv�5f@��h:np@�������:              �?        ��Φ       p�*	_�f���A�*�

summaries/lossh8�@
}
summaries/histogram_loss*a	    '@    '@      �?!    '@) �j��a9@2!��v�@زv�5f@�������:              �?        3a@��       p�*	�1g���A�*�

summaries/loss)��@
}
summaries/histogram_loss*a	    %w@    %w@      �?!    %w@)@$��8�<@2زv�5f@��h:np@�������:              �?        �}��       p�*	ʇg���A�*�

summaries/loss�֗@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��/Ƀ6@2!��v�@زv�5f@�������:              �?        6wq��       p�*	[
h���A�*�

summaries/loss�	�@
}
summaries/histogram_loss*a	   �9�@   �9�@      �?!   �9�@)@X�K�<@2زv�5f@��h:np@�������:              �?        :����       p�*	�h���A�*�

summaries/loss�,�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@�T�6@2!��v�@زv�5f@�������:              �?        ԟ���       p�*	gDi���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) Q���j;@2زv�5f@��h:np@�������:              �?        R3�       p�*	~�i���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��1@   ��1@      �?!   ��1@)@�O�|9@2!��v�@زv�5f@�������:              �?        Vvg�       p�*	Xj���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   �}@   �}@      �?!   �}@)@H����<@2زv�5f@��h:np@�������:              �?        �뺦       p�*	܃j���A�*�

summaries/loss~��@
}
summaries/histogram_loss*a	   ��_@   ��_@      �?!   ��_@) ���9@2!��v�@زv�5f@�������:              �?        `�_��       p�*	Kk���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��4@   ��4@      �?!   ��4@)@��/<@2زv�5f@��h:np@�������:              �?        �ͫ�       p�*	E�k���A��*�

summaries/lossp�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @\�6H9@2!��v�@زv�5f@�������:              �?        碱q�       p�*	�Sl���A��*�

summaries/loss�ף@
}
summaries/histogram_loss*a	   ��z@   ��z@      �?!   ��z@) �T�7:@2زv�5f@��h:np@�������:              �?        <EQ�       p�*	��l���A��*�

summaries/lossL�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��;@2زv�5f@��h:np@�������:              �?        ��n�       p�*	��m���A��*�

summaries/lossj��@
}
summaries/histogram_loss*a	   @m�@   @m�@      �?!   @m�@) ���V�7@2!��v�@زv�5f@�������:              �?        ��#G�       p�*	�Vn���A��*�

summaries/loss�q�@
}
summaries/histogram_loss*a	   @4@   @4@      �?!   @4@) ��U�;@2زv�5f@��h:np@�������:              �?        ?y=��       p�*	��n���A��*�

summaries/lossy�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@LA�I�:@2زv�5f@��h:np@�������:              �?        ��G�       p�*	�@o���A��*�

summaries/loss�\�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @XO�a:@2زv�5f@��h:np@�������:              �?        ���e�       p�*	�o���A��*�

summaries/losse9�@
}
summaries/histogram_loss*a	   �,@   �,@      �?!   �,@)@v�[آ;@2زv�5f@��h:np@�������:              �?        U�0�       p�*	rp���A��*�

summaries/loss&�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) i���'9@2!��v�@زv�5f@�������:              �?        ��hȦ       p�*	�p���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    c�@    c�@      �?!    c�@) ����v;@2زv�5f@��h:np@�������:              �?        �s6�       p�*	q���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��~&>@2زv�5f@��h:np@�������:              �?        ��0�       p�*	!qq���A��*�

summaries/lossj*�@
}
summaries/histogram_loss*a	   @M�@   @M�@      �?!   @M�@) ��]A�<@2زv�5f@��h:np@�������:              �?        ��뒦       p�*	��q���A��*�

summaries/loss�Ԣ@
}
summaries/histogram_loss*a	   @�Z@   @�Z@      �?!   @�Z@) ir8��9@2!��v�@زv�5f@�������:              �?        B���       p�*	sr���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    \�@    \�@      �?!    \�@)  �N;@2زv�5f@��h:np@�������:              �?        �3?%�       p�*	Ihr���A��*�

summaries/loss6�@
}
summaries/histogram_loss*a	   �ƽ@   �ƽ@      �?!   �ƽ@) ��;�:@2زv�5f@��h:np@�������:              �?        �b��       p�*	��r���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	    ~w@    ~w@      �?!    ~w@) @ &'�<@2زv�5f@��h:np@�������:              �?        ����       p�*	�s���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  B��;@2زv�5f@��h:np@�������:              �?        �fv��       p�*	�[s���A��*�

summaries/loss�E�@
}
summaries/histogram_loss*a	    �h@    �h@      �?!    �h@) @��<@2زv�5f@��h:np@�������:              �?        ��wk�       p�*	��s���A��*�

summaries/losssU�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@jdû =@2زv�5f@��h:np@�������:              �?        �៦       p�*	ht���A��*�

summaries/loss�(�@
}
summaries/histogram_loss*a	   �E@   �E@      �?!   �E@)@@U�F<@2زv�5f@��h:np@�������:              �?        ѧ�$�       p�*	htt���A��*�

summaries/loss�ȩ@
}
summaries/histogram_loss*a	   @9@   @9@      �?!   @9@) ��+�&<@2زv�5f@��h:np@�������:              �?        a�mJ�       p�*	��t���A��*�

summaries/loss@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�x�/p;@2زv�5f@��h:np@�������:              �?        t��       p�*	V�u���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �s @   �s @      �?!   �s @) �A3/�;@2زv�5f@��h:np@�������:              �?        ��c<�       p�*	�v���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ഼@   ഼@      �?!   ഼@)@���u�:@2زv�5f@��h:np@�������:              �?        �>�Ҧ       p�*	�3w���A��*�

summaries/loss�4�@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@JM�z9@2!��v�@زv�5f@�������:              �?        ��p�       p�*	D�w���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �4@   �4@      �?!   �4@) ��#�;@2زv�5f@��h:np@�������:              �?        �'nD�       p�*	T�x���A��*�

summaries/loss�٢@
}
summaries/histogram_loss*a	   �<[@   �<[@      �?!   �<[@) ���9@2!��v�@زv�5f@�������:              �?        ����       p�*	7Vy���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @b@   @b@      �?!   @b@) Qk�
.9@2!��v�@زv�5f@�������:              �?        �xy�       p�*	��y���A��*�

summaries/lossz(�@
}
summaries/histogram_loss*a	   @e@   @e@      �?!   @e@) ��p$�9@2!��v�@زv�5f@�������:              �?        i�WS�       p�*	>zz���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��s@   ��s@      �?!   ��s@) ��|�<@2زv�5f@��h:np@�������:              �?        ���       p�*	��z���A��*�

summaries/lossHJ�@
}
summaries/histogram_loss*a	    I�@    I�@      �?!    I�@) m�^S=@2زv�5f@��h:np@�������:              �?        V%k˦       p�*	�Q{���A��*�

summaries/loss$I�@
}
summaries/histogram_loss*a	   �$�@   �$�@      �?!   �$�@) Dc� ;@2زv�5f@��h:np@�������:              �?        WR��       p�*	f�{���A��*�

summaries/lossbI�@
}
summaries/histogram_loss*a	   @,	@   @,	@      �?!   @,	@) aBj�;@2زv�5f@��h:np@�������:              �?        �2�       p�*	��|���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@foҐ9;@2زv�5f@��h:np@�������:              �?        %�6�       p�*	B
}���A��*�

summaries/loss�ߪ@
}
summaries/histogram_loss*a	    �[@    �[@      �?!    �[@)@��My�<@2زv�5f@��h:np@�������:              �?        ���       p�*	�}���A��*�

summaries/loss~D�@
}
summaries/histogram_loss*a	   ��H@   ��H@      �?!   ��H@) ��j�O<@2زv�5f@��h:np@�������:              �?        ����       p�*	M~���A��*�

summaries/lossK��@
}
summaries/histogram_loss*a	   `I�@   `I�@      �?!   `I�@)@~�5A�=@2زv�5f@��h:np@�������:              �?        /9�z�       p�*	R�~���A��*�

summaries/loss�X�@
}
summaries/histogram_loss*a	   `K@   `K@      �?!   `K@)@��uV<@2زv�5f@��h:np@�������:              �?        ˼8�       p�*	 C���A��*�

summaries/lossV�@
}
summaries/histogram_loss*a	   ��J@   ��J@      �?!   ��J@)@8�6�U<@2زv�5f@��h:np@�������:              �?        �!HL�       p�*	n����A��*�

summaries/loss�>�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@N����;@2زv�5f@��h:np@�������:              �?        ��s�       p�*	5\����A��*�

summaries/loss�w�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @F�6>@2زv�5f@��h:np@�������:              �?        �y>��       p�*	����A��*�

summaries/lossx��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) .���8@2!��v�@زv�5f@�������:              �?        �/M�       p�*	N�����A��*�

summaries/loss
,�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �R:@2زv�5f@��h:np@�������:              �?        ��N��       p�*	�����A��*�

summaries/loss$��@
}
summaries/histogram_loss*a	   �$�@   �$�@      �?!   �$�@) D��';@2زv�5f@��h:np@�������:              �?        I͜%�       p�*	o�����A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   �#~@   �#~@      �?!   �#~@) �n���<@2زv�5f@��h:np@�������:              �?        Ц       p�*	_'����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �Cs@   �Cs@      �?!   �Cs@) �l�7�?@2��h:np@S���߮@�������:              �?        ��צ       p�*	ũ����A��*�

summaries/loss�T�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �V�5>@2زv�5f@��h:np@�������:              �?        P!���       p�*	�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    S�@    S�@      �?!    S�@) ���=@2زv�5f@��h:np@�������:              �?        �}��       p�*	܄���A��*�

summaries/lossd��@
}
summaries/histogram_loss*a	   �,V@   �,V@      �?!   �,V@) ���t<@2زv�5f@��h:np@�������:              �?        ��/��       p�*	9�����A��*�

summaries/loss�@
}
summaries/histogram_loss*a	    C�@    C�@      �?!    C�@) ���0�;@2زv�5f@��h:np@�������:              �?        InWۦ       p�*	g*����A��*�

summaries/loss�ۧ@
}
summaries/histogram_loss*a	   �~�@   �~�@      �?!   �~�@)@�P.�;@2زv�5f@��h:np@�������:              �?        f��       p�*	i�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ���[�=@2زv�5f@��h:np@�������:              �?        Q�uy�       p�*	g+����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) �5j��<@2زv�5f@��h:np@�������:              �?        H�Ŧ       p�*	�����A��*�

summaries/loss�Z�@
}
summaries/histogram_loss*a	    _�@    _�@      �?!    _�@)@��L�Y;@2زv�5f@��h:np@�������:              �?        =�>��       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @^@   @^@      �?!   @^@) �װ+�<@2زv�5f@��h:np@�������:              �?        �����       p�*	;�����A��*�

summaries/loss2�@
}
summaries/histogram_loss*a	   @F�@   @F�@      �?!   @F�@) q��~�:@2زv�5f@��h:np@�������:              �?        ^�=�       p�*	�.����A��*�

summaries/loss;ޤ@
}
summaries/histogram_loss*a	   `Ǜ@   `Ǜ@      �?!   `Ǜ@)@f� _�:@2زv�5f@��h:np@�������:              �?        ��l�       p�*	?�����A��*�

summaries/loss�2�@
}
summaries/histogram_loss*a	   @S�@   @S�@      �?!   @S�@) )!nj�:@2زv�5f@��h:np@�������:              �?        �9ͦ       p�*	�P����A��*�

summaries/loss�i�@
}
summaries/histogram_loss*a	   �6�@   �6�@      �?!   �6�@) �I�^;@2زv�5f@��h:np@�������:              �?        ���Ԧ       p�*	�����A��*�

summaries/loss�Ş@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @���8@2!��v�@زv�5f@�������:              �?        V|�       p�*	�u����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   � �@   � �@      �?!   � �@) 	��U�8@2!��v�@زv�5f@�������:              �?        ���I�       p�*	�����A��*�

summaries/loss6��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ْ�?";@2زv�5f@��h:np@�������:              �?        ����       p�*	������A��*�

summaries/loss�/�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �Vy�K;@2زv�5f@��h:np@�������:              �?        ���<�       p�*	����A��*�

summaries/loss�Ħ@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) 1�V );@2زv�5f@��h:np@�������:              �?        ���1�       p�*	ލ���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��}@   ��}@      �?!   ��}@) q�u�7@2!��v�@زv�5f@�������:              �?        ��1��       p�*	4�����A��*�

summaries/loss�C�@
}
summaries/histogram_loss*a	   @t�@   @t�@      �?!   @t�@) ��Vb�=@2زv�5f@��h:np@�������:              �?        h�_�       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �C�@   �C�@      �?!   �C�@) ᎄk�:@2زv�5f@��h:np@�������:              �?        =ܤ�       p�*	�ӏ���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �5^@   �5^@      �?!   �5^@)@h�%x�<@2زv�5f@��h:np@�������:              �?        K�b�       p�*	�`����A��*�

summaries/lossHo�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) ��(�:@2زv�5f@��h:np@�������:              �?        G��e�       p�*	������A��*�

summaries/loss�ɡ@
}
summaries/histogram_loss*a	   �49@   �49@      �?!   �49@)@�j�Џ9@2!��v�@زv�5f@�������:              �?        ��T��       p�*	�Ց���A��*�

summaries/lossx�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) r�nY8@2!��v�@زv�5f@�������:              �?        �NGզ       p�*	$*����A��*�

summaries/lossⰞ@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ��K��8@2!��v�@زv�5f@�������:              �?        �иw�       p�*	�v����A��*�

summaries/lossBȟ@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) Ą��8@2!��v�@زv�5f@�������:              �?        �|E��       p�*	~ǒ���A��*�

summaries/loss8:�@
}
summaries/histogram_loss*a	    G�@    G�@      �?!    G�@) [�@O;@2زv�5f@��h:np@�������:              �?        %5��       p�*	L����A��*�

summaries/loss:7�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) I~v��<@2زv�5f@��h:np@�������:              �?        ���æ       p�*	�g����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �٣@   �٣@      �?!   �٣@) q;P.�:@2زv�5f@��h:np@�������:              �?        #�'��       p�*	`�����A��*�

summaries/loss�ע@
}
summaries/histogram_loss*a	   ��Z@   ��Z@      �?!   ��Z@) d{6c�9@2!��v�@زv�5f@�������:              �?        q�       p�*	K�����A��*�

summaries/loss	��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@���)�;@2زv�5f@��h:np@�������:              �?        k��ئ       p�*	�N����A*�

summaries/loss۩@
}
summaries/histogram_loss*a	   �c;@   �c;@      �?!   �c;@) �:��,<@2زv�5f@��h:np@�������:              �?        �y��       p�*	ݜ����A×*�

summaries/lossm/�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@M�b�:@2زv�5f@��h:np@�������:              �?        �ψB�       p�*	|����Aė*�

summaries/loss���@
}
summaries/histogram_loss*a	   @�q@   @�q@      �?!   @�q@) ���@�<@2زv�5f@��h:np@�������:              �?        ���K�       p�*	�@����Aŗ*�

summaries/loss�&�@
}
summaries/histogram_loss*a	   `ۤ@   `ۤ@      �?!   `ۤ@)@��Ǣ:@2زv�5f@��h:np@�������:              �?        �G��       p�*	C�����AƗ*�

summaries/lossr2�@
}
summaries/histogram_loss*a	   @N�@   @N�@      �?!   @N�@) �.s��=@2زv�5f@��h:np@�������:              �?        ?��I�       p�*	sٕ���AǗ*�

summaries/loss̯�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �"l�:@2زv�5f@��h:np@�������:              �?        <_���       p�*	h&����Aȗ*�

summaries/loss��@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@��;�:@2زv�5f@��h:np@�������:              �?        8󿩦       p�*	�s����Aɗ*�

summaries/lossK�@
}
summaries/histogram_loss*a	   �aI@   �aI@      �?!   �aI@) 1M&�Q<@2زv�5f@��h:np@�������:              �?        ]*�R�       p�*	Ė���Aʗ*�

summaries/lossR��@
}
summaries/histogram_loss*a	   @*�@   @*�@      �?!   @*�@) �7�p:@2زv�5f@��h:np@�������:              �?        �R��       p�*	�����A˗*�

summaries/lossȤ@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@8��;�:@2زv�5f@��h:np@�������:              �?        �7��       p�*	�b����A̗*�

summaries/loss��@
}
summaries/histogram_loss*a	   �0>@   �0>@      �?!   �0>@) s�j�9@2!��v�@زv�5f@�������:              �?        ��EJ�       p�*	������A͗*�

summaries/loss�@�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �����<@2زv�5f@��h:np@�������:              �?        ���       p�*	*�����AΗ*�

summaries/lossD��@
}
summaries/histogram_loss*a	   ��W@   ��W@      �?!   ��W@) �@�Ox<@2زv�5f@��h:np@�������:              �?        �]#��       p�*	zM����Aϗ*�

summaries/loss�G�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  �;�R=@2زv�5f@��h:np@�������:              �?        ����       p�*	ǚ����AЗ*�

summaries/loss��@
}
summaries/histogram_loss*a	   �8�@   �8�@      �?!   �8�@) ��n�<;@2زv�5f@��h:np@�������:              �?        D����       p�*	�"����Aї*�

summaries/lossԉ�@
}
summaries/histogram_loss*a	   �:�@   �:�@      �?!   �:�@) �%u8p:@2زv�5f@��h:np@�������:              �?        W�[0�       p�*	{����Aҗ*�

summaries/loss3e�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@
R��	;@2زv�5f@��h:np@�������:              �?        ����       p�*	&ș���Aӗ*�

summaries/loss.��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �P�u;@2زv�5f@��h:np@�������:              �?        ���       p�*	�����Aԗ*�

summaries/lossQ�@
}
summaries/histogram_loss*a	    �`@    �`@      �?!    �`@)@hy\�<@2زv�5f@��h:np@�������:              �?        &[æ       p�*	�o����A՗*�

summaries/loss�i�@
}
summaries/histogram_loss*a	   �>-@   �>-@      �?!   �>-@) $� D<@2زv�5f@��h:np@�������:              �?        j)Ŧ       p�*	L���A֗*�

summaries/lossJ@�@
}
summaries/histogram_loss*a	   @	�@   @	�@      �?!   @	�@) YE��<@2زv�5f@��h:np@�������:              �?        KE�%�       p�*	�����Aח*�

summaries/loss�@
}
summaries/histogram_loss*a	    �a@    �a@      �?!    �a@) 0�D�<@2زv�5f@��h:np@�������:              �?        ,��*�       p�*	�e����Aؗ*�

summaries/loss�M�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) qd��\:@2زv�5f@��h:np@�������:              �?        ���q�       p�*	�����Aٗ*�

summaries/lossӦ@
}
summaries/histogram_loss*a	   �c�@   �c�@      �?!   �c�@) �@�-;@2زv�5f@��h:np@�������:              �?        >YMO�       p�*	�����Aڗ*�

summaries/loss,��@
}
summaries/histogram_loss*a	   ��5@   ��5@      �?!   ��5@) ��%�<@2زv�5f@��h:np@�������:              �?        9��n�       p�*	�W����Aۗ*�

summaries/loss���@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) )X�x�=@2زv�5f@��h:np@�������:              �?        DFR�       p�*	������Aܗ*�

summaries/loss��@
}
summaries/histogram_loss*a	   �V@   �V@      �?!   �V@) Y^�Y9@2!��v�@زv�5f@�������:              �?        ]�1H�       p�*	v�����Aݗ*�

summaries/loss�F�@
}
summaries/histogram_loss*a	   ��H@   ��H@      �?!   ��H@) �'�m�9@2!��v�@زv�5f@�������:              �?        ��h\�       p�*	�O����Aޗ*�

summaries/loss�O�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ����;@2زv�5f@��h:np@�������:              �?        E�r%�       p�*	¤����Aߗ*�

summaries/lossǝ�@
}
summaries/histogram_loss*a	   ำ@   ำ@      �?!   ำ@)@,��0�:@2زv�5f@��h:np@�������:              �?        /�ɵ�       p�*	�����A��*�

summaries/loss�M�@
}
summaries/histogram_loss*a	   ��)@   ��)@      �?!   ��)@) a���;@2زv�5f@��h:np@�������:              �?        ɭ2�       p�*	'J����A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   `u�@   `u�@      �?!   `u�@)@_�;@2زv�5f@��h:np@�������:              �?        ��W�       p�*	$�����A�*�

summaries/loss�O�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) y�g�8@2!��v�@زv�5f@�������:              �?        p?�I�       p�*	�����A�*�

summaries/loss�>�@
}
summaries/histogram_loss*a	   @�'@   @�'@      �?!   @�'@) �α�c9@2!��v�@زv�5f@�������:              �?        }�b��       p�*	�:����A�*�

summaries/lossi��@
}
summaries/histogram_loss*a	    -�@    -�@      �?!    -�@)@D[�:8@2!��v�@زv�5f@�������:              �?        ���̦       p�*	s�����A�*�

summaries/loss�K�@
}
summaries/histogram_loss*a	   �~)@   �~)@      �?!   �~)@)@*�(h9@2!��v�@زv�5f@�������:              �?        ~s�Ҧ       p�*	�ӟ���A�*�

summaries/loss4�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@��*�;@2زv�5f@��h:np@�������:              �?        ,�B�       p�*	q!����A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   @P�@   @P�@      �?!   @P�@) ��{ �:@2زv�5f@��h:np@�������:              �?        ��O�       p�*	�p����A�*�

summaries/lossxy�@
}
summaries/histogram_loss*a	    /�@    /�@      �?!    /�@) ��s�:@2زv�5f@��h:np@�������:              �?        :�I�       p�*	ܺ����A�*�

summaries/loss,a�@
}
summaries/histogram_loss*a	   �%@   �%@      �?!   �%@) 䗨�;@2زv�5f@��h:np@�������:              �?        Q�/�       p�*	p����A�*�

summaries/lossh�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @@�ӷ:@2زv�5f@��h:np@�������:              �?        yk�I�       p�*	�R����A�*�

summaries/loss�̤@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) q=�:@2زv�5f@��h:np@�������:              �?        AwGf�       p�*	/�����A�*�

summaries/lossq�@
}
summaries/histogram_loss*a	    "@    "@      �?!    "@) @Ȼ%�;@2زv�5f@��h:np@�������:              �?        g�9�       p�*	�����A�*�

summaries/loss0�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @�y�?=@2زv�5f@��h:np@�������:              �?        N��+�       p�*	�>����A�*�

summaries/loss*3�@
}
summaries/histogram_loss*a	   @e�@   @e�@      �?!   @e�@) �p&�8@2!��v�@زv�5f@�������:              �?        ����       p�*	������A�*�

summaries/loss�/�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ѻ�8@2!��v�@زv�5f@�������:              �?        [D�<�       p�*	j٢���A�*�

summaries/loss(u�@
}
summaries/histogram_loss*a	    �N@    �N@      �?!    �N@) �e��_<@2زv�5f@��h:np@�������:              �?        �:俦       p�*	�'����A�*�

summaries/loss�P�@
}
summaries/histogram_loss*a	   @
@   @
@      �?!   @
@) 9�"~�;@2زv�5f@��h:np@�������:              �?        ��Q�       p�*	tz����A�*�

summaries/losso�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�`�U�:@2زv�5f@��h:np@�������:              �?        xɬ�       p�*	�ɣ���A�*�

summaries/lossjr�@
}
summaries/histogram_loss*a	   @M@   @M@      �?!   @M@) �����;@2زv�5f@��h:np@�������:              �?        �d��       p�*	�����A��*�

summaries/loss"�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) !ݩM:@2زv�5f@��h:np@�������:              �?        ���       p�*	�a����A��*�

summaries/loss�ӫ@
}
summaries/histogram_loss*a	   `zz@   `zz@      �?!   `zz@)@���*�<@2زv�5f@��h:np@�������:              �?        ��mͦ       p�*	S�����A��*�

summaries/loss/ �@
}
summaries/histogram_loss*a	   �@@   �@@      �?!   �@@)@(�9<@2زv�5f@��h:np@�������:              �?        ��}��       p�*	������A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   `�u@   `�u@      �?!   `�u@)@���<@2زv�5f@��h:np@�������:              �?        �,n�       p�*	�?����A��*�

summaries/loss�j�@
}
summaries/histogram_loss*a	   �R�@   �R�@      �?!   �R�@)@D����:@2زv�5f@��h:np@�������:              �?        �9���       p�*	m�����A��*�

summaries/loss8�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) ��'�:@2زv�5f@��h:np@�������:              �?        sbG�       p�*	�ӥ���A��*�

summaries/lossoq�@
}
summaries/histogram_loss*a	   �-N@   �-N@      �?!   �-N@)@��k�^<@2زv�5f@��h:np@�������:              �?        �c��       p�*	�����A��*�

summaries/loss�ߡ@
}
summaries/histogram_loss*a	   `�;@   `�;@      �?!   `�;@)@nP\ږ9@2!��v�@زv�5f@�������:              �?        :�޿�       p�*	l����A��*�

summaries/loss"�@
}
summaries/histogram_loss*a	   @� @   @� @      �?!   @� @) !�Ś�;@2زv�5f@��h:np@�������:              �?        B���       p�*	E�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �>��:@2زv�5f@��h:np@�������:              �?        �����       p�*	������A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �B�@   �B�@      �?!   �B�@) yN���=@2زv�5f@��h:np@�������:              �?        �L�J�       p�*	wG����A��*�

summaries/loss�˩@
}
summaries/histogram_loss*a	    u9@    u9@      �?!    u9@) ��t�'<@2زv�5f@��h:np@�������:              �?        ���T�       p�*	G�����A��*�

summaries/loss�գ@
}
summaries/histogram_loss*a	    �z@    �z@      �?!    �z@) ��v6:@2زv�5f@��h:np@�������:              �?        a��       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �U@   �U@      �?!   �U@) �h���;@2زv�5f@��h:np@�������:              �?        �`.9�       p�*	�+����A��*�

summaries/loss:�@
}
summaries/histogram_loss*a	    A@    A@      �?!    A@) ���;@2زv�5f@��h:np@�������:              �?        �����       p�*	�v����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �?�@   �?�@      �?!   �?�@) ��D;@2زv�5f@��h:np@�������:              �?        �=�Ѧ       p�*	<�����A��*�

summaries/lossUm�@
}
summaries/histogram_loss*a	   ��-@   ��-@      �?!   ��-@)@��:b<@2زv�5f@��h:np@�������:              �?        2LH��       p�*	����A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   ` �@   ` �@      �?!   ` �@)@����A;@2زv�5f@��h:np@�������:              �?        ����       p�*	�S����A��*�

summaries/loss�c�@
}
summaries/histogram_loss*a	    t@    t@      �?!    t@)@�z���;@2زv�5f@��h:np@�������:              �?        ���.�       p�*	������A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �st@   �st@      �?!   �st@) ����<@2زv�5f@��h:np@�������:              �?        �'*H�       p�*	-����A��*�

summaries/lossZ)�@
}
summaries/histogram_loss*a	   @+%@   @+%@      �?!   @+%@) �|���;@2زv�5f@��h:np@�������:              �?        EV���       p�*	�7����A��*�

summaries/loss<Z�@
}
summaries/histogram_loss*a	   �G�@   �G�@      �?!   �G�@) ��I\�:@2زv�5f@��h:np@�������:              �?        ��J��       p�*	������A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��s@   ��s@      �?!   ��s@)@�����<@2زv�5f@��h:np@�������:              �?        ��(�       p�*	�����A��*�

summaries/loss:�@
}
summaries/histogram_loss*a	   @g@@   @g@@      �?!   @g@@) I�D:<@2زv�5f@��h:np@�������:              �?        ��<�       p�*	������A��*�

summaries/lossb�@
}
summaries/histogram_loss*a	   `B�@   `B�@      �?!   `B�@)@Z��]�=@2زv�5f@��h:np@�������:              �?        B2G�       p�*	5H����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �>�@   �>�@      �?!   �>�@)@?";�=@2زv�5f@��h:np@�������:              �?        ˒!��       p�*	ǽ����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @�W@   @�W@      �?!   @�W@) 	�@7x<@2زv�5f@��h:np@�������:              �?        �R���       p�*	�
����A��*�

summaries/lossJ�@
}
summaries/histogram_loss*a	   @iC@   @iC@      �?!   @iC@) Y,LB<@2زv�5f@��h:np@�������:              �?        �6���       p�*	;W����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) A���:@2زv�5f@��h:np@�������:              �?        �i�W�       p�*	w�����A��*�

summaries/losst��@
}
summaries/histogram_loss*a	   �Nr@   �Nr@      �?!   �Nr@) $!�>�<@2زv�5f@��h:np@�������:              �?        "���       p�*	c����A��*�

summaries/loss�5�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ɢ٣�;@2زv�5f@��h:np@�������:              �?        ����       p�*	~9����A��*�

summaries/loss+�@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@N\��8@2!��v�@زv�5f@�������:              �?        ��o�       p�*	������A��*�

summaries/lossz�@
}
summaries/histogram_loss*a	   @/�@   @/�@      �?!   @/�@) �Csv�:@2زv�5f@��h:np@�������:              �?        !'E�       p�*	pή���A��*�

summaries/loss�V�@
}
summaries/histogram_loss*a	   @֊@   @֊@      �?!   @֊@) ���?@2��h:np@S���߮@�������:              �?        �	9�       p�*	 ����A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   ��=@   ��=@      �?!   ��=@)@���2<@2زv�5f@��h:np@�������:              �?        0����       p�*	,d����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �q�@   �q�@      �?!   �q�@)@x*`'�:@2زv�5f@��h:np@�������:              �?        �� �       p�*	ů����A��*�

summaries/loss*��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �/�
9;@2زv�5f@��h:np@�������:              �?        /�s��       p�*	z�����A��*�

summaries/lossZݦ@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) � �0;@2زv�5f@��h:np@�������:              �?        ��ꍦ       p�*	[E����A��*�

summaries/lossTx�@
}
summaries/histogram_loss*a	   �
/@   �
/@      �?!   �
/@) ��<@2زv�5f@��h:np@�������:              �?        ����       p�*	ҍ����A��*�

summaries/loss�8�@
}
summaries/histogram_loss*a	    '@    '@      �?!    '@)@��#�;@2زv�5f@��h:np@�������:              �?        WɁw�       p�*	8ڰ���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �B@   �B@      �?!   �B@) d�pB:@2زv�5f@��h:np@�������:              �?        w?�C�       p�*	�"����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �>�@   �>�@      �?!   �>�@) $��$�;@2زv�5f@��h:np@�������:              �?        �q�D�       p�*	�n����A��*�

summaries/lossk[�@
}
summaries/histogram_loss*a	   `m+@   `m+@      �?!   `m+@)@���t<@2زv�5f@��h:np@�������:              �?        G��Ħ       p�*	)�����A��*�

summaries/loss[�@
}
summaries/histogram_loss*a	   `a�@   `a�@      �?!   `a�@)@����8@2!��v�@زv�5f@�������:              �?        ��L�       p�*	h����A��*�

summaries/loss6 �@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �bB#E=@2زv�5f@��h:np@�������:              �?        ���k�       p�*	TQ����A��*�

summaries/loss(�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@���"�:@2زv�5f@��h:np@�������:              �?        ��U��       p�*	3�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @~�%�:@2زv�5f@��h:np@�������:              �?        �Ce�       p�*	�����A��*�

summaries/loss�4�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�/C��<@2زv�5f@��h:np@�������:              �?        ��C��       p�*	*9����A��*�

summaries/lossXe�@
}
summaries/histogram_loss*a	    �L@    �L@      �?!    �L@) ��?�Z<@2زv�5f@��h:np@�������:              �?        '!�       p�*	Q�����A��*�

summaries/loss,w�@
}
summaries/histogram_loss*a	   ��N@   ��N@      �?!   ��N@) �{z�`<@2زv�5f@��h:np@�������:              �?        ٓ#��       p�*	�ҳ���A��*�

summaries/lossNc�@
}
summaries/histogram_loss*a	   �iL@   �iL@      �?!   �iL@) �Z��?@2زv�5f@��h:np@�������:              �?        ��Ц       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �V�@   �V�@      �?!   �V�@) Y^��L:@2زv�5f@��h:np@�������:              �?        p��       p�*	x����A��*�

summaries/loss>��@
}
summaries/histogram_loss*a	   �'�@   �'�@      �?!   �'�@) ��;�x;@2زv�5f@��h:np@�������:              �?        PUh��       p�*	Wϴ���A��*�

summaries/loss
��@
}
summaries/histogram_loss*a	   @�U@   @�U@      �?!   @�U@) �g��9@2!��v�@زv�5f@�������:              �?        �/e��       p�*	 &����A��*�

summaries/loss�ŝ@
}
summaries/histogram_loss*a	   ຸ@   ຸ@      �?!   ຸ@)@���O8@2!��v�@زv�5f@�������:              �?        �Q�       p�*	�y����A��*�

summaries/loss젟@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) d��R�8@2!��v�@زv�5f@�������:              �?        6��E�       p�*	ֵ���A��*�

summaries/loss@��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  $jJ>@2زv�5f@��h:np@�������:              �?        xGlX�       p�*	�/����A��*�

summaries/lossů@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) d�C�+>@2زv�5f@��h:np@�������:              �?        ��ed�       p�*	5�����A��*�

summaries/lossH��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) u�b#=@2زv�5f@��h:np@�������:              �?        }hϯ�       p�*	V����A��*�

summaries/loss�֞@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) � l�8@2!��v�@زv�5f@�������:              �?        �5ۦ       p�*	�T����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @�#~:@2زv�5f@��h:np@�������:              �?        ���       p�*	������A��*�

summaries/loss쎨@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) d�b��;@2زv�5f@��h:np@�������:              �?        ��-�       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ` @   ` @      �?!   ` @)@�1a(9@2!��v�@زv�5f@�������:              �?        �}�ڦ       p�*	 _����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    �b@    �b@      �?!    �b@) @��y�9@2!��v�@زv�5f@�������:              �?        �Xyl�       p�*	T�����A��*�

summaries/loss|!�@
}
summaries/histogram_loss*a	   �/@   �/@      �?!   �/@) M�w
9@2!��v�@زv�5f@�������:              �?        K��u�       p�*	������A��*�

summaries/lossLͤ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ����:@2زv�5f@��h:np@�������:              �?        �޳ �       p�*	^k����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��C,6@2!��v�@زv�5f@�������:              �?        )�91�       p�*	������A��*�

summaries/lossZ.�@
}
summaries/histogram_loss*a	   @�e@   @�e@      �?!   @�e@) ��:@2!��v�@زv�5f@�������:              �?        �4cL�       p�*	J	����A��*�

summaries/loss0��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @�{;@2زv�5f@��h:np@�������:              �?        k*Z��       p�*	�g����A��*�

summaries/loss	L�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@�g�)8@2!��v�@زv�5f@�������:              �?        ��}�       p�*	������A��*�

summaries/loss�ئ@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) D�@}/;@2زv�5f@��h:np@�������:              �?        ���       p�*	/�����A��*�

summaries/loss9Z�@
}
summaries/histogram_loss*a	    G�@    G�@      �?!    G�@)@,nH;@2زv�5f@��h:np@�������:              �?        ų>��       p�*	AK����A��*�

summaries/lossUt�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�D>@2زv�5f@��h:np@�������:              �?        ��U��       p�*	5�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    7?@    7?@      �?!    7?@) �A�9@2!��v�@زv�5f@�������:              �?        ��+�       p�*	:����A��*�

summaries/lossUL�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@%߮:@2زv�5f@��h:np@�������:              �?        ��^�       p�*	�2����A��*�

summaries/loss�H�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ��U��:@2زv�5f@��h:np@�������:              �?        q�4̦       p�*	�~����A��*�

summaries/losst/�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �y�b@@2��h:np@S���߮@�������:              �?        
/��       p�*	�ʼ���A��*�

summaries/loss�3�@
}
summaries/histogram_loss*a	   �yf@   �yf@      �?!   �yf@) q�돟<@2زv�5f@��h:np@�������:              �?        a�D�       p�*	�����A*�

summaries/loss~F�@
}
summaries/histogram_loss*a	   �Ϩ@   �Ϩ@      �?!   �Ϩ@) �Iu��:@2زv�5f@��h:np@�������:              �?        0�	�       p�*	+g����AØ*�

summaries/lossj�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ��6�3;@2زv�5f@��h:np@�������:              �?        �3y	�       p�*	R�����AĘ*�

summaries/loss���@
}
summaries/histogram_loss*a	   @Q�@   @Q�@      �?!   @Q�@) ���=@2زv�5f@��h:np@�������:              �?        w��       p�*	������AŘ*�

summaries/loss�H�@
}
summaries/histogram_loss*a	   @	@   @	@      �?!   @	@) ��W�;@2زv�5f@��h:np@�������:              �?        D�T��       p�*	�K����AƘ*�

summaries/loss�
�@
}
summaries/histogram_loss*a	    X@    X@      �?!    X@)@`�l\9@2!��v�@زv�5f@�������:              �?        ?+���       p�*	W�����Aǘ*�

summaries/lossNЦ@
}
summaries/histogram_loss*a	   �	�@   �	�@      �?!   �	�@) 񵩳,;@2زv�5f@��h:np@�������:              �?        �ӈ�       p�*	�����AȘ*�

summaries/loss@�@
}
summaries/histogram_loss*a	    h @    h @      �?!    h @)  ��;@2زv�5f@��h:np@�������:              �?        t��       p�*	�-����Aɘ*�

summaries/loss`�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  Ɏ��8@2!��v�@زv�5f@�������:              �?        �HB�       p�*	�w����Aʘ*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) Y(���=@2زv�5f@��h:np@�������:              �?        �e`��       p�*	�Ŀ���A˘*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  �~;@2زv�5f@��h:np@�������:              �?        �@9ئ       p�*	s����A̘*�

summaries/loss��@
}
summaries/histogram_loss*a	   �@�@   �@�@      �?!   �@�@) �.:8@2!��v�@زv�5f@�������:              �?        �j�       p�*	mX����A͘*�

summaries/loss���@
}
summaries/histogram_loss*a	   �X@   �X@      �?!   �X@) I�rSB:@2زv�5f@��h:np@�������:              �?        "�?}�       p�*	+�����AΘ*�

summaries/lossV�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) 9tD��:@2زv�5f@��h:np@�������:              �?        Vq�!�       p�*	�����AϘ*�

summaries/loss-n�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@��q�;@2زv�5f@��h:np@�������:              �?        X���       p�*	y=����AИ*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) D����:@2زv�5f@��h:np@�������:              �?        ��4�       p�*	�����Aј*�

summaries/loss��@
}
summaries/histogram_loss*a	   �Ѐ@   �Ѐ@      �?!   �Ѐ@) �RF:@2زv�5f@��h:np@�������:              �?        nQ]&�       p�*	?�����AҘ*�

summaries/lossԯ�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ����8@2!��v�@زv�5f@�������:              �?        ����       p�*	�.����AӘ*�

summaries/lossH��@
}
summaries/histogram_loss*a	    i�@    i�@      �?!    i�@) �,�;@2زv�5f@��h:np@�������:              �?        ��٦       p�*	������AԘ*�

summaries/loss�L�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��y�\:@2زv�5f@��h:np@�������:              �?        ?���       p�*	�����A՘*�

summaries/lossyL�@
}
summaries/histogram_loss*a	    �i@    �i@      �?!    �i@)@L�;�
:@2زv�5f@��h:np@�������:              �?        #�)z�       p�*	�&����A֘*�

summaries/loss���@
}
summaries/histogram_loss*a	    v�@    v�@      �?!    v�@) @fb�:@2زv�5f@��h:np@�������:              �?        ���       p�*	v����Aט*�

summaries/loss�[�@
}
summaries/histogram_loss*a	   @q�@   @q�@      �?!   @q�@) ��'ȳ:@2زv�5f@��h:np@�������:              �?        <�m�       p�*	�����Aؘ*�

summaries/lossPة@
}
summaries/histogram_loss*a	    
;@    
;@      �?!    
;@) @��+<@2زv�5f@��h:np@�������:              �?        �Fsئ       p�*	����A٘*�

summaries/loss���@
}
summaries/histogram_loss*a	   �>S@   �>S@      �?!   �>S@) $dY��9@2!��v�@زv�5f@�������:              �?        ��(	�       p�*	�X����Aژ*�

summaries/lossZ�@
}
summaries/histogram_loss*a	   @"@   @"@      �?!   @"@) �����;@2زv�5f@��h:np@�������:              �?        ����       p�*	H�����Aۘ*�

summaries/loss���@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@�Pu�,9@2!��v�@زv�5f@�������:              �?        ɸH]�       p�*	������Aܘ*�

summaries/loss�m�@
}
summaries/histogram_loss*a	   @�-@   @�-@      �?!   @�-@) 1�f�r9@2!��v�@زv�5f@�������:              �?        ���       p�*	�=����Aݘ*�

summaries/loss�@�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ��f�8@2!��v�@زv�5f@�������:              �?        UΩ	�       p�*	U�����Aޘ*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��"@   ��"@      �?!   ��"@)@����>@2زv�5f@��h:np@�������:              �?        �t��       p�*	������Aߘ*�

summaries/lossP��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @�§o;@2زv�5f@��h:np@�������:              �?        K�^j�       p�*	� ����A��*�

summaries/loss�@
}
summaries/histogram_loss*a	    C�@    C�@      �?!    C�@) ���2;@2زv�5f@��h:np@�������:              �?        �!��       p�*	!p����A�*�

summaries/losst̥@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) $E4L�:@2زv�5f@��h:np@�������:              �?        �����       p�*	^�����A�*�

summaries/loss��@
}
summaries/histogram_loss*a	    �O@    �O@      �?!    �O@) @ ��9@2!��v�@زv�5f@�������:              �?        D���       p�*	u����A�*�

summaries/lossc<�@
}
summaries/histogram_loss*a	   `�g@   `�g@      �?!   `�g@)@�#�n�<@2زv�5f@��h:np@�������:              �?        ͲOҦ       p�*	;Q����A�*�

summaries/loss�ɦ@
}
summaries/histogram_loss*a	   �5�@   �5�@      �?!   �5�@) ��{�*;@2زv�5f@��h:np@�������:              �?        4�]��       p�*	������A�*�

summaries/loss�@
}
summaries/histogram_loss*a	    >@    >@      �?!    >@)@8�=:9@2!��v�@زv�5f@�������:              �?        w�       p�*	}�����A�*�

summaries/loss�ر@
}
summaries/histogram_loss*a	   �;@   �;@      �?!   �;@) Y��X�>@2زv�5f@��h:np@�������:              �?        ���¦       p�*	75����A�*�

summaries/lossv��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �!�9=@2زv�5f@��h:np@�������:              �?        ���       p�*	Ä����A�*�

summaries/loss7g�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@tS�=@2زv�5f@��h:np@�������:              �?        �e"�       p�*	������A�*�

summaries/lossS��@
}
summaries/histogram_loss*a	   `*�@   `*�@      �?!   `*�@)@:$���:@2زv�5f@��h:np@�������:              �?        ��h�       p�*	����A�*�

summaries/lossƦ�@
}
summaries/histogram_loss*a	   ��T@   ��T@      �?!   ��T@) IX�zp<@2زv�5f@��h:np@�������:              �?        �}!�       p�*	<f����A�*�

summaries/loss�)�@
}
summaries/histogram_loss*a	   �1%@   �1%@      �?!   �1%@)@xGc��;@2زv�5f@��h:np@�������:              �?        �T�       p�*	������A�*�

summaries/loss���@
}
summaries/histogram_loss*a	    W@    W@      �?!    W@)@�lv<@2زv�5f@��h:np@�������:              �?        ��
�       p�*	������A�*�

summaries/loss�e�@
}
summaries/histogram_loss*a	   �,@   �,@      �?!   �,@)@X���<@2زv�5f@��h:np@�������:              �?        #�0�       p�*	J����A�*�

summaries/loss�x�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@���c;@2زv�5f@��h:np@�������:              �?        S����       p�*	B�����A�*�

summaries/loss.��@
}
summaries/histogram_loss*a	   ��u@   ��u@      �?!   ��u@) ����<@2زv�5f@��h:np@�������:              �?        Q$D�       p�*	B�����A�*�

summaries/lossgE�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�+��:@2زv�5f@��h:np@�������:              �?        ܘh��       p�*	�����A�*�

summaries/loss�^�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @H��;@2زv�5f@��h:np@�������:              �?        �b��       p�*	*6����A�*�

summaries/losszk�@
}
summaries/histogram_loss*a	   @o-@   @o-@      �?!   @o-@) ��$r9@2!��v�@زv�5f@�������:              �?        q3�"�       p�*	�����A�*�

summaries/lossx�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) �
�I>@2زv�5f@��h:np@�������:              �?        )S�       p�*	������A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   ��<@   ��<@      �?!   ��<@) d�Ke0<@2زv�5f@��h:np@�������:              �?        �����       p�*	`@����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �3�@   �3�@      �?!   �3�@) ĵ��:@2زv�5f@��h:np@�������:              �?        �%q�       p�*	ۋ����A��*�

summaries/loss|�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) s���8@2!��v�@زv�5f@�������:              �?        ��V�       p�*	�����A��*�

summaries/loss0�@
}
summaries/histogram_loss*a	    f~@    f~@      �?!    f~@) @
��?:@2زv�5f@��h:np@�������:              �?        ��NZ�       p�*	J%����A��*�

summaries/loss�:�@
}
summaries/histogram_loss*a	   �P�@   �P�@      �?!   �P�@) $��:@2زv�5f@��h:np@�������:              �?        ��9�       p�*	;p����A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) d�tDF:@2زv�5f@��h:np@�������:              �?        ʲ�       p�*	ع����A��*�

summaries/loss"Ӣ@
}
summaries/histogram_loss*a	   @dZ@   @dZ@      �?!   @dZ@) !DJ��9@2!��v�@زv�5f@�������:              �?        ���H�       p�*	�
����A��*�

summaries/lossh�@
}
summaries/histogram_loss*a	    � @    � @      �?!    � @) �B*�;@2زv�5f@��h:np@�������:              �?         �U�       p�*	Y����A��*�

summaries/loss�Ŝ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) q��j 8@2!��v�@زv�5f@�������:              �?        D�
�       p�*	פ����A��*�

summaries/lossB��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) Ai$Xg=@2زv�5f@��h:np@�������:              �?        >]˦       p�*	g�����A��*�

summaries/loss�}�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ឿ�;@2زv�5f@��h:np@�������:              �?        {'�5�       p�*	�I����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �{�@   �{�@      �?!   �{�@)@G�{�8@2!��v�@زv�5f@�������:              �?        �'�f�       p�*	B�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@޺x��=@2زv�5f@��h:np@�������:              �?        � R�       p�*	������A��*�

summaries/loss,k�@
}
summaries/histogram_loss*a	   �e-@   �e-@      �?!   �e-@) �s=�<@2زv�5f@��h:np@�������:              �?        ��P�       p�*	7����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �cV@   �cV@      �?!   �cV@)@Rd���9@2!��v�@زv�5f@�������:              �?        ����       p�*	'�����A��*�

summaries/loss�q�@
}
summaries/histogram_loss*a	   �?�@   �?�@      �?!   �?�@) f�:@2زv�5f@��h:np@�������:              �?        D?{�       p�*	������A��*�

summaries/loss�&�@
}
summaries/histogram_loss*a	   ��D@   ��D@      �?!   ��D@) ��J�9@2!��v�@زv�5f@�������:              �?        �N�       p�*	9@����A��*�

summaries/loss�%�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �,8^�:@2زv�5f@��h:np@�������:              �?        %g��       p�*	Ŏ����A��*�

summaries/loss⁝@
}
summaries/histogram_loss*a	   @<�@   @<�@      �?!   @<�@) �bF$:8@2!��v�@زv�5f@�������:              �?        邸Ħ       p�*	������A��*�

summaries/loss�+�@
}
summaries/histogram_loss*a	   �{�@   �{�@      �?!   �{�@) !c�n8@2!��v�@زv�5f@�������:              �?        �0���       p�*	1(����A��*�

summaries/lossa�@
}
summaries/histogram_loss*a	    a@    a@      �?!    a@)@0���9@2!��v�@زv�5f@�������:              �?        r �]�       p�*	�s����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @V@   @V@      �?!   @V@) �(Ø;@2زv�5f@��h:np@�������:              �?        P�'p�       p�*	n�����A��*�

summaries/loss�~�@
}
summaries/histogram_loss*a	   �ۏ@   �ۏ@      �?!   �ۏ@) !�Ӳl:@2زv�5f@��h:np@�������:              �?        �6mm�       p�*	�����A��*�

summaries/loss�D�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@��p�9@2!��v�@زv�5f@�������:              �?        <��<�       p�*	_^����A��*�

summaries/loss.��@
}
summaries/histogram_loss*a	   �@   �@      �?!   �@) :4��;@2زv�5f@��h:np@�������:              �?         �#�       p�*	o�����A��*�

summaries/loss�C�@
}
summaries/histogram_loss*a	    vH@    vH@      �?!    vH@) @f�}O<@2زv�5f@��h:np@�������:              �?        ��
�       p�*	�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @0@   @0@      �?!   @0@) ���x9@2!��v�@زv�5f@�������:              �?        ��4�       p�*	�N����A��*�

summaries/lossqZ�@
}
summaries/histogram_loss*a	    N@    N@      �?!    N@)@x���;@2زv�5f@��h:np@�������:              �?        �wa�       p�*	И����A��*�

summaries/lossĢ@
}
summaries/histogram_loss*a	   @�X@   @�X@      �?!   @�X@) �&*�9@2!��v�@زv�5f@�������:              �?        JZ�       p�*	�����A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) $`$I:@2زv�5f@��h:np@�������:              �?        pGЏ�       p�*	+2����A��*�

summaries/lossd\�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �2�}8@2!��v�@زv�5f@�������:              �?        �u��       p�*	F}����A��*�

summaries/loss�8�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@Dk%V:@2زv�5f@��h:np@�������:              �?        �ŧզ       p�*	�����A��*�

summaries/loss@�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  �rT�:@2زv�5f@��h:np@�������:              �?        ?`�˦       p�*	@����A��*�

summaries/loss�R�@
}
summaries/histogram_loss*a	   �Z�@   �Z�@      �?!   �Z�@) ��JW;@2زv�5f@��h:np@�������:              �?        �:0��       p�*	�c����A��*�

summaries/loss�?�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) I�dqX:@2زv�5f@��h:np@�������:              �?        �ar��       p�*	k�����A��*�

summaries/loss�>�@
}
summaries/histogram_loss*a	   ��g@   ��g@      �?!   ��g@)@��R:@2زv�5f@��h:np@�������:              �?        I<�g�       p�*	2����A��*�

summaries/lossz�@
}
summaries/histogram_loss*a	   @CO@   @CO@      �?!   @CO@) �21�a<@2زv�5f@��h:np@�������:              �?        �Q���       p�*	�Q����A��*�

summaries/lossۉ�@
}
summaries/histogram_loss*a	   `;�@   `;�@      �?!   `;�@)@V!b=@2زv�5f@��h:np@�������:              �?        �h�\�       p�*	
�����A��*�

summaries/loss|��@
}
summaries/histogram_loss*a	   �o5@   �o5@      �?!   �o5@) �%�<@2زv�5f@��h:np@�������:              �?        T�U�       p�*	d�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @[�@   @[�@      �?!   @[�@) i@�E;@2زv�5f@��h:np@�������:              �?        X�uU�       p�*	q[����A��*�

summaries/losst�@
}
summaries/histogram_loss*a	   ��"@   ��"@      �?!   ��"@) $	�OW9@2!��v�@زv�5f@�������:              �?        %�N�       p�*	ک����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) d��o;@2زv�5f@��h:np@�������:              �?        ��I�       p�*	�����A��*�

summaries/loss�#�@
}
summaries/histogram_loss*a	    r�@    r�@      �?!    r�@) @,���<@2زv�5f@��h:np@�������:              �?        ��z�       p�*	#b����A��*�

summaries/loss*Ѥ@
}
summaries/histogram_loss*a	   @%�@   @%�@      �?!   @%�@) �f-*�:@2زv�5f@��h:np@�������:              �?        3韢�       p�*	ش����A��*�

summaries/loss\�z@
}
summaries/histogram_loss*a	   �KZ@   �KZ@      �?!   �KZ@) "b�.@2�DK��@{2�.��@�������:              �?        M��C�       p�*	X ����A��*�

summaries/lossv��@
}
summaries/histogram_loss*a	   �n0@   �n0@      �?!   �n0@) �~pa0@2�DK��@{2�.��@�������:              �?        {ê�       p�*	RI����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �١@   �١@      �?!   �١@) q�a�8@2!��v�@زv�5f@�������:              �?        �l���       p�*	Ȗ����A��*�

summaries/loss;l�@
}
summaries/histogram_loss*a	   `�m@   `�m@      �?!   `�m@)@f�8�:@2زv�5f@��h:np@�������:              �?        ^@��       p�*	P�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@|q�=@2زv�5f@��h:np@�������:              �?        �&�       p�*	w0����A��*�

summaries/loss�ĭ@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @��|=@2زv�5f@��h:np@�������:              �?        p�9H�       p�*	J|����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    S�@    S�@      �?!    S�@)@ܯ���:@2زv�5f@��h:np@�������:              �?        �\>�       p�*	������A��*�

summaries/loss4*�@
}
summaries/histogram_loss*a	   �F%@   �F%@      �?!   �F%@) �F�]9@2!��v�@زv�5f@�������:              �?        ���Z�       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �o�J:@2زv�5f@��h:np@�������:              �?        �	W�       p�*	e����A��*�

summaries/loss䄢@
}
summaries/histogram_loss*a	   ��P@   ��P@      �?!   ��P@) ��b�9@2!��v�@زv�5f@�������:              �?        �g��       p�*	\�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) d��e;@2زv�5f@��h:np@�������:              �?        4x��       p�*	T�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �B�@   �B�@      �?!   �B�@)@�Gx:@2زv�5f@��h:np@�������:              �?        ր�f�       p�*	�J����A��*�

summaries/lossjt�@
}
summaries/histogram_loss*a	   @�n@   @�n@      �?!   @�n@) ��.�<@2زv�5f@��h:np@�������:              �?        ���s�       p�*	`�����A��*�

summaries/lossW�@
}
summaries/histogram_loss*a	   ��"@   ��"@      �?!   ��"@)@dD4��;@2زv�5f@��h:np@�������:              �?        �@'��       p�*	������A��*�

summaries/loss'��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@��V�o;@2زv�5f@��h:np@�������:              �?        Z`��       p�*	�1����A��*�

summaries/loss`ݡ@
}
summaries/histogram_loss*a	    �;@    �;@      �?!    �;@)  ���9@2!��v�@زv�5f@�������:              �?        0o��       p�*	D�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    s@    s@      �?!    s@)@`�>[�<@2زv�5f@��h:np@�������:              �?        Ms�"�       p�*	o�����A��*�

summaries/loss+�@
}
summaries/histogram_loss*a	   `@   `@      �?!   `@)@�ZOH>@2زv�5f@��h:np@�������:              �?        ��ǯ�       p�*	�"����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �T�@   �T�@      �?!   �T�@)@<���v;@2زv�5f@��h:np@�������:              �?        MF��       p�*	�n����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) S��8@2!��v�@زv�5f@�������:              �?        铠L�       p�*	�����A��*�

summaries/loss�c�@
}
summaries/histogram_loss*a	   �{,@   �{,@      �?!   �{,@)@�@<@2زv�5f@��h:np@�������:              �?        �mh�       p�*	�����A��*�

summaries/loss[�@
}
summaries/histogram_loss*a	   `b�@   `b�@      �?!   `b�@)@� ��Y;@2زv�5f@��h:np@�������:              �?        ����       p�*	�T����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @8�K�8@2!��v�@زv�5f@�������:              �?        <c4��       p�*	f�����A��*�

summaries/loss�#�@
}
summaries/histogram_loss*a	   �qd@   �qd@      �?!   �qd@)@�v� �<@2زv�5f@��h:np@�������:              �?        VΦ�       p�*	������A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @1�@   @1�@      �?!   @1�@) ����7@2!��v�@زv�5f@�������:              �?        w�.�       p�*	�8����A��*�

summaries/lossӧ@
}
summaries/histogram_loss*a	    a�@    a�@      �?!    a�@) ��@�;@2زv�5f@��h:np@�������:              �?        ���t�       p�*	������A��*�

summaries/lossr��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �ك��;@2زv�5f@��h:np@�������:              �?        ����       p�*	������A��*�

summaries/lossl��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) d�'f�;@2زv�5f@��h:np@�������:              �?        ��m*�       p�*	�����A��*�

summaries/loss\�@
}
summaries/histogram_loss*a	   �k@@   �k@@      �?!   �k@@) DҎ:<@2زv�5f@��h:np@�������:              �?        r����       p�*	�h����A��*�

summaries/loss*��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �ʬ��:@2زv�5f@��h:np@�������:              �?        ��j	�       p�*	�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��t@   ��t@      �?!   ��t@) �v��':@2زv�5f@��h:np@�������:              �?        �P܊�       p�*	m�����A��*�

summaries/loss�Y�@
}
summaries/histogram_loss*a	   @8�@   @8�@      �?!   @8�@) ��4�:@2زv�5f@��h:np@�������:              �?        U�m�       p�*	eS����A��*�

summaries/lossd��@
}
summaries/histogram_loss*a	   �,@   �,@      �?!   �,@) ���A:@2زv�5f@��h:np@�������:              �?        X"{�       p�*	�����A��*�

summaries/loss7��@
}
summaries/histogram_loss*a	   �f?@   �f?@      �?!   �f?@)@t�l|�9@2!��v�@زv�5f@�������:              �?        �ѧ6�       p�*	�����A*�

summaries/lossi �@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@Ċ?b<;@2زv�5f@��h:np@�������:              �?        ����       p�*	�?����AÙ*�

summaries/loss�d�@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@���!�;@2زv�5f@��h:np@�������:              �?        ���       p�*	ҍ����Aę*�

summaries/lossA��@
}
summaries/histogram_loss*a	    HR@    HR@      �?!    HR@)@ �x�i<@2زv�5f@��h:np@�������:              �?        ٺ{[�       p�*	{�����Ař*�

summaries/loss)D�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@�s�a�;@2زv�5f@��h:np@�������:              �?        p�[�       p�*	 '����Aƙ*�

summaries/loss1Y�@
}
summaries/histogram_loss*a	    &K@    &K@      �?!    &K@)@��	�V<@2زv�5f@��h:np@�������:              �?        �{��       p�*	�r����AǙ*�

summaries/lossB��@
}
summaries/histogram_loss*a	   @�R@   @�R@      �?!   @�R@) A�AOk<@2زv�5f@��h:np@�������:              �?        y����       p�*	/�����Aș*�

summaries/loss�ɦ@
}
summaries/histogram_loss*a	    1�@    1�@      �?!    1�@) ��~*;@2زv�5f@��h:np@�������:              �?        s�N�       p�*	�����Aə*�

summaries/lossA6�@
}
summaries/histogram_loss*a	    �f@    �f@      �?!    �f@)@ _�a�<@2زv�5f@��h:np@�������:              �?        $E��       p�*	�Z����Aʙ*�

summaries/lossm�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@�җX>;@2زv�5f@��h:np@�������:              �?        �ͽ�       p�*	�����A˙*�

summaries/loss%<�@
}
summaries/histogram_loss*a	   ��g@   ��g@      �?!   ��g@)@V�Oi:@2زv�5f@��h:np@�������:              �?        ��繦       p�*	c�����A̙*�

summaries/loss�|�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) Q�yl:@2زv�5f@��h:np@�������:              �?        �9���       p�*	A����A͙*�

summaries/loss��@
}
summaries/histogram_loss*a	   �y�@   �y�@      �?!   �y�@) ��}=;@2زv�5f@��h:np@�������:              �?        l�-�       p�*	v�����AΙ*�

summaries/lossX8�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) ��uk�:@2زv�5f@��h:np@�������:              �?         �qw�       p�*	]�����Aϙ*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �a��6;@2زv�5f@��h:np@�������:              �?        ηE��       p�*	� ����AЙ*�

summaries/loss���@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) 틠;@2زv�5f@��h:np@�������:              �?        ����       p�*	vn����Aљ*�

summaries/loss�o�@
}
summaries/histogram_loss*a	   ��M@   ��M@      �?!   ��M@) A�G&^<@2زv�5f@��h:np@�������:              �?        ���       p�*	������Aҙ*�

summaries/loss�ب@
}
summaries/histogram_loss*a	    @    @      �?!    @) @�=�;@2زv�5f@��h:np@�������:              �?        ���y�       p�*	�����Aә*�

summaries/loss��@
}
summaries/histogram_loss*a	   �ޣ@   �ޣ@      �?!   �ޣ@) �6;�:@2زv�5f@��h:np@�������:              �?        d�<��       p�*	�P����Aԙ*�

summaries/loss�\�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) AGB�a:@2زv�5f@��h:np@�������:              �?        /�ș�       p�*	b�����Aՙ*�

summaries/loss�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) 1wY�!;@2زv�5f@��h:np@�������:              �?        ;�l�       p�*	c�����A֙*�

summaries/lossEZ�@
}
summaries/histogram_loss*a	   �H+@   �H+@      �?!   �H+@)@���<@2زv�5f@��h:np@�������:              �?        �"n��       p�*	ǃ����Aי*�

summaries/lossƯ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��,>@2زv�5f@��h:np@�������:              �?        �TM�       p�*	�����Aؙ*�

summaries/loss F�@
}
summaries/histogram_loss*a	    Ĉ@    Ĉ@      �?!    Ĉ@)  a���<@2زv�5f@��h:np@�������:              �?        �#}�       p�*	������Aٙ*�

summaries/lossԡ�@
}
summaries/histogram_loss*a	   �:�@   �:�@      �?!   �:�@) ��C8@2!��v�@زv�5f@�������:              �?        k�#Ϧ       p�*	F�����Aڙ*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��a@   ��a@      �?!   ��a@) AU�,�<@2زv�5f@��h:np@�������:              �?        ��p}�       p�*	Y����Aۙ*�

summaries/loss��@
}
summaries/histogram_loss*a	    �`@    �`@      �?!    �`@)@8S<�<@2زv�5f@��h:np@�������:              �?        �R�O�       p�*	�h����Aܙ*�

summaries/loss���@
}
summaries/histogram_loss*a	    v0@    v0@      �?!    v0@) @fǹy9@2!��v�@زv�5f@�������:              �?        |U��       p�*	������Aݙ*�

summaries/loss��@
}
summaries/histogram_loss*a	   @@   @@      �?!   @@) ��r�;@2زv�5f@��h:np@�������:              �?        ����       p�*	������Aޙ*�

summaries/loss�d�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @~FJd:@2زv�5f@��h:np@�������:              �?        E�A/�       p�*	^K����Aߙ*�

summaries/loss��@
}
summaries/histogram_loss*a	   @x^@   @x^@      �?!   @x^@) �wp)�<@2زv�5f@��h:np@�������:              �?        �5ܦ       p�*	�����A��*�

summaries/loss�r�@
}
summaries/histogram_loss*a	    R�@    R�@      �?!    R�@) @$ɪa;@2زv�5f@��h:np@�������:              �?        F�3�       p�*	������A�*�

summaries/lossU��@
}
summaries/histogram_loss*a	   �j@   �j@      �?!   �j@)@�V��;@2زv�5f@��h:np@�������:              �?        ��P�       p�*	Z(����A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@X>h��=@2زv�5f@��h:np@�������:              �?        c��Ǧ       p�*	u����A�*�

summaries/loss�Ǯ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ����=@2زv�5f@��h:np@�������:              �?        |� �       p�*	v�����A�*�

summaries/loss�A�@
}
summaries/histogram_loss*a	   �8H@   �8H@      �?!   �8H@) �G=ӵ9@2!��v�@زv�5f@�������:              �?        �-pަ       p�*	����A�*�

summaries/loss��@
}
summaries/histogram_loss*a	    �#@    �#@      �?!    �#@)@��L��;@2زv�5f@��h:np@�������:              �?        y�f��       p�*	�X����A�*�

summaries/lossׅ�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@���g;@2زv�5f@��h:np@�������:              �?        v���       p�*	{�����A�*�

summaries/loss�Ȥ@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@��3��:@2زv�5f@��h:np@�������:              �?        ��P�       p�*	������A�*�

summaries/loss�X�@
}
summaries/histogram_loss*a	   �@   �@      �?!   �@) �8s�9@2!��v�@زv�5f@�������:              �?        �P�զ       p�*	�;����A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) $Ql98@2!��v�@زv�5f@�������:              �?        �"Լ�       p�*	Z�����A�*�

summaries/lossb�@
}
summaries/histogram_loss*a	   @,~@   @,~@      �?!   @,~@) aj�Q?:@2زv�5f@��h:np@�������:              �?        ��ڪ�       p�*	�����A�*�

summaries/loss|K�@
}
summaries/histogram_loss*a	   �o�@   �o�@      �?!   �o�@) y��T;@2زv�5f@��h:np@�������:              �?        �*i�       p�*	�����A�*�

summaries/loss+��@
}
summaries/histogram_loss*a	   `%S@   `%S@      �?!   `%S@)@N���9@2!��v�@زv�5f@�������:              �?        k���       p�*	�h����A�*�

summaries/lossvq�@
}
summaries/histogram_loss*a	   �.�@   �.�@      �?!   �.�@) �X��;@2زv�5f@��h:np@�������:              �?        b�       p�*	1�����A�*�

summaries/lossv�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��Fr�:@2زv�5f@��h:np@�������:              �?        ]kv9�       p�*	����A�*�

summaries/lossL��@
}
summaries/histogram_loss*a	   �I�@   �I�@      �?!   �I�@) ��<�:@2زv�5f@��h:np@�������:              �?        �� �       p�*	rP����A�*�

summaries/lossT�@
}
summaries/histogram_loss*a	   �j�@   �j�@      �?!   �j�@) �$�*�:@2زv�5f@��h:np@�������:              �?        �G�       p�*	Q�����A�*�

summaries/lossZ��@
}
summaries/histogram_loss*a	   @+�@   @+�@      �?!   @+�@) ���^�8@2!��v�@زv�5f@�������:              �?        K�b��       p�*	K�����A�*�

summaries/loss�J�@
}
summaries/histogram_loss*a	    T�@    T�@      �?!    T�@)  9��[:@2زv�5f@��h:np@�������:              �?        ?Q6��       p�*	/8����A�*�

summaries/lossS_�@
}
summaries/histogram_loss*a	   `�+@   `�+@      �?!   `�+@)@:-ſ<@2زv�5f@��h:np@�������:              �?        ���Ʀ       p�*	͌����A��*�

summaries/lossԪ�@
}
summaries/histogram_loss*a	   �Z�@   �Z�@      �?!   �Z�@) �; ;@2زv�5f@��h:np@�������:              �?        S��l�       p�*	o�����A��*�

summaries/losszg�@
}
summaries/histogram_loss*a	   @�,@   @�,@      �?!   @�,@) ��Q�p9@2!��v�@زv�5f@�������:              �?        ��ڸ�       p�*	N)����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) 䱊��;@2زv�5f@��h:np@�������:              �?        ���Ц       p�*	�v����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @Z@   @Z@      �?!   @Z@) ��N9@2!��v�@زv�5f@�������:              �?        4�w��       p�*	������A��*�

summaries/lossB�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) A���8@2!��v�@زv�5f@�������:              �?        �P	�       p�*	
����A��*�

summaries/lossD��@
}
summaries/histogram_loss*a	   �(w@   �(w@      �?!   �(w@) �֚w�7@2!��v�@زv�5f@�������:              �?        �c�d�       p�*	�[����A��*�

summaries/loss ٦@
}
summaries/histogram_loss*a	    $�@    $�@      �?!    $�@)  �i�/;@2زv�5f@��h:np@�������:              �?        ���       p�*	������A��*�

summaries/loss�D�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �(�W>@2زv�5f@��h:np@�������:              �?        �@Φ       p�*	
�����A��*�

summaries/loss�#�@
}
summaries/histogram_loss*a	   �}�@   �}�@      �?!   �}�@)@HN�ա:@2زv�5f@��h:np@�������:              �?        ����       p�*	-D����A��*�

summaries/loss[n�@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@�,�e>@2زv�5f@��h:np@�������:              �?        �|R6�       p�*	r�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��_@   ��_@      �?!   ��_@) 1�x��9@2!��v�@زv�5f@�������:              �?        ���       p�*	�����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ���q=@2زv�5f@��h:np@�������:              �?        a1��       p�*	�#����A��*�

summaries/loss�?�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �C$o�<@2زv�5f@��h:np@�������:              �?        R]�       p�*	�n����A��*�

summaries/lossۭ�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@V�d�:@2زv�5f@��h:np@�������:              �?        d�_�       p�*	������A��*�

summaries/loss�{�@
}
summaries/histogram_loss*a	   @uo@   @uo@      �?!   @uo@) 936��<@2زv�5f@��h:np@�������:              �?        �K�@�       p�*	�����A��*�

summaries/loss`�@
}
summaries/histogram_loss*a	    l@@    l@@      �?!    l@@)  ��:<@2زv�5f@��h:np@�������:              �?        E��V�       p�*	+l����A��*�

summaries/lossh��@
}
summaries/histogram_loss*a	    Mp@    Mp@      �?!    Mp@) �rW޹<@2زv�5f@��h:np@�������:              �?        �W��       p�*	������A��*�

summaries/lossc��@
}
summaries/histogram_loss*a	   `x@   `x@      �?!   `x@)@����/:@2زv�5f@��h:np@�������:              �?        f�y^�       p�*	�����A��*�

summaries/loss�n�@
}
summaries/histogram_loss*a	   ��M@   ��M@      �?!   ��M@) b�)�9@2!��v�@زv�5f@�������:              �?        ~t�W�       p�*	
�����A��*�

summaries/loss)ӥ@
}
summaries/histogram_loss*a	    e�@    e�@      �?!    e�@)@$�Ax�:@2زv�5f@��h:np@�������:              �?        ��t�       p�*	1�����A��*�

summaries/loss<ݩ@
}
summaries/histogram_loss*a	   ��;@   ��;@      �?!   ��;@) �)v-<@2زv�5f@��h:np@�������:              �?        ��`�       p�*	�����A��*�

summaries/loss�ש@
}
summaries/histogram_loss*a	    �:@    �:@      �?!    �:@) ����+<@2زv�5f@��h:np@�������:              �?        �)�.�       p�*	�d����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    �Q@    �Q@      �?!    �Q@)  Ǉ�9@2!��v�@زv�5f@�������:              �?        n&&r�       p�*	�����A��*�

summaries/loss,��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �i���:@2زv�5f@��h:np@�������:              �?        q�h�       p�*	������A��*�

summaries/loss"��@
}
summaries/histogram_loss*a	   @�U@   @�U@      �?!   @�U@) !�n�-?@2زv�5f@��h:np@�������:              �?        �����       p�*	�G����A��*�

summaries/loss_�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �C�;@2زv�5f@��h:np@�������:              �?        �� �       p�*	�����A��*�

summaries/loss�ѧ@
}
summaries/histogram_loss*a	    6�@    6�@      �?!    6�@) @6�π;@2زv�5f@��h:np@�������:              �?        ��[�       p�*	������A��*�

summaries/loss�ݯ@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@d�>34>@2زv�5f@��h:np@�������:              �?        �ݕ)�       p�*	(����A��*�

summaries/lossHۨ@
}
summaries/histogram_loss*a	    i@    i@      �?!    i@) �"�;@2زv�5f@��h:np@�������:              �?        �����       p�*	q����A��*�

summaries/loss�?�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) Q�'��:@2زv�5f@��h:np@�������:              �?        i[�X�       p�*	������A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @�a@   @�a@      �?!   @�a@) �.���9@2!��v�@زv�5f@�������:              �?        a��@�       p�*	�����A��*�

summaries/loss�ŧ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) D}�|;@2زv�5f@��h:np@�������:              �?        ���       p�*	�S����A��*�

summaries/loss\Ч@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) Dhg`�;@2زv�5f@��h:np@�������:              �?        �Y�Y�       p�*	E�����A��*�

summaries/loss�L�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �#�QU;@2زv�5f@��h:np@�������:              �?        �����       p�*	_�����A��*�

summaries/loss�~�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@ډ��;@2زv�5f@��h:np@�������:              �?        �?qJ�       p�*	z3����A��*�

summaries/loss�-�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ����;@2زv�5f@��h:np@�������:              �?        ���       p�*	������A��*�

summaries/loss@��@
}
summaries/histogram_loss*a	    ȳ@    ȳ@      �?!    ȳ@)  ��B8@2!��v�@زv�5f@�������:              �?        h�ϓ�       p�*	������A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    �]@    �]@      �?!    �]@)  �M�9@2!��v�@زv�5f@�������:              �?        +����       p�*	O% ���A��*�

summaries/lossգ@
}
summaries/histogram_loss*a	   ��z@   ��z@      �?!   ��z@) ���D6:@2زv�5f@��h:np@�������:              �?        XK֭�       p�*	� ���A��*�

summaries/loss�u�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ���i:@2زv�5f@��h:np@�������:              �?        ����       p�*	X� ���A��*�

summaries/lossK��@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@����l=@2زv�5f@��h:np@�������:              �?        +ڴ_�       p�*	�5���A��*�

summaries/loss�e�@
}
summaries/histogram_loss*a	    �,@    �,@      �?!    �,@)  D�Hp9@2!��v�@زv�5f@�������:              �?        �}J:�       p�*	Ƅ���A��*�

summaries/lossç@
}
summaries/histogram_loss*a	    c�@    c�@      �?!    c�@) �d|;@2زv�5f@��h:np@�������:              �?        �}P�       p�*	�����A��*�

summaries/loss�'�@
}
summaries/histogram_loss*a	   ��$@   ��$@      �?!   ��$@)@Lz�\9@2!��v�@زv�5f@�������:              �?        d�36�       p�*	���A��*�

summaries/lossF�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �4�~�:@2زv�5f@��h:np@�������:              �?        Z0��       p�*	�m���A��*�

summaries/loss�ߢ@
}
summaries/histogram_loss*a	    �[@    �[@      �?!    �[@)@v���9@2!��v�@زv�5f@�������:              �?        &Hۦ       p�*	�����A��*�

summaries/lossBZ�@
}
summaries/histogram_loss*a	   @H�@   @H�@      �?!   @H�@) A�b��=@2زv�5f@��h:np@�������:              �?        ��P�       p�*	b���A��*�

summaries/lossP��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @�b��:@2زv�5f@��h:np@�������:              �?        ��0Ħ       p�*	^���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @���e8@2!��v�@زv�5f@�������:              �?        2=��       p�*	G����A��*�

summaries/lossOY�@
}
summaries/histogram_loss*a	   �)K@   �)K@      �?!   �)K@)@��V<@2زv�5f@��h:np@�������:              �?        �h�æ       p�*	*����A��*�

summaries/lossJ��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) Y�D��;@2زv�5f@��h:np@�������:              �?        �&���       p�*	�L���A��*�

summaries/loss�?�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �ʔ��:@2زv�5f@��h:np@�������:              �?        ���       p�*	�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��P@   ��P@      �?!   ��P@)@ �i$f<@2زv�5f@��h:np@�������:              �?        ]k!�       p�*	�����A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@�oj=@2زv�5f@��h:np@�������:              �?        #���       p�*	B?���A��*�

summaries/lossz��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �I�`�8@2!��v�@زv�5f@�������:              �?        2�沦       p�*	a����A��*�

summaries/lossdɧ@
}
summaries/histogram_loss*a	   �,�@   �,�@      �?!   �,�@) ċ�~;@2زv�5f@��h:np@�������:              �?        �O_I�       p�*	�����A��*�

summaries/loss�8�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) ��E�=@2زv�5f@��h:np@�������:              �?        �����       p�*	T���A��*�

summaries/lossS�@
}
summaries/histogram_loss*a	    cJ@    cJ@      �?!    cJ@) �$VQ�9@2!��v�@زv�5f@�������:              �?        �X�n�       p�*	W����A��*�

summaries/loss>�@
}
summaries/histogram_loss*a	    �G@    �G@      �?!    �G@) @�Ҧ�9@2!��v�@زv�5f@�������:              �?        _	���       p�*	9���A��*�

summaries/loss/A�@
}
summaries/histogram_loss*a	   �%@   �%@      �?!   �%@)@�9�g�;@2زv�5f@��h:np@�������:              �?        	wܧ�       p�*	�X���A��*�

summaries/lossi�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@D&�8@2!��v�@زv�5f@�������:              �?        �D_��       p�*	�����A��*�

summaries/lossS�@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@:��L�:@2زv�5f@��h:np@�������:              �?        S窿�       p�*	
���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  Ye��=@2زv�5f@��h:np@�������:              �?        �Q�Ц       p�*	�_���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) 	��,�=@2زv�5f@��h:np@�������:              �?        �9�       p�*	�����A��*�

summaries/loss(��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �%��;@2زv�5f@��h:np@�������:              �?        ��E��       p�*	K	���A��*�

summaries/lossR,�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �w��<@2زv�5f@��h:np@�������:              �?        ���h�       p�*	l[	���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �34@   �34@      �?!   �34@) a)�<@2زv�5f@��h:np@�������:              �?        [ٯB�       p�*	�	���A��*�

summaries/lossY�@
}
summaries/histogram_loss*a	   �!k@   �!k@      �?!   �!k@)@�[�:@2زv�5f@��h:np@�������:              �?        ��r̦       p�*	�
���A��*�

summaries/lossΨ@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�(P��;@2زv�5f@��h:np@�������:              �?        1�|��       p�*	�V
���A��*�

summaries/loss�$�@
}
summaries/histogram_loss*a	   ��D@   ��D@      �?!   ��D@) $��#E<@2زv�5f@��h:np@�������:              �?        ��.��       p�*	z�
���A��*�

summaries/lossEn�@
}
summaries/histogram_loss*a	   ��M@   ��M@      �?!   ��M@)@�״��9@2!��v�@زv�5f@�������:              �?        �?7�       p�*	!�
���A��*�

summaries/lossxЧ@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) Δi�;@2زv�5f@��h:np@�������:              �?        :�>!�       p�*	a���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   `W`@   `W`@      �?!   `W`@)@&]v)�<@2زv�5f@��h:np@�������:              �?        ʔ�N�       p�*	. ���A��*�

summaries/loss�'�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) _���:@2زv�5f@��h:np@�������:              �?        �o(��       p�*	����A��*�

summaries/loss�"�@
}
summaries/histogram_loss*a	   �_D@   �_D@      �?!   �_D@)@�.ݟD<@2زv�5f@��h:np@�������:              �?        ���       p�*	B����A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    Bp@    Bp@      �?!    Bp@)@H�2��<@2زv�5f@��h:np@�������:              �?        �Ğa�       p�*	�����A��*�

summaries/loss�]�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) �~�֮;@2زv�5f@��h:np@�������:              �?        �[�t�       p�*	?���A��*�

summaries/lossީ@
}
summaries/histogram_loss*a	   ��;@   ��;@      �?!   ��;@)@n��-<@2زv�5f@��h:np@�������:              �?        �΢d�       p�*	����A*�

summaries/loss;�@
}
summaries/histogram_loss*a	   �`�@   �`�@      �?!   �`�@)@��/�O;@2زv�5f@��h:np@�������:              �?        \�9�       p�*	-����AÚ*�

summaries/lossM�@
}
summaries/histogram_loss*a	    �I@    �I@      �?!    �I@) ts�R<@2زv�5f@��h:np@�������:              �?        ��5٦       p�*	�F���AĚ*�

summaries/lossҖ�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �L�;@2زv�5f@��h:np@�������:              �?        �׎)�       p�*	����AŚ*�

summaries/loss��@
}
summaries/histogram_loss*a	   �@   �@      �?!   �@)@�7.�;@2زv�5f@��h:np@�������:              �?        Җ�m�       p�*	�����Aƚ*�

summaries/lossW�@
}
summaries/histogram_loss*a	   �*@@   �*@@      �?!   �*@@)@�r�l�9@2!��v�@زv�5f@�������:              �?        ����       p�*	+4���Aǚ*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) Y��Kf;@2زv�5f@��h:np@�������:              �?        7�	�       p�*	τ���AȚ*�

summaries/loss���@
}
summaries/histogram_loss*a	   @�T@   @�T@      �?!   @�T@) �FB�o<@2زv�5f@��h:np@�������:              �?        ${Eئ       p�*	�����Aɚ*�

summaries/lossap�@
}
summaries/histogram_loss*a	    n@    n@      �?!    n@)@0��Գ<@2زv�5f@��h:np@�������:              �?        �ے��       p�*	�#���Aʚ*�

summaries/loss�D�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) a�u9@2!��v�@زv�5f@�������:              �?        ��G�       p�*	�q���A˚*�

summaries/loss��@
}
summaries/histogram_loss*a	   �~A@   �~A@      �?!   �~A@)@��Ȥ9@2!��v�@زv�5f@�������:              �?        ^��       p�*	w����A̚*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��W��7@2!��v�@زv�5f@�������:              �?        l��b�       p�*	}���A͚*�

summaries/loss*�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �M� 4@2{2�.��@!��v�@�������:              �?        ��       p�*	�v���AΚ*�

summaries/lossF�@
}
summaries/histogram_loss*a	   �H@@   �H@@      �?!   �H@@) �J?�9<@2زv�5f@��h:np@�������:              �?        ��1L�       p�*	�����AϚ*�

summaries/loss�ԩ@
}
summaries/histogram_loss*a	   ��:@   ��:@      �?!   ��:@)@�GP�*<@2زv�5f@��h:np@�������:              �?        �x�*�       p�*	����AК*�

summaries/lossj��@
}
summaries/histogram_loss*a	   @m�@   @m�@      �?!   @m�@) �a?m�:@2زv�5f@��h:np@�������:              �?        �#\�       p�*	�x���Aњ*�

summaries/lossצ@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) �+��.;@2زv�5f@��h:np@�������:              �?        i}���       p�*	�����AҚ*�

summaries/lossж�@
}
summaries/histogram_loss*a	    �6@    �6@      �?!    �6@) @K� <@2زv�5f@��h:np@�������:              �?        `kB�       p�*	����AӚ*�

summaries/loss6�@
}
summaries/histogram_loss*a	   �Ɲ@   �Ɲ@      �?!   �Ɲ@) ����:@2زv�5f@��h:np@�������:              �?        l�^��       p�*	�e���AԚ*�

summaries/lossh¢@
}
summaries/histogram_loss*a	    MX@    MX@      �?!    MX@) �rЧ�9@2!��v�@زv�5f@�������:              �?        ���>�       p�*	�����A՚*�

summaries/loss���@
}
summaries/histogram_loss*a	    �^@    �^@      �?!    �^@) @����9@2!��v�@زv�5f@�������:              �?        �lP�       p�*	���A֚*�

summaries/loss�t�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)  Y�Q�;@2زv�5f@��h:np@�������:              �?        턗ݦ       p�*	T���Aך*�

summaries/loss쏢@
}
summaries/histogram_loss*a	   ��Q@   ��Q@      �?!   ��Q@) d`��9@2!��v�@زv�5f@�������:              �?        �.i>�       p�*	����Aؚ*�

summaries/loss�9�@
}
summaries/histogram_loss*a	   �8'@   �8'@      �?!   �8'@)@fԴmb9@2!��v�@زv�5f@�������:              �?        �	8�       p�*	�����Aٚ*�

summaries/loss��@
}
summaries/histogram_loss*a	    @    @      �?!    @) @�pG�;@2زv�5f@��h:np@�������:              �?        ����       p�*	�I���Aښ*�

summaries/lossЦ�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @�l��;@2زv�5f@��h:np@�������:              �?        ڢ?�       p�*	(����Aۚ*�

summaries/loss�ˣ@
}
summaries/histogram_loss*a	   @ry@   @ry@      �?!   @ry@) �7s73:@2زv�5f@��h:np@�������:              �?        �s��       p�*	����Aܚ*�

summaries/loss`Ӧ@
}
summaries/histogram_loss*a	    l�@    l�@      �?!    l�@)  ���-;@2زv�5f@��h:np@�������:              �?        X7	!�       p�*	�.���Aݚ*�

summaries/loss��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) 9xz��;@2زv�5f@��h:np@�������:              �?        @"g�       p�*	9z���Aޚ*�

summaries/loss�z�@
}
summaries/histogram_loss*a	   �Q�@   �Q�@      �?!   �Q�@)@���ͽ:@2زv�5f@��h:np@�������:              �?        ��¦       p�*	e����Aߚ*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��7@   ��7@      �?!   ��7@) dl�(�9@2!��v�@زv�5f@�������:              �?        ���Ϧ       p�*	����A��*�

summaries/loss`ߩ@
}
summaries/histogram_loss*a	    �;@    �;@      �?!    �;@)  �+.<@2زv�5f@��h:np@�������:              �?        ��{�       p�*	\���A�*�

summaries/loss�.�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ��o(�:@2زv�5f@��h:np@�������:              �?        \9Y�       p�*	ϧ���A�*�

summaries/lossj�@
}
summaries/histogram_loss*a	   @M�@   @M�@      �?!   @M�@) ���AB;@2زv�5f@��h:np@�������:              �?        ��ǥ�       p�*	�����A�*�

summaries/lossD��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �|u�;@2زv�5f@��h:np@�������:              �?        P
�[�       p�*	-@���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) d��0�:@2زv�5f@��h:np@�������:              �?        s�,�       p�*	�����A�*�

summaries/loss���@
}
summaries/histogram_loss*a	    Up@    Up@      �?!    Up@) ��'�:@2زv�5f@��h:np@�������:              �?        ^ֈs�       p�*	����A�*�

summaries/lossk�@
}
summaries/histogram_loss*a	   `-b@   `-b@      �?!   `-b@)@�X��<@2زv�5f@��h:np@�������:              �?        �F5��       p�*	�e���A�*�

summaries/loss
��@
}
summaries/histogram_loss*a	   @A�@   @A�@      �?!   @A�@) �ƺf;@2زv�5f@��h:np@�������:              �?        C�Oئ       p�*	�����A�*�

summaries/losse>�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@�D�|�;@2زv�5f@��h:np@�������:              �?        l%=i�       p�*	�����A�*�

summaries/loss�J�@
}
summaries/histogram_loss*a	    [I@    [I@      �?!    [I@) �eг�9@2!��v�@زv�5f@�������:              �?        �E¦       p�*	8I���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	   �^ @   �^ @      �?!   �^ @) $.B� 9@2!��v�@زv�5f@�������:              �?        W��       p�*	F����A�*�

summaries/loss軨@
}
summaries/histogram_loss*a	    }@    }@      �?!    }@) �0���;@2زv�5f@��h:np@�������:              �?        �9k�       p�*	����A�*�

summaries/loss�~�@
}
summaries/histogram_loss*a	    �/@    �/@      �?!    �/@) �LN3<@2زv�5f@��h:np@�������:              �?        q���       p�*	9���A�*�

summaries/loss6M�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �a�\:@2زv�5f@��h:np@�������:              �?        �aN��       p�*	����A�*�

summaries/lossuգ@
}
summaries/histogram_loss*a	   ��z@   ��z@      �?!   ��z@)@�y>a6:@2زv�5f@��h:np@�������:              �?        {�N�       p�*	�����A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   �Q@   �Q@      �?!   �Q@) $��W�;@2زv�5f@��h:np@�������:              �?        ���Ӧ       p�*	�,���A�*�

summaries/loss�נ@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) ��C9@2!��v�@زv�5f@�������:              �?        t$�$�       p�*	pw���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @̪�K:@2زv�5f@��h:np@�������:              �?        ��!�       p�*	m����A�*�

summaries/loss�i�@
}
summaries/histogram_loss*a	   �1@   �1@      �?!   �1@) �ry��;@2زv�5f@��h:np@�������:              �?        �k��       p�*	c���A�*�

summaries/loss٧@
}
summaries/histogram_loss*a	   �/�@   �/�@      �?!   �/�@)@@�_�;@2زv�5f@��h:np@�������:              �?        H5w��       p�*	�d���A��*�

summaries/lossJ��@
}
summaries/histogram_loss*a	   @�p@   @�p@      �?!   @�p@) YcS+�<@2زv�5f@��h:np@�������:              �?        ����       p�*	����A��*�

summaries/loss�Ԧ@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@.;@2زv�5f@��h:np@�������:              �?        ���B�       p�*	V���A��*�

summaries/loss,d�@
}
summaries/histogram_loss*a	   ��,@   ��,@      �?!   ��,@) ��>@2زv�5f@��h:np@�������:              �?        D��Ħ       p�*	sb���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �� Eq:@2زv�5f@��h:np@�������:              �?        �&��       p�*	�����A��*�

summaries/losszC�@
}
summaries/histogram_loss*a	   @oh@   @oh@      �?!   @oh@) �E�ͤ<@2زv�5f@��h:np@�������:              �?        ��xȦ       p�*	� ���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��l��:@2زv�5f@��h:np@�������:              �?        j��#�       p�*	#e ���A��*�

summaries/loss�ڭ@
}
summaries/histogram_loss*a	   `W�@   `W�@      �?!   `W�@)@&A�V�=@2زv�5f@��h:np@�������:              �?        �Z�Ӧ       p�*	ܸ ���A��*�

summaries/lossد�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �A:!";@2زv�5f@��h:np@�������:              �?        ��`g�       p�*	�!���A��*�

summaries/lossv��@
}
summaries/histogram_loss*a	   �@   �@      �?!   �@) ��ܿ�;@2زv�5f@��h:np@�������:              �?        fX(<�       p�*	\!���A��*�

summaries/loss �@
}
summaries/histogram_loss*a	    @@    @@      �?!    @@)   � 9@2!��v�@زv�5f@�������:              �?        ٨fr�       p�*	a�!���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    � @    � @      �?!    � @)@|eC��;@2زv�5f@��h:np@�������:              �?        BZ�7�       p�*	7�!���A��*�

summaries/lossQ^�@
}
summaries/histogram_loss*a	    �+@    �+@      �?!    �+@)@he(�m9@2!��v�@زv�5f@�������:              �?        l��A�       p�*	.U"���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �A�@   �A�@      �?!   �A�@)@8�7�=@2زv�5f@��h:np@�������:              �?        D��       p�*	��"���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@��m�L9@2!��v�@زv�5f@�������:              �?        	#�)�       p�*	��"���A��*�

summaries/lossf�@
}
summaries/histogram_loss*a	   ��,@   ��,@      �?!   ��,@) Ĕl�<@2زv�5f@��h:np@�������:              �?        E�R�       p�*	�N#���A��*�

summaries/lossG��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@��ހv>@2زv�5f@��h:np@�������:              �?        i+�       p�*	��#���A��*�

summaries/lossKe�@
}
summaries/histogram_loss*a	   `�L@   `�L@      �?!   `�L@)@��Z<@2زv�5f@��h:np@�������:              �?        �h�ئ       p�*	��#���A��*�

summaries/loss4�@
}
summaries/histogram_loss*a	   ��#@   ��#@      �?!   ��#@) ��� �>@2زv�5f@��h:np@�������:              �?        �c5}�       p�*	[A$���A��*�

summaries/loss�ת@
}
summaries/histogram_loss*a	   ��Z@   ��Z@      �?!   ��Z@) �08܀<@2زv�5f@��h:np@�������:              �?        �9�+�       p�*	J�$���A��*�

summaries/loss�r�@
}
summaries/histogram_loss*a	   �U.@   �U.@      �?!   �U.@)@:"�&
<@2زv�5f@��h:np@�������:              �?        vpƈ�       p�*	(�$���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �]�@   �]�@      �?!   �]�@) d�8	�:@2زv�5f@��h:np@�������:              �?        z�Kۦ       p�*	�@%���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    �c@    �c@      �?!    �c@) ��9@2!��v�@زv�5f@�������:              �?        �	�       p�*	>�%���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �4@   �4@      �?!   �4@)@i�j�;@2زv�5f@��h:np@�������:              �?        �f^�       p�*	��%���A��*�

summaries/lossNv�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ���b;@2زv�5f@��h:np@�������:              �?        r'GX�       p�*	"9&���A��*�

summaries/lossd��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �i �u;@2زv�5f@��h:np@�������:              �?        OX�=�       p�*	�&���A��*�

summaries/lossJЯ@
}
summaries/histogram_loss*a	   @	�@   @	�@      �?!   @	�@) Y��/>@2زv�5f@��h:np@�������:              �?        ���ئ       p�*	~�&���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �G��:@2زv�5f@��h:np@�������:              �?        �       p�*	M-'���A��*�

summaries/loss�t�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@T%ai:@2زv�5f@��h:np@�������:              �?        �~4�       p�*	}|'���A��*�

summaries/loss8Ũ@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) �[��;@2زv�5f@��h:np@�������:              �?        �r�       p�*	��'���A��*�

summaries/loss�*�@
}
summaries/histogram_loss*a	   `S�@   `S�@      �?!   `S�@)@vN���:@2زv�5f@��h:np@�������:              �?        f:���       p�*	�(���A��*�

summaries/loss"��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) !Ч$C8@2!��v�@زv�5f@�������:              �?        I����       p�*	i(���A��*�

summaries/lossdҠ@
}
summaries/histogram_loss*a	   �L@   �L@      �?!   �L@) �z�A9@2!��v�@زv�5f@�������:              �?        ���       p�*	�(���A��*�

summaries/lossR��@
}
summaries/histogram_loss*a	   @
�@   @
�@      �?!   @
�@) ��\�^8@2!��v�@زv�5f@�������:              �?        ��@�       p�*	)���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �V�@   �V�@      �?!   �V�@)@�熳2;@2زv�5f@��h:np@�������:              �?        �cO�       p�*	Y)���A��*�

summaries/loss�'�@
}
summaries/histogram_loss*a	    �D@    �D@      �?!    �D@)  �(��9@2!��v�@زv�5f@�������:              �?        X]i�       p�*	ߦ)���A��*�

summaries/loss<�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) ��c=H:@2زv�5f@��h:np@�������:              �?        ��e��       p�*	'�)���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �T}@   �T}@      �?!   �T}@) Dj~�7@2!��v�@زv�5f@�������:              �?        ��9�       p�*	:?*���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �6�;@2زv�5f@��h:np@�������:              �?        "PLݦ       p�*	��*���A��*�

summaries/loss>Ҥ@
}
summaries/histogram_loss*a	   �G�@   �G�@      �?!   �G�@) �q��:@2زv�5f@��h:np@�������:              �?        ����       p�*	 �*���A��*�

summaries/lossت@
}
summaries/histogram_loss*a	   �[@   �[@      �?!   �[@) 1�;�<@2زv�5f@��h:np@�������:              �?        Y!�	�       p�*	�)+���A��*�

summaries/loss �@
}
summaries/histogram_loss*a	     �@     �@      �?!     �@)   @��=@2زv�5f@��h:np@�������:              �?        �����       p�*	��+���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �� @   �� @      �?!   �� @) ���;@2زv�5f@��h:np@�������:              �?        ��}�       p�*	L�+���A��*�

summaries/loss< �@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �d�=@2زv�5f@��h:np@�������:              �?        �ZU�       p�*	ˡ,���A��*�

summaries/loss(�@
}
summaries/histogram_loss*a	    % @    % @      �?!    % @) �U���;@2زv�5f@��h:np@�������:              �?        2�m�       p�*	��,���A��*�

summaries/loss�|�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)  ���>@2زv�5f@��h:np@�������:              �?        jSB��       p�*	�I-���A��*�

summaries/loss�7�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �9��<@2زv�5f@��h:np@�������:              �?        ��GV�       p�*	�-���A��*�

summaries/loss�ҩ@
}
summaries/histogram_loss*a	   @R:@   @R:@      �?!   @R:@) ��}�)<@2زv�5f@��h:np@�������:              �?        .՗6�       p�*	��-���A��*�

summaries/loss�z�@
}
summaries/histogram_loss*a	   @U/@   @U/@      �?!   @U/@) 9���<@2زv�5f@��h:np@�������:              �?        ,{GŦ       p�*	�>.���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    p�@    p�@      �?!    p�@)  y��:@2زv�5f@��h:np@�������:              �?        �1�I�       p�*	֏.���A��*�

summaries/loss�d�@
}
summaries/histogram_loss*a	   @�,@   @�,@      �?!   @�,@) ����o9@2!��v�@زv�5f@�������:              �?        쾒��       p�*	��.���A��*�

summaries/loss� �@
}
summaries/histogram_loss*a	   @$@   @$@      �?!   @$@) �ǹ�;@2زv�5f@��h:np@�������:              �?        ��Vd�       p�*	+2/���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��]@   ��]@      �?!   ��]@) d��b�9@2!��v�@زv�5f@�������:              �?        V_+�       p�*	�~/���A��*�

summaries/loss�˩@
}
summaries/histogram_loss*a	   �w9@   �w9@      �?!   �w9@) A���'<@2زv�5f@��h:np@�������:              �?        )d���       p�*	l�/���A��*�

summaries/lossJ��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) Y�I��;@2زv�5f@��h:np@�������:              �?        ���h�       p�*	�$0���A��*�

summaries/loss�*�@
}
summaries/histogram_loss*a	   �U�@   �U�@      �?!   �U�@) �c1��:@2زv�5f@��h:np@�������:              �?        ��
�       p�*	�o0���A��*�

summaries/loss�!�@
}
summaries/histogram_loss*a	   @3�@   @3�@      �?!   @3�@) )D�6G;@2زv�5f@��h:np@�������:              �?        A
K�       p�*	j�0���A��*�

summaries/lossb(�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) a�,E�:@2زv�5f@��h:np@�������:              �?        ���^�       p�*	�1���A��*�

summaries/loss֡@
}
summaries/histogram_loss*a	   @�:@   @�:@      �?!   @�:@) �Z��9@2!��v�@زv�5f@�������:              �?        �J�t�       p�*	�a1���A��*�

summaries/loss0��@
}
summaries/histogram_loss*a	    &V@    &V@      �?!    &V@) @ژ�s<@2زv�5f@��h:np@�������:              �?        T�Zæ       p�*	��1���A��*�

summaries/loss�<�@
}
summaries/histogram_loss*a	   ��G@   ��G@      �?!   ��G@) a�$M<@2زv�5f@��h:np@�������:              �?        7S�]�       p�*	G2���A��*�

summaries/loss�Š@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@.���=9@2!��v�@زv�5f@�������:              �?        G\�Ŧ       p�*	�O2���A��*�

summaries/loss�/�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @��m�:@2زv�5f@��h:np@�������:              �?        �H�ܦ       p�*	n�2���A��*�

summaries/loss^��@
}
summaries/histogram_loss*a	   �k�@   �k�@      �?!   �k�@) ���o;@2زv�5f@��h:np@�������:              �?        m'\�       p�*	l�2���A��*�

summaries/lossUQ�@
}
summaries/histogram_loss*a	   �*�@   �*�@      �?!   �*�@)@���^:@2زv�5f@��h:np@�������:              �?        �ɫ9�       p�*	W>3���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �1�@   �1�@      �?!   �1�@) $�K!�:@2زv�5f@��h:np@�������:              �?        �$�       p�*	a�3���A��*�

summaries/loss*:�@
}
summaries/histogram_loss*a	   @EG@   @EG@      �?!   @EG@) ��?SL<@2زv�5f@��h:np@�������:              �?        ����       p�*	C�3���A��*�

summaries/lossj4�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ���&�:@2زv�5f@��h:np@�������:              �?        @��Φ       p�*	CX4���A��*�

summaries/loss8X�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) �i�=@2زv�5f@��h:np@�������:              �?        "/�       p�*	��4���A��*�

summaries/loss$ޢ@
}
summaries/histogram_loss*a	   ��[@   ��[@      �?!   ��[@) D��y�9@2!��v�@زv�5f@�������:              �?         ����       p�*	 5���A��*�

summaries/loss(�@
}
summaries/histogram_loss*a	    %�@    %�@      �?!    %�@) �Upc�<@2زv�5f@��h:np@�������:              �?        G/�       p�*	3T5���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   �C#@   �C#@      �?!   �C#@)@���vX9@2!��v�@زv�5f@�������:              �?        �<v��       p�*	n�5���A��*�

summaries/loss�{�@
}
summaries/histogram_loss*a	    p�@    p�@      �?!    p�@)  �k:@2زv�5f@��h:np@�������:              �?        HĖ��       p�*	e�5���A��*�

summaries/loss�C�@
}
summaries/histogram_loss*a	   �|h@   �|h@      �?!   �|h@) ��9�:@2زv�5f@��h:np@�������:              �?        ��L�       p�*	�W6���A��*�

summaries/loss�L�@
}
summaries/histogram_loss*a	   ��)@   ��)@      �?!   ��)@) �
xsh9@2!��v�@زv�5f@�������:              �?        ��E�       p�*	��6���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��W@   ��W@      �?!   ��W@)@�8�w<@2زv�5f@��h:np@�������:              �?        =2;�       p�*	��6���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    @    @      �?!    @)@H+��:@2زv�5f@��h:np@�������:              �?        �u��       p�*	�R7���A��*�

summaries/loss�[�@
}
summaries/histogram_loss*a	   @z@   @z@      �?!   @z@) �#)�;@2زv�5f@��h:np@�������:              �?        D甂�       p�*	�7���A��*�

summaries/loss�0�@
}
summaries/histogram_loss*a	   @f@   @f@      �?!   @f@) �ʍ�<@2زv�5f@��h:np@�������:              �?        h,]��       p�*	��7���A*�

summaries/loss�b�@
}
summaries/histogram_loss*a	   �WL@   �WL@      �?!   �WL@) A��G�9@2!��v�@زv�5f@�������:              �?        ڇ��       p�*	�E8���AÛ*�

summaries/loss��@
}
summaries/histogram_loss*a	   �y�@   �y�@      �?!   �y�@) �چ0�8@2!��v�@زv�5f@�������:              �?        �)>�       p�*	�8���Aě*�

summaries/loss�K�@
}
summaries/histogram_loss*a	    x�@    x�@      �?!    x�@)@�)
��8@2!��v�@زv�5f@�������:              �?        �/ɦ       p�*	f�8���Aś*�

summaries/lossJ��@
}
summaries/histogram_loss*a	   @iV@   @iV@      �?!   @iV@) Y$���9@2!��v�@زv�5f@�������:              �?        ���       p�*	�K9���Aƛ*�

summaries/loss�՞@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@���&�8@2!��v�@زv�5f@�������:              �?        ?��       p�*	K�9���AǛ*�

summaries/loss(�@
}
summaries/histogram_loss*a	   @ �@   @ �@      �?!   @ �@) (4�P:@2زv�5f@��h:np@�������:              �?        ��wK�       p�*	�*:���Aț*�

summaries/loss ��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)   d}s:@2زv�5f@��h:np@�������:              �?        ����       p�*	f�:���Aɛ*�

summaries/loss�6�@
}
summaries/histogram_loss*a	    ݆@    ݆@      �?!    ݆@)@ȓ�U:@2زv�5f@��h:np@�������:              �?        _MeC�       p�*	��:���Aʛ*�

summaries/loss��@
}
summaries/histogram_loss*a	   �p�@   �p�@      �?!   �p�@) 7+T�:@2زv�5f@��h:np@�������:              �?        T��Ŧ       p�*	oH;���A˛*�

summaries/lossD.�@
}
summaries/histogram_loss*a	   ��%@   ��%@      �?!   ��%@) � y�^9@2!��v�@زv�5f@�������:              �?        �0��       p�*	��;���A̛*�

summaries/loss��@
}
summaries/histogram_loss*a	   @VB@   @VB@      �?!   @VB@) �`q5?<@2زv�5f@��h:np@�������:              �?        �E�       p�*	H<���A͛*�

summaries/lossd�@
}
summaries/histogram_loss*a	   ��\@   ��\@      �?!   ��\@) ���9@2!��v�@زv�5f@�������:              �?        ��FR�       p�*	�{<���AΛ*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) �p?:6=@2زv�5f@��h:np@�������:              �?        �|�Ǧ       p�*	�<���Aϛ*�

summaries/loss0c�@
}
summaries/histogram_loss*a	    f�@    f�@      �?!    f�@) @�+2	;@2زv�5f@��h:np@�������:              �?        ��(7�       p�*	�%=���AЛ*�

summaries/lossƀ�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) I��Pf;@2زv�5f@��h:np@�������:              �?        ��� �       p�*	>z=���Aћ*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��?@   ��?@      �?!   ��?@) A���>@2زv�5f@��h:np@�������:              �?        a�&{�       p�*	X�=���Aқ*�

summaries/loss�͡@
}
summaries/histogram_loss*a	   @�9@   @�9@      �?!   @�9@) �]��9@2!��v�@زv�5f@�������:              �?        +.���       p�*	\>���Aӛ*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) Q)'�;@2زv�5f@��h:np@�������:              �?        ap���       p�*	Hq>���Aԛ*�

summaries/lossܘ�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) D�p=@2زv�5f@��h:np@�������:              �?        �	G�       p�*	��>���A՛*�

summaries/loss~�@
}
summaries/histogram_loss*a	   ��}@   ��}@      �?!   ��}@) �#�r�<@2زv�5f@��h:np@�������:              �?        "`�Ӧ       p�*	!?���A֛*�

summaries/loss({�@
}
summaries/histogram_loss*a	    e@    e@      �?!    e@) ��O�&9@2!��v�@زv�5f@�������:              �?        �kEɦ       p�*	�k?���Aכ*�

summaries/loss4ڣ@
}
summaries/histogram_loss*a	   �F{@   �F{@      �?!   �F{@) �&�7:@2زv�5f@��h:np@�������:              �?        =�FԦ       p�*	��?���A؛*�

summaries/lossdl�@
}
summaries/histogram_loss*a	   ��M@   ��M@      �?!   ��M@) �!W�9@2!��v�@زv�5f@�������:              �?        �c50�       p�*	; @���Aٛ*�

summaries/loss�F�@
}
summaries/histogram_loss*a	   ��(@   ��(@      �?!   ��(@)@D;���;@2زv�5f@��h:np@�������:              �?        � ���       p�*	n@���Aڛ*�

summaries/lossd��@
}
summaries/histogram_loss*a	   �,�@   �,�@      �?!   �,�@) �+��>@2زv�5f@��h:np@�������:              �?        me� �       p�*	*�@���Aۛ*�

summaries/loss�>�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �?��;@2زv�5f@��h:np@�������:              �?        u����       p�*	)$A���Aܛ*�

summaries/loss[��@
}
summaries/histogram_loss*a	   `�@   `�@      �?!   `�@)@V�q.9@2!��v�@زv�5f@�������:              �?        v�륦       p�*	7rA���Aݛ*�

summaries/losspJ�@
}
summaries/histogram_loss*a	    N�@    N�@      �?!    N�@) @<�B�:@2زv�5f@��h:np@�������:              �?        ��UB�       p�*	H�A���Aޛ*�

summaries/loss4ڤ@
}
summaries/histogram_loss*a	   �F�@   �F�@      �?!   �F�@) �&'�:@2زv�5f@��h:np@�������:              �?        \nT�       p�*	�sB���Aߛ*�

summaries/loss�?�@
}
summaries/histogram_loss*a	   ��g@   ��g@      �?!   ��g@) �T��<@2زv�5f@��h:np@�������:              �?        ��       p�*	(�B���A��*�

summaries/loss̷�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �R!�$;@2زv�5f@��h:np@�������:              �?        z]��       p�*	�IC���A�*�

summaries/lossSӨ@
}
summaries/histogram_loss*a	   `j@   `j@      �?!   `j@)@:{؂�;@2زv�5f@��h:np@�������:              �?        �2=i�       p�*	�C���A�*�

summaries/lossХ@
}
summaries/histogram_loss*a	   @ �@   @ �@      �?!   @ �@) ��r�:@2زv�5f@��h:np@�������:              �?        �z逦       p�*	$�C���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) ��m��:@2زv�5f@��h:np@�������:              �?        �S��       p�*	�ID���A�*�

summaries/lossKk�@
}
summaries/histogram_loss*a	   `im@   `im@      �?!   `im@)@�q� �<@2زv�5f@��h:np@�������:              �?        ��{�       p�*	��D���A�*�

summaries/loss�x�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@)@``�j:@2زv�5f@��h:np@�������:              �?        Fˬ�       p�*	��D���A�*�

summaries/loss?+�@
}
summaries/histogram_loss*a	   �g�@   �g�@      �?!   �g�@)@`��Q:@2زv�5f@��h:np@�������:              �?        .M\�       p�*	�EE���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   �6�@   �6�@      �?!   �6�@) ���m:@2زv�5f@��h:np@�������:              �?        �����       p�*	�E���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   �>�@   �>�@      �?!   �>�@)@�6�n;@2زv�5f@��h:np@�������:              �?        /5މ�       p�*	 �E���A�*�

summaries/loss!%�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@�#{8�:@2زv�5f@��h:np@�������:              �?        `��t�       p�*	|BF���A�*�

summaries/loss�\�@
}
summaries/histogram_loss*a	   ��k@   ��k@      �?!   ��k@) �*�:@2زv�5f@��h:np@�������:              �?        ��ܣ�       p�*	�F���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	    r�@    r�@      �?!    r�@)@Bz��1@2{2�.��@!��v�@�������:              �?        ��_m�       p�*	%�F���A�*�

summaries/lossvs�@
}
summaries/histogram_loss*a	   �n@   �n@      �?!   �n@) ��4�`4@2{2�.��@!��v�@�������:              �?        ���       p�*	�3G���A�*�

summaries/loss�Q�@
}
summaries/histogram_loss*a	    9*@    9*@      �?!    9*@) ��i9@2!��v�@زv�5f@�������:              �?        �cO#�       p�*	G���A�*�

summaries/loss���@
}
summaries/histogram_loss*a	   �_@   �_@      �?!   �_@) ��59@2!��v�@زv�5f@�������:              �?        ����       p�*	j�G���A�*�

summaries/loss��@
}
summaries/histogram_loss*a	    �#@    �#@      �?!    �#@) ���Y9@2!��v�@زv�5f@�������:              �?        �ե�       p�*	k(H���A�*�

summaries/lossb��@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) aȖ%H8@2!��v�@زv�5f@�������:              �?        .ڟ��       p�*	#�H���A�*�

summaries/loss�&�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) @���l8@2!��v�@زv�5f@�������:              �?        S΁�       p�*	4�H���A�*�

summaries/loss�å@
}
summaries/histogram_loss*a	   @~�@   @~�@      �?!   @~�@) 1����:@2زv�5f@��h:np@�������:              �?        X�       p�*	w,I���A�*�

summaries/loss�.�@
}
summaries/histogram_loss*a	   ��%@   ��%@      �?!   ��%@)@����;@2زv�5f@��h:np@�������:              �?        �z�x�       p�*	��I���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    �0@    �0@      �?!    �0@) , 5<@2زv�5f@��h:np@�������:              �?        }޲�       p�*	C�I���A��*�

summaries/loss�t�@
}
summaries/histogram_loss*a	   ��N@   ��N@      �?!   ��N@) V	�_<@2زv�5f@��h:np@�������:              �?        ��=��       p�*	0-J���A��*�

summaries/loss$�@
}
summaries/histogram_loss*a	   �D@   �D@      �?!   �D@) D��+9@2!��v�@زv�5f@�������:              �?        O�~&�       p�*	Y�J���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@k�,�:@2زv�5f@��h:np@�������:              �?        Y��Ȧ       p�*	,�J���A��*�

summaries/loss֞�@
}
summaries/histogram_loss*a	   �ړ@   �ړ@      �?!   �ړ@) �6�t=@2زv�5f@��h:np@�������:              �?        ���       p�*	t)K���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �| @   �| @      �?!   �| @) ��ň�;@2زv�5f@��h:np@�������:              �?        ��}j�       p�*	#�K���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �Y@   �Y@      �?!   �Y@) qg��9@2!��v�@زv�5f@�������:              �?        ��Q6�       p�*	��K���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @Q@   @Q@      �?!   @Q@) 	�li�9@2!��v�@زv�5f@�������:              �?        D*���       p�*	hxL���A��*�

summaries/lossӦ�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@z��1;@2زv�5f@��h:np@�������:              �?        t�k$�       p�*	�)M���A��*�

summaries/loss\/�@
}
summaries/histogram_loss*a	   ��%@   ��%@      �?!   ��%@) D���;@2زv�5f@��h:np@�������:              �?        ���       p�*	��M���A��*�

summaries/lossR;�@
}
summaries/histogram_loss*a	   @jG@   @jG@      �?!   @jG@) ����L<@2زv�5f@��h:np@�������:              �?        [��Ѧ       p�*	3�N���A��*�

summaries/loss�¥@
}
summaries/histogram_loss*a	   �V�@   �V�@      �?!   �V�@) Y�$�:@2زv�5f@��h:np@�������:              �?        c�Mu�       p�*	.�O���A��*�

summaries/loss�@
}
summaries/histogram_loss*a	   @c�@   @c�@      �?!   @c�@) ��r�G:@2زv�5f@��h:np@�������:              �?        �m��       p�*	�'P���A��*�

summaries/loss p�@
}
summaries/histogram_loss*a	     �@     �@      �?!     �@)   @d�:@2زv�5f@��h:np@�������:              �?        ��C�       p�*	)zP���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@4����8@2!��v�@زv�5f@�������:              �?        (_���       p�*	O�P���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@nӚ��;@2زv�5f@��h:np@�������:              �?        ]�       p�*	6"Q���A��*�

summaries/lossV��@
}
summaries/histogram_loss*a	   �Jq@   �Jq@      �?!   �Jq@) 95i��<@2زv�5f@��h:np@�������:              �?        ��	g�       p�*	�uQ���A��*�

summaries/lossAͥ@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@)@�ʖ��:@2زv�5f@��h:np@�������:              �?        �Y��       p�*	>�Q���A��*�

summaries/loss~U�@
}
summaries/histogram_loss*a	   ��*@   ��*@      �?!   ��*@) �:: <@2زv�5f@��h:np@�������:              �?        +o���       p�*	_(R���A��*�

summaries/loss�/�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �&�v�:@2زv�5f@��h:np@�������:              �?        H�<�       p�*	sR���A��*�

summaries/lossq�@
}
summaries/histogram_loss*a	    C@    C@      �?!    C@)@xXA<@2زv�5f@��h:np@�������:              �?        V>���       p�*	e�R���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �p=@   �p=@      �?!   �p=@) ���32<@2زv�5f@��h:np@�������:              �?        1���       p�*	�S���A��*�

summaries/lossH��@
}
summaries/histogram_loss*a	    I4@    I4@      �?!    I4@) �[a�9@2!��v�@زv�5f@�������:              �?        ơ��       p�*	
kS���A��*�

summaries/lossQ3�@
}
summaries/histogram_loss*a	    j&@    j&@      �?!    j&@)@���2�;@2زv�5f@��h:np@�������:              �?        ��S�       p�*	��S���A��*�

summaries/loss�v�@
}
summaries/histogram_loss*a	   ��.@   ��.@      �?!   ��.@)@�̳�<@2زv�5f@��h:np@�������:              �?        ����       p�*	�T���A��*�

summaries/lossD��@
}
summaries/histogram_loss*a	   �H�@   �H�@      �?!   �H�@) �h��;@2زv�5f@��h:np@�������:              �?        ��l�       p�*	VT���A��*�

summaries/loss
?�@
}
summaries/histogram_loss*a	   @�@   @�@      �?!   @�@) {5X:@2زv�5f@��h:np@�������:              �?        A'Ӧ       p�*	�T���A��*�

summaries/lossjC�@
}
summaries/histogram_loss*a	   @m�@   @m�@      �?!   @m�@) �)���:@2زv�5f@��h:np@�������:              �?        ��㮦       p�*	t�T���A��*�

summaries/lossR��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ���;@2زv�5f@��h:np@�������:              �?        ȵ5ʦ       p�*	�CU���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @;|@   @;|@      �?!   @;|@) i;�X::@2زv�5f@��h:np@�������:              �?        �G�2�       p�*	N�U���A��*�

summaries/loss�-�@
}
summaries/histogram_loss*a	   `�%@   `�%@      �?!   `�%@)@ʚӠ^9@2!��v�@زv�5f@�������:              �?        �q���       p�*	`�U���A��*�

summaries/loss�Q�@
}
summaries/histogram_loss*a	   �<
@   �<
@      �?!   �<
@)@����;@2زv�5f@��h:np@�������:              �?        �G:
�       p�*	�6V���A��*�

summaries/loss�Ǫ@
}
summaries/histogram_loss*a	   ��X@   ��X@      �?!   ��X@) Yav{<@2زv�5f@��h:np@�������:              �?        ib�       p�*	i�V���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @v�@   @v�@      �?!   @v�@) ��O�=@2زv�5f@��h:np@�������:              �?        u��ɦ       p�*	�V���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @6s@   @6s@      �?!   @6s@) �<E#:@2زv�5f@��h:np@�������:              �?        �9ʦ       p�*	h%W���A��*�

summaries/loss Ʃ@
}
summaries/histogram_loss*a	    �8@    �8@      �?!    �8@)   I�%<@2زv�5f@��h:np@�������:              �?        W�)h�       p�*	�sW���A��*�

summaries/lossݱ�@
}
summaries/histogram_loss*a	   �;�@   �;�@      �?!   �;�@)@2֮}:@2زv�5f@��h:np@�������:              �?        �#��       p�*	�W���A��*�

summaries/loss�Q�@
}
summaries/histogram_loss*a	   `2�@   `2�@      �?!   `2�@)@��(y;@2زv�5f@��h:np@�������:              �?        �D���       p�*	jX���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) a�X8@2!��v�@زv�5f@�������:              �?        ���)�       p�*	�oX���A��*�

summaries/lossb�@
}
summaries/histogram_loss*a	   @� @   @� @      �?!   @� @) a/�AR9@2!��v�@زv�5f@�������:              �?        ��X��       p�*	5�X���A��*�

summaries/loss�˘@
}
summaries/histogram_loss*a	    t@    t@      �?!    t@)  ����6@2!��v�@زv�5f@�������:              �?        �����       p�*	S�Y���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	    4�@    4�@      �?!    4�@)  �}:@2زv�5f@��h:np@�������:              �?        �rG��       p�*	�Z���A��*�

summaries/lossX,�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) ���n8@2!��v�@زv�5f@�������:              �?        0wD�       p�*	��Z���A��*�

summaries/loss
��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ��_z;@2زv�5f@��h:np@�������:              �?        i�       p�*	��Z���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) $�*X�=@2زv�5f@��h:np@�������:              �?        ����       p�*	�D[���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) ��c�=@2زv�5f@��h:np@�������:              �?        0��G�       p�*	ݗ[���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   ��]@   ��]@      �?!   ��]@) !��܇<@2زv�5f@��h:np@�������:              �?        ����       p�*	p�[���A��*�

summaries/losshM�@
}
summaries/histogram_loss*a	    ��@    ��@      �?!    ��@) ��1T�=@2زv�5f@��h:np@�������:              �?        E��Ʀ       p�*	i:\���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �-x�o=@2زv�5f@��h:np@�������:              �?        �9.Φ       p�*	�\���A��*�

summaries/lossLe�@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �C�=@2زv�5f@��h:np@�������:              �?         ����       p�*	��\���A��*�

summaries/lossv��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) �̇wj;@2زv�5f@��h:np@�������:              �?        �U�       p�*	�2]���A��*�

summaries/loss"�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) !%?�0=@2زv�5f@��h:np@�������:              �?        �B�=�       p�*	��]���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   @~�@   @~�@      �?!   @~�@) 1T�]�:@2زv�5f@��h:np@�������:              �?        %ty��       p�*	h�]���A��*�

summaries/loss.��@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@) +��;;@2زv�5f@��h:np@�������:              �?        �ZQӦ       p�*	f4^���A��*�

summaries/loss�-�@
}
summaries/histogram_loss*a	   `��@   `��@      �?!   `��@)@�2��=@2زv�5f@��h:np@�������:              �?        O���       p�*	�^���A��*�

summaries/lossv�@
}
summaries/histogram_loss*a	   ��]@   ��]@      �?!   ��]@) �q��9@2!��v�@زv�5f@�������:              �?        �uϦ       p�*	f�^���A��*�

summaries/loss�å@
}
summaries/histogram_loss*a	    z�@    z�@      �?!    z�@) @���:@2زv�5f@��h:np@�������:              �?        ɦ       p�*	#4_���A��*�

summaries/loss�k�@
}
summaries/histogram_loss*a	   `r�@   `r�@      �?!   `r�@)@��x�^=@2زv�5f@��h:np@�������:              �?        "�-�       p�*	�_���A��*�

summaries/loss�F�@
}
summaries/histogram_loss*a	   ��(@   ��(@      �?!   ��(@) dQ���;@2زv�5f@��h:np@�������:              �?        �J��       p�*	g�_���A��*�

summaries/lossvҡ@
}
summaries/histogram_loss*a	   �N:@   �N:@      �?!   �N:@) �s\��9@2!��v�@زv�5f@�������:              �?        �x̦       p�*	:=`���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) $��k;@2زv�5f@��h:np@�������:              �?        {�nҦ       p�*	��`���A��*�

summaries/loss�|�@
}
summaries/histogram_loss*a	   @�o@   @�o@      �?!   @�o@) �:���<@2زv�5f@��h:np@�������:              �?        #�=5�       p�*	?�`���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   @8�@   @8�@      �?!   @8�@) ��=�:@2زv�5f@��h:np@�������:              �?        Β�       p�*	<2a���A��*�

summaries/lossĩ@
}
summaries/histogram_loss*a	   ��8@   ��8@      �?!   ��8@) ��%<@2زv�5f@��h:np@�������:              �?        ��*ئ       p�*	�a���A��*�

summaries/loss ��@
}
summaries/histogram_loss*a	    6@    6@      �?!    6@)  [��9@2!��v�@زv�5f@�������:              �?        ��M	�       p�*	�a���A��*�

summaries/loss�
�@
}
summaries/histogram_loss*a	   `Ra@   `Ra@      �?!   `Ra@)@t4ȑ<@2زv�5f@��h:np@�������:              �?        u+dŦ       p�*	�xb���A��*�

summaries/loss.�@
}
summaries/histogram_loss*a	    �%@    �%@      �?!    �%@)@ �eq�;@2زv�5f@��h:np@�������:              �?        G��8�       p�*	��b���A��*�

summaries/loss���@
}
summaries/histogram_loss*a	   �:@   �:@      �?!   �:@) �,7�;@2زv�5f@��h:np@�������:              �?        ��8�       p�*	z1c���A��*�

summaries/loss4Ϋ@
}
summaries/histogram_loss*a	   ��y@   ��y@      �?!   ��y@) ���G�<@2زv�5f@��h:np@�������:              �?        �lKh�       p�*	L�c���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �7�@   �7�@      �?!   �7�@)@b˥D;@2زv�5f@��h:np@�������:              �?        sFk�       p�*	��c���A��*�

summaries/lossn+�@
}
summaries/histogram_loss*a	   �m�@   �m�@      �?!   �m�@) ш8lJ;@2زv�5f@��h:np@�������:              �?        �Iͤ�       p�*	0d���A��*�

summaries/loss'�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@|��t�:@2زv�5f@��h:np@�������:              �?        ��Z�       p�*	4�d���A��*�

summaries/loss�E�@
}
summaries/histogram_loss*a	   @��@   @��@      �?!   @��@) 	.ִ�:@2زv�5f@��h:np@�������:              �?        r7ܦ       p�*	 �d���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   `=@   `=@      �?!   `=@)@R�X��9@2!��v�@زv�5f@�������:              �?        ����       p�*	�e���A��*�

summaries/loss��@
}
summaries/histogram_loss*a	   �q=@   �q=@      �?!   �q=@)@xv�62<@2زv�5f@��h:np@�������:              �?        etF��       p�*	�oe���A��*�

summaries/lossb��@
}
summaries/histogram_loss*a	   @l�@   @l�@      �?!   @l�@) a\�3=@2زv�5f@��h:np@�������:              �?        /m�z�       p�*	��e���A��*�

summaries/loss4Ǩ@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) �xЃ�;@2زv�5f@��h:np@�������:              �?        �gD�       p�*	�f���A��*�

summaries/loss�C�@
}
summaries/histogram_loss*a	    r�@    r�@      �?!    r�@)@�@NR;@2زv�5f@��h:np@�������:              �?        6���       p�*	�`f���A*�

summaries/loss��@
}
summaries/histogram_loss*a	    A@    A@      �?!    A@) Hs��;@2زv�5f@��h:np@�������:              �?        �����       p�*	�f���AÜ*�

summaries/loss��@
}
summaries/histogram_loss*a	    @    @      �?!    @)@��G�;@2زv�5f@��h:np@�������:              �?        N�{�       p�*	��f���AĜ*�

summaries/loss� �@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@)@��3�:@2زv�5f@��h:np@�������:              �?        ٪k�       p�*	�Qg���AŜ*�

summaries/losss�@
}
summaries/histogram_loss*a	    cn@    cn@      �?!    cn@)@^e�:@2زv�5f@��h:np@�������:              �?        "���       p�*	�g���AƜ*�

summaries/lossΐ�@
}
summaries/histogram_loss*a	   ��@   ��@      �?!   ��@) qvvr:@2زv�5f@��h:np@�������:              �?        ��y��       p�*	k�g���Aǜ*�

summaries/loss%͝@
}
summaries/histogram_loss*a	   ���@   ���@      �?!   ���@)@֑�PQ8@2!��v�@زv�5f@�������:              �?        O�Pئ       p�*	_Fh���AȜ*�

summaries/lossp!�@
}
summaries/histogram_loss*a	    .�@    .�@      �?!    .�@) @�)G;@2زv�5f@��h:np@�������:              �?        �O{S�       p�*	��h���Aɜ*�

summaries/loss���@
}
summaries/histogram_loss*a	   @4T@   @4T@      �?!   @4T@) �J�=�9@2!��v�@زv�5f@�������:              �?        TL���       p�*	5�h���Aʜ*�

summaries/loss�0�@
}
summaries/histogram_loss*a	    �@    �@      �?!    �@) @��C!8@2!��v�@زv�5f@�������:              �?        ��v��       p�*	9i���A˜*�

summaries/lossZ�@
}
summaries/histogram_loss*a	   @+�@   @+�@      �?!   @+�@) �<�:@2زv�5f@��h:np@�������:              �?        ��<��       p�*	�i���A̜*�

summaries/loss ۫@
}
summaries/histogram_loss*a	    `{@    `{@      �?!    `{@)  @V��<@2زv�5f@��h:np@�������:              �?        �`H�       p�*	�i���A͜*�

summaries/loss�F�@
}
summaries/histogram_loss*a	   ��h@   ��h@      �?!   ��h@) ���<@2زv�5f@��h:np@�������:              �?        �k�\�       p�*	2j���AΜ*�

summaries/loss�]�@
}
summaries/histogram_loss*a	    ��@    ��@  