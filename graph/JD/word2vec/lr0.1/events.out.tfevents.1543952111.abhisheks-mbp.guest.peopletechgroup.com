       £K"	  Ą»µ×Abrain.Event:2×r’Ż@     \ŗ	ńĶ»µ×A"Š
I
args_0Const*
value
B :N*
dtype0*
_output_shapes
: 
I
args_1Const*
_output_shapes
: *
value
B :*
dtype0
H
args_2Const*
value	B :*
dtype0*
_output_shapes
: 
T
args_3Const*
valueB Bvisualization*
dtype0*
_output_shapes
: 
I
args_4Const*
value
B BJD*
dtype0*
_output_shapes
: 
U
tensors/component_0Const*
value	B : *
dtype0*
_output_shapes
: 
L

batch_sizeConst*
value	B	 R*
dtype0	*
_output_shapes
: 
K
args_0_1Const*
value
B BJD*
dtype0*
_output_shapes
: 
J
args_1_1Const*
value	B :*
dtype0*
_output_shapes
: 
W
tensors_1/component_0Const*
value	B : *
dtype0*
_output_shapes
: 
G
ConstConst*
value	B : *
dtype0*
_output_shapes
: 
o
global_step
VariableV2*
dtype0*
_output_shapes
: *
	container *
shape: *
shared_name 

global_step/AssignAssignglobal_stepConst*
use_locking(*
T0*
_class
loc:@global_step*
validate_shape(*
_output_shapes
: 
j
global_step/readIdentityglobal_step*
T0*
_class
loc:@global_step*
_output_shapes
: 

data/IteratorIterator*
shared_name *%
output_shapes
::	*
_output_shapes
: *
	container *
output_types
2
”
data/TensorDatasetTensorDatasettensors/component_0* 
_class
loc:@data/Iterator*
Toutput_types
2*
_output_shapes
: *
output_shapes
: 

data/FlatMapDatasetFlatMapDatasetdata/TensorDatasetargs_0args_1args_2args_3args_4*%
output_shapes
::	* 
_class
loc:@data/Iterator* 
fR
tf_map_func_PCOijuOn9mg*
output_types
2*

Targuments	
2*
_output_shapes
: 
g
data/MakeIteratorMakeIteratordata/FlatMapDatasetdata/Iterator* 
_class
loc:@data/Iterator
\
data/IteratorToStringHandleIteratorToStringHandledata/Iterator*
_output_shapes
: 
¢
data/Iterator_1Iterator*
output_types
2*
shared_name *1
output_shapes 
:’’’’’’’’’:’’’’’’’’’*
_output_shapes
: *
	container 
§
data/TensorDataset_1TensorDatasettensors_1/component_0*
output_shapes
: *"
_class
loc:@data/Iterator_1*
Toutput_types
2*
_output_shapes
: 

data/FlatMapDataset_1FlatMapDatasetdata/TensorDataset_1args_0_1args_1_1*"
_class
loc:@data/Iterator_1* 
fR
tf_map_func_8pkeaS83Jbc*
output_types
2*

Targuments
2*
_output_shapes
: *1
output_shapes 
:’’’’’’’’’:’’’’’’’’’
o
data/MakeIterator_1MakeIteratordata/FlatMapDataset_1data/Iterator_1*"
_class
loc:@data/Iterator_1
`
data/IteratorToStringHandle_1IteratorToStringHandledata/Iterator_1*
_output_shapes
: 

data/IteratorGetNextIteratorGetNextdata/Iterator*%
output_shapes
::	*&
_output_shapes
::	*
output_types
2
¹
data/IteratorGetNext_1IteratorGetNextdata/Iterator_1*1
output_shapes 
:’’’’’’’’’:’’’’’’’’’*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’*
output_types
2

.nce_weights/Initializer/truncated_normal/shapeConst*
_output_shapes
:*
_class
loc:@nce_weights*
valueB"'  Č   *
dtype0

-nce_weights/Initializer/truncated_normal/meanConst*
_class
loc:@nce_weights*
valueB
 *    *
dtype0*
_output_shapes
: 

/nce_weights/Initializer/truncated_normal/stddevConst*
_class
loc:@nce_weights*
valueB
 *ĆŠ=*
dtype0*
_output_shapes
: 
ģ
8nce_weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormal.nce_weights/Initializer/truncated_normal/shape*
dtype0* 
_output_shapes
:
NČ*

seed *
T0*
_class
loc:@nce_weights*
seed2 
é
,nce_weights/Initializer/truncated_normal/mulMul8nce_weights/Initializer/truncated_normal/TruncatedNormal/nce_weights/Initializer/truncated_normal/stddev*
T0*
_class
loc:@nce_weights* 
_output_shapes
:
NČ
×
(nce_weights/Initializer/truncated_normalAdd,nce_weights/Initializer/truncated_normal/mul-nce_weights/Initializer/truncated_normal/mean* 
_output_shapes
:
NČ*
T0*
_class
loc:@nce_weights
£
nce_weights
VariableV2*
dtype0* 
_output_shapes
:
NČ*
shared_name *
_class
loc:@nce_weights*
	container *
shape:
NČ
Ē
nce_weights/AssignAssignnce_weights(nce_weights/Initializer/truncated_normal*
T0*
_class
loc:@nce_weights*
validate_shape(* 
_output_shapes
:
NČ*
use_locking(
t
nce_weights/readIdentitynce_weights* 
_output_shapes
:
NČ*
T0*
_class
loc:@nce_weights
h
weights/zeros/shape_as_tensorConst*
_output_shapes
:*
valueB:N*
dtype0
X
weights/zeros/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 

weights/zerosFillweights/zeros/shape_as_tensorweights/zeros/Const*

index_type0*
_output_shapes	
:N*
T0
v
nce_bias
VariableV2*
dtype0*
_output_shapes	
:N*
	container *
shape:N*
shared_name 

nce_bias/AssignAssignnce_biasweights/zeros*
_output_shapes	
:N*
use_locking(*
T0*
_class
loc:@nce_bias*
validate_shape(
f
nce_bias/readIdentitynce_bias*
_class
loc:@nce_bias*
_output_shapes	
:N*
T0

-embeddings_1/Initializer/random_uniform/shapeConst*
_class
loc:@embeddings_1*
valueB"'  Č   *
dtype0*
_output_shapes
:

+embeddings_1/Initializer/random_uniform/minConst*
_class
loc:@embeddings_1*
valueB
 *    *
dtype0*
_output_shapes
: 

+embeddings_1/Initializer/random_uniform/maxConst*
_output_shapes
: *
_class
loc:@embeddings_1*
valueB
 *  ?*
dtype0
ē
5embeddings_1/Initializer/random_uniform/RandomUniformRandomUniform-embeddings_1/Initializer/random_uniform/shape* 
_output_shapes
:
NČ*

seed *
T0*
_class
loc:@embeddings_1*
seed2 *
dtype0
Ī
+embeddings_1/Initializer/random_uniform/subSub+embeddings_1/Initializer/random_uniform/max+embeddings_1/Initializer/random_uniform/min*
T0*
_class
loc:@embeddings_1*
_output_shapes
: 
ā
+embeddings_1/Initializer/random_uniform/mulMul5embeddings_1/Initializer/random_uniform/RandomUniform+embeddings_1/Initializer/random_uniform/sub*
T0*
_class
loc:@embeddings_1* 
_output_shapes
:
NČ
Ō
'embeddings_1/Initializer/random_uniformAdd+embeddings_1/Initializer/random_uniform/mul+embeddings_1/Initializer/random_uniform/min* 
_output_shapes
:
NČ*
T0*
_class
loc:@embeddings_1
„
embeddings_1
VariableV2*
shape:
NČ*
dtype0* 
_output_shapes
:
NČ*
shared_name *
_class
loc:@embeddings_1*
	container 
É
embeddings_1/AssignAssignembeddings_1'embeddings_1/Initializer/random_uniform*
T0*
_class
loc:@embeddings_1*
validate_shape(* 
_output_shapes
:
NČ*
use_locking(
w
embeddings_1/readIdentityembeddings_1*
T0*
_class
loc:@embeddings_1* 
_output_shapes
:
NČ

 embeddings/embedding_lookup/axisConst*
_class
loc:@embeddings_1*
value	B : *
dtype0*
_output_shapes
: 
į
embeddings/embedding_lookupGatherV2embeddings_1/readdata/IteratorGetNext embeddings/embedding_lookup/axis*
Taxis0*
Tindices0*
Tparams0*
_class
loc:@embeddings_1* 
_output_shapes
:
Č
~
filling/filling_lookup/axisConst*
_class
loc:@embeddings_1*
value	B : *
dtype0*
_output_shapes
: 
ć
filling/filling_lookupGatherV2embeddings_1/readdata/IteratorGetNext_1:1filling/filling_lookup/axis*
Taxis0*
Tindices0*
Tparams0*
_class
loc:@embeddings_1*(
_output_shapes
:’’’’’’’’’Č
`
filling/Mean/reduction_indicesConst*
_output_shapes
: *
value	B : *
dtype0

filling/MeanMeanfilling/filling_lookupfilling/Mean/reduction_indices*
_output_shapes	
:Č*
	keep_dims( *

Tidx0*
T0
o
nce_loss/nce_loss/CastCastdata/IteratorGetNext:1*

SrcT0*
_output_shapes
:	*

DstT0	
r
nce_loss/nce_loss/Reshape/shapeConst*
valueB:
’’’’’’’’’*
dtype0*
_output_shapes
:

nce_loss/nce_loss/ReshapeReshapence_loss/nce_loss/Castnce_loss/nce_loss/Reshape/shape*
T0	*
Tshape0*
_output_shapes	
:
ė
,nce_loss/nce_loss/LogUniformCandidateSamplerLogUniformCandidateSamplernce_loss/nce_loss/Cast*
num_sampled@*+
_output_shapes
:@:	:@*

seed *
unique(*
seed2 *
num_true*
	range_maxN

nce_loss/nce_loss/StopGradientStopGradient,nce_loss/nce_loss/LogUniformCandidateSampler*
_output_shapes
:@*
T0	

 nce_loss/nce_loss/StopGradient_1StopGradient.nce_loss/nce_loss/LogUniformCandidateSampler:1*
T0*
_output_shapes
:	

 nce_loss/nce_loss/StopGradient_2StopGradient.nce_loss/nce_loss/LogUniformCandidateSampler:2*
T0*
_output_shapes
:@
_
nce_loss/nce_loss/concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
¹
nce_loss/nce_loss/concatConcatV2nce_loss/nce_loss/Reshapence_loss/nce_loss/StopGradientnce_loss/nce_loss/concat/axis*

Tidx0*
T0	*
N*
_output_shapes	
:Ą

'nce_loss/nce_loss/embedding_lookup/axisConst*
_class
loc:@nce_weights*
value	B : *
dtype0*
_output_shapes
: 
ń
"nce_loss/nce_loss/embedding_lookupGatherV2nce_weights/readnce_loss/nce_loss/concat'nce_loss/nce_loss/embedding_lookup/axis* 
_output_shapes
:
ĄČ*
Taxis0*
Tindices0	*
Tparams0*
_class
loc:@nce_weights
b
nce_loss/nce_loss/ShapeConst*
valueB:*
dtype0*
_output_shapes
:
o
%nce_loss/nce_loss/strided_slice/stackConst*
dtype0*
_output_shapes
:*
valueB: 
q
'nce_loss/nce_loss/strided_slice/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
q
'nce_loss/nce_loss/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
Ó
nce_loss/nce_loss/strided_sliceStridedSlicence_loss/nce_loss/Shape%nce_loss/nce_loss/strided_slice/stack'nce_loss/nce_loss/strided_slice/stack_1'nce_loss/nce_loss/strided_slice/stack_2*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
Index0*
T0
d
nce_loss/nce_loss/stack/1Const*
dtype0*
_output_shapes
: *
valueB :
’’’’’’’’’

nce_loss/nce_loss/stackPacknce_loss/nce_loss/strided_slicence_loss/nce_loss/stack/1*
N*
_output_shapes
:*
T0*

axis 
n
nce_loss/nce_loss/Slice/beginConst*
_output_shapes
:*
valueB"        *
dtype0
Ä
nce_loss/nce_loss/SliceSlice"nce_loss/nce_loss/embedding_lookupnce_loss/nce_loss/Slice/beginnce_loss/nce_loss/stack*
Index0*
T0*0
_output_shapes
:’’’’’’’’’’’’’’’’’’
d
nce_loss/nce_loss/Shape_1Const*
valueB:*
dtype0*
_output_shapes
:
q
'nce_loss/nce_loss/strided_slice_1/stackConst*
dtype0*
_output_shapes
:*
valueB: 
s
)nce_loss/nce_loss/strided_slice_1/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_1/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
Ż
!nce_loss/nce_loss/strided_slice_1StridedSlicence_loss/nce_loss/Shape_1'nce_loss/nce_loss/strided_slice_1/stack)nce_loss/nce_loss/strided_slice_1/stack_1)nce_loss/nce_loss/strided_slice_1/stack_2*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0*
shrink_axis_mask
]
nce_loss/nce_loss/stack_1/1Const*
value	B : *
dtype0*
_output_shapes
: 

nce_loss/nce_loss/stack_1Pack!nce_loss/nce_loss/strided_slice_1nce_loss/nce_loss/stack_1/1*
T0*

axis *
N*
_output_shapes
:
o
nce_loss/nce_loss/Slice_1/sizeConst*
valueB"’’’’’’’’*
dtype0*
_output_shapes
:
Į
nce_loss/nce_loss/Slice_1Slice"nce_loss/nce_loss/embedding_lookupnce_loss/nce_loss/stack_1nce_loss/nce_loss/Slice_1/size*
Index0*
T0*(
_output_shapes
:’’’’’’’’’Č
³
nce_loss/nce_loss/MatMulMatMulembeddings/embedding_lookupnce_loss/nce_loss/Slice_1*
T0*(
_output_shapes
:’’’’’’’’’*
transpose_a( *
transpose_b(

)nce_loss/nce_loss/embedding_lookup_1/axisConst*
_class
loc:@nce_bias*
value	B : *
dtype0*
_output_shapes
: 
ź
$nce_loss/nce_loss/embedding_lookup_1GatherV2nce_bias/readnce_loss/nce_loss/concat)nce_loss/nce_loss/embedding_lookup_1/axis*
Tindices0	*
Tparams0*
_class
loc:@nce_bias*
_output_shapes	
:Ą*
Taxis0
d
nce_loss/nce_loss/Shape_2Const*
valueB:*
dtype0*
_output_shapes
:
i
nce_loss/nce_loss/Slice_2/beginConst*
valueB: *
dtype0*
_output_shapes
:
·
nce_loss/nce_loss/Slice_2Slice$nce_loss/nce_loss/embedding_lookup_1nce_loss/nce_loss/Slice_2/beginnce_loss/nce_loss/Shape_2*
_output_shapes	
:*
Index0*
T0
d
nce_loss/nce_loss/Shape_3Const*
valueB:*
dtype0*
_output_shapes
:
q
nce_loss/nce_loss/Slice_3/sizeConst*
valueB:
’’’’’’’’’*
dtype0*
_output_shapes
:
µ
nce_loss/nce_loss/Slice_3Slice$nce_loss/nce_loss/embedding_lookup_1nce_loss/nce_loss/Shape_3nce_loss/nce_loss/Slice_3/size*
Index0*
T0*
_output_shapes
:@
p
nce_loss/nce_loss/Shape_4Shapence_loss/nce_loss/Slice*
T0*
out_type0*
_output_shapes
:
q
'nce_loss/nce_loss/strided_slice_2/stackConst*
valueB:*
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_2/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_2/stack_2Const*
dtype0*
_output_shapes
:*
valueB:
į
!nce_loss/nce_loss/strided_slice_2StridedSlicence_loss/nce_loss/Shape_4'nce_loss/nce_loss/strided_slice_2/stack)nce_loss/nce_loss/strided_slice_2/stack_1)nce_loss/nce_loss/strided_slice_2/stack_2*
new_axis_mask *
end_mask *
_output_shapes
:*
Index0*
T0*
shrink_axis_mask *
ellipsis_mask *

begin_mask 
t
#nce_loss/nce_loss/concat_1/values_0Const*
_output_shapes
:*
valueB"’’’’   *
dtype0
a
nce_loss/nce_loss/concat_1/axisConst*
value	B : *
dtype0*
_output_shapes
: 
É
nce_loss/nce_loss/concat_1ConcatV2#nce_loss/nce_loss/concat_1/values_0!nce_loss/nce_loss/strided_slice_2nce_loss/nce_loss/concat_1/axis*
N*
_output_shapes
:*

Tidx0*
T0
b
 nce_loss/nce_loss/ExpandDims/dimConst*
value	B :*
dtype0*
_output_shapes
: 
¤
nce_loss/nce_loss/ExpandDims
ExpandDimsembeddings/embedding_lookup nce_loss/nce_loss/ExpandDims/dim*
T0*$
_output_shapes
:Č*

Tdim0
Ø
nce_loss/nce_loss/Reshape_1Reshapence_loss/nce_loss/Slicence_loss/nce_loss/concat_1*
T0*
Tshape0*4
_output_shapes"
 :’’’’’’’’’’’’’’’’’’

nce_loss/nce_loss/MulMulnce_loss/nce_loss/ExpandDimsnce_loss/nce_loss/Reshape_1*$
_output_shapes
:Č*
T0
v
#nce_loss/nce_loss/concat_2/values_0Const*
valueB:
’’’’’’’’’*
dtype0*
_output_shapes
:
a
nce_loss/nce_loss/concat_2/axisConst*
value	B : *
dtype0*
_output_shapes
: 
É
nce_loss/nce_loss/concat_2ConcatV2#nce_loss/nce_loss/concat_2/values_0!nce_loss/nce_loss/strided_slice_2nce_loss/nce_loss/concat_2/axis*
T0*
N*
_output_shapes
:*

Tidx0
¢
nce_loss/nce_loss/Reshape_2Reshapence_loss/nce_loss/Mulnce_loss/nce_loss/concat_2*
Tshape0*0
_output_shapes
:’’’’’’’’’’’’’’’’’’*
T0
t
nce_loss/nce_loss/Shape_5Shapence_loss/nce_loss/Reshape_2*
T0*
out_type0*
_output_shapes
:
q
'nce_loss/nce_loss/strided_slice_3/stackConst*
dtype0*
_output_shapes
:*
valueB:
s
)nce_loss/nce_loss/strided_slice_3/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_3/stack_2Const*
_output_shapes
:*
valueB:*
dtype0
Ż
!nce_loss/nce_loss/strided_slice_3StridedSlicence_loss/nce_loss/Shape_5'nce_loss/nce_loss/strided_slice_3/stack)nce_loss/nce_loss/strided_slice_3/stack_1)nce_loss/nce_loss/strided_slice_3/stack_2*
T0*
Index0*
shrink_axis_mask*
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
_output_shapes
: 
]
nce_loss/nce_loss/stack_2/1Const*
dtype0*
_output_shapes
: *
value	B :

nce_loss/nce_loss/stack_2Pack!nce_loss/nce_loss/strided_slice_3nce_loss/nce_loss/stack_2/1*
T0*

axis *
N*
_output_shapes
:
a
nce_loss/nce_loss/ones/ConstConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 

nce_loss/nce_loss/onesFillnce_loss/nce_loss/stack_2nce_loss/nce_loss/ones/Const*
T0*

index_type0*'
_output_shapes
:’’’’’’’’’
±
nce_loss/nce_loss/MatMul_1MatMulnce_loss/nce_loss/Reshape_2nce_loss/nce_loss/ones*'
_output_shapes
:’’’’’’’’’*
transpose_a( *
transpose_b( *
T0
t
!nce_loss/nce_loss/Reshape_3/shapeConst*
valueB:
’’’’’’’’’*
dtype0*
_output_shapes
:
”
nce_loss/nce_loss/Reshape_3Reshapence_loss/nce_loss/MatMul_1!nce_loss/nce_loss/Reshape_3/shape*
T0*
Tshape0*#
_output_shapes
:’’’’’’’’’
r
!nce_loss/nce_loss/Reshape_4/shapeConst*
valueB"’’’’   *
dtype0*
_output_shapes
:
¦
nce_loss/nce_loss/Reshape_4Reshapence_loss/nce_loss/Reshape_3!nce_loss/nce_loss/Reshape_4/shape*
T0*
Tshape0*'
_output_shapes
:’’’’’’’’’
r
!nce_loss/nce_loss/Reshape_5/shapeConst*
valueB"’’’’   *
dtype0*
_output_shapes
:

nce_loss/nce_loss/Reshape_5Reshapence_loss/nce_loss/Slice_2!nce_loss/nce_loss/Reshape_5/shape*
T0*
Tshape0*
_output_shapes
:	

nce_loss/nce_loss/addAddnce_loss/nce_loss/Reshape_4nce_loss/nce_loss/Reshape_5*
_output_shapes
:	*
T0
}
nce_loss/nce_loss/add_1Addnce_loss/nce_loss/MatMulnce_loss/nce_loss/Slice_3*
T0*
_output_shapes
:	@
h
nce_loss/nce_loss/LogLog nce_loss/nce_loss/StopGradient_1*
T0*
_output_shapes
:	
t
nce_loss/nce_loss/subSubnce_loss/nce_loss/addnce_loss/nce_loss/Log*
_output_shapes
:	*
T0
e
nce_loss/nce_loss/Log_1Log nce_loss/nce_loss/StopGradient_2*
_output_shapes
:@*
T0
z
nce_loss/nce_loss/sub_1Subnce_loss/nce_loss/add_1nce_loss/nce_loss/Log_1*
T0*
_output_shapes
:	@
a
nce_loss/nce_loss/concat_3/axisConst*
value	B :*
dtype0*
_output_shapes
: 
¶
nce_loss/nce_loss/concat_3ConcatV2nce_loss/nce_loss/subnce_loss/nce_loss/sub_1nce_loss/nce_loss/concat_3/axis*

Tidx0*
T0*
N*
_output_shapes
:	A
r
!nce_loss/nce_loss/ones_like/ShapeConst*
valueB"      *
dtype0*
_output_shapes
:
f
!nce_loss/nce_loss/ones_like/ConstConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
„
nce_loss/nce_loss/ones_likeFill!nce_loss/nce_loss/ones_like/Shape!nce_loss/nce_loss/ones_like/Const*
_output_shapes
:	*
T0*

index_type0
`
nce_loss/nce_loss/truediv/yConst*
_output_shapes
: *
valueB
 *  ?*
dtype0

nce_loss/nce_loss/truedivRealDivnce_loss/nce_loss/ones_likence_loss/nce_loss/truediv/y*
_output_shapes
:	*
T0
}
,nce_loss/nce_loss/zeros_like/shape_as_tensorConst*
valueB"   @   *
dtype0*
_output_shapes
:
g
"nce_loss/nce_loss/zeros_like/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    
²
nce_loss/nce_loss/zeros_likeFill,nce_loss/nce_loss/zeros_like/shape_as_tensor"nce_loss/nce_loss/zeros_like/Const*
T0*

index_type0*
_output_shapes
:	@
a
nce_loss/nce_loss/concat_4/axisConst*
_output_shapes
: *
value	B :*
dtype0
æ
nce_loss/nce_loss/concat_4ConcatV2nce_loss/nce_loss/truedivnce_loss/nce_loss/zeros_likence_loss/nce_loss/concat_4/axis*
N*
_output_shapes
:	A*

Tidx0*
T0

2nce_loss/sampled_losses/zeros_like/shape_as_tensorConst*
valueB"   A   *
dtype0*
_output_shapes
:
m
(nce_loss/sampled_losses/zeros_like/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
Ä
"nce_loss/sampled_losses/zeros_likeFill2nce_loss/sampled_losses/zeros_like/shape_as_tensor(nce_loss/sampled_losses/zeros_like/Const*
_output_shapes
:	A*
T0*

index_type0

$nce_loss/sampled_losses/GreaterEqualGreaterEqualnce_loss/nce_loss/concat_3"nce_loss/sampled_losses/zeros_like*
T0*
_output_shapes
:	A
ø
nce_loss/sampled_losses/SelectSelect$nce_loss/sampled_losses/GreaterEqualnce_loss/nce_loss/concat_3"nce_loss/sampled_losses/zeros_like*
_output_shapes
:	A*
T0
h
nce_loss/sampled_losses/NegNegnce_loss/nce_loss/concat_3*
T0*
_output_shapes
:	A
³
 nce_loss/sampled_losses/Select_1Select$nce_loss/sampled_losses/GreaterEqualnce_loss/sampled_losses/Negnce_loss/nce_loss/concat_3*
T0*
_output_shapes
:	A

nce_loss/sampled_losses/mulMulnce_loss/nce_loss/concat_3nce_loss/nce_loss/concat_4*
T0*
_output_shapes
:	A

nce_loss/sampled_losses/subSubnce_loss/sampled_losses/Selectnce_loss/sampled_losses/mul*
_output_shapes
:	A*
T0
n
nce_loss/sampled_losses/ExpExp nce_loss/sampled_losses/Select_1*
T0*
_output_shapes
:	A
m
nce_loss/sampled_losses/Log1pLog1pnce_loss/sampled_losses/Exp*
T0*
_output_shapes
:	A

nce_loss/sampled_lossesAddnce_loss/sampled_losses/subnce_loss/sampled_losses/Log1p*
T0*
_output_shapes
:	A
_
nce_loss/ShapeConst*
valueB"   A   *
dtype0*
_output_shapes
:
f
nce_loss/strided_slice/stackConst*
valueB:*
dtype0*
_output_shapes
:
h
nce_loss/strided_slice/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
h
nce_loss/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
¦
nce_loss/strided_sliceStridedSlicence_loss/Shapence_loss/strided_slice/stacknce_loss/strided_slice/stack_1nce_loss/strided_slice/stack_2*
T0*
Index0*
shrink_axis_mask*
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
_output_shapes
: 
R
nce_loss/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
z
nce_loss/stackPacknce_loss/strided_slicence_loss/stack/1*
T0*

axis *
N*
_output_shapes
:
X
nce_loss/ones/ConstConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
~
nce_loss/onesFillnce_loss/stacknce_loss/ones/Const*'
_output_shapes
:’’’’’’’’’*
T0*

index_type0

nce_loss/MatMulMatMulnce_loss/sampled_lossesnce_loss/ones*
T0*
_output_shapes
:	*
transpose_a( *
transpose_b( 
i
nce_loss/Reshape/shapeConst*
valueB:
’’’’’’’’’*
dtype0*
_output_shapes
:
x
nce_loss/ReshapeReshapence_loss/MatMulnce_loss/Reshape/shape*
_output_shapes	
:*
T0*
Tshape0
X
nce_loss/ConstConst*
dtype0*
_output_shapes
:*
valueB: 
{
nce_loss/nce_loss_1Meannce_loss/Reshapence_loss/Const*
T0*
_output_shapes
: *
	keep_dims( *

Tidx0
[
optimize/gradients/ShapeConst*
_output_shapes
: *
valueB *
dtype0
a
optimize/gradients/grad_ys_0Const*
valueB
 *  ?*
dtype0*
_output_shapes
: 

optimize/gradients/FillFilloptimize/gradients/Shapeoptimize/gradients/grad_ys_0*
T0*

index_type0*
_output_shapes
: 

9optimize/gradients/nce_loss/nce_loss_1_grad/Reshape/shapeConst*
dtype0*
_output_shapes
:*
valueB:
Å
3optimize/gradients/nce_loss/nce_loss_1_grad/ReshapeReshapeoptimize/gradients/Fill9optimize/gradients/nce_loss/nce_loss_1_grad/Reshape/shape*
T0*
Tshape0*
_output_shapes
:
|
1optimize/gradients/nce_loss/nce_loss_1_grad/ConstConst*
dtype0*
_output_shapes
:*
valueB:
Ų
0optimize/gradients/nce_loss/nce_loss_1_grad/TileTile3optimize/gradients/nce_loss/nce_loss_1_grad/Reshape1optimize/gradients/nce_loss/nce_loss_1_grad/Const*

Tmultiples0*
T0*
_output_shapes	
:
x
3optimize/gradients/nce_loss/nce_loss_1_grad/Const_1Const*
valueB
 *   C*
dtype0*
_output_shapes
: 
Ė
3optimize/gradients/nce_loss/nce_loss_1_grad/truedivRealDiv0optimize/gradients/nce_loss/nce_loss_1_grad/Tile3optimize/gradients/nce_loss/nce_loss_1_grad/Const_1*
T0*
_output_shapes	
:

.optimize/gradients/nce_loss/Reshape_grad/ShapeConst*
_output_shapes
:*
valueB"      *
dtype0
Ų
0optimize/gradients/nce_loss/Reshape_grad/ReshapeReshape3optimize/gradients/nce_loss/nce_loss_1_grad/truediv.optimize/gradients/nce_loss/Reshape_grad/Shape*
_output_shapes
:	*
T0*
Tshape0
Ņ
.optimize/gradients/nce_loss/MatMul_grad/MatMulMatMul0optimize/gradients/nce_loss/Reshape_grad/Reshapence_loss/ones*
T0*(
_output_shapes
:’’’’’’’’’*
transpose_a( *
transpose_b(
Ō
0optimize/gradients/nce_loss/MatMul_grad/MatMul_1MatMulnce_loss/sampled_losses0optimize/gradients/nce_loss/Reshape_grad/Reshape*
transpose_b( *
T0*
_output_shapes

:A*
transpose_a(
¤
8optimize/gradients/nce_loss/MatMul_grad/tuple/group_depsNoOp/^optimize/gradients/nce_loss/MatMul_grad/MatMul1^optimize/gradients/nce_loss/MatMul_grad/MatMul_1
¤
@optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependencyIdentity.optimize/gradients/nce_loss/MatMul_grad/MatMul9^optimize/gradients/nce_loss/MatMul_grad/tuple/group_deps*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul*
_output_shapes
:	A
©
Boptimize/gradients/nce_loss/MatMul_grad/tuple/control_dependency_1Identity0optimize/gradients/nce_loss/MatMul_grad/MatMul_19^optimize/gradients/nce_loss/MatMul_grad/tuple/group_deps*C
_class9
75loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul_1*
_output_shapes

:A*
T0

@optimize/gradients/nce_loss/sampled_losses_grad/tuple/group_depsNoOpA^optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependency
Ę
Hoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependencyIdentity@optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependencyA^optimize/gradients/nce_loss/sampled_losses_grad/tuple/group_deps*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul*
_output_shapes
:	A
Č
Joptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency_1Identity@optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependencyA^optimize/gradients/nce_loss/sampled_losses_grad/tuple/group_deps*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul*
_output_shapes
:	A
²
7optimize/gradients/nce_loss/sampled_losses/sub_grad/NegNegHoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency*
T0*
_output_shapes
:	A
Ń
Doptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/sampled_losses/sub_grad/NegI^optimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency
Ö
Loptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependencyIdentityHoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependencyE^optimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/group_deps*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul*
_output_shapes
:	A
Š
Noptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency_1Identity7optimize/gradients/nce_loss/sampled_losses/sub_grad/NegE^optimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/group_deps*
_output_shapes
:	A*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/sampled_losses/sub_grad/Neg
Ķ
;optimize/gradients/nce_loss/sampled_losses/Log1p_grad/add/xConstK^optimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency_1*
valueB
 *  ?*
dtype0*
_output_shapes
: 
Ä
9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/addAdd;optimize/gradients/nce_loss/sampled_losses/Log1p_grad/add/xnce_loss/sampled_losses/Exp*
_output_shapes
:	A*
T0
³
@optimize/gradients/nce_loss/sampled_losses/Log1p_grad/Reciprocal
Reciprocal9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/add*
T0*
_output_shapes
:	A
ų
9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/mulMulJoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency_1@optimize/gradients/nce_loss/sampled_losses/Log1p_grad/Reciprocal*
T0*
_output_shapes
:	A
¢
Qoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/shape_as_tensorConst*
valueB"   A   *
dtype0*
_output_shapes
:

Goptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
”
Aoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_likeFillQoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/shape_as_tensorGoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/Const*
T0*

index_type0*
_output_shapes
:	A
Ø
=optimize/gradients/nce_loss/sampled_losses/Select_grad/SelectSelect$nce_loss/sampled_losses/GreaterEqualLoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependencyAoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like*
T0*
_output_shapes
:	A
Ŗ
?optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1Select$nce_loss/sampled_losses/GreaterEqualAoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_likeLoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency*
_output_shapes
:	A*
T0
Ń
Goptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/group_depsNoOp>^optimize/gradients/nce_loss/sampled_losses/Select_grad/Select@^optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1
ą
Ooptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/control_dependencyIdentity=optimize/gradients/nce_loss/sampled_losses/Select_grad/SelectH^optimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/group_deps*
T0*P
_classF
DBloc:@optimize/gradients/nce_loss/sampled_losses/Select_grad/Select*
_output_shapes
:	A
ę
Qoptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/control_dependency_1Identity?optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1H^optimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/group_deps*
T0*R
_classH
FDloc:@optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1*
_output_shapes
:	A
Ō
7optimize/gradients/nce_loss/sampled_losses/mul_grad/MulMulNoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency_1nce_loss/nce_loss/concat_4*
T0*
_output_shapes
:	A
Ö
9optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1MulNoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency_1nce_loss/nce_loss/concat_3*
_output_shapes
:	A*
T0
Ā
Doptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul:^optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1
Ī
Loptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/sampled_losses/mul_grad/MulE^optimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/group_deps*
_output_shapes
:	A*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul
Ō
Noptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1E^optimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/group_deps*
_output_shapes
:	A*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1
Ą
7optimize/gradients/nce_loss/sampled_losses/Exp_grad/mulMul9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/mulnce_loss/sampled_losses/Exp*
_output_shapes
:	A*
T0
¤
Soptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/shape_as_tensorConst*
valueB"   A   *
dtype0*
_output_shapes
:

Ioptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    
§
Coptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_likeFillSoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/shape_as_tensorIoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/Const*
T0*

index_type0*
_output_shapes
:	A

?optimize/gradients/nce_loss/sampled_losses/Select_1_grad/SelectSelect$nce_loss/sampled_losses/GreaterEqual7optimize/gradients/nce_loss/sampled_losses/Exp_grad/mulCoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like*
T0*
_output_shapes
:	A

Aoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1Select$nce_loss/sampled_losses/GreaterEqualCoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like7optimize/gradients/nce_loss/sampled_losses/Exp_grad/mul*
T0*
_output_shapes
:	A
×
Ioptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/group_depsNoOp@^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/SelectB^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1
č
Qoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependencyIdentity?optimize/gradients/nce_loss/sampled_losses/Select_1_grad/SelectJ^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/group_deps*
_output_shapes
:	A*
T0*R
_classH
FDloc:@optimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select
ī
Soptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependency_1IdentityAoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1J^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@optimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1*
_output_shapes
:	A
»
7optimize/gradients/nce_loss/sampled_losses/Neg_grad/NegNegQoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependency*
T0*
_output_shapes
:	A
Ń
optimize/gradients/AddNAddNOoptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/control_dependencyLoptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/control_dependencySoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependency_17optimize/gradients/nce_loss/sampled_losses/Neg_grad/Neg*
T0*P
_classF
DBloc:@optimize/gradients/nce_loss/sampled_losses/Select_grad/Select*
N*
_output_shapes
:	A
y
7optimize/gradients/nce_loss/nce_loss/concat_3_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 
½
6optimize/gradients/nce_loss/nce_loss/concat_3_grad/modFloorModnce_loss/nce_loss/concat_3/axis7optimize/gradients/nce_loss/nce_loss/concat_3_grad/Rank*
T0*
_output_shapes
: 

8optimize/gradients/nce_loss/nce_loss/concat_3_grad/ShapeConst*
valueB"      *
dtype0*
_output_shapes
:

:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape_1Const*
valueB"   @   *
dtype0*
_output_shapes
:
Ø
?optimize/gradients/nce_loss/nce_loss/concat_3_grad/ConcatOffsetConcatOffset6optimize/gradients/nce_loss/nce_loss/concat_3_grad/mod8optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape_1*
N* 
_output_shapes
::

8optimize/gradients/nce_loss/nce_loss/concat_3_grad/SliceSliceoptimize/gradients/AddN?optimize/gradients/nce_loss/nce_loss/concat_3_grad/ConcatOffset8optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape*
Index0*
T0*
_output_shapes
:	

:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1Sliceoptimize/gradients/AddNAoptimize/gradients/nce_loss/nce_loss/concat_3_grad/ConcatOffset:1:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape_1*
_output_shapes
:	@*
Index0*
T0
Ć
Coptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/group_depsNoOp9^optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice;^optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1
Ī
Koptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependencyIdentity8optimize/gradients/nce_loss/nce_loss/concat_3_grad/SliceD^optimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/group_deps*
T0*K
_classA
?=loc:@optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice*
_output_shapes
:	
Ō
Moptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency_1Identity:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1D^optimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/group_deps*
T0*M
_classC
A?loc:@optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1*
_output_shapes
:	@
Æ
1optimize/gradients/nce_loss/nce_loss/sub_grad/NegNegKoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency*
T0*
_output_shapes
:	
Č
>optimize/gradients/nce_loss/nce_loss/sub_grad/tuple/group_depsNoOpL^optimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency2^optimize/gradients/nce_loss/nce_loss/sub_grad/Neg
×
Foptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependencyIdentityKoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency?^optimize/gradients/nce_loss/nce_loss/sub_grad/tuple/group_deps*
_output_shapes
:	*
T0*K
_classA
?=loc:@optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice
ø
Hoptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependency_1Identity1optimize/gradients/nce_loss/nce_loss/sub_grad/Neg?^optimize/gradients/nce_loss/nce_loss/sub_grad/tuple/group_deps*D
_class:
86loc:@optimize/gradients/nce_loss/nce_loss/sub_grad/Neg*
_output_shapes
:	*
T0

5optimize/gradients/nce_loss/nce_loss/sub_1_grad/ShapeConst*
valueB"   @   *
dtype0*
_output_shapes
:

7optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape_1Const*
valueB:@*
dtype0*
_output_shapes
:

Eoptimize/gradients/nce_loss/nce_loss/sub_1_grad/BroadcastGradientArgsBroadcastGradientArgs5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape7optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape_1*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’*
T0

3optimize/gradients/nce_loss/nce_loss/sub_1_grad/SumSumMoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency_1Eoptimize/gradients/nce_loss/nce_loss/sub_1_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*
	keep_dims( *

Tidx0
ę
7optimize/gradients/nce_loss/nce_loss/sub_1_grad/ReshapeReshape3optimize/gradients/nce_loss/nce_loss/sub_1_grad/Sum5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape*
_output_shapes
:	@*
T0*
Tshape0

5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Sum_1SumMoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency_1Goptimize/gradients/nce_loss/nce_loss/sub_1_grad/BroadcastGradientArgs:1*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0

3optimize/gradients/nce_loss/nce_loss/sub_1_grad/NegNeg5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Sum_1*
T0*
_output_shapes
:
å
9optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1Reshape3optimize/gradients/nce_loss/nce_loss/sub_1_grad/Neg7optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape_1*
_output_shapes
:@*
T0*
Tshape0
¾
@optimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape:^optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1
Ę
Hoptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/nce_loss/sub_1_grad/ReshapeA^optimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/group_deps*
_output_shapes
:	@*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape
Ē
Joptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1A^optimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/group_deps*
_output_shapes
:@*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1

3optimize/gradients/nce_loss/nce_loss/add_grad/ShapeShapence_loss/nce_loss/Reshape_4*
T0*
out_type0*
_output_shapes
:

5optimize/gradients/nce_loss/nce_loss/add_grad/Shape_1Const*
valueB"      *
dtype0*
_output_shapes
:

Coptimize/gradients/nce_loss/nce_loss/add_grad/BroadcastGradientArgsBroadcastGradientArgs3optimize/gradients/nce_loss/nce_loss/add_grad/Shape5optimize/gradients/nce_loss/nce_loss/add_grad/Shape_1*
T0*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’

1optimize/gradients/nce_loss/nce_loss/add_grad/SumSumFoptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependencyCoptimize/gradients/nce_loss/nce_loss/add_grad/BroadcastGradientArgs*
	keep_dims( *

Tidx0*
T0*
_output_shapes
:
č
5optimize/gradients/nce_loss/nce_loss/add_grad/ReshapeReshape1optimize/gradients/nce_loss/nce_loss/add_grad/Sum3optimize/gradients/nce_loss/nce_loss/add_grad/Shape*
T0*
Tshape0*'
_output_shapes
:’’’’’’’’’

3optimize/gradients/nce_loss/nce_loss/add_grad/Sum_1SumFoptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependencyEoptimize/gradients/nce_loss/nce_loss/add_grad/BroadcastGradientArgs:1*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
ę
7optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1Reshape3optimize/gradients/nce_loss/nce_loss/add_grad/Sum_15optimize/gradients/nce_loss/nce_loss/add_grad/Shape_1*
_output_shapes
:	*
T0*
Tshape0
ø
>optimize/gradients/nce_loss/nce_loss/add_grad/tuple/group_depsNoOp6^optimize/gradients/nce_loss/nce_loss/add_grad/Reshape8^optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1
Ę
Foptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependencyIdentity5optimize/gradients/nce_loss/nce_loss/add_grad/Reshape?^optimize/gradients/nce_loss/nce_loss/add_grad/tuple/group_deps*
T0*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/add_grad/Reshape*'
_output_shapes
:’’’’’’’’’
Ä
Hoptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependency_1Identity7optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1?^optimize/gradients/nce_loss/nce_loss/add_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1*
_output_shapes
:	

5optimize/gradients/nce_loss/nce_loss/add_1_grad/ShapeShapence_loss/nce_loss/MatMul*
_output_shapes
:*
T0*
out_type0

7optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape_1Const*
valueB:@*
dtype0*
_output_shapes
:

Eoptimize/gradients/nce_loss/nce_loss/add_1_grad/BroadcastGradientArgsBroadcastGradientArgs5optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape7optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape_1*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’*
T0

3optimize/gradients/nce_loss/nce_loss/add_1_grad/SumSumHoptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependencyEoptimize/gradients/nce_loss/nce_loss/add_1_grad/BroadcastGradientArgs*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
ļ
7optimize/gradients/nce_loss/nce_loss/add_1_grad/ReshapeReshape3optimize/gradients/nce_loss/nce_loss/add_1_grad/Sum5optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape*(
_output_shapes
:’’’’’’’’’*
T0*
Tshape0

5optimize/gradients/nce_loss/nce_loss/add_1_grad/Sum_1SumHoptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependencyGoptimize/gradients/nce_loss/nce_loss/add_1_grad/BroadcastGradientArgs:1*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
ē
9optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1Reshape5optimize/gradients/nce_loss/nce_loss/add_1_grad/Sum_17optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:@
¾
@optimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape:^optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1
Ļ
Hoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/nce_loss/add_1_grad/ReshapeA^optimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape*(
_output_shapes
:’’’’’’’’’
Ē
Joptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1A^optimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/group_deps*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1*
_output_shapes
:@*
T0

9optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/ShapeShapence_loss/nce_loss/Reshape_3*
T0*
out_type0*
_output_shapes
:

;optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/ReshapeReshapeFoptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependency9optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/Shape*
Tshape0*#
_output_shapes
:’’’’’’’’’*
T0

9optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/ShapeConst*
valueB:*
dtype0*
_output_shapes
:
’
;optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/ReshapeReshapeHoptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependency_19optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/Shape*
T0*
Tshape0*
_output_shapes	
:
÷
7optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMulMatMulHoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependencynce_loss/nce_loss/Slice_1* 
_output_shapes
:
Č*
transpose_a( *
transpose_b( *
T0

9optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1MatMulHoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependencyembeddings/embedding_lookup*
T0*(
_output_shapes
:’’’’’’’’’Č*
transpose_a(*
transpose_b( 
æ
Aoptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul:^optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1
É
Ioptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMulB^optimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/group_deps* 
_output_shapes
:
Č*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul
×
Koptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1B^optimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/group_deps*(
_output_shapes
:’’’’’’’’’Č*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1
x
6optimize/gradients/nce_loss/nce_loss/Slice_3_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 

7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/ShapeConst*
valueB:@*
dtype0*
_output_shapes
:
{
9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
ģ
7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stackPack6optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Rank9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack/1*
T0*

axis *
N*
_output_shapes
:
Ļ
9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/ReshapeReshapence_loss/nce_loss/Shape_37optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack*
T0*
Tshape0*
_output_shapes

:

9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Shape_1Const*
valueB:Ą*
dtype0*
_output_shapes
:
Õ
5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/subSub9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Shape_17optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Shape*
T0*
_output_shapes
:
µ
7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/sub_1Sub5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/subnce_loss/nce_loss/Shape_3*
T0*
_output_shapes
:
ļ
;optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Reshape_1Reshape7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/sub_17optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack*
_output_shapes

:*
T0*
Tshape0

=optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concat/axisConst*
value	B :*
dtype0*
_output_shapes
: 
¹
8optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concatConcatV29optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Reshape;optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Reshape_1=optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concat/axis*
N*
_output_shapes

:*

Tidx0*
T0
ł
5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/PadPadJoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependency_18optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concat*
T0*
	Tpaddings0*
_output_shapes	
:Ą

9optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/ShapeShapence_loss/nce_loss/MatMul_1*
_output_shapes
:*
T0*
out_type0
ž
;optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/ReshapeReshape;optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/Reshape9optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/Shape*
T0*
Tshape0*'
_output_shapes
:’’’’’’’’’
x
6optimize/gradients/nce_loss/nce_loss/Slice_2_grad/RankConst*
_output_shapes
: *
value	B :*
dtype0

7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/ShapeConst*
valueB:*
dtype0*
_output_shapes
:
{
9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack/1Const*
dtype0*
_output_shapes
: *
value	B :
ģ
7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stackPack6optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Rank9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack/1*

axis *
N*
_output_shapes
:*
T0
Õ
9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/ReshapeReshapence_loss/nce_loss/Slice_2/begin7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack*
T0*
Tshape0*
_output_shapes

:

9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Shape_1Const*
valueB:Ą*
dtype0*
_output_shapes
:
Õ
5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/subSub9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Shape_17optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Shape*
_output_shapes
:*
T0
»
7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/sub_1Sub5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/subnce_loss/nce_loss/Slice_2/begin*
T0*
_output_shapes
:
ļ
;optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Reshape_1Reshape7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/sub_17optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack*
_output_shapes

:*
T0*
Tshape0

=optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concat/axisConst*
_output_shapes
: *
value	B :*
dtype0
¹
8optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concatConcatV29optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Reshape;optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Reshape_1=optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concat/axis*
T0*
N*
_output_shapes

:*

Tidx0
ź
5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/PadPad;optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/Reshape8optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concat*
T0*
	Tpaddings0*
_output_shapes	
:Ą
x
6optimize/gradients/nce_loss/nce_loss/Slice_1_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 

7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/ShapeShapence_loss/nce_loss/Slice_1*
T0*
out_type0*
_output_shapes
:
{
9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack/1Const*
dtype0*
_output_shapes
: *
value	B :
ģ
7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stackPack6optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Rank9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack/1*
T0*

axis *
N*
_output_shapes
:
Ļ
9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/ReshapeReshapence_loss/nce_loss/stack_17optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack*
_output_shapes

:*
T0*
Tshape0

9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Shape_1Const*
valueB"Ą   Č   *
dtype0*
_output_shapes
:
Õ
5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/subSub9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Shape_17optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Shape*
T0*
_output_shapes
:
µ
7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/sub_1Sub5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/subnce_loss/nce_loss/stack_1*
T0*
_output_shapes
:
ļ
;optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Reshape_1Reshape7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/sub_17optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack*
T0*
Tshape0*
_output_shapes

:

=optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concat/axisConst*
value	B :*
dtype0*
_output_shapes
: 
¹
8optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concatConcatV29optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Reshape;optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Reshape_1=optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concat/axis*

Tidx0*
T0*
N*
_output_shapes

:
’
5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/PadPadKoptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependency_18optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concat*
	Tpaddings0* 
_output_shapes
:
ĄČ*
T0
ł
9optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMulMatMul;optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/Reshapence_loss/nce_loss/ones*0
_output_shapes
:’’’’’’’’’’’’’’’’’’*
transpose_a( *
transpose_b(*
T0
÷
;optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1MatMulnce_loss/nce_loss/Reshape_2;optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/Reshape*
transpose_b( *
T0*'
_output_shapes
:’’’’’’’’’*
transpose_a(
Å
Coptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/group_depsNoOp:^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul<^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1
į
Koptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/control_dependencyIdentity9optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMulD^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/group_deps*0
_output_shapes
:’’’’’’’’’’’’’’’’’’*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul
Ž
Moptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/control_dependency_1Identity;optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1D^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/group_deps*'
_output_shapes
:’’’’’’’’’*
T0*N
_classD
B@loc:@optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1

optimize/gradients/AddN_1AddN5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Pad5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Pad*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Pad*
N*
_output_shapes	
:Ą*
T0
Ŗ
Boptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ShapeConst*
_class
loc:@nce_bias*
valueB	RN*
dtype0	*
_output_shapes
:
į
Doptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ToInt32CastBoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Shape*

SrcT0	*
_class
loc:@nce_bias*
_output_shapes
:*

DstT0

Aoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/SizeConst*
_output_shapes
: *
value
B :Ą*
dtype0

Koptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims/dimConst*
_output_shapes
: *
value	B : *
dtype0

Goptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims
ExpandDimsAoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/SizeKoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims/dim*
_output_shapes
:*

Tdim0*
T0

Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stackConst*
valueB:*
dtype0*
_output_shapes
:

Roptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_1Const*
valueB: *
dtype0*
_output_shapes
:

Roptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_2Const*
_output_shapes
:*
valueB:*
dtype0
®
Joptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_sliceStridedSliceDoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ToInt32Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stackRoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_1Roptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_2*
_output_shapes
: *
Index0*
T0*
shrink_axis_mask *

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask

Hoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
č
Coptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concatConcatV2Goptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDimsJoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_sliceHoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concat/axis*
T0*
N*
_output_shapes
:*

Tidx0
ć
Doptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ReshapeReshapeoptimize/gradients/AddN_1Coptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concat*
T0*
Tshape0*
_output_shapes	
:Ą
č
Foptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Reshape_1Reshapence_loss/nce_loss/concatGoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims*
T0	*
Tshape0*
_output_shapes	
:Ą

9optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/ShapeConst*!
valueB"      Č   *
dtype0*
_output_shapes
:

;optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/ReshapeReshapeKoptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/control_dependency9optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/Shape*
T0*
Tshape0*$
_output_shapes
:Č

3optimize/gradients/nce_loss/nce_loss/Mul_grad/ShapeConst*!
valueB"      Č   *
dtype0*
_output_shapes
:

5optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape_1Shapence_loss/nce_loss/Reshape_1*
T0*
out_type0*
_output_shapes
:

Coptimize/gradients/nce_loss/nce_loss/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs3optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape5optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape_1*
T0*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’
Į
1optimize/gradients/nce_loss/nce_loss/Mul_grad/MulMul;optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/Reshapence_loss/nce_loss/Reshape_1*
T0*$
_output_shapes
:Č
š
1optimize/gradients/nce_loss/nce_loss/Mul_grad/SumSum1optimize/gradients/nce_loss/nce_loss/Mul_grad/MulCoptimize/gradients/nce_loss/nce_loss/Mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*
	keep_dims( *

Tidx0
å
5optimize/gradients/nce_loss/nce_loss/Mul_grad/ReshapeReshape1optimize/gradients/nce_loss/nce_loss/Mul_grad/Sum3optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape*$
_output_shapes
:Č*
T0*
Tshape0
Ä
3optimize/gradients/nce_loss/nce_loss/Mul_grad/Mul_1Mulnce_loss/nce_loss/ExpandDims;optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/Reshape*
T0*$
_output_shapes
:Č
ö
3optimize/gradients/nce_loss/nce_loss/Mul_grad/Sum_1Sum3optimize/gradients/nce_loss/nce_loss/Mul_grad/Mul_1Eoptimize/gradients/nce_loss/nce_loss/Mul_grad/BroadcastGradientArgs:1*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
ū
7optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1Reshape3optimize/gradients/nce_loss/nce_loss/Mul_grad/Sum_15optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape_1*
Tshape0*4
_output_shapes"
 :’’’’’’’’’’’’’’’’’’*
T0
ø
>optimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/group_depsNoOp6^optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape8^optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1
Ć
Foptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependencyIdentity5optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape?^optimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/group_deps*
T0*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape*$
_output_shapes
:Č
Ł
Hoptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependency_1Identity7optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1?^optimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1*4
_output_shapes"
 :’’’’’’’’’’’’’’’’’’

:optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/ShapeConst*
valueB"   Č   *
dtype0*
_output_shapes
:

<optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/ReshapeReshapeFoptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependency:optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/Shape*
T0*
Tshape0* 
_output_shapes
:
Č

9optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/ShapeShapence_loss/nce_loss/Slice*
T0*
out_type0*
_output_shapes
:

;optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/ReshapeReshapeHoptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependency_19optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/Shape*
Tshape0*0
_output_shapes
:’’’’’’’’’’’’’’’’’’*
T0
Ŗ
optimize/gradients/AddN_2AddNIoptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependency<optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/Reshape*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul*
N* 
_output_shapes
:
Č*
T0
³
9optimize/gradients/embeddings/embedding_lookup_grad/ShapeConst*
_class
loc:@embeddings_1*%
valueB	"'      Č       *
dtype0	*
_output_shapes
:
Ó
;optimize/gradients/embeddings/embedding_lookup_grad/ToInt32Cast9optimize/gradients/embeddings/embedding_lookup_grad/Shape*
_output_shapes
:*

DstT0*

SrcT0	*
_class
loc:@embeddings_1
{
8optimize/gradients/embeddings/embedding_lookup_grad/SizeConst*
_output_shapes
: *
value
B :*
dtype0

Boptimize/gradients/embeddings/embedding_lookup_grad/ExpandDims/dimConst*
value	B : *
dtype0*
_output_shapes
: 
ū
>optimize/gradients/embeddings/embedding_lookup_grad/ExpandDims
ExpandDims8optimize/gradients/embeddings/embedding_lookup_grad/SizeBoptimize/gradients/embeddings/embedding_lookup_grad/ExpandDims/dim*

Tdim0*
T0*
_output_shapes
:

Goptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stackConst*
valueB:*
dtype0*
_output_shapes
:

Ioptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_1Const*
valueB: *
dtype0*
_output_shapes
:

Ioptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:

Aoptimize/gradients/embeddings/embedding_lookup_grad/strided_sliceStridedSlice;optimize/gradients/embeddings/embedding_lookup_grad/ToInt32Goptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stackIoptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_1Ioptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask*
_output_shapes
:

?optimize/gradients/embeddings/embedding_lookup_grad/concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
Ä
:optimize/gradients/embeddings/embedding_lookup_grad/concatConcatV2>optimize/gradients/embeddings/embedding_lookup_grad/ExpandDimsAoptimize/gradients/embeddings/embedding_lookup_grad/strided_slice?optimize/gradients/embeddings/embedding_lookup_grad/concat/axis*
T0*
N*
_output_shapes
:*

Tidx0
Ö
;optimize/gradients/embeddings/embedding_lookup_grad/ReshapeReshapeoptimize/gradients/AddN_2:optimize/gradients/embeddings/embedding_lookup_grad/concat* 
_output_shapes
:
Č*
T0*
Tshape0
Ņ
=optimize/gradients/embeddings/embedding_lookup_grad/Reshape_1Reshapedata/IteratorGetNext>optimize/gradients/embeddings/embedding_lookup_grad/ExpandDims*
T0*
Tshape0*
_output_shapes	
:
v
4optimize/gradients/nce_loss/nce_loss/Slice_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 

5optimize/gradients/nce_loss/nce_loss/Slice_grad/ShapeShapence_loss/nce_loss/Slice*
T0*
out_type0*
_output_shapes
:
y
7optimize/gradients/nce_loss/nce_loss/Slice_grad/stack/1Const*
_output_shapes
: *
value	B :*
dtype0
ę
5optimize/gradients/nce_loss/nce_loss/Slice_grad/stackPack4optimize/gradients/nce_loss/nce_loss/Slice_grad/Rank7optimize/gradients/nce_loss/nce_loss/Slice_grad/stack/1*
T0*

axis *
N*
_output_shapes
:
Ļ
7optimize/gradients/nce_loss/nce_loss/Slice_grad/ReshapeReshapence_loss/nce_loss/Slice/begin5optimize/gradients/nce_loss/nce_loss/Slice_grad/stack*
T0*
Tshape0*
_output_shapes

:

7optimize/gradients/nce_loss/nce_loss/Slice_grad/Shape_1Const*
valueB"Ą   Č   *
dtype0*
_output_shapes
:
Ļ
3optimize/gradients/nce_loss/nce_loss/Slice_grad/subSub7optimize/gradients/nce_loss/nce_loss/Slice_grad/Shape_15optimize/gradients/nce_loss/nce_loss/Slice_grad/Shape*
T0*
_output_shapes
:
µ
5optimize/gradients/nce_loss/nce_loss/Slice_grad/sub_1Sub3optimize/gradients/nce_loss/nce_loss/Slice_grad/subnce_loss/nce_loss/Slice/begin*
_output_shapes
:*
T0
é
9optimize/gradients/nce_loss/nce_loss/Slice_grad/Reshape_1Reshape5optimize/gradients/nce_loss/nce_loss/Slice_grad/sub_15optimize/gradients/nce_loss/nce_loss/Slice_grad/stack*
T0*
Tshape0*
_output_shapes

:
}
;optimize/gradients/nce_loss/nce_loss/Slice_grad/concat/axisConst*
value	B :*
dtype0*
_output_shapes
: 
±
6optimize/gradients/nce_loss/nce_loss/Slice_grad/concatConcatV27optimize/gradients/nce_loss/nce_loss/Slice_grad/Reshape9optimize/gradients/nce_loss/nce_loss/Slice_grad/Reshape_1;optimize/gradients/nce_loss/nce_loss/Slice_grad/concat/axis*
T0*
N*
_output_shapes

:*

Tidx0
ė
3optimize/gradients/nce_loss/nce_loss/Slice_grad/PadPad;optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/Reshape6optimize/gradients/nce_loss/nce_loss/Slice_grad/concat*
	Tpaddings0* 
_output_shapes
:
ĄČ*
T0

optimize/gradients/AddN_3AddN5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Pad3optimize/gradients/nce_loss/nce_loss/Slice_grad/Pad*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Pad*
N* 
_output_shapes
:
ĄČ*
T0
¹
@optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ShapeConst*
_class
loc:@nce_weights*%
valueB	"'      Č       *
dtype0	*
_output_shapes
:
ą
Boptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ToInt32Cast@optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Shape*

SrcT0	*
_class
loc:@nce_weights*
_output_shapes
:*

DstT0

?optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/SizeConst*
value
B :Ą*
dtype0*
_output_shapes
: 

Ioptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims/dimConst*
value	B : *
dtype0*
_output_shapes
: 

Eoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims
ExpandDims?optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/SizeIoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims/dim*
_output_shapes
:*

Tdim0*
T0

Noptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stackConst*
valueB:*
dtype0*
_output_shapes
:

Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_1Const*
_output_shapes
:*
valueB: *
dtype0

Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
¦
Hoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_sliceStridedSliceBoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ToInt32Noptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stackPoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_1Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_2*
T0*
Index0*
shrink_axis_mask *
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask*
_output_shapes
:

Foptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concat/axisConst*
_output_shapes
: *
value	B : *
dtype0
ą
Aoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concatConcatV2Eoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDimsHoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_sliceFoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concat/axis*

Tidx0*
T0*
N*
_output_shapes
:
ä
Boptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ReshapeReshapeoptimize/gradients/AddN_3Aoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concat*
T0*
Tshape0* 
_output_shapes
:
ĄČ
ä
Doptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Reshape_1Reshapence_loss/nce_loss/concatEoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims*
T0	*
Tshape0*
_output_shapes	
:Ą
k
&optimize/GradientDescent/learning_rateConst*
valueB
 *ĶĢĢ=*
dtype0*
_output_shapes
: 
ķ
/optimize/GradientDescent/update_nce_weights/mulMulBoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Reshape&optimize/GradientDescent/learning_rate* 
_output_shapes
:
ĄČ*
T0*
_class
loc:@nce_weights
¶
6optimize/GradientDescent/update_nce_weights/ScatterSub
ScatterSubnce_weightsDoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Reshape_1/optimize/GradientDescent/update_nce_weights/mul* 
_output_shapes
:
NČ*
use_locking( *
Tindices0	*
T0*
_class
loc:@nce_weights
ä
,optimize/GradientDescent/update_nce_bias/mulMulDoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Reshape&optimize/GradientDescent/learning_rate*
T0*
_class
loc:@nce_bias*
_output_shapes	
:Ą
§
3optimize/GradientDescent/update_nce_bias/ScatterSub
ScatterSubnce_biasFoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Reshape_1,optimize/GradientDescent/update_nce_bias/mul*
use_locking( *
Tindices0	*
T0*
_class
loc:@nce_bias*
_output_shapes	
:N
č
0optimize/GradientDescent/update_embeddings_1/mulMul;optimize/gradients/embeddings/embedding_lookup_grad/Reshape&optimize/GradientDescent/learning_rate*
T0*
_class
loc:@embeddings_1* 
_output_shapes
:
Č
³
7optimize/GradientDescent/update_embeddings_1/ScatterSub
ScatterSubembeddings_1=optimize/gradients/embeddings/embedding_lookup_grad/Reshape_10optimize/GradientDescent/update_embeddings_1/mul* 
_output_shapes
:
NČ*
use_locking( *
Tindices0*
T0*
_class
loc:@embeddings_1
Š
optimize/GradientDescent/updateNoOp8^optimize/GradientDescent/update_embeddings_1/ScatterSub4^optimize/GradientDescent/update_nce_bias/ScatterSub7^optimize/GradientDescent/update_nce_weights/ScatterSub
¢
optimize/GradientDescent/valueConst ^optimize/GradientDescent/update*
dtype0*
_output_shapes
: *
_class
loc:@global_step*
value	B :
¦
optimize/GradientDescent	AssignAddglobal_stepoptimize/GradientDescent/value*
use_locking( *
T0*
_class
loc:@global_step*
_output_shapes
: 
b
summaries/loss/tagsConst*
valueB Bsummaries/loss*
dtype0*
_output_shapes
: 
j
summaries/lossScalarSummarysummaries/loss/tagsnce_loss/nce_loss_1*
_output_shapes
: *
T0
u
summaries/histogram_loss/tagConst*
dtype0*
_output_shapes
: *)
value B Bsummaries/histogram_loss

summaries/histogram_lossHistogramSummarysummaries/histogram_loss/tagnce_loss/nce_loss_1*
T0*
_output_shapes
: 
w
summaries/Merge/MergeSummaryMergeSummarysummaries/losssummaries/histogram_loss*
N*
_output_shapes
: 
P

save/ConstConst*
valueB Bmodel*
dtype0*
_output_shapes
: 

save/SaveV2/tensor_namesConst*
dtype0*
_output_shapes
:*E
value<B:Bembeddings_1Bglobal_stepBnce_biasBnce_weights
k
save/SaveV2/shape_and_slicesConst*
_output_shapes
:*
valueBB B B B *
dtype0

save/SaveV2SaveV2
save/Constsave/SaveV2/tensor_namessave/SaveV2/shape_and_slicesembeddings_1global_stepnce_biasnce_weights*
dtypes
2
}
save/control_dependencyIdentity
save/Const^save/SaveV2*
T0*
_class
loc:@save/Const*
_output_shapes
: 
£
save/RestoreV2/tensor_namesConst"/device:CPU:0*
dtype0*
_output_shapes
:*E
value<B:Bembeddings_1Bglobal_stepBnce_biasBnce_weights
}
save/RestoreV2/shape_and_slicesConst"/device:CPU:0*
valueBB B B B *
dtype0*
_output_shapes
:
®
save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices"/device:CPU:0*
dtypes
2*$
_output_shapes
::::
Ø
save/AssignAssignembeddings_1save/RestoreV2*
use_locking(*
T0*
_class
loc:@embeddings_1*
validate_shape(* 
_output_shapes
:
NČ
 
save/Assign_1Assignglobal_stepsave/RestoreV2:1*
use_locking(*
T0*
_class
loc:@global_step*
validate_shape(*
_output_shapes
: 

save/Assign_2Assignnce_biassave/RestoreV2:2*
use_locking(*
T0*
_class
loc:@nce_bias*
validate_shape(*
_output_shapes	
:N
Ŗ
save/Assign_3Assignnce_weightssave/RestoreV2:3*
T0*
_class
loc:@nce_weights*
validate_shape(* 
_output_shapes
:
NČ*
use_locking(
V
save/restore_allNoOp^save/Assign^save/Assign_1^save/Assign_2^save/Assign_3
^
initNoOp^embeddings_1/Assign^global_step/Assign^nce_bias/Assign^nce_weights/AssignÕ
½
ź
tf_init_func_O9ko4JKUVjc
arg0
pyfunc_placeholder
pyfunc_placeholder_1
pyfunc_placeholder_2
pyfunc_placeholder_3
pyfunc_placeholder_4

pyfunc	25A wrapper for Defun that facilitates shape inference.²
PyFuncPyFuncpyfunc_placeholderpyfunc_placeholder_1pyfunc_placeholder_2pyfunc_placeholder_3pyfunc_placeholder_4*
token
pyfunc_0*
Tin	
2*
Tout
2	"
pyfuncPyFunc:output:0
ų
¹
tf_map_func_8pkeaS83Jbc
arg0 
generatordataset_placeholder"
generatordataset_placeholder_1
generatordataset25A wrapper for Defun that facilitates shape inference.
GeneratorDatasetGeneratorDatasetarg0generatordataset_placeholdergeneratordataset_placeholder_1*1
finalize_func R
tf_finalize_func_371o9XzBGzc*)
	init_funcR
tf_init_func_ab9wLUDUcC8*
output_types
2*
Tinit_func_args
2*)
	next_funcR
tf_next_func_I9alSJeqyO0*
Tnext_func_args
 *
Tfinalize_func_args
 *1
output_shapes 
:’’’’’’’’’:’’’’’’’’’"-
generatordatasetGeneratorDataset:handle:0
ż
x
tf_next_func_I9alSJeqyO0
arg0	

pyfunc
pyfunc_025A wrapper for Defun that facilitates shape inference.I
PyFuncPyFuncarg0*
token
pyfunc_4*
Tin
2	*
Tout
2"
pyfunc_0PyFunc:output:1"
pyfuncPyFunc:output:0
»
„
tf_map_func_PCOijuOn9mg
arg0 
generatordataset_placeholder"
generatordataset_placeholder_1"
generatordataset_placeholder_2"
generatordataset_placeholder_3"
generatordataset_placeholder_4
generatordataset25A wrapper for Defun that facilitates shape inference.į
GeneratorDatasetGeneratorDatasetarg0generatordataset_placeholdergeneratordataset_placeholder_1generatordataset_placeholder_2generatordataset_placeholder_3generatordataset_placeholder_4*
Tfinalize_func_args
 *%
output_shapes
::	*1
finalize_func R
tf_finalize_func_Lpot2pAvNXE*)
	init_funcR
tf_init_func_O9ko4JKUVjc*
output_types
2*
Tinit_func_args

2*
Tnext_func_args
 *)
	next_funcR
tf_next_func_udg8Vh0kgto"-
generatordatasetGeneratorDataset:handle:0
Õ
n
tf_finalize_func_Lpot2pAvNXE
arg0	

pyfunc	25A wrapper for Defun that facilitates shape inference.H
PyFuncPyFuncarg0*
Tin
2	*
Tout
2	*
token
pyfunc_2"
pyfuncPyFunc:output:0
Õ
n
tf_finalize_func_371o9XzBGzc
arg0	

pyfunc	25A wrapper for Defun that facilitates shape inference.H
PyFuncPyFuncarg0*
Tin
2	*
Tout
2	*
token
pyfunc_5"
pyfuncPyFunc:output:0
ż
x
tf_next_func_udg8Vh0kgto
arg0	

pyfunc
pyfunc_025A wrapper for Defun that facilitates shape inference.I
PyFuncPyFuncarg0*
Tin
2	*
Tout
2*
token
pyfunc_1"
pyfunc_0PyFunc:output:1"
pyfuncPyFunc:output:0
©

tf_init_func_ab9wLUDUcC8
arg0
pyfunc_placeholder
pyfunc_placeholder_1

pyfunc	25A wrapper for Defun that facilitates shape inference.m
PyFuncPyFuncpyfunc_placeholderpyfunc_placeholder_1*
Tin
2*
Tout
2	*
token
pyfunc_3"
pyfuncPyFunc:output:0"*1©¤W     óź/m	3ĒĻ»µ×AJÆ
'ņ&
:
Add
x"T
y"T
z"T"
Ttype:
2	
W
AddN
inputs"T*N
sum"T"
Nint(0"!
Ttype:
2	
x
Assign
ref"T

value"T

output_ref"T"	
Ttype"
validate_shapebool("
use_lockingbool(
s
	AssignAdd
ref"T

value"T

output_ref"T" 
Ttype:
2	"
use_lockingbool( 
R
BroadcastGradientArgs
s0"T
s1"T
r0"T
r1"T"
Ttype0:
2	
8
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype
I
ConcatOffset

concat_dim
shape*N
offset*N"
Nint(0
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
,
Exp
x"T
y"T"
Ttype:

2
W

ExpandDims

input"T
dim"Tdim
output"T"	
Ttype"
Tdimtype0:
2	
^
Fill
dims"
index_type

value"T
output"T"	
Ttype"

index_typetype0:
2	
·
FlatMapDataset
input_dataset
other_arguments2
Targuments

handle"	
ffunc"

Targuments
list(type)("
output_types
list(type)(0" 
output_shapeslist(shape)(0
9
FloorMod
x"T
y"T
z"T"
Ttype:

2	

GatherV2
params"Tparams
indices"Tindices
axis"Taxis
output"Tparams"
Tparamstype"
Tindicestype:
2	"
Taxistype:
2	
B
GreaterEqual
x"T
y"T
z
"
Ttype:
2	
V
HistogramSummary
tag
values"T
summary"
Ttype0:
2	
.
Identity

input"T
output"T"	
Ttype

Iterator

handle"
shared_namestring"
	containerstring"
output_types
list(type)(0" 
output_shapeslist(shape)(0

IteratorGetNext
iterator

components2output_types"
output_types
list(type)(0" 
output_shapeslist(shape)(0
C
IteratorToStringHandle
resource_handle
string_handle
,
Log
x"T
y"T"
Ttype:

2
.
Log1p
x"T
y"T"
Ttype:

2
ō
LogUniformCandidateSampler
true_classes	
sampled_candidates	
true_expected_count
sampled_expected_count"
num_trueint(0"
num_sampledint(0"
uniquebool"
	range_maxint(0"
seedint "
seed2int 
,
MakeIterator
dataset
iterator
p
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:
	2

Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
8
MergeSummary
inputs*N
summary"
Nint(0
=
Mul
x"T
y"T
z"T"
Ttype:
2	
.
Neg
x"T
y"T"
Ttype:

2	

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
_
Pad

input"T
paddings"	Tpaddings
output"T"	
Ttype"
	Tpaddingstype0:
2	
~
RandomUniform

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	
>
RealDiv
x"T
y"T
z"T"
Ttype:
2	
5

Reciprocal
x"T
y"T"
Ttype:

2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
P
ScalarSummary
tags
values"T
summary"
Ttype:
2	
„

ScatterSub
ref"T
indices"Tindices
updates"T

output_ref"T" 
Ttype:
2	"
Tindicestype:
2	"
use_lockingbool( 
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
a
Slice

input"T
begin"Index
size"Index
output"T"	
Ttype"
Indextype:
2	
2
StopGradient

input"T
output"T"	
Ttype
ö
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
:
Sub
x"T
y"T
z"T"
Ttype:
2	

Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
~
TensorDataset

components2Toutput_types

handle"
Toutput_types
list(type)(0" 
output_shapeslist(shape)(0
c
Tile

input"T
	multiples"
Tmultiples
output"T"	
Ttype"

Tmultiplestype0:
2	

TruncatedNormal

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	
s

VariableV2
ref"dtype"
shapeshape"
dtypetype"
	containerstring "
shared_namestring *1.9.02
b'unknown'Š
I
args_0Const*
_output_shapes
: *
value
B :N*
dtype0
I
args_1Const*
value
B :*
dtype0*
_output_shapes
: 
H
args_2Const*
value	B :*
dtype0*
_output_shapes
: 
T
args_3Const*
valueB Bvisualization*
dtype0*
_output_shapes
: 
I
args_4Const*
dtype0*
_output_shapes
: *
value
B BJD
U
tensors/component_0Const*
value	B : *
dtype0*
_output_shapes
: 
L

batch_sizeConst*
value	B	 R*
dtype0	*
_output_shapes
: 
K
args_0_1Const*
value
B BJD*
dtype0*
_output_shapes
: 
J
args_1_1Const*
value	B :*
dtype0*
_output_shapes
: 
W
tensors_1/component_0Const*
dtype0*
_output_shapes
: *
value	B : 
G
ConstConst*
value	B : *
dtype0*
_output_shapes
: 
o
global_step
VariableV2*
shared_name *
dtype0*
_output_shapes
: *
	container *
shape: 

global_step/AssignAssignglobal_stepConst*
use_locking(*
T0*
_class
loc:@global_step*
validate_shape(*
_output_shapes
: 
j
global_step/readIdentityglobal_step*
_class
loc:@global_step*
_output_shapes
: *
T0

data/IteratorIterator*
_output_shapes
: *
	container *
output_types
2*
shared_name *%
output_shapes
::	
”
data/TensorDatasetTensorDatasettensors/component_0* 
_class
loc:@data/Iterator*
Toutput_types
2*
_output_shapes
: *
output_shapes
: 

data/FlatMapDatasetFlatMapDatasetdata/TensorDatasetargs_0args_1args_2args_3args_4* 
fR
tf_map_func_PCOijuOn9mg*
output_types
2*

Targuments	
2*
_output_shapes
: *%
output_shapes
::	* 
_class
loc:@data/Iterator
g
data/MakeIteratorMakeIteratordata/FlatMapDatasetdata/Iterator* 
_class
loc:@data/Iterator
\
data/IteratorToStringHandleIteratorToStringHandledata/Iterator*
_output_shapes
: 
¢
data/Iterator_1Iterator*
shared_name *1
output_shapes 
:’’’’’’’’’:’’’’’’’’’*
_output_shapes
: *
	container *
output_types
2
§
data/TensorDataset_1TensorDatasettensors_1/component_0*
_output_shapes
: *
output_shapes
: *"
_class
loc:@data/Iterator_1*
Toutput_types
2

data/FlatMapDataset_1FlatMapDatasetdata/TensorDataset_1args_0_1args_1_1*

Targuments
2*
_output_shapes
: *1
output_shapes 
:’’’’’’’’’:’’’’’’’’’*"
_class
loc:@data/Iterator_1* 
fR
tf_map_func_8pkeaS83Jbc*
output_types
2
o
data/MakeIterator_1MakeIteratordata/FlatMapDataset_1data/Iterator_1*"
_class
loc:@data/Iterator_1
`
data/IteratorToStringHandle_1IteratorToStringHandledata/Iterator_1*
_output_shapes
: 

data/IteratorGetNextIteratorGetNextdata/Iterator*
output_types
2*%
output_shapes
::	*&
_output_shapes
::	
¹
data/IteratorGetNext_1IteratorGetNextdata/Iterator_1*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’*
output_types
2*1
output_shapes 
:’’’’’’’’’:’’’’’’’’’

.nce_weights/Initializer/truncated_normal/shapeConst*
dtype0*
_output_shapes
:*
_class
loc:@nce_weights*
valueB"'  Č   

-nce_weights/Initializer/truncated_normal/meanConst*
_output_shapes
: *
_class
loc:@nce_weights*
valueB
 *    *
dtype0

/nce_weights/Initializer/truncated_normal/stddevConst*
_class
loc:@nce_weights*
valueB
 *ĆŠ=*
dtype0*
_output_shapes
: 
ģ
8nce_weights/Initializer/truncated_normal/TruncatedNormalTruncatedNormal.nce_weights/Initializer/truncated_normal/shape*
dtype0* 
_output_shapes
:
NČ*

seed *
T0*
_class
loc:@nce_weights*
seed2 
é
,nce_weights/Initializer/truncated_normal/mulMul8nce_weights/Initializer/truncated_normal/TruncatedNormal/nce_weights/Initializer/truncated_normal/stddev*
_class
loc:@nce_weights* 
_output_shapes
:
NČ*
T0
×
(nce_weights/Initializer/truncated_normalAdd,nce_weights/Initializer/truncated_normal/mul-nce_weights/Initializer/truncated_normal/mean*
T0*
_class
loc:@nce_weights* 
_output_shapes
:
NČ
£
nce_weights
VariableV2* 
_output_shapes
:
NČ*
shared_name *
_class
loc:@nce_weights*
	container *
shape:
NČ*
dtype0
Ē
nce_weights/AssignAssignnce_weights(nce_weights/Initializer/truncated_normal* 
_output_shapes
:
NČ*
use_locking(*
T0*
_class
loc:@nce_weights*
validate_shape(
t
nce_weights/readIdentitynce_weights*
T0*
_class
loc:@nce_weights* 
_output_shapes
:
NČ
h
weights/zeros/shape_as_tensorConst*
valueB:N*
dtype0*
_output_shapes
:
X
weights/zeros/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 

weights/zerosFillweights/zeros/shape_as_tensorweights/zeros/Const*
_output_shapes	
:N*
T0*

index_type0
v
nce_bias
VariableV2*
shape:N*
shared_name *
dtype0*
_output_shapes	
:N*
	container 

nce_bias/AssignAssignnce_biasweights/zeros*
_class
loc:@nce_bias*
validate_shape(*
_output_shapes	
:N*
use_locking(*
T0
f
nce_bias/readIdentitynce_bias*
_output_shapes	
:N*
T0*
_class
loc:@nce_bias

-embeddings_1/Initializer/random_uniform/shapeConst*
_class
loc:@embeddings_1*
valueB"'  Č   *
dtype0*
_output_shapes
:

+embeddings_1/Initializer/random_uniform/minConst*
dtype0*
_output_shapes
: *
_class
loc:@embeddings_1*
valueB
 *    

+embeddings_1/Initializer/random_uniform/maxConst*
_class
loc:@embeddings_1*
valueB
 *  ?*
dtype0*
_output_shapes
: 
ē
5embeddings_1/Initializer/random_uniform/RandomUniformRandomUniform-embeddings_1/Initializer/random_uniform/shape* 
_output_shapes
:
NČ*

seed *
T0*
_class
loc:@embeddings_1*
seed2 *
dtype0
Ī
+embeddings_1/Initializer/random_uniform/subSub+embeddings_1/Initializer/random_uniform/max+embeddings_1/Initializer/random_uniform/min*
T0*
_class
loc:@embeddings_1*
_output_shapes
: 
ā
+embeddings_1/Initializer/random_uniform/mulMul5embeddings_1/Initializer/random_uniform/RandomUniform+embeddings_1/Initializer/random_uniform/sub*
_class
loc:@embeddings_1* 
_output_shapes
:
NČ*
T0
Ō
'embeddings_1/Initializer/random_uniformAdd+embeddings_1/Initializer/random_uniform/mul+embeddings_1/Initializer/random_uniform/min*
T0*
_class
loc:@embeddings_1* 
_output_shapes
:
NČ
„
embeddings_1
VariableV2*
shape:
NČ*
dtype0* 
_output_shapes
:
NČ*
shared_name *
_class
loc:@embeddings_1*
	container 
É
embeddings_1/AssignAssignembeddings_1'embeddings_1/Initializer/random_uniform*
use_locking(*
T0*
_class
loc:@embeddings_1*
validate_shape(* 
_output_shapes
:
NČ
w
embeddings_1/readIdentityembeddings_1*
T0*
_class
loc:@embeddings_1* 
_output_shapes
:
NČ

 embeddings/embedding_lookup/axisConst*
_class
loc:@embeddings_1*
value	B : *
dtype0*
_output_shapes
: 
į
embeddings/embedding_lookupGatherV2embeddings_1/readdata/IteratorGetNext embeddings/embedding_lookup/axis*
Tparams0*
_class
loc:@embeddings_1* 
_output_shapes
:
Č*
Taxis0*
Tindices0
~
filling/filling_lookup/axisConst*
_class
loc:@embeddings_1*
value	B : *
dtype0*
_output_shapes
: 
ć
filling/filling_lookupGatherV2embeddings_1/readdata/IteratorGetNext_1:1filling/filling_lookup/axis*
Taxis0*
Tindices0*
Tparams0*
_class
loc:@embeddings_1*(
_output_shapes
:’’’’’’’’’Č
`
filling/Mean/reduction_indicesConst*
_output_shapes
: *
value	B : *
dtype0

filling/MeanMeanfilling/filling_lookupfilling/Mean/reduction_indices*
_output_shapes	
:Č*
	keep_dims( *

Tidx0*
T0
o
nce_loss/nce_loss/CastCastdata/IteratorGetNext:1*

SrcT0*
_output_shapes
:	*

DstT0	
r
nce_loss/nce_loss/Reshape/shapeConst*
_output_shapes
:*
valueB:
’’’’’’’’’*
dtype0

nce_loss/nce_loss/ReshapeReshapence_loss/nce_loss/Castnce_loss/nce_loss/Reshape/shape*
T0	*
Tshape0*
_output_shapes	
:
ė
,nce_loss/nce_loss/LogUniformCandidateSamplerLogUniformCandidateSamplernce_loss/nce_loss/Cast*
unique(*
seed2 *
num_true*
	range_maxN*
num_sampled@*+
_output_shapes
:@:	:@*

seed 

nce_loss/nce_loss/StopGradientStopGradient,nce_loss/nce_loss/LogUniformCandidateSampler*
T0	*
_output_shapes
:@

 nce_loss/nce_loss/StopGradient_1StopGradient.nce_loss/nce_loss/LogUniformCandidateSampler:1*
T0*
_output_shapes
:	

 nce_loss/nce_loss/StopGradient_2StopGradient.nce_loss/nce_loss/LogUniformCandidateSampler:2*
T0*
_output_shapes
:@
_
nce_loss/nce_loss/concat/axisConst*
dtype0*
_output_shapes
: *
value	B : 
¹
nce_loss/nce_loss/concatConcatV2nce_loss/nce_loss/Reshapence_loss/nce_loss/StopGradientnce_loss/nce_loss/concat/axis*
T0	*
N*
_output_shapes	
:Ą*

Tidx0

'nce_loss/nce_loss/embedding_lookup/axisConst*
dtype0*
_output_shapes
: *
_class
loc:@nce_weights*
value	B : 
ń
"nce_loss/nce_loss/embedding_lookupGatherV2nce_weights/readnce_loss/nce_loss/concat'nce_loss/nce_loss/embedding_lookup/axis*
Taxis0*
Tindices0	*
Tparams0*
_class
loc:@nce_weights* 
_output_shapes
:
ĄČ
b
nce_loss/nce_loss/ShapeConst*
dtype0*
_output_shapes
:*
valueB:
o
%nce_loss/nce_loss/strided_slice/stackConst*
valueB: *
dtype0*
_output_shapes
:
q
'nce_loss/nce_loss/strided_slice/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
q
'nce_loss/nce_loss/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
Ó
nce_loss/nce_loss/strided_sliceStridedSlicence_loss/nce_loss/Shape%nce_loss/nce_loss/strided_slice/stack'nce_loss/nce_loss/strided_slice/stack_1'nce_loss/nce_loss/strided_slice/stack_2*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
Index0*
T0
d
nce_loss/nce_loss/stack/1Const*
valueB :
’’’’’’’’’*
dtype0*
_output_shapes
: 

nce_loss/nce_loss/stackPacknce_loss/nce_loss/strided_slicence_loss/nce_loss/stack/1*

axis *
N*
_output_shapes
:*
T0
n
nce_loss/nce_loss/Slice/beginConst*
_output_shapes
:*
valueB"        *
dtype0
Ä
nce_loss/nce_loss/SliceSlice"nce_loss/nce_loss/embedding_lookupnce_loss/nce_loss/Slice/beginnce_loss/nce_loss/stack*
Index0*
T0*0
_output_shapes
:’’’’’’’’’’’’’’’’’’
d
nce_loss/nce_loss/Shape_1Const*
valueB:*
dtype0*
_output_shapes
:
q
'nce_loss/nce_loss/strided_slice_1/stackConst*
dtype0*
_output_shapes
:*
valueB: 
s
)nce_loss/nce_loss/strided_slice_1/stack_1Const*
_output_shapes
:*
valueB:*
dtype0
s
)nce_loss/nce_loss/strided_slice_1/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
Ż
!nce_loss/nce_loss/strided_slice_1StridedSlicence_loss/nce_loss/Shape_1'nce_loss/nce_loss/strided_slice_1/stack)nce_loss/nce_loss/strided_slice_1/stack_1)nce_loss/nce_loss/strided_slice_1/stack_2*
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0*
shrink_axis_mask*

begin_mask *
ellipsis_mask 
]
nce_loss/nce_loss/stack_1/1Const*
value	B : *
dtype0*
_output_shapes
: 

nce_loss/nce_loss/stack_1Pack!nce_loss/nce_loss/strided_slice_1nce_loss/nce_loss/stack_1/1*
T0*

axis *
N*
_output_shapes
:
o
nce_loss/nce_loss/Slice_1/sizeConst*
valueB"’’’’’’’’*
dtype0*
_output_shapes
:
Į
nce_loss/nce_loss/Slice_1Slice"nce_loss/nce_loss/embedding_lookupnce_loss/nce_loss/stack_1nce_loss/nce_loss/Slice_1/size*
Index0*
T0*(
_output_shapes
:’’’’’’’’’Č
³
nce_loss/nce_loss/MatMulMatMulembeddings/embedding_lookupnce_loss/nce_loss/Slice_1*
transpose_b(*
T0*(
_output_shapes
:’’’’’’’’’*
transpose_a( 

)nce_loss/nce_loss/embedding_lookup_1/axisConst*
_class
loc:@nce_bias*
value	B : *
dtype0*
_output_shapes
: 
ź
$nce_loss/nce_loss/embedding_lookup_1GatherV2nce_bias/readnce_loss/nce_loss/concat)nce_loss/nce_loss/embedding_lookup_1/axis*
Tindices0	*
Tparams0*
_class
loc:@nce_bias*
_output_shapes	
:Ą*
Taxis0
d
nce_loss/nce_loss/Shape_2Const*
valueB:*
dtype0*
_output_shapes
:
i
nce_loss/nce_loss/Slice_2/beginConst*
valueB: *
dtype0*
_output_shapes
:
·
nce_loss/nce_loss/Slice_2Slice$nce_loss/nce_loss/embedding_lookup_1nce_loss/nce_loss/Slice_2/beginnce_loss/nce_loss/Shape_2*
_output_shapes	
:*
Index0*
T0
d
nce_loss/nce_loss/Shape_3Const*
valueB:*
dtype0*
_output_shapes
:
q
nce_loss/nce_loss/Slice_3/sizeConst*
valueB:
’’’’’’’’’*
dtype0*
_output_shapes
:
µ
nce_loss/nce_loss/Slice_3Slice$nce_loss/nce_loss/embedding_lookup_1nce_loss/nce_loss/Shape_3nce_loss/nce_loss/Slice_3/size*
Index0*
T0*
_output_shapes
:@
p
nce_loss/nce_loss/Shape_4Shapence_loss/nce_loss/Slice*
out_type0*
_output_shapes
:*
T0
q
'nce_loss/nce_loss/strided_slice_2/stackConst*
valueB:*
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_2/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_2/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
į
!nce_loss/nce_loss/strided_slice_2StridedSlicence_loss/nce_loss/Shape_4'nce_loss/nce_loss/strided_slice_2/stack)nce_loss/nce_loss/strided_slice_2/stack_1)nce_loss/nce_loss/strided_slice_2/stack_2*
T0*
Index0*
shrink_axis_mask *
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
_output_shapes
:
t
#nce_loss/nce_loss/concat_1/values_0Const*
dtype0*
_output_shapes
:*
valueB"’’’’   
a
nce_loss/nce_loss/concat_1/axisConst*
_output_shapes
: *
value	B : *
dtype0
É
nce_loss/nce_loss/concat_1ConcatV2#nce_loss/nce_loss/concat_1/values_0!nce_loss/nce_loss/strided_slice_2nce_loss/nce_loss/concat_1/axis*
T0*
N*
_output_shapes
:*

Tidx0
b
 nce_loss/nce_loss/ExpandDims/dimConst*
value	B :*
dtype0*
_output_shapes
: 
¤
nce_loss/nce_loss/ExpandDims
ExpandDimsembeddings/embedding_lookup nce_loss/nce_loss/ExpandDims/dim*$
_output_shapes
:Č*

Tdim0*
T0
Ø
nce_loss/nce_loss/Reshape_1Reshapence_loss/nce_loss/Slicence_loss/nce_loss/concat_1*
T0*
Tshape0*4
_output_shapes"
 :’’’’’’’’’’’’’’’’’’

nce_loss/nce_loss/MulMulnce_loss/nce_loss/ExpandDimsnce_loss/nce_loss/Reshape_1*$
_output_shapes
:Č*
T0
v
#nce_loss/nce_loss/concat_2/values_0Const*
valueB:
’’’’’’’’’*
dtype0*
_output_shapes
:
a
nce_loss/nce_loss/concat_2/axisConst*
dtype0*
_output_shapes
: *
value	B : 
É
nce_loss/nce_loss/concat_2ConcatV2#nce_loss/nce_loss/concat_2/values_0!nce_loss/nce_loss/strided_slice_2nce_loss/nce_loss/concat_2/axis*
T0*
N*
_output_shapes
:*

Tidx0
¢
nce_loss/nce_loss/Reshape_2Reshapence_loss/nce_loss/Mulnce_loss/nce_loss/concat_2*0
_output_shapes
:’’’’’’’’’’’’’’’’’’*
T0*
Tshape0
t
nce_loss/nce_loss/Shape_5Shapence_loss/nce_loss/Reshape_2*
out_type0*
_output_shapes
:*
T0
q
'nce_loss/nce_loss/strided_slice_3/stackConst*
valueB:*
dtype0*
_output_shapes
:
s
)nce_loss/nce_loss/strided_slice_3/stack_1Const*
dtype0*
_output_shapes
:*
valueB:
s
)nce_loss/nce_loss/strided_slice_3/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
Ż
!nce_loss/nce_loss/strided_slice_3StridedSlicence_loss/nce_loss/Shape_5'nce_loss/nce_loss/strided_slice_3/stack)nce_loss/nce_loss/strided_slice_3/stack_1)nce_loss/nce_loss/strided_slice_3/stack_2*
shrink_axis_mask*
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
Index0*
T0
]
nce_loss/nce_loss/stack_2/1Const*
value	B :*
dtype0*
_output_shapes
: 

nce_loss/nce_loss/stack_2Pack!nce_loss/nce_loss/strided_slice_3nce_loss/nce_loss/stack_2/1*
N*
_output_shapes
:*
T0*

axis 
a
nce_loss/nce_loss/ones/ConstConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 

nce_loss/nce_loss/onesFillnce_loss/nce_loss/stack_2nce_loss/nce_loss/ones/Const*

index_type0*'
_output_shapes
:’’’’’’’’’*
T0
±
nce_loss/nce_loss/MatMul_1MatMulnce_loss/nce_loss/Reshape_2nce_loss/nce_loss/ones*
T0*'
_output_shapes
:’’’’’’’’’*
transpose_a( *
transpose_b( 
t
!nce_loss/nce_loss/Reshape_3/shapeConst*
valueB:
’’’’’’’’’*
dtype0*
_output_shapes
:
”
nce_loss/nce_loss/Reshape_3Reshapence_loss/nce_loss/MatMul_1!nce_loss/nce_loss/Reshape_3/shape*#
_output_shapes
:’’’’’’’’’*
T0*
Tshape0
r
!nce_loss/nce_loss/Reshape_4/shapeConst*
dtype0*
_output_shapes
:*
valueB"’’’’   
¦
nce_loss/nce_loss/Reshape_4Reshapence_loss/nce_loss/Reshape_3!nce_loss/nce_loss/Reshape_4/shape*
T0*
Tshape0*'
_output_shapes
:’’’’’’’’’
r
!nce_loss/nce_loss/Reshape_5/shapeConst*
dtype0*
_output_shapes
:*
valueB"’’’’   

nce_loss/nce_loss/Reshape_5Reshapence_loss/nce_loss/Slice_2!nce_loss/nce_loss/Reshape_5/shape*
T0*
Tshape0*
_output_shapes
:	

nce_loss/nce_loss/addAddnce_loss/nce_loss/Reshape_4nce_loss/nce_loss/Reshape_5*
_output_shapes
:	*
T0
}
nce_loss/nce_loss/add_1Addnce_loss/nce_loss/MatMulnce_loss/nce_loss/Slice_3*
T0*
_output_shapes
:	@
h
nce_loss/nce_loss/LogLog nce_loss/nce_loss/StopGradient_1*
T0*
_output_shapes
:	
t
nce_loss/nce_loss/subSubnce_loss/nce_loss/addnce_loss/nce_loss/Log*
_output_shapes
:	*
T0
e
nce_loss/nce_loss/Log_1Log nce_loss/nce_loss/StopGradient_2*
_output_shapes
:@*
T0
z
nce_loss/nce_loss/sub_1Subnce_loss/nce_loss/add_1nce_loss/nce_loss/Log_1*
T0*
_output_shapes
:	@
a
nce_loss/nce_loss/concat_3/axisConst*
value	B :*
dtype0*
_output_shapes
: 
¶
nce_loss/nce_loss/concat_3ConcatV2nce_loss/nce_loss/subnce_loss/nce_loss/sub_1nce_loss/nce_loss/concat_3/axis*
N*
_output_shapes
:	A*

Tidx0*
T0
r
!nce_loss/nce_loss/ones_like/ShapeConst*
_output_shapes
:*
valueB"      *
dtype0
f
!nce_loss/nce_loss/ones_like/ConstConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
„
nce_loss/nce_loss/ones_likeFill!nce_loss/nce_loss/ones_like/Shape!nce_loss/nce_loss/ones_like/Const*
T0*

index_type0*
_output_shapes
:	
`
nce_loss/nce_loss/truediv/yConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 

nce_loss/nce_loss/truedivRealDivnce_loss/nce_loss/ones_likence_loss/nce_loss/truediv/y*
T0*
_output_shapes
:	
}
,nce_loss/nce_loss/zeros_like/shape_as_tensorConst*
valueB"   @   *
dtype0*
_output_shapes
:
g
"nce_loss/nce_loss/zeros_like/ConstConst*
_output_shapes
: *
valueB
 *    *
dtype0
²
nce_loss/nce_loss/zeros_likeFill,nce_loss/nce_loss/zeros_like/shape_as_tensor"nce_loss/nce_loss/zeros_like/Const*
_output_shapes
:	@*
T0*

index_type0
a
nce_loss/nce_loss/concat_4/axisConst*
value	B :*
dtype0*
_output_shapes
: 
æ
nce_loss/nce_loss/concat_4ConcatV2nce_loss/nce_loss/truedivnce_loss/nce_loss/zeros_likence_loss/nce_loss/concat_4/axis*
T0*
N*
_output_shapes
:	A*

Tidx0

2nce_loss/sampled_losses/zeros_like/shape_as_tensorConst*
valueB"   A   *
dtype0*
_output_shapes
:
m
(nce_loss/sampled_losses/zeros_like/ConstConst*
_output_shapes
: *
valueB
 *    *
dtype0
Ä
"nce_loss/sampled_losses/zeros_likeFill2nce_loss/sampled_losses/zeros_like/shape_as_tensor(nce_loss/sampled_losses/zeros_like/Const*
T0*

index_type0*
_output_shapes
:	A

$nce_loss/sampled_losses/GreaterEqualGreaterEqualnce_loss/nce_loss/concat_3"nce_loss/sampled_losses/zeros_like*
_output_shapes
:	A*
T0
ø
nce_loss/sampled_losses/SelectSelect$nce_loss/sampled_losses/GreaterEqualnce_loss/nce_loss/concat_3"nce_loss/sampled_losses/zeros_like*
T0*
_output_shapes
:	A
h
nce_loss/sampled_losses/NegNegnce_loss/nce_loss/concat_3*
T0*
_output_shapes
:	A
³
 nce_loss/sampled_losses/Select_1Select$nce_loss/sampled_losses/GreaterEqualnce_loss/sampled_losses/Negnce_loss/nce_loss/concat_3*
_output_shapes
:	A*
T0

nce_loss/sampled_losses/mulMulnce_loss/nce_loss/concat_3nce_loss/nce_loss/concat_4*
T0*
_output_shapes
:	A

nce_loss/sampled_losses/subSubnce_loss/sampled_losses/Selectnce_loss/sampled_losses/mul*
_output_shapes
:	A*
T0
n
nce_loss/sampled_losses/ExpExp nce_loss/sampled_losses/Select_1*
T0*
_output_shapes
:	A
m
nce_loss/sampled_losses/Log1pLog1pnce_loss/sampled_losses/Exp*
T0*
_output_shapes
:	A

nce_loss/sampled_lossesAddnce_loss/sampled_losses/subnce_loss/sampled_losses/Log1p*
T0*
_output_shapes
:	A
_
nce_loss/ShapeConst*
valueB"   A   *
dtype0*
_output_shapes
:
f
nce_loss/strided_slice/stackConst*
valueB:*
dtype0*
_output_shapes
:
h
nce_loss/strided_slice/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
h
nce_loss/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
¦
nce_loss/strided_sliceStridedSlicence_loss/Shapence_loss/strided_slice/stacknce_loss/strided_slice/stack_1nce_loss/strided_slice/stack_2*
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
Index0*
T0*
shrink_axis_mask
R
nce_loss/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
z
nce_loss/stackPacknce_loss/strided_slicence_loss/stack/1*
T0*

axis *
N*
_output_shapes
:
X
nce_loss/ones/ConstConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
~
nce_loss/onesFillnce_loss/stacknce_loss/ones/Const*'
_output_shapes
:’’’’’’’’’*
T0*

index_type0

nce_loss/MatMulMatMulnce_loss/sampled_lossesnce_loss/ones*
_output_shapes
:	*
transpose_a( *
transpose_b( *
T0
i
nce_loss/Reshape/shapeConst*
valueB:
’’’’’’’’’*
dtype0*
_output_shapes
:
x
nce_loss/ReshapeReshapence_loss/MatMulnce_loss/Reshape/shape*
Tshape0*
_output_shapes	
:*
T0
X
nce_loss/ConstConst*
_output_shapes
:*
valueB: *
dtype0
{
nce_loss/nce_loss_1Meannce_loss/Reshapence_loss/Const*
_output_shapes
: *
	keep_dims( *

Tidx0*
T0
[
optimize/gradients/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
a
optimize/gradients/grad_ys_0Const*
valueB
 *  ?*
dtype0*
_output_shapes
: 

optimize/gradients/FillFilloptimize/gradients/Shapeoptimize/gradients/grad_ys_0*
_output_shapes
: *
T0*

index_type0

9optimize/gradients/nce_loss/nce_loss_1_grad/Reshape/shapeConst*
_output_shapes
:*
valueB:*
dtype0
Å
3optimize/gradients/nce_loss/nce_loss_1_grad/ReshapeReshapeoptimize/gradients/Fill9optimize/gradients/nce_loss/nce_loss_1_grad/Reshape/shape*
_output_shapes
:*
T0*
Tshape0
|
1optimize/gradients/nce_loss/nce_loss_1_grad/ConstConst*
valueB:*
dtype0*
_output_shapes
:
Ų
0optimize/gradients/nce_loss/nce_loss_1_grad/TileTile3optimize/gradients/nce_loss/nce_loss_1_grad/Reshape1optimize/gradients/nce_loss/nce_loss_1_grad/Const*

Tmultiples0*
T0*
_output_shapes	
:
x
3optimize/gradients/nce_loss/nce_loss_1_grad/Const_1Const*
dtype0*
_output_shapes
: *
valueB
 *   C
Ė
3optimize/gradients/nce_loss/nce_loss_1_grad/truedivRealDiv0optimize/gradients/nce_loss/nce_loss_1_grad/Tile3optimize/gradients/nce_loss/nce_loss_1_grad/Const_1*
T0*
_output_shapes	
:

.optimize/gradients/nce_loss/Reshape_grad/ShapeConst*
valueB"      *
dtype0*
_output_shapes
:
Ų
0optimize/gradients/nce_loss/Reshape_grad/ReshapeReshape3optimize/gradients/nce_loss/nce_loss_1_grad/truediv.optimize/gradients/nce_loss/Reshape_grad/Shape*
_output_shapes
:	*
T0*
Tshape0
Ņ
.optimize/gradients/nce_loss/MatMul_grad/MatMulMatMul0optimize/gradients/nce_loss/Reshape_grad/Reshapence_loss/ones*(
_output_shapes
:’’’’’’’’’*
transpose_a( *
transpose_b(*
T0
Ō
0optimize/gradients/nce_loss/MatMul_grad/MatMul_1MatMulnce_loss/sampled_losses0optimize/gradients/nce_loss/Reshape_grad/Reshape*
T0*
_output_shapes

:A*
transpose_a(*
transpose_b( 
¤
8optimize/gradients/nce_loss/MatMul_grad/tuple/group_depsNoOp/^optimize/gradients/nce_loss/MatMul_grad/MatMul1^optimize/gradients/nce_loss/MatMul_grad/MatMul_1
¤
@optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependencyIdentity.optimize/gradients/nce_loss/MatMul_grad/MatMul9^optimize/gradients/nce_loss/MatMul_grad/tuple/group_deps*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul*
_output_shapes
:	A
©
Boptimize/gradients/nce_loss/MatMul_grad/tuple/control_dependency_1Identity0optimize/gradients/nce_loss/MatMul_grad/MatMul_19^optimize/gradients/nce_loss/MatMul_grad/tuple/group_deps*
_output_shapes

:A*
T0*C
_class9
75loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul_1

@optimize/gradients/nce_loss/sampled_losses_grad/tuple/group_depsNoOpA^optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependency
Ę
Hoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependencyIdentity@optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependencyA^optimize/gradients/nce_loss/sampled_losses_grad/tuple/group_deps*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul*
_output_shapes
:	A
Č
Joptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency_1Identity@optimize/gradients/nce_loss/MatMul_grad/tuple/control_dependencyA^optimize/gradients/nce_loss/sampled_losses_grad/tuple/group_deps*
T0*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul*
_output_shapes
:	A
²
7optimize/gradients/nce_loss/sampled_losses/sub_grad/NegNegHoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency*
T0*
_output_shapes
:	A
Ń
Doptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/sampled_losses/sub_grad/NegI^optimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency
Ö
Loptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependencyIdentityHoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependencyE^optimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/group_deps*A
_class7
53loc:@optimize/gradients/nce_loss/MatMul_grad/MatMul*
_output_shapes
:	A*
T0
Š
Noptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency_1Identity7optimize/gradients/nce_loss/sampled_losses/sub_grad/NegE^optimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/sampled_losses/sub_grad/Neg*
_output_shapes
:	A
Ķ
;optimize/gradients/nce_loss/sampled_losses/Log1p_grad/add/xConstK^optimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency_1*
_output_shapes
: *
valueB
 *  ?*
dtype0
Ä
9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/addAdd;optimize/gradients/nce_loss/sampled_losses/Log1p_grad/add/xnce_loss/sampled_losses/Exp*
T0*
_output_shapes
:	A
³
@optimize/gradients/nce_loss/sampled_losses/Log1p_grad/Reciprocal
Reciprocal9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/add*
_output_shapes
:	A*
T0
ų
9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/mulMulJoptimize/gradients/nce_loss/sampled_losses_grad/tuple/control_dependency_1@optimize/gradients/nce_loss/sampled_losses/Log1p_grad/Reciprocal*
_output_shapes
:	A*
T0
¢
Qoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/shape_as_tensorConst*
valueB"   A   *
dtype0*
_output_shapes
:

Goptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
”
Aoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_likeFillQoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/shape_as_tensorGoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like/Const*
_output_shapes
:	A*
T0*

index_type0
Ø
=optimize/gradients/nce_loss/sampled_losses/Select_grad/SelectSelect$nce_loss/sampled_losses/GreaterEqualLoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependencyAoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_like*
T0*
_output_shapes
:	A
Ŗ
?optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1Select$nce_loss/sampled_losses/GreaterEqualAoptimize/gradients/nce_loss/sampled_losses/Select_grad/zeros_likeLoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency*
T0*
_output_shapes
:	A
Ń
Goptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/group_depsNoOp>^optimize/gradients/nce_loss/sampled_losses/Select_grad/Select@^optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1
ą
Ooptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/control_dependencyIdentity=optimize/gradients/nce_loss/sampled_losses/Select_grad/SelectH^optimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/group_deps*
T0*P
_classF
DBloc:@optimize/gradients/nce_loss/sampled_losses/Select_grad/Select*
_output_shapes
:	A
ę
Qoptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/control_dependency_1Identity?optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1H^optimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/group_deps*
_output_shapes
:	A*
T0*R
_classH
FDloc:@optimize/gradients/nce_loss/sampled_losses/Select_grad/Select_1
Ō
7optimize/gradients/nce_loss/sampled_losses/mul_grad/MulMulNoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency_1nce_loss/nce_loss/concat_4*
T0*
_output_shapes
:	A
Ö
9optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1MulNoptimize/gradients/nce_loss/sampled_losses/sub_grad/tuple/control_dependency_1nce_loss/nce_loss/concat_3*
T0*
_output_shapes
:	A
Ā
Doptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul:^optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1
Ī
Loptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/sampled_losses/mul_grad/MulE^optimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul*
_output_shapes
:	A
Ō
Noptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1E^optimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/sampled_losses/mul_grad/Mul_1*
_output_shapes
:	A
Ą
7optimize/gradients/nce_loss/sampled_losses/Exp_grad/mulMul9optimize/gradients/nce_loss/sampled_losses/Log1p_grad/mulnce_loss/sampled_losses/Exp*
T0*
_output_shapes
:	A
¤
Soptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB"   A   

Ioptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
§
Coptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_likeFillSoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/shape_as_tensorIoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like/Const*
T0*

index_type0*
_output_shapes
:	A

?optimize/gradients/nce_loss/sampled_losses/Select_1_grad/SelectSelect$nce_loss/sampled_losses/GreaterEqual7optimize/gradients/nce_loss/sampled_losses/Exp_grad/mulCoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like*
T0*
_output_shapes
:	A

Aoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1Select$nce_loss/sampled_losses/GreaterEqualCoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/zeros_like7optimize/gradients/nce_loss/sampled_losses/Exp_grad/mul*
T0*
_output_shapes
:	A
×
Ioptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/group_depsNoOp@^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/SelectB^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1
č
Qoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependencyIdentity?optimize/gradients/nce_loss/sampled_losses/Select_1_grad/SelectJ^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/group_deps*
T0*R
_classH
FDloc:@optimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select*
_output_shapes
:	A
ī
Soptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependency_1IdentityAoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1J^optimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@optimize/gradients/nce_loss/sampled_losses/Select_1_grad/Select_1*
_output_shapes
:	A
»
7optimize/gradients/nce_loss/sampled_losses/Neg_grad/NegNegQoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependency*
T0*
_output_shapes
:	A
Ń
optimize/gradients/AddNAddNOoptimize/gradients/nce_loss/sampled_losses/Select_grad/tuple/control_dependencyLoptimize/gradients/nce_loss/sampled_losses/mul_grad/tuple/control_dependencySoptimize/gradients/nce_loss/sampled_losses/Select_1_grad/tuple/control_dependency_17optimize/gradients/nce_loss/sampled_losses/Neg_grad/Neg*
T0*P
_classF
DBloc:@optimize/gradients/nce_loss/sampled_losses/Select_grad/Select*
N*
_output_shapes
:	A
y
7optimize/gradients/nce_loss/nce_loss/concat_3_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 
½
6optimize/gradients/nce_loss/nce_loss/concat_3_grad/modFloorModnce_loss/nce_loss/concat_3/axis7optimize/gradients/nce_loss/nce_loss/concat_3_grad/Rank*
T0*
_output_shapes
: 

8optimize/gradients/nce_loss/nce_loss/concat_3_grad/ShapeConst*
_output_shapes
:*
valueB"      *
dtype0

:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape_1Const*
_output_shapes
:*
valueB"   @   *
dtype0
Ø
?optimize/gradients/nce_loss/nce_loss/concat_3_grad/ConcatOffsetConcatOffset6optimize/gradients/nce_loss/nce_loss/concat_3_grad/mod8optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape_1*
N* 
_output_shapes
::

8optimize/gradients/nce_loss/nce_loss/concat_3_grad/SliceSliceoptimize/gradients/AddN?optimize/gradients/nce_loss/nce_loss/concat_3_grad/ConcatOffset8optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape*
_output_shapes
:	*
Index0*
T0

:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1Sliceoptimize/gradients/AddNAoptimize/gradients/nce_loss/nce_loss/concat_3_grad/ConcatOffset:1:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Shape_1*
Index0*
T0*
_output_shapes
:	@
Ć
Coptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/group_depsNoOp9^optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice;^optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1
Ī
Koptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependencyIdentity8optimize/gradients/nce_loss/nce_loss/concat_3_grad/SliceD^optimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/group_deps*
T0*K
_classA
?=loc:@optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice*
_output_shapes
:	
Ō
Moptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency_1Identity:optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1D^optimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/group_deps*M
_classC
A?loc:@optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice_1*
_output_shapes
:	@*
T0
Æ
1optimize/gradients/nce_loss/nce_loss/sub_grad/NegNegKoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency*
T0*
_output_shapes
:	
Č
>optimize/gradients/nce_loss/nce_loss/sub_grad/tuple/group_depsNoOpL^optimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency2^optimize/gradients/nce_loss/nce_loss/sub_grad/Neg
×
Foptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependencyIdentityKoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency?^optimize/gradients/nce_loss/nce_loss/sub_grad/tuple/group_deps*K
_classA
?=loc:@optimize/gradients/nce_loss/nce_loss/concat_3_grad/Slice*
_output_shapes
:	*
T0
ø
Hoptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependency_1Identity1optimize/gradients/nce_loss/nce_loss/sub_grad/Neg?^optimize/gradients/nce_loss/nce_loss/sub_grad/tuple/group_deps*D
_class:
86loc:@optimize/gradients/nce_loss/nce_loss/sub_grad/Neg*
_output_shapes
:	*
T0

5optimize/gradients/nce_loss/nce_loss/sub_1_grad/ShapeConst*
valueB"   @   *
dtype0*
_output_shapes
:

7optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape_1Const*
_output_shapes
:*
valueB:@*
dtype0

Eoptimize/gradients/nce_loss/nce_loss/sub_1_grad/BroadcastGradientArgsBroadcastGradientArgs5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape7optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape_1*
T0*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’

3optimize/gradients/nce_loss/nce_loss/sub_1_grad/SumSumMoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency_1Eoptimize/gradients/nce_loss/nce_loss/sub_1_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*
	keep_dims( *

Tidx0
ę
7optimize/gradients/nce_loss/nce_loss/sub_1_grad/ReshapeReshape3optimize/gradients/nce_loss/nce_loss/sub_1_grad/Sum5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape*
T0*
Tshape0*
_output_shapes
:	@

5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Sum_1SumMoptimize/gradients/nce_loss/nce_loss/concat_3_grad/tuple/control_dependency_1Goptimize/gradients/nce_loss/nce_loss/sub_1_grad/BroadcastGradientArgs:1*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0

3optimize/gradients/nce_loss/nce_loss/sub_1_grad/NegNeg5optimize/gradients/nce_loss/nce_loss/sub_1_grad/Sum_1*
T0*
_output_shapes
:
å
9optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1Reshape3optimize/gradients/nce_loss/nce_loss/sub_1_grad/Neg7optimize/gradients/nce_loss/nce_loss/sub_1_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:@
¾
@optimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape:^optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1
Ę
Hoptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/nce_loss/sub_1_grad/ReshapeA^optimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/group_deps*
_output_shapes
:	@*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape
Ē
Joptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1A^optimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/group_deps*
_output_shapes
:@*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/sub_1_grad/Reshape_1

3optimize/gradients/nce_loss/nce_loss/add_grad/ShapeShapence_loss/nce_loss/Reshape_4*
_output_shapes
:*
T0*
out_type0

5optimize/gradients/nce_loss/nce_loss/add_grad/Shape_1Const*
valueB"      *
dtype0*
_output_shapes
:

Coptimize/gradients/nce_loss/nce_loss/add_grad/BroadcastGradientArgsBroadcastGradientArgs3optimize/gradients/nce_loss/nce_loss/add_grad/Shape5optimize/gradients/nce_loss/nce_loss/add_grad/Shape_1*
T0*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’

1optimize/gradients/nce_loss/nce_loss/add_grad/SumSumFoptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependencyCoptimize/gradients/nce_loss/nce_loss/add_grad/BroadcastGradientArgs*
	keep_dims( *

Tidx0*
T0*
_output_shapes
:
č
5optimize/gradients/nce_loss/nce_loss/add_grad/ReshapeReshape1optimize/gradients/nce_loss/nce_loss/add_grad/Sum3optimize/gradients/nce_loss/nce_loss/add_grad/Shape*
T0*
Tshape0*'
_output_shapes
:’’’’’’’’’

3optimize/gradients/nce_loss/nce_loss/add_grad/Sum_1SumFoptimize/gradients/nce_loss/nce_loss/sub_grad/tuple/control_dependencyEoptimize/gradients/nce_loss/nce_loss/add_grad/BroadcastGradientArgs:1*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
ę
7optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1Reshape3optimize/gradients/nce_loss/nce_loss/add_grad/Sum_15optimize/gradients/nce_loss/nce_loss/add_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:	
ø
>optimize/gradients/nce_loss/nce_loss/add_grad/tuple/group_depsNoOp6^optimize/gradients/nce_loss/nce_loss/add_grad/Reshape8^optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1
Ę
Foptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependencyIdentity5optimize/gradients/nce_loss/nce_loss/add_grad/Reshape?^optimize/gradients/nce_loss/nce_loss/add_grad/tuple/group_deps*
T0*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/add_grad/Reshape*'
_output_shapes
:’’’’’’’’’
Ä
Hoptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependency_1Identity7optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1?^optimize/gradients/nce_loss/nce_loss/add_grad/tuple/group_deps*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/add_grad/Reshape_1*
_output_shapes
:	*
T0

5optimize/gradients/nce_loss/nce_loss/add_1_grad/ShapeShapence_loss/nce_loss/MatMul*
out_type0*
_output_shapes
:*
T0

7optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape_1Const*
valueB:@*
dtype0*
_output_shapes
:

Eoptimize/gradients/nce_loss/nce_loss/add_1_grad/BroadcastGradientArgsBroadcastGradientArgs5optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape7optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape_1*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’*
T0

3optimize/gradients/nce_loss/nce_loss/add_1_grad/SumSumHoptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependencyEoptimize/gradients/nce_loss/nce_loss/add_1_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*
	keep_dims( *

Tidx0
ļ
7optimize/gradients/nce_loss/nce_loss/add_1_grad/ReshapeReshape3optimize/gradients/nce_loss/nce_loss/add_1_grad/Sum5optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape*(
_output_shapes
:’’’’’’’’’*
T0*
Tshape0

5optimize/gradients/nce_loss/nce_loss/add_1_grad/Sum_1SumHoptimize/gradients/nce_loss/nce_loss/sub_1_grad/tuple/control_dependencyGoptimize/gradients/nce_loss/nce_loss/add_1_grad/BroadcastGradientArgs:1*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
ē
9optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1Reshape5optimize/gradients/nce_loss/nce_loss/add_1_grad/Sum_17optimize/gradients/nce_loss/nce_loss/add_1_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:@
¾
@optimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape:^optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1
Ļ
Hoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/nce_loss/add_1_grad/ReshapeA^optimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape*(
_output_shapes
:’’’’’’’’’
Ē
Joptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1A^optimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/group_deps*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/add_1_grad/Reshape_1*
_output_shapes
:@

9optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/ShapeShapence_loss/nce_loss/Reshape_3*
T0*
out_type0*
_output_shapes
:

;optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/ReshapeReshapeFoptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependency9optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/Shape*
T0*
Tshape0*#
_output_shapes
:’’’’’’’’’

9optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/ShapeConst*
valueB:*
dtype0*
_output_shapes
:
’
;optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/ReshapeReshapeHoptimize/gradients/nce_loss/nce_loss/add_grad/tuple/control_dependency_19optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/Shape*
T0*
Tshape0*
_output_shapes	
:
÷
7optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMulMatMulHoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependencynce_loss/nce_loss/Slice_1*
T0* 
_output_shapes
:
Č*
transpose_a( *
transpose_b( 

9optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1MatMulHoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependencyembeddings/embedding_lookup*
transpose_b( *
T0*(
_output_shapes
:’’’’’’’’’Č*
transpose_a(
æ
Aoptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/group_depsNoOp8^optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul:^optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1
É
Ioptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependencyIdentity7optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMulB^optimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/group_deps*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul* 
_output_shapes
:
Č
×
Koptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependency_1Identity9optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1B^optimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul_1*(
_output_shapes
:’’’’’’’’’Č
x
6optimize/gradients/nce_loss/nce_loss/Slice_3_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 

7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/ShapeConst*
valueB:@*
dtype0*
_output_shapes
:
{
9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack/1Const*
dtype0*
_output_shapes
: *
value	B :
ģ
7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stackPack6optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Rank9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack/1*
T0*

axis *
N*
_output_shapes
:
Ļ
9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/ReshapeReshapence_loss/nce_loss/Shape_37optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack*
T0*
Tshape0*
_output_shapes

:

9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Shape_1Const*
valueB:Ą*
dtype0*
_output_shapes
:
Õ
5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/subSub9optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Shape_17optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Shape*
_output_shapes
:*
T0
µ
7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/sub_1Sub5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/subnce_loss/nce_loss/Shape_3*
T0*
_output_shapes
:
ļ
;optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Reshape_1Reshape7optimize/gradients/nce_loss/nce_loss/Slice_3_grad/sub_17optimize/gradients/nce_loss/nce_loss/Slice_3_grad/stack*
T0*
Tshape0*
_output_shapes

:

=optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concat/axisConst*
dtype0*
_output_shapes
: *
value	B :
¹
8optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concatConcatV29optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Reshape;optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Reshape_1=optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concat/axis*
T0*
N*
_output_shapes

:*

Tidx0
ł
5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/PadPadJoptimize/gradients/nce_loss/nce_loss/add_1_grad/tuple/control_dependency_18optimize/gradients/nce_loss/nce_loss/Slice_3_grad/concat*
_output_shapes	
:Ą*
T0*
	Tpaddings0

9optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/ShapeShapence_loss/nce_loss/MatMul_1*
_output_shapes
:*
T0*
out_type0
ž
;optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/ReshapeReshape;optimize/gradients/nce_loss/nce_loss/Reshape_4_grad/Reshape9optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/Shape*
T0*
Tshape0*'
_output_shapes
:’’’’’’’’’
x
6optimize/gradients/nce_loss/nce_loss/Slice_2_grad/RankConst*
dtype0*
_output_shapes
: *
value	B :

7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/ShapeConst*
valueB:*
dtype0*
_output_shapes
:
{
9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
ģ
7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stackPack6optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Rank9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack/1*
T0*

axis *
N*
_output_shapes
:
Õ
9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/ReshapeReshapence_loss/nce_loss/Slice_2/begin7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack*
_output_shapes

:*
T0*
Tshape0

9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Shape_1Const*
valueB:Ą*
dtype0*
_output_shapes
:
Õ
5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/subSub9optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Shape_17optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Shape*
_output_shapes
:*
T0
»
7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/sub_1Sub5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/subnce_loss/nce_loss/Slice_2/begin*
T0*
_output_shapes
:
ļ
;optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Reshape_1Reshape7optimize/gradients/nce_loss/nce_loss/Slice_2_grad/sub_17optimize/gradients/nce_loss/nce_loss/Slice_2_grad/stack*
_output_shapes

:*
T0*
Tshape0

=optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concat/axisConst*
_output_shapes
: *
value	B :*
dtype0
¹
8optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concatConcatV29optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Reshape;optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Reshape_1=optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concat/axis*
_output_shapes

:*

Tidx0*
T0*
N
ź
5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/PadPad;optimize/gradients/nce_loss/nce_loss/Reshape_5_grad/Reshape8optimize/gradients/nce_loss/nce_loss/Slice_2_grad/concat*
_output_shapes	
:Ą*
T0*
	Tpaddings0
x
6optimize/gradients/nce_loss/nce_loss/Slice_1_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 

7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/ShapeShapence_loss/nce_loss/Slice_1*
T0*
out_type0*
_output_shapes
:
{
9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack/1Const*
value	B :*
dtype0*
_output_shapes
: 
ģ
7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stackPack6optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Rank9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack/1*
T0*

axis *
N*
_output_shapes
:
Ļ
9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/ReshapeReshapence_loss/nce_loss/stack_17optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack*
T0*
Tshape0*
_output_shapes

:

9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Shape_1Const*
valueB"Ą   Č   *
dtype0*
_output_shapes
:
Õ
5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/subSub9optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Shape_17optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Shape*
T0*
_output_shapes
:
µ
7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/sub_1Sub5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/subnce_loss/nce_loss/stack_1*
_output_shapes
:*
T0
ļ
;optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Reshape_1Reshape7optimize/gradients/nce_loss/nce_loss/Slice_1_grad/sub_17optimize/gradients/nce_loss/nce_loss/Slice_1_grad/stack*
Tshape0*
_output_shapes

:*
T0

=optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concat/axisConst*
value	B :*
dtype0*
_output_shapes
: 
¹
8optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concatConcatV29optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Reshape;optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Reshape_1=optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concat/axis*
T0*
N*
_output_shapes

:*

Tidx0
’
5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/PadPadKoptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependency_18optimize/gradients/nce_loss/nce_loss/Slice_1_grad/concat*
T0*
	Tpaddings0* 
_output_shapes
:
ĄČ
ł
9optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMulMatMul;optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/Reshapence_loss/nce_loss/ones*
T0*0
_output_shapes
:’’’’’’’’’’’’’’’’’’*
transpose_a( *
transpose_b(
÷
;optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1MatMulnce_loss/nce_loss/Reshape_2;optimize/gradients/nce_loss/nce_loss/Reshape_3_grad/Reshape*
transpose_b( *
T0*'
_output_shapes
:’’’’’’’’’*
transpose_a(
Å
Coptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/group_depsNoOp:^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul<^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1
į
Koptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/control_dependencyIdentity9optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMulD^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/group_deps*
T0*L
_classB
@>loc:@optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul*0
_output_shapes
:’’’’’’’’’’’’’’’’’’
Ž
Moptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/control_dependency_1Identity;optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1D^optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/group_deps*'
_output_shapes
:’’’’’’’’’*
T0*N
_classD
B@loc:@optimize/gradients/nce_loss/nce_loss/MatMul_1_grad/MatMul_1

optimize/gradients/AddN_1AddN5optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Pad5optimize/gradients/nce_loss/nce_loss/Slice_2_grad/Pad*
T0*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/Slice_3_grad/Pad*
N*
_output_shapes	
:Ą
Ŗ
Boptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ShapeConst*
dtype0	*
_output_shapes
:*
_class
loc:@nce_bias*
valueB	RN
į
Doptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ToInt32CastBoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Shape*

SrcT0	*
_class
loc:@nce_bias*
_output_shapes
:*

DstT0

Aoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/SizeConst*
_output_shapes
: *
value
B :Ą*
dtype0

Koptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims/dimConst*
value	B : *
dtype0*
_output_shapes
: 

Goptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims
ExpandDimsAoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/SizeKoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims/dim*
_output_shapes
:*

Tdim0*
T0

Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stackConst*
valueB:*
dtype0*
_output_shapes
:

Roptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_1Const*
_output_shapes
:*
valueB: *
dtype0

Roptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
®
Joptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_sliceStridedSliceDoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ToInt32Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stackRoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_1Roptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_slice/stack_2*
shrink_axis_mask *
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask*
_output_shapes
: *
T0*
Index0

Hoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
č
Coptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concatConcatV2Goptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDimsJoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/strided_sliceHoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concat/axis*
T0*
N*
_output_shapes
:*

Tidx0
ć
Doptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ReshapeReshapeoptimize/gradients/AddN_1Coptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/concat*
T0*
Tshape0*
_output_shapes	
:Ą
č
Foptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Reshape_1Reshapence_loss/nce_loss/concatGoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/ExpandDims*
Tshape0*
_output_shapes	
:Ą*
T0	

9optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/ShapeConst*
_output_shapes
:*!
valueB"      Č   *
dtype0

;optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/ReshapeReshapeKoptimize/gradients/nce_loss/nce_loss/MatMul_1_grad/tuple/control_dependency9optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/Shape*
Tshape0*$
_output_shapes
:Č*
T0

3optimize/gradients/nce_loss/nce_loss/Mul_grad/ShapeConst*!
valueB"      Č   *
dtype0*
_output_shapes
:

5optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape_1Shapence_loss/nce_loss/Reshape_1*
T0*
out_type0*
_output_shapes
:

Coptimize/gradients/nce_loss/nce_loss/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs3optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape5optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape_1*
T0*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’
Į
1optimize/gradients/nce_loss/nce_loss/Mul_grad/MulMul;optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/Reshapence_loss/nce_loss/Reshape_1*
T0*$
_output_shapes
:Č
š
1optimize/gradients/nce_loss/nce_loss/Mul_grad/SumSum1optimize/gradients/nce_loss/nce_loss/Mul_grad/MulCoptimize/gradients/nce_loss/nce_loss/Mul_grad/BroadcastGradientArgs*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
å
5optimize/gradients/nce_loss/nce_loss/Mul_grad/ReshapeReshape1optimize/gradients/nce_loss/nce_loss/Mul_grad/Sum3optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape*
T0*
Tshape0*$
_output_shapes
:Č
Ä
3optimize/gradients/nce_loss/nce_loss/Mul_grad/Mul_1Mulnce_loss/nce_loss/ExpandDims;optimize/gradients/nce_loss/nce_loss/Reshape_2_grad/Reshape*
T0*$
_output_shapes
:Č
ö
3optimize/gradients/nce_loss/nce_loss/Mul_grad/Sum_1Sum3optimize/gradients/nce_loss/nce_loss/Mul_grad/Mul_1Eoptimize/gradients/nce_loss/nce_loss/Mul_grad/BroadcastGradientArgs:1*
_output_shapes
:*
	keep_dims( *

Tidx0*
T0
ū
7optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1Reshape3optimize/gradients/nce_loss/nce_loss/Mul_grad/Sum_15optimize/gradients/nce_loss/nce_loss/Mul_grad/Shape_1*4
_output_shapes"
 :’’’’’’’’’’’’’’’’’’*
T0*
Tshape0
ø
>optimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/group_depsNoOp6^optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape8^optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1
Ć
Foptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependencyIdentity5optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape?^optimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/group_deps*$
_output_shapes
:Č*
T0*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape
Ł
Hoptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependency_1Identity7optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1?^optimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/group_deps*4
_output_shapes"
 :’’’’’’’’’’’’’’’’’’*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/Mul_grad/Reshape_1

:optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/ShapeConst*
valueB"   Č   *
dtype0*
_output_shapes
:

<optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/ReshapeReshapeFoptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependency:optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/Shape*
T0*
Tshape0* 
_output_shapes
:
Č

9optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/ShapeShapence_loss/nce_loss/Slice*
T0*
out_type0*
_output_shapes
:

;optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/ReshapeReshapeHoptimize/gradients/nce_loss/nce_loss/Mul_grad/tuple/control_dependency_19optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/Shape*
T0*
Tshape0*0
_output_shapes
:’’’’’’’’’’’’’’’’’’
Ŗ
optimize/gradients/AddN_2AddNIoptimize/gradients/nce_loss/nce_loss/MatMul_grad/tuple/control_dependency<optimize/gradients/nce_loss/nce_loss/ExpandDims_grad/Reshape*
T0*J
_class@
><loc:@optimize/gradients/nce_loss/nce_loss/MatMul_grad/MatMul*
N* 
_output_shapes
:
Č
³
9optimize/gradients/embeddings/embedding_lookup_grad/ShapeConst*
_class
loc:@embeddings_1*%
valueB	"'      Č       *
dtype0	*
_output_shapes
:
Ó
;optimize/gradients/embeddings/embedding_lookup_grad/ToInt32Cast9optimize/gradients/embeddings/embedding_lookup_grad/Shape*

SrcT0	*
_class
loc:@embeddings_1*
_output_shapes
:*

DstT0
{
8optimize/gradients/embeddings/embedding_lookup_grad/SizeConst*
value
B :*
dtype0*
_output_shapes
: 

Boptimize/gradients/embeddings/embedding_lookup_grad/ExpandDims/dimConst*
dtype0*
_output_shapes
: *
value	B : 
ū
>optimize/gradients/embeddings/embedding_lookup_grad/ExpandDims
ExpandDims8optimize/gradients/embeddings/embedding_lookup_grad/SizeBoptimize/gradients/embeddings/embedding_lookup_grad/ExpandDims/dim*

Tdim0*
T0*
_output_shapes
:

Goptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stackConst*
dtype0*
_output_shapes
:*
valueB:

Ioptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_1Const*
valueB: *
dtype0*
_output_shapes
:

Ioptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:

Aoptimize/gradients/embeddings/embedding_lookup_grad/strided_sliceStridedSlice;optimize/gradients/embeddings/embedding_lookup_grad/ToInt32Goptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stackIoptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_1Ioptimize/gradients/embeddings/embedding_lookup_grad/strided_slice/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask*
_output_shapes
:

?optimize/gradients/embeddings/embedding_lookup_grad/concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 
Ä
:optimize/gradients/embeddings/embedding_lookup_grad/concatConcatV2>optimize/gradients/embeddings/embedding_lookup_grad/ExpandDimsAoptimize/gradients/embeddings/embedding_lookup_grad/strided_slice?optimize/gradients/embeddings/embedding_lookup_grad/concat/axis*
T0*
N*
_output_shapes
:*

Tidx0
Ö
;optimize/gradients/embeddings/embedding_lookup_grad/ReshapeReshapeoptimize/gradients/AddN_2:optimize/gradients/embeddings/embedding_lookup_grad/concat*
T0*
Tshape0* 
_output_shapes
:
Č
Ņ
=optimize/gradients/embeddings/embedding_lookup_grad/Reshape_1Reshapedata/IteratorGetNext>optimize/gradients/embeddings/embedding_lookup_grad/ExpandDims*
T0*
Tshape0*
_output_shapes	
:
v
4optimize/gradients/nce_loss/nce_loss/Slice_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 

5optimize/gradients/nce_loss/nce_loss/Slice_grad/ShapeShapence_loss/nce_loss/Slice*
T0*
out_type0*
_output_shapes
:
y
7optimize/gradients/nce_loss/nce_loss/Slice_grad/stack/1Const*
dtype0*
_output_shapes
: *
value	B :
ę
5optimize/gradients/nce_loss/nce_loss/Slice_grad/stackPack4optimize/gradients/nce_loss/nce_loss/Slice_grad/Rank7optimize/gradients/nce_loss/nce_loss/Slice_grad/stack/1*
_output_shapes
:*
T0*

axis *
N
Ļ
7optimize/gradients/nce_loss/nce_loss/Slice_grad/ReshapeReshapence_loss/nce_loss/Slice/begin5optimize/gradients/nce_loss/nce_loss/Slice_grad/stack*
T0*
Tshape0*
_output_shapes

:

7optimize/gradients/nce_loss/nce_loss/Slice_grad/Shape_1Const*
valueB"Ą   Č   *
dtype0*
_output_shapes
:
Ļ
3optimize/gradients/nce_loss/nce_loss/Slice_grad/subSub7optimize/gradients/nce_loss/nce_loss/Slice_grad/Shape_15optimize/gradients/nce_loss/nce_loss/Slice_grad/Shape*
_output_shapes
:*
T0
µ
5optimize/gradients/nce_loss/nce_loss/Slice_grad/sub_1Sub3optimize/gradients/nce_loss/nce_loss/Slice_grad/subnce_loss/nce_loss/Slice/begin*
T0*
_output_shapes
:
é
9optimize/gradients/nce_loss/nce_loss/Slice_grad/Reshape_1Reshape5optimize/gradients/nce_loss/nce_loss/Slice_grad/sub_15optimize/gradients/nce_loss/nce_loss/Slice_grad/stack*
_output_shapes

:*
T0*
Tshape0
}
;optimize/gradients/nce_loss/nce_loss/Slice_grad/concat/axisConst*
value	B :*
dtype0*
_output_shapes
: 
±
6optimize/gradients/nce_loss/nce_loss/Slice_grad/concatConcatV27optimize/gradients/nce_loss/nce_loss/Slice_grad/Reshape9optimize/gradients/nce_loss/nce_loss/Slice_grad/Reshape_1;optimize/gradients/nce_loss/nce_loss/Slice_grad/concat/axis*

Tidx0*
T0*
N*
_output_shapes

:
ė
3optimize/gradients/nce_loss/nce_loss/Slice_grad/PadPad;optimize/gradients/nce_loss/nce_loss/Reshape_1_grad/Reshape6optimize/gradients/nce_loss/nce_loss/Slice_grad/concat* 
_output_shapes
:
ĄČ*
T0*
	Tpaddings0

optimize/gradients/AddN_3AddN5optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Pad3optimize/gradients/nce_loss/nce_loss/Slice_grad/Pad*H
_class>
<:loc:@optimize/gradients/nce_loss/nce_loss/Slice_1_grad/Pad*
N* 
_output_shapes
:
ĄČ*
T0
¹
@optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ShapeConst*
dtype0	*
_output_shapes
:*
_class
loc:@nce_weights*%
valueB	"'      Č       
ą
Boptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ToInt32Cast@optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Shape*
_class
loc:@nce_weights*
_output_shapes
:*

DstT0*

SrcT0	

?optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/SizeConst*
dtype0*
_output_shapes
: *
value
B :Ą

Ioptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims/dimConst*
value	B : *
dtype0*
_output_shapes
: 

Eoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims
ExpandDims?optimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/SizeIoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims/dim*

Tdim0*
T0*
_output_shapes
:

Noptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stackConst*
valueB:*
dtype0*
_output_shapes
:

Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_1Const*
valueB: *
dtype0*
_output_shapes
:

Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
¦
Hoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_sliceStridedSliceBoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ToInt32Noptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stackPoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_1Poptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_slice/stack_2*
Index0*
T0*
shrink_axis_mask *
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask*
_output_shapes
:

Foptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concat/axisConst*
_output_shapes
: *
value	B : *
dtype0
ą
Aoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concatConcatV2Eoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDimsHoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/strided_sliceFoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concat/axis*
T0*
N*
_output_shapes
:*

Tidx0
ä
Boptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ReshapeReshapeoptimize/gradients/AddN_3Aoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/concat*
T0*
Tshape0* 
_output_shapes
:
ĄČ
ä
Doptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Reshape_1Reshapence_loss/nce_loss/concatEoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/ExpandDims*
T0	*
Tshape0*
_output_shapes	
:Ą
k
&optimize/GradientDescent/learning_rateConst*
dtype0*
_output_shapes
: *
valueB
 *ĶĢĢ=
ķ
/optimize/GradientDescent/update_nce_weights/mulMulBoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Reshape&optimize/GradientDescent/learning_rate*
T0*
_class
loc:@nce_weights* 
_output_shapes
:
ĄČ
¶
6optimize/GradientDescent/update_nce_weights/ScatterSub
ScatterSubnce_weightsDoptimize/gradients/nce_loss/nce_loss/embedding_lookup_grad/Reshape_1/optimize/GradientDescent/update_nce_weights/mul*
use_locking( *
Tindices0	*
T0*
_class
loc:@nce_weights* 
_output_shapes
:
NČ
ä
,optimize/GradientDescent/update_nce_bias/mulMulDoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Reshape&optimize/GradientDescent/learning_rate*
_class
loc:@nce_bias*
_output_shapes	
:Ą*
T0
§
3optimize/GradientDescent/update_nce_bias/ScatterSub
ScatterSubnce_biasFoptimize/gradients/nce_loss/nce_loss/embedding_lookup_1_grad/Reshape_1,optimize/GradientDescent/update_nce_bias/mul*
_output_shapes	
:N*
use_locking( *
Tindices0	*
T0*
_class
loc:@nce_bias
č
0optimize/GradientDescent/update_embeddings_1/mulMul;optimize/gradients/embeddings/embedding_lookup_grad/Reshape&optimize/GradientDescent/learning_rate*
T0*
_class
loc:@embeddings_1* 
_output_shapes
:
Č
³
7optimize/GradientDescent/update_embeddings_1/ScatterSub
ScatterSubembeddings_1=optimize/gradients/embeddings/embedding_lookup_grad/Reshape_10optimize/GradientDescent/update_embeddings_1/mul*
_class
loc:@embeddings_1* 
_output_shapes
:
NČ*
use_locking( *
Tindices0*
T0
Š
optimize/GradientDescent/updateNoOp8^optimize/GradientDescent/update_embeddings_1/ScatterSub4^optimize/GradientDescent/update_nce_bias/ScatterSub7^optimize/GradientDescent/update_nce_weights/ScatterSub
¢
optimize/GradientDescent/valueConst ^optimize/GradientDescent/update*
_class
loc:@global_step*
value	B :*
dtype0*
_output_shapes
: 
¦
optimize/GradientDescent	AssignAddglobal_stepoptimize/GradientDescent/value*
_output_shapes
: *
use_locking( *
T0*
_class
loc:@global_step
b
summaries/loss/tagsConst*
valueB Bsummaries/loss*
dtype0*
_output_shapes
: 
j
summaries/lossScalarSummarysummaries/loss/tagsnce_loss/nce_loss_1*
T0*
_output_shapes
: 
u
summaries/histogram_loss/tagConst*)
value B Bsummaries/histogram_loss*
dtype0*
_output_shapes
: 

summaries/histogram_lossHistogramSummarysummaries/histogram_loss/tagnce_loss/nce_loss_1*
_output_shapes
: *
T0
w
summaries/Merge/MergeSummaryMergeSummarysummaries/losssummaries/histogram_loss*
_output_shapes
: *
N
P

save/ConstConst*
valueB Bmodel*
dtype0*
_output_shapes
: 

save/SaveV2/tensor_namesConst*E
value<B:Bembeddings_1Bglobal_stepBnce_biasBnce_weights*
dtype0*
_output_shapes
:
k
save/SaveV2/shape_and_slicesConst*
valueBB B B B *
dtype0*
_output_shapes
:

save/SaveV2SaveV2
save/Constsave/SaveV2/tensor_namessave/SaveV2/shape_and_slicesembeddings_1global_stepnce_biasnce_weights*
dtypes
2
}
save/control_dependencyIdentity
save/Const^save/SaveV2*
_output_shapes
: *
T0*
_class
loc:@save/Const
£
save/RestoreV2/tensor_namesConst"/device:CPU:0*E
value<B:Bembeddings_1Bglobal_stepBnce_biasBnce_weights*
dtype0*
_output_shapes
:
}
save/RestoreV2/shape_and_slicesConst"/device:CPU:0*
valueBB B B B *
dtype0*
_output_shapes
:
®
save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices"/device:CPU:0*
dtypes
2*$
_output_shapes
::::
Ø
save/AssignAssignembeddings_1save/RestoreV2*
_class
loc:@embeddings_1*
validate_shape(* 
_output_shapes
:
NČ*
use_locking(*
T0
 
save/Assign_1Assignglobal_stepsave/RestoreV2:1*
use_locking(*
T0*
_class
loc:@global_step*
validate_shape(*
_output_shapes
: 

save/Assign_2Assignnce_biassave/RestoreV2:2*
use_locking(*
T0*
_class
loc:@nce_bias*
validate_shape(*
_output_shapes	
:N
Ŗ
save/Assign_3Assignnce_weightssave/RestoreV2:3*
use_locking(*
T0*
_class
loc:@nce_weights*
validate_shape(* 
_output_shapes
:
NČ
V
save/restore_allNoOp^save/Assign^save/Assign_1^save/Assign_2^save/Assign_3
^
initNoOp^embeddings_1/Assign^global_step/Assign^nce_bias/Assign^nce_weights/AssignÕ
½
ź
tf_init_func_O9ko4JKUVjc
arg0
pyfunc_placeholder
pyfunc_placeholder_1
pyfunc_placeholder_2
pyfunc_placeholder_3
pyfunc_placeholder_4

pyfunc	25A wrapper for Defun that facilitates shape inference.²
PyFuncPyFuncpyfunc_placeholderpyfunc_placeholder_1pyfunc_placeholder_2pyfunc_placeholder_3pyfunc_placeholder_4*
Tout
2	*
token
pyfunc_0*
Tin	
2"
pyfuncPyFunc:output:0
ų
¹
tf_map_func_8pkeaS83Jbc
arg0 
generatordataset_placeholder"
generatordataset_placeholder_1
generatordataset25A wrapper for Defun that facilitates shape inference.
GeneratorDatasetGeneratorDatasetarg0generatordataset_placeholdergeneratordataset_placeholder_1*
output_types
2*
Tinit_func_args
2*)
	next_funcR
tf_next_func_I9alSJeqyO0*
Tnext_func_args
 *
Tfinalize_func_args
 *1
output_shapes 
:’’’’’’’’’:’’’’’’’’’*1
finalize_func R
tf_finalize_func_371o9XzBGzc*)
	init_funcR
tf_init_func_ab9wLUDUcC8"-
generatordatasetGeneratorDataset:handle:0
ż
x
tf_next_func_I9alSJeqyO0
arg0	

pyfunc
pyfunc_025A wrapper for Defun that facilitates shape inference.I
PyFuncPyFuncarg0*
Tin
2	*
Tout
2*
token
pyfunc_4"
pyfunc_0PyFunc:output:1"
pyfuncPyFunc:output:0
»
„
tf_map_func_PCOijuOn9mg
arg0 
generatordataset_placeholder"
generatordataset_placeholder_1"
generatordataset_placeholder_2"
generatordataset_placeholder_3"
generatordataset_placeholder_4
generatordataset25A wrapper for Defun that facilitates shape inference.į
GeneratorDatasetGeneratorDatasetarg0generatordataset_placeholdergeneratordataset_placeholder_1generatordataset_placeholder_2generatordataset_placeholder_3generatordataset_placeholder_4*%
output_shapes
::	*1
finalize_func R
tf_finalize_func_Lpot2pAvNXE*)
	init_funcR
tf_init_func_O9ko4JKUVjc*
output_types
2*
Tinit_func_args
