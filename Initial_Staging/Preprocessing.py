from nltk.corpus import stopwords, words
import re, os, enchant, string
from pyspark import SparkContext, SparkConf, SQLContext


"""
Candidate Education - JobApplicationKey, CandidateKey, Institution, Degree

Staging - JobApplicationKey, CandidateKey, ReqKey, total_rounds, rounds_cleared, score

Main_Data - JobApplicationKey, CandidateKey, ReqKey, SrcReferringType, WorkingDays, WorkExperience, CompanyName, City, StateProvince, 
Country, EmploymentTypeDesired, ShiftPreference, RelocationPosibility, ReqKey, ReqTitle, WorkDurationType, GradeLevel,
RequiredEducation, RequiredExperienceYears, Shift, ReqDescription, JobCodeTitle, JobCategory, CountryName, City, State, Country

"""

# TODO: Collect the location of Req and start and end date to get number of years of experience
# TODO: Handle the case when resume is almost similar to or same as given requisitions

all_words = set(w.lower() for w in words.words())
stops = stopwords.words('english')


def check_file_path(path):
    """
    Check if file exist at a given path or not!!
    :param path: path in the string format
    :return: 1 or 0 typically mentioning type of file to process
    """
    if os.path.isfile(path):
        fl = os.path.splitext(path)[-1].lower()
        if fl == '.csv':
            return 1
        else:
            return 0
    else:
        raise ValueError('Path is not directed to a file')


def define_config(master="local", AppName="None"):
    """
    Define the configuration detail for the spark program to work on
    :param master: where to run this driver script
    :param AppName: Name of the app
    :return:
    """
    conf = SparkConf().setMaster(master).setAppName(AppName)
    return SparkContext(conf=conf)


def remove_punctuation(line):
    """
    Remove punctuation from a given line
    :param line: single line to process
    :return: translated line
    """
    translator = str.maketrans(string.punctuation, ' ' * len(string.punctuation))
    return line.translate(translator)  # Removes the punctuation


def correct_wrong_words(line):
    """
    Corrected wrong words present in the text of a doc
    :param line: single line to process
    :return: corrected line
    """
    # for them rather than checking these directly.
    splitted_line = line.split()
    unusual_words = set(splitted_line) - all_words

    # Use the suggestions only when the words are appended together as most of the nouns are not exactly dictionary
    # words.

    d = enchant.Dict("en_US")
    for word in unusual_words:
        suggestion = d.suggest(word)
        line = re.sub(word, word if (len(suggestion) < 1 or len(suggestion[0].split()) < 2) else suggestion[0], line)
    return line


def remove_stopwords(line):
    """
    Remove stopwords from a given line
    :param line: single line to process
    :return: refined line
    """
    lst = []
    for x in line.split():
        if x not in stops:
            lst.append(x)
    return ' '.join(lst)


def pre_process(preprocess=True, mode='Experience', train_mode=True):

    """
    Pre process the data and save the intermediate result according to different text data [Experience vs JD].
    :param preprocess: Wants to preprocess or not
    :param mode: Self explanatory
    :param train_mode: makes the file for training vs prediction
    :return: None
    """

    try:  # Mkdir to save the file
        os.mkdir("../Data/Preprocessed")  # Checkpoints to save intermediate results
    except OSError:
        print('Path Defined!!!')

    if check_file_path(path="../Data/final-data.csv"):
        pass
    else:
        raise ValueError("Error in Reading the input")

    if preprocess:
        sc = define_config(master="local[*]", AppName="Word2Vec")
        sql_context = SQLContext(sc)
        df = sql_context.read.csv('../Data/final-data.csv', header=False)

        if mode == 'Experience':
            # Remove the extra quotes
            experience = df.rdd.map(lambda x: ((x[0], x[1], x[2]), ' '.join([s.strip('\"') for s in x[5].split()])))
            # Reduce by same key to get all the experiences for a person
            exp = experience.reduceByKey(lambda x, y: x + ' ' + y)
            # Lower case the text for each candidate
            lowercase_exp = exp.mapValues(lambda y: y.lower())
            # remove punctuations
            punc_text = lowercase_exp.mapValues(lambda y: remove_punctuation(y))
            # Correct the wrong or appended words
            right_words = punc_text.mapValues(lambda y: correct_wrong_words(y))
            # Remove the stop words
            filtered_words = right_words.mapValues(lambda x: remove_stopwords(x))

            if train_mode:

                # Reduce to a single variable for training in skip-gram
                text = filtered_words.values().distinct().reduce(lambda accum, x: accum + ' ' + x)
                with open("../Data/Preprocessed/Experience.txt", 'w') as f:
                    f.write(text)
            else:
                text = filtered_words.collect()
                with open("../Data/Preprocessed/Experience_predict.txt", 'w') as f:
                    for line in text:
                        f.write(str(line)+'\n')

        elif mode == 'JD':

            # Take the necessary text out from HTML tags
            jd = df.rdd.filter(lambda x: x[20] not in ['(PENDING)', 'tba', 'tbc', 'a', 'na', 'NA', 'N/A', 'a', 'x', 'xx', 'xxxx', '.',
                                                       'see attached', 'More to come', 'not provided', 'to be provided', 'Description - Qualifications',
                                                        'To follow.', 'To Follow', 'insert jd here', 'insert jd here.','insert JD here', 'JD', 'Job Description', 'POE',
                                                        'position description here', 'Need JD', 'test', 'TEST', 'To be Confirmed. Awaiting Spec.',
                                                        'To Come', '<strong>To follow. </strong>', 'unposted', '<p></p><p></p>', 'n/a<br /><br />',
                                                        'Add', 'TBD', '-', '...', '....', '**', '***', 'blah', '', ' ']).map(lambda x: (x[2], re.sub('<[^<]+?>', ' ', x[20])))
            # Lowercase the jd
            lowercase_jd = jd.distinct().mapValues(lambda y: y.lower())
            # Remove punctuation
            punc_text = lowercase_jd.mapValues(lambda y: remove_punctuation(y))
            # Remove bad words
            correct_words = punc_text.mapValues(lambda x: correct_wrong_words(x))
            # Remove the stop words
            filtered_words = correct_words.mapValues(lambda x: remove_stopwords(x))

            if train_mode:
                # Reduce to a single variable for training
                text = filtered_words.values().reduce(lambda accum, x: accum + ' ' + x)
                with open("../Data/Preprocessed/JD.txt", 'w') as f:
                    f.write(text)
            else:
                text = filtered_words.collect()
                with open("../Data/Preprocessed/JD_predict.txt", 'w') as f:
                    for line in text:
                        f.write(str(line)+'\n')


pre_process(mode=None, train_mode=False)