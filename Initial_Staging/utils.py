"""
@author: Abhishek Gupta
-- Connect to Bucket and download the data
"""
import boto3


def define_config(bucket, folder):
    s3 = boto3.resource('s3')
    res = s3.Bucket(bucket)
    return res, list(res.objects.filter(Prefix=folder))


def download_file(bucket, folder):
    res, lst = define_config(bucket, folder)
    for item in lst[1:]:
        string = item.key
        name = string.split('/')[-1]
        res.download_file(string, '../Data/{}'.format(name))


if __name__ == '__main__':
    download_file('ptg-expedia', 'resume-reco/')
