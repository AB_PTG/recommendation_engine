from pyspark import SparkConf, SparkContext, SQLContext
from pyspark.sql.functions import explode
import re, heapq
import numpy as np
import pandas as pd
import warnings

warnings.filterwarnings('ignore')
# TODO: Write sophisticated code to handle all scenarios except full time emp like interns, part-time
# TODO: Write the code for computeSim in numpy matrix format rather than using pandas dataframe to make it faster


def define_config(master="local", name=None):
    conf = SparkConf().setMaster(master).setAppName(name)
    return SparkContext(conf=conf)


def load_companies():
    # Fortunes 1000 companies
    data = pd.read_csv('../Data/fortune1000-final.csv', encoding='iso-8859-1')

    company = dict(zip(data['rank'], data['rank']))
    return company


def load_institutes():
    # Work with Ivy league only - 8 univ + Nearby location university
    institutes = {'brown university': 0, 'columbia university': 1, 'cornell university': 2, 'dartmouth college': 3,
                  'harvard university': 4, 'university of pennsylvania': 5, 'princeton university': 6, 'yale university': 7,
                  'University of Washington': 8, 'Washington State University': 9, 'Western Washington University': 10,
                  'Seattle University': 11}
    return institutes


sc = define_config(master="local[*]", name="Recommendation_Sys")
companies = sc.broadcast(load_companies())
inst = sc.broadcast(load_institutes())


def define_company_bit_vector(x):
    """
    define a 1000 x 1 vector for each row
    :param x: information about the company a candidate has worked in
    :return: final 1000 x 1 vector indicating fortune 1000 companies.
    """
    lst = x.lower().split('|')
    val_to_return = 0
    if len(lst) == 0:  # Some candidates don't even have a degree.
        return val_to_return
    for item in lst:
        if item in inst.value:
            val_to_return += 1
    return val_to_return


def define_inst_bit_vector(x):
    """
    define a 8 x 1 vector for each row
    :param x: information about the institutes a candidate has studied in
    :return: final 8 x 1 vector indicating ivy league univ
    """
    lst = x.lower().split('|')
    val_to_return = 0
    if len(lst) == 0:  # Some candidates don't even have a degree.
        return val_to_return
    for item in lst:
        if item in inst.value:
            val_to_return += 1
    return val_to_return


def define_time(x):
    """
    Important function indicating the time difference between candidate and Job they have applied for
    :param x:
    :return:
    """
    # Perform a check on x[0] and x[1]
    if x[1] is None or x[1] in ['', ' ', 'n\\a', 'None']:  # Normal string is M-N Years or Not indicated
        return 1
    elif x[0] is None or x[0] in ['', ' ']:  # Normal string is days in numbers
        return 0.5

    lst = [int(item) for item in re.findall(r'[\d]+', x[1])]
    if len(lst) == 0:
        return 1

    else:
        min_yrs, max_yrs = min(lst), max(lst)
        val = float(x[0]) / 365.0
        if val > max_yrs:  # Not considering these candidates as overqualified
            return 1
        elif val < max_yrs:  # How far are we from median value of years in experience
            avg_req_exp = (min_yrs + max_yrs) / 2
            absyrs = abs(avg_req_exp - x[0])
            return np.exp(-absyrs)


def redefine_data(x):
    keys = x[0]
    values = x[1]
    values = values + (keys[2],)
    return (keys[:-1], values)


def shapeExperience(x):
    fields = x.split('|')
    return (fields[2], tuple(fields[:2] + fields[3:]))


def shapeJD(x):
    fields = x.split('|')
    return (fields[0], tuple(fields[1:]))


def CosineSimilarities(x):
    key = x[0]
    field1, field2 = x[1][0], x[1][1]
    if field1 is None or field2 is None:
        return 0.1
    field1, field2 = np.array(field1, dtype=np.float32), np.array(field2, dtype=np.float32)
    num = np.sum(np.multiply(field1, field2))
    den = np.dot(np.sqrt(np.sum(np.square(field1))), np.sqrt(np.sum(np.square(field2))))
    return (key, num / den)


def ComputeScore(x):
    """
    This function is written to handle multiple Requisitions simultaneously
    :param x: Multiple Values in the form of candidate profile for a single JD.
    [[J1, K1, refer, CompanyBitVec, shift, GradeLevel, emp_type, abs_time_diff, instituteBitVec, initialScore], ...]
    :return: Score corresponding to each candidate with their corresponding JobApp and Key. Thus, it will look like [[JobApp1, Key1, Score1], [JobApp2, Key2, Score2],...]
    """

    df = pd.DataFrame(x,
                      columns=['J1', 'K1', 'refer', 'CompanyBitVec', 'shift', 'GradeLevel', 'emp_type', 'abs_time_diff',
                               'instituteBitVec', 'initialScore']).drop_duplicates(subset=['J1', 'K1'])

    def computeVecSim(vector):
        vector = vector.reshape(vector.shape[0], 1)
        temp = abs(vector - vector.T)
        return np.exp(-temp)

    def getRating(x, ratings):  # Take N largest vals from all compared to random uniform distribution
        neighbors = []
        for Sim, rating in zip(x, ratings):
            neighbors.append((Sim, rating))

        kneighbors = heapq.nlargest(10, neighbors, key=lambda t: t[0])  # Getting N largest values from x
        simTotal = total = 0
        for Sim, rating in kneighbors:
            if Sim > 0:
                simTotal += 1
                total += Sim * rating  # Weight each score by the initial rating given by resume and JD with past interviews
        if simTotal == 0:
            return 0
        else:
            return total / simTotal

    # What happens to new Requisitions <- No Candidate

    if df.shape[0] == 0:
        return 0

    refer = computeVecSim(df.refer.values)
    companySim = computeVecSim(df.CompanyBitVec.values)
    instituteSim = computeVecSim(df.instituteBitVec.values)
    shift = computeVecSim(df['shift'].values)  # shift is in build function of dataframe
    emp_type = computeVecSim(df.emp_type.values)
    time_diff = computeVecSim(df.abs_time_diff.values)

    similarity = refer * shift * emp_type * time_diff * companySim * instituteSim
    np.fill_diagonal(similarity, 0)
    if len(df.J1) > 1:
        df['finalScore'] = np.apply_along_axis(getRating, 1, similarity, df['initialScore'].values)
    else:
        df['finalScore'] = df['initialScore']  # For Req having 1 candidate applied for them
    final_df = df[['J1', 'K1', 'finalScore']]

    final_df['J1'] = final_df.J1.astype(int)
    final_df['K1'] = final_df.K1.astype(int)
    final_df['finalScore'] = final_df.finalScore.astype(np.float32)
    return final_df.values.tolist()


def getReco(x):
    recommendations = []
    for lst in x:
        J1, K1, finalRating = lst
        recommendations.append((K1, finalRating))
    recommendations.sort(key=lambda x: x[1], reverse=True)

    return tuple(recommendations[:10])


def define_similarity_score():

    sqlContext = SQLContext(sc)

    # Read the data
    data = sqlContext.read.csv('../Data/final-data.csv', header=False)

    data2 = sqlContext.read.csv('../Data/cand_edu.csv', header=False)

    expr = sc.textFile('../Output/Experience/vec_repr.txt')

    jd = sc.textFile('../Output/JD/vec_repr.txt')

    staging = sqlContext.read.csv('../Data/staging.csv', header=False)

    df = sc.textFile('../Output/JD/k_points.txt').map(lambda x: x.split('|')).map(lambda x: (x[0], x[1:])).toDF()

    # Get the vector and jd in shape
    shaped_expr = expr.map(shapeExperience)
    shaped_jd = jd.map(shapeJD)

    # Get the referral in shape
    referral = data.rdd.map(lambda x: ((x[0], x[1], x[2]), x[3]))

    refer = referral.distinct().mapValues(lambda x: 1 if x in ['Internal Worker', 'Employee Referral', 'Contingent Worker'] else 0.75)  # value 1 to join

    # Get the employment type desired in shape
    emp_type_desired = data.rdd.distinct().map(lambda x: ((x[0], x[1], x[2]), (x[10], x[15])))

    emp_type = emp_type_desired.mapValues(lambda x: 1 if x[0] == 'EE' and x[1] == 'Employee' else 0.5)

    # Create Company Bit vector
    CompanyName = data.rdd.distinct().map(lambda x: ((x[0], x[1], x[2]), x[6])).mapValues(lambda x: x if x is not None else '')

    Company = CompanyName.reduceByKey(lambda x, y: x + '|' + y)

    CompanyBitVec = Company.mapValues(define_company_bit_vector)  # value 2 to join

    # Get shift in shape
    shiftPref = data.rdd.map(lambda x: ((x[0], x[1], x[2]), (x[11], x[19])))

    shift = shiftPref.distinct().mapValues(lambda x: 1 if x[0] == x[1] and x[0] is not None and x[1] is not None else 0.75)  # value 3 to join

    # Get the distinct grade levels
    GradeLevel = data.rdd.map(lambda x: ((x[0], x[1], x[2]), x[16])).distinct().mapValues(lambda x: x if x is not None else 'Not Provided')  # value 4 to join

    # Get the timings for each experience to match with required experience
    time_diff = data.rdd.map(lambda x: ((x[0], x[1], x[2]), (x[4], x[18]))).distinct()

    time = time_diff.mapValues(define_time)  # value 5 to join

    # Get the institute data inline - We have only 2 values as keys here.
    data_2 = data2.rdd.distinct().map(lambda x: ((x[0], x[1]), (x[2]))).mapValues(lambda x: x if x is not None else '')

    institute = data_2.reduceByKey(lambda x, y: x + '|' + y)

    instituteBitVec = institute.mapValues(define_inst_bit_vector)  # value 6 to join

    # Finally take the input of different JD & Experience and use them to build the cosine similarity

    final_data = refer.join(CompanyBitVec)
    final_data = final_data.join(shift).mapValues(lambda x: x[0] + (x[1],))
    final_data = final_data.join(GradeLevel).mapValues(lambda x: x[0] + (x[1],))
    final_data = final_data.join(emp_type).mapValues(lambda x: x[0] + (x[1],))
    final_data = final_data.join(time).mapValues(lambda x: x[0] + (x[1],))
    final_data = final_data.map(redefine_data)
    final_data = final_data.join(instituteBitVec).mapValues(lambda x: x[0] + (x[1],))
    final_data = final_data.map(lambda x: (x[0] + (x[1][-2],), x[1][:-2] + (x[1][-1],)))

    # get the experience and jd in shape along with their cosine similarity

    combined_vec = shaped_expr.leftOuterJoin(shaped_jd)
    updated_vec = combined_vec.map(lambda x: ((x[1][0][0], x[1][0][1], x[0]), ((x[1][0][2:]), (x[1][1]))))
    computed_similarity = updated_vec.map(lambda x: CosineSimilarities(x))

    dataset = final_data.join(computed_similarity).mapValues(lambda x: x[0] + (x[1],))

    staging_data = staging.rdd.distinct().map(lambda x: ((x[0], x[1], x[2]), float(x[-1])))

    final_dataset = dataset.leftOuterJoin(staging_data).mapValues(lambda x: x[0] + (x[1],)).mapValues(lambda x: x[:-2] + (x[-1] * x[-2] if x[-1] is not None else x[-2] * 0.1,))

    # Perform the Content Based Reco

    cr1 = final_dataset.map(lambda x: (x[0][-1], [x[0][0], x[0][1]] + [y for y in x[1]]))
    cr2 = cr1.reduceByKey(lambda x, y: x + y).mapValues(lambda x: [x[i: i+10] for i in range(0, len(x), 10)])
    cr3 = cr2.mapValues(ComputeScore).map(lambda x: (int(x[0]), x[1]))  # This step will take quite some time <<-- Final Stage to perform Reco.

    newdf = df.withColumn('_2', explode(df['_2'])).rdd.map(lambda x: (int(x[1]), (int(x[0]))))
    cr4 = newdf.leftOuterJoin(cr3).map(lambda x: x[1]).filter(lambda x: x[1] is not None)

    cr5 = cr4.reduceByKey(lambda x, y: x + y)
    cr6 = cr5.mapValues(getReco)

    #print(cr5.count())

    for item in cr6.collect():
        print(item)

define_similarity_score()
