"""
@author: Stanford guys
"""
from collections import Counter
import json, os
import random
import numpy as np
import tensorflow as tf

def read_data(path):
    """
    Read the saved text files
    :param path: path where text file resides
    :return: actual data in string format [unicode format]
    """
    with open(path, 'r') as f:
        data = tf.compat.as_str(f.read())
    return data


def build_dictionary(mode, text, visual_fld, vocab_size=10000):
    """
    This method will define a dictionary from most popular words where dictionary size are defined by vocab_size
    :param: mode: mode to save the dictionary
    :param text: The normalized and clean text
    :param visual_fld: Path to save the common words for further visualization
    :param vocab_size: vocab size defined for common words present in a text
    :return: Dictionary and index dictionary together.
    """
    try:
        os.mkdir('../visualization/' + mode)  # Checkpoints to save intermediator results
    except OSError:
        print('Viz path defined!!!')

    file = open('../visualization/' + mode + '/vocab.tsv', 'w+')
    text = text.split()
    count = [('UNK', -1)]
    count.extend(Counter(text).most_common(vocab_size-1))
    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary)
        file.write(word + "\n")
    idx_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    file.close()
    with open('../Output/' + mode + '/dictionary.json', 'w') as fp:
        json.dump(dictionary, fp, indent=4)
    return dictionary, idx_dictionary


def word_to_index(text, dictionary):
    """
    Capture the index of words in text using dictionary
    :param text: The normalized and clean text
    :param dictionary:
    :return: List of all the indexes mapped to the words.
    """
    words = text.split()
    return [dictionary[word] if word in dictionary else 0 for word in words]


def generate_sample(index_words, window_size):
    """
    Kind of an important method to generate the
    :param index_words:
    :param window_size:
    :return:
    """
    for index, center_word in enumerate(index_words):
        context = random.randint(1, window_size)  # Arbitrary window size from 1 to window_size
        for target in index_words[max(0, index - context): index]:
            yield center_word, target

        for target in index_words[index+1: index+context+1]:
            yield center_word, target


def predict_generator(path, mode):
    # Generate the dataset to be filled with generated dictionary
    with open('../Output/' + mode.decode('utf-8') + '/dictionary.json', 'r') as fp:
        dictionary = json.load(fp)
    with open(path, 'r') as f:
        for line in f:  # lookup for each word in current exp line in dictionary
            line = line.replace('(', "").replace(')', "").split(',')
            """
            line = line.replace('(', '').replace(')', '').split(',')
            if mode.decode('utf-8') == "JD":
                idx, ln = [int(line[0].strip("'"))], line[1]
                ln = word_to_index(ln, dictionary)  # line could be of arbitrary length
                yield idx, ln
            else:
                idx1, idx2, idx3, ln = line[0].strip(" ").strip("'"), line[1].strip(" ").strip("'"), line[2].strip(" ").strip("'"), line[3].strip(" ").strip("'")
                return [int(idx1), int(idx2), int(idx3)], ln
            """

            idx, ln = [int(x.strip(" ").strip("'")) for x in line[:-1]], line[-1]
            ln = word_to_index(ln, dictionary)  # line could be of arbitrary length
            yield idx, ln


def most_common_words(visual_fld, mode, num_visualize):
    "create a list of most common words"
    words = open('../visualization/' + mode + '/vocab.tsv', 'r').readlines()[:num_visualize]
    words = [word for word in words]

    with open('../visualization/' + mode + '/vocab_' + str(num_visualize) + '.tsv', 'w') as f:
        for word in words:
            f.write(word)


def batch_generator(path, vocab_size, batch_size, skip_window, visual_fld, mode='Experience'):
    mode = mode.decode('utf-8')
    visual_fld = visual_fld.decode('utf-8')
    """
    Return a batch of text and label
    :param path: Path where data is present
    :param vocab_size: vocab size for most important words
    :param batch_size: defining size of data to return in 1 iteration
    :param skip_window: window size to roll over at a time
    :param visual_fld: Path to save the common words for further visualization
    :param mode: control the generation of respective data
    :return: a batch of specified batch size
    """
    if mode == 'Experience':
        experience = read_data(path)
        dictionary, idx_dictionary = build_dictionary(mode, experience, visual_fld, vocab_size)
        index_words = word_to_index(experience, dictionary)
        sample = generate_sample(index_words, skip_window)

        # Create a batch of batch size

        while True:  # Take 1 sample at a time and fill the center and target
            center_batch = np.zeros(batch_size, dtype=np.int32)  # Integer for embedding lookup
            target_batch = np.zeros([batch_size, 1])
            for idx in range(batch_size):
                center_batch[idx], target_batch[idx] = next(sample)
            yield center_batch, target_batch

    elif mode == 'JD':
        jd = read_data(path)
        dictionary, idx_dictionary = build_dictionary(mode, jd, visual_fld, vocab_size)
        index_words = word_to_index(jd, dictionary)
        sample = generate_sample(index_words, skip_window)

        # Create a batch of batch size

        while True:
            center_batch = np.zeros(batch_size, dtype=np.int32)
            target_batch = np.zeros([batch_size, 1])
            for idx in range(batch_size):
                center_batch[idx], target_batch[idx] = next(sample)
            yield center_batch, target_batch

    else:
        raise ValueError('Mode is not recognized!!!')
