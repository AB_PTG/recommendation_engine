import os
import numpy as np
import tensorflow as tf
from Train import Parameters
from tensorflow.contrib.tensorboard.plugins import projector
from Train import utils

params = Parameters.read_config('../Config/config.json')
VOCAB_SIZE = params['SkipGram']['VOCAB_SIZE']
BATCH_SIZE = params['SkipGram']['BATCH_SIZE']
EMBEDDING_SIZE = params['SkipGram']['EMBEDDING_SIZE']
SKIP_WINDOW = params['SkipGram']['SKIP_WINDOW']
NUM_SAMPLED = params['SkipGram']['NUM_SAMPLED']
LEARNING_RATE = params['SkipGram']['LEARNING_RATE']
NUM_TRAINING_STEPS = params['SkipGram']['NUM_TRAINING_STEPS']
VISUAL_FLD = params['SkipGram']['VISUAL_FLD']
NUM_VISUALIZE = params['SkipGram']['NUM_VISUALIZE']
skip_step = params['SkipGram']['skip_step']


class SkipGramModel(object):

    def __init__(self, mode, dataset, to_be_filled):
        # Initialize the required params in json file
        self.VOCAB_SIZE = VOCAB_SIZE
        self.BATCH_SIZE = BATCH_SIZE
        self.EMBEDDING_SIZE = EMBEDDING_SIZE
        self.SKIP_WINDOW = SKIP_WINDOW
        self.NUM_SAMPLED = NUM_SAMPLED
        self.LEARNING_RATE = LEARNING_RATE
        self.NUM_TRAINING_STEPS = NUM_TRAINING_STEPS
        self.VISUAL_FLD = VISUAL_FLD
        self.NUM_VISUALIZE = NUM_VISUALIZE
        self.skip_step = skip_step
        self.mode = mode
        self.global_step = tf.get_variable('global_step', initializer=tf.constant(0), trainable=False)  # We would not want to train it
        self.dataset = dataset
        self.to_be_filled = to_be_filled

    def _data(self):
        # Import data
        with tf.name_scope('data'):
            self.iterator = self.dataset.make_initializable_iterator()
            self.iterator2 = self.to_be_filled.make_initializable_iterator()
            self.center_words, self.target_words = self.iterator.get_next()
            self.idx, self.filling_words = self.iterator2.get_next()

    def _define_weights(self):
        # Define weights
        with tf.name_scope('weights'):
            self._weights = tf.get_variable(name='nce_weights', shape=[self.VOCAB_SIZE, self.EMBEDDING_SIZE],
                                            initializer=tf.truncated_normal_initializer(stddev= 1.0 / np.sqrt(self.EMBEDDING_SIZE)))
            self._bias = tf.get_variable(name='nce_bias', initializer=tf.zeros([self.VOCAB_SIZE]))

    def _define_embeddings(self):
        # Define Embeddings
        with tf.name_scope('embeddings'):
            self.embed_matrix = tf.get_variable(name='embeddings',shape=[self.VOCAB_SIZE, self.EMBEDDING_SIZE],
                                         initializer=tf.random_uniform_initializer())
            self.embed = tf.nn.embedding_lookup(self.embed_matrix, self.center_words, name='embedding_lookup')

    def _define_filling(self):
        with tf.name_scope('filling'):
            self.embed2 = tf.nn.embedding_lookup(self.embed_matrix, self.filling_words, name='filling_lookup')
            self.doc2vec = tf.reduce_mean(self.embed2, axis=0)  # should give (1, 200) as an output

    def _loss(self):
        # Define loss
        with tf.name_scope('nce_loss'):
            self.loss = tf.reduce_mean(tf.nn.nce_loss(weights=self._weights,
                                                      biases=self._bias,
                                                      labels=self.target_words,
                                                      inputs=self.embed,
                                                      num_sampled=self.NUM_SAMPLED,
                                                      num_classes=self.VOCAB_SIZE),
                                                      name='nce_loss')

    def _optimize(self):
        with tf.name_scope('optimize'):
            self.optimizer = tf.train.GradientDescentOptimizer(self.LEARNING_RATE).minimize(self.loss,
                                                                                            global_step=self.global_step)

    def _create_summaries(self):
        with tf.name_scope('summaries'):
            tf.summary.scalar('loss', self.loss)
            tf.summary.histogram('histogram_loss', self.loss)
            self.summaries = tf.summary.merge_all()

    def Make_Graph(self):
        # Arrange Graph
        self._data()
        self._define_weights()
        self._define_embeddings()
        self._define_filling()
        self._loss()
        self._optimize()
        self._create_summaries()

    def Train(self, mode):
        saver = tf.train.Saver()

        try:
            os.mkdir(mode + "/checkpoints")  # Checkpoints to save intermediator results
        except OSError:
            print('Error in defining the path')

        step = 0
        with tf.Session() as sess:
            sess.run(self.iterator.initializer)  # Initialization for dataset
            sess.run(tf.global_variables_initializer())  # initializing global variables

            ckpt = tf.train.get_checkpoint_state(os.path.dirname(os.path.join(mode, 'checkpoints/checkpoint')))

            # Restore from previous checkpoint if any
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)

            total_loss = 0.0
            writer = tf.summary.FileWriter('../graph/' + mode + '/word2vec/lr'
                                           + str(self.LEARNING_RATE),
                                           sess.graph)
            step = self.global_step.eval()   # To update the initial step

            for idx1 in range(step, step+self.NUM_TRAINING_STEPS):
                try:
                    batch_loss, _, summaries = sess.run([self.loss, self.optimizer, self.summaries])
                    total_loss += batch_loss
                    writer.add_summary(summaries, global_step=idx1)
                    if (idx1+1) % self.skip_step == 0:  # Print loss and save the summaries
                        print('Loss at Step {}: {:5.1f}'.format(idx1, total_loss / self.skip_step))

                        total_loss = 0.0
                        saver.save(sess, mode+'/checkpoints/skip_gram', idx1)

                except tf.errors.OutOfRangeError:  # Reinitialize the iterator once it runs out of words
                    sess.run(self.iterator.initializer)
            writer.close()

    def retrieve_embeddings(self, mode):
        """
        This function will retrieve embeddings for the given set of experiences or JD. Get the words of 1 resume/ exp /
        Requisition at a time and get the embeddings for the doc.
        :return: None
        """
        saver = tf.train.Saver()
        flag = True
        step = 0
        with tf.Session() as sess:
            sess.run(self.iterator2.initializer)
            sess.run(tf.global_variables_initializer())

            ckpt = tf.train.get_checkpoint_state(os.path.dirname(os.path.join(mode, 'checkpoints/checkpoint')))

            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)

            while flag:
                try:
                    doc2vec, idx = sess.run([self.doc2vec, self.idx])
                    doc2vec, idx = doc2vec.astype(str), idx.astype(str)
                    z = idx.tolist()
                    z.extend(doc2vec.tolist())
                    with open('../Output/{}/vec_repr.txt'.format(mode), 'a+') as f:
                        f.write('|'.join(z))  # Either first one or first 3 values will be used
                        f.write('\n')
                    step += 1
                    if step % 1e3 == 0:
                        print('Number of examples filled: {}'.format(step))

                except tf.errors.OutOfRangeError:
                    flag = False
        # TODO: Just upload the file vec_repr in S3 here.

    def visualize(self, mode):
        """
        Used to project the visualization for given mode
        :param mode: Experience vs JD
        :return: None
        """
        utils.most_common_words(self.VISUAL_FLD, mode, self.NUM_VISUALIZE)

        saver = tf.train.Saver()
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())

            ckpt = tf.train.get_checkpoint_state(os.path.dirname(os.path.join(mode, 'checkpoints/checkpoint')))
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)

            final_embed_matrix = sess.run(self.embed_matrix)

            embedding_var = tf.Variable(final_embed_matrix[:self.NUM_VISUALIZE], name='embeded')
            sess.run(embedding_var.initializer)

            summary_writer = tf.summary.FileWriter('../visualization/' + mode)

            config = projector.ProjectorConfig()
            embedding = config.embeddings.add()
            embedding.tensor_name = embedding_var.name

            embedding.metadata_path = 'vocab_' + str(self.NUM_VISUALIZE) + '.tsv'

            projector.visualize_embeddings(summary_writer, config)
            saver_embed = tf.train.Saver([embedding_var])
            saver_embed.save(sess, os.path.join('../visualization/' + mode, 'model.ckpt'), 1)


def gen(VOCAB_SIZE, BATCH_SIZE, SKIP_WINDOW, VISUAL_FLD, mode):
    yield from utils.batch_generator("../Data/Preprocessed/" + mode.decode('utf-8')
                                     + ".txt", VOCAB_SIZE, BATCH_SIZE, SKIP_WINDOW, VISUAL_FLD, mode=mode)


def fill_gen(mode, flag):
    yield from utils.predict_generator('../Data/Preprocessed/' + mode.decode('utf-8')
                                       + '_predict.txt', mode)


def compute_vector_repr(mode, Train=True):
    data = tf.data.Dataset.from_generator(gen, output_types=(tf.int32, tf.int32),
                                   output_shapes=(tf.TensorShape([BATCH_SIZE]), tf.TensorShape([BATCH_SIZE, 1])),
                                   args=(VOCAB_SIZE, BATCH_SIZE, SKIP_WINDOW, VISUAL_FLD, mode))

    # Beware as we don't know what would be the size of keys and value for the data to predict
    to_be_filled = tf.data.Dataset().batch(1).from_generator(fill_gen, output_types=(tf.int32, tf.int32),
                                                            output_shapes=(tf.TensorShape([None]), tf.TensorShape([None])),
                                                             args=(mode, 1))
    model = SkipGramModel(mode, data, to_be_filled)
    model.Make_Graph()

    if Train:
        model.Train(mode)
        model.visualize(mode)
        return None
    else:
        return model.retrieve_embeddings(mode)


if __name__ == '__main__':
    compute_vector_repr(mode=params['SkipGram']['mode'])
