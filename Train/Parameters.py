import json


def read_config(path):
    """
    Read the configuration parameters for doc2vec model
    :param path: path where file resides
    :return: dictionary with all required parameters of model in use.
    """
    with open(path, 'r') as f:
        datastore = json.load(f)
    return datastore
